/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "compdefdialog.h"
#include "ui_compdefdialog.h"
#include <QDebug>
#include <QAbstractButton>
#include <QFileDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>

#ifdef __linux__
#include <sys/sysinfo.h>
#endif
#ifdef _WIN32
//only used to get used program memory
#include <windows.h>
#include <psapi.h>
#endif
CompDefDialog::CompDefDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CompDefDialog)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
    connect(&timer, SIGNAL(timeout()), this, SLOT(timerSlot()));
    setWindowFlags(Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint);
    compDef=nullptr;
}

CompDefDialog::~CompDefDialog()
{
    delete ui;
}

void CompDefDialog::setCompDef(CompDef *comp)
{
    compDef=comp;
    ui->nameEdit->setText(comp->name);
    ui->minAreaSpin->setValue(comp->minArea);
    ui->upsampleSpin->setValue(comp->upsample);
    ui->horizontalFlipCB->setChecked(comp->horizontalFlip);
    ui->verticalFlipCB->setChecked(comp->verticalFlip);
    ui->CSpin->setValue(comp->C);
    ui->epsSpin->setValue(comp->eps);
    ui->numThreadsSpin->setValue(comp->numThreads);
    ui->threshSpin->setValue(comp->fhogThreshold);
    ui->fhogNOrientCombo->setCurrentIndex(comp->fhogNOrient);
    if(comp->trainingIOUThresh>=0){
        ui->trainingIOUThreshCB->setChecked(false);
        ui->trainingIOUThreshSpin->setValue(comp->trainingIOUThresh);
        ui->trainingIOUThreshSpin->setEnabled(true);
    } else {
        ui->trainingIOUThreshCB->setChecked(true);
        ui->trainingIOUThreshSpin->setValue(-comp->trainingIOUThresh);
        ui->trainingIOUThreshSpin->setEnabled(false);
    }
    ui->cvEnableBox->setChecked(compDef->colorValidator[0][0]!=0);
    ui->cvSizeSpin->setValue(compDef->colorValidator[0][1]*100);
    ui->rValSpin->setValue(compDef->colorValidator[1][0]);
    ui->gValSpin->setValue(compDef->colorValidator[2][0]);
    ui->bValSpin->setValue(compDef->colorValidator[3][0]);
    ui->hValSpin->setValue(compDef->colorValidator[4][0]);
    ui->sValSpin->setValue(compDef->colorValidator[5][0]);
    ui->vValSpin->setValue(compDef->colorValidator[6][0]);
    ui->rDevSpin->setValue(compDef->colorValidator[1][1]);
    ui->gDevSpin->setValue(compDef->colorValidator[2][1]);
    ui->bDevSpin->setValue(compDef->colorValidator[3][1]);
    ui->hDevSpin->setValue(compDef->colorValidator[4][1]);
    ui->sDevSpin->setValue(compDef->colorValidator[5][1]);
    ui->vDevSpin->setValue(compDef->colorValidator[6][1]);
    ui->spNuSpin->setValue(compDef->compSpParams.nu);
    ui->spTreeDepthSpin->setValue(compDef->compSpParams.treeDepth);
    ui->spCascadeDepthSpin->setValue(compDef->compSpParams.cascadeDepth);
    ui->spNumTestSplitsSpin->setValue(compDef->compSpParams.numTestSplits);
    ui->spFeaturePoolSizeSpin->setValue(compDef->compSpParams.featurePoolSize);
    ui->spOversamplingAmountSpin->setValue(compDef->compSpParams.oversamplingAmount);
    ui->spOversamplingJitterSpin->setValue(compDef->compSpParams.oversamplingTranslationJitter);
    ui->spEnableCB->setChecked(compDef->compSpParams.enabled);
    ui->dnnMinLRSpin->setValue(compDef->compDNNParams.minLearningRate);
    ui->dnnMaxLRSpin->setValue(compDef->compDNNParams.maxLearningRate);
    ui->dnnBatchSize1Spin->setValue(compDef->compDNNParams.batchSize1);
    ui->dnnBsSamples1Spin->setValue(compDef->compDNNParams.bsSamples1);
    ui->dnnBatchSize2Spin->setValue(compDef->compDNNParams.batchSize2);
    ui->dnnBsSamples2Spin->setValue(compDef->compDNNParams.bsSamples2);
    ui->dnnBatchSize3Spin->setValue(compDef->compDNNParams.batchSize3);
    ui->dnnChipWidthSpin->setValue(compDef->compDNNParams.chipWidth);
    ui->dnnChipHeightSpin->setValue(compDef->compDNNParams.chipHeight);
    ui->dnnDisturbColorsCB->setChecked(compDef->compDNNParams.disturbColors);
    ui->dnnMaxRotationDegrees->setValue(compDef->compDNNParams.maxRotation);
    ui->dnnIterationsWithoutProgressSpin->setValue(compDef->compDNNParams.stepsWihoutProgress);
    ui->dnnLrShrinkFactor->setValue(compDef->compDNNParams.lrShrinkFactor);
    ui->dnnTrainingModeCombo->setCurrentIndex(compDef->compDNNParams.trainingMode);
    {
        int idx=ui->dnnNFilterCombo->findText(QString::number(compDef->compDNNParams.nFilter));
        if(idx>=0)
            ui->dnnNFilterCombo->setCurrentIndex(idx);
    }
    ui->dnnEnableCB->setChecked(compDef->compDNNParams.enabled);
    ui->dnnBiasSpin->setValue(compDef->compDNNParams.bias);
    ui->dnnBBRegressionCB->setChecked(compDef->compDNNParams.bbRegression);
    int value=0;
    for(int i=0; i<ui->dnnNFilterCombo->count(); i++){
        value=ui->dnnNFilterCombo->itemText(i).toInt();
        if(value>=compDef->compDNNParams.nFilter){
            //qDebug()<<value;
            ui->dnnNFilterCombo->setCurrentIndex(i);
            compDef->compDNNParams.nFilter=value;
            break;
        }
    }
    if(value==0)
        compDef->compDNNParams.nFilter=ui->dnnNFilterCombo->currentText().toInt();
    loadBoarListDNN(ui->dnnTransferClassifierCombo, compDef->compDNNParams.transferClassifier);
}

void CompDefDialog::storeConfig()
{
    qDebug()<<"save";
    compDef->name=ui->nameEdit->text();
    compDef->minArea=ui->minAreaSpin->value();
    compDef->upsample=ui->upsampleSpin->value();
    compDef->horizontalFlip=ui->horizontalFlipCB->isChecked();
    compDef->verticalFlip=ui->verticalFlipCB->isChecked();
    compDef->C=ui->CSpin->value();
    compDef->eps=ui->epsSpin->value();
    compDef->numThreads=ui->numThreadsSpin->value();
    compDef->fhogThreshold=ui->threshSpin->value();
    compDef->fhogNOrient=ui->fhogNOrientCombo->currentIndex();
    if(ui->trainingIOUThreshCB->isChecked()){
        compDef->trainingIOUThresh = -ui->trainingIOUThreshSpin->value();
    } else {
        compDef->trainingIOUThresh = ui->trainingIOUThreshSpin->value();
    }
    compDef->colorValidator[0][0]=ui->cvEnableBox->isChecked()?1:0;
    compDef->colorValidator[0][1]=ui->cvSizeSpin->value()/100;
    compDef->colorValidator[1][0]=ui->rValSpin->value();
    compDef->colorValidator[1][1]=ui->rDevSpin->value();
    compDef->colorValidator[2][0]=ui->gValSpin->value();
    compDef->colorValidator[2][1]=ui->gDevSpin->value();
    compDef->colorValidator[3][0]=ui->bValSpin->value();
    compDef->colorValidator[3][1]=ui->bDevSpin->value();
    compDef->colorValidator[4][0]=ui->hValSpin->value();
    compDef->colorValidator[4][1]=ui->hDevSpin->value();
    compDef->colorValidator[5][0]=ui->sValSpin->value();
    compDef->colorValidator[5][1]=ui->sDevSpin->value();
    compDef->colorValidator[6][0]=ui->vValSpin->value();
    compDef->colorValidator[6][1]=ui->vDevSpin->value();
    compDef->compSpParams.nu=ui->spNuSpin->value();
    compDef->compSpParams.treeDepth=ui->spTreeDepthSpin->value();
    compDef->compSpParams.cascadeDepth=ui->spCascadeDepthSpin->value();
    compDef->compSpParams.numTestSplits=ui->spNumTestSplitsSpin->value();
    compDef->compSpParams.featurePoolSize=ui->spFeaturePoolSizeSpin->value();
    compDef->compSpParams.oversamplingAmount=ui->spOversamplingAmountSpin->value();
    compDef->compSpParams.oversamplingTranslationJitter=ui->spOversamplingJitterSpin->value();
    compDef->compSpParams.enabled=ui->spEnableCB->isChecked();
    compDef->compDNNParams.minLearningRate=ui->dnnMinLRSpin->value();
    compDef->compDNNParams.maxLearningRate=ui->dnnMaxLRSpin->value();
    compDef->compDNNParams.batchSize1=ui->dnnBatchSize1Spin->value();
    compDef->compDNNParams.bsSamples1=ui->dnnBsSamples1Spin->value();
    compDef->compDNNParams.batchSize2=ui->dnnBatchSize2Spin->value();
    compDef->compDNNParams.bsSamples2=ui->dnnBsSamples2Spin->value();
    compDef->compDNNParams.batchSize3=ui->dnnBatchSize3Spin->value();
    compDef->compDNNParams.chipWidth=ui->dnnChipWidthSpin->value();
    compDef->compDNNParams.chipHeight=ui->dnnChipHeightSpin->value();
    compDef->compDNNParams.disturbColors=ui->dnnDisturbColorsCB->isChecked();
    compDef->compDNNParams.maxRotation=ui->dnnMaxRotationDegrees->value();
    compDef->compDNNParams.stepsWihoutProgress=ui->dnnIterationsWithoutProgressSpin->value();
    compDef->compDNNParams.lrShrinkFactor=ui->dnnLrShrinkFactor->value();
    compDef->compDNNParams.trainingMode=ui->dnnTrainingModeCombo->currentIndex();
    if(ui->dnnTransferClassifierCombo->count()>0)
        compDef->compDNNParams.transferClassifier=ui->dnnTransferClassifierCombo->currentData().toLongLong();
    compDef->compDNNParams.enabled=ui->dnnEnableCB->isChecked();
    compDef->compDNNParams.bias=ui->dnnBiasSpin->value();
    compDef->compDNNParams.bbRegression=ui->dnnBBRegressionCB->isChecked();
    compDef->compDNNParams.nFilter=ui->dnnNFilterCombo->currentText().toInt();
    compDef->dirty=true;
}

void CompDefDialog::accept()
{
    qDebug()<<"accept";
}


void CompDefDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    int role=ui->buttonBox->buttonRole(button);
    qDebug()<<role;
    if(role==0){
        // save button pressed
        storeConfig();
    } else {
        //discard button pressed - do nothing
    }
    this->done(role);
}


void CompDefDialog::on_fhogTrainBtn_clicked()
{
    //store the settings which might have been changed, and train the classifier
    storeConfig();
    if(compDef->compDNNParams.enabled){
        auto answer=QMessageBox::warning(this,tr("clasificator DNN"), tr("Învățarea nu are sens, deoarece DNN e activat. Continuați?"), QMessageBox::Yes, QMessageBox::No);
        if(QMessageBox::Yes!=answer)
            return;
    }

    QSqlDatabase defaultdb=QSqlDatabase::database();
    QString databaseName=defaultdb.databaseName();
    trainingTab=ui->tabWidget->currentIndex();

    trainFuture = QtConcurrent::run(
                compDef, &CompDef::trainFHOG, databaseName);
    timeRunning.start();
    timer.start(1000);
    ui->tabWidget->setCurrentIndex(5);
    this->setEnabled(false);
}

///import autoinspect definition
void CompDefDialog::on_importBtn_clicked()
{
    /// @todo sterge calea
    static QString dirName="G:/boards/autoinspect/classifiers/4058787/carcasa/";
    this->hide();
    dirName=QFileDialog::getExistingDirectory(this, tr("director clasificator autoinspect"), dirName);
    compDef->importAutoinspect(dirName);
    this->done(IMPORTAI);
}

void CompDefDialog::timerSlot()
{
    int millis, seconds, minutes;
    int elapsed=timeRunning.elapsed();
    millis=elapsed%1000;
    elapsed /= 1000;
    seconds = elapsed%60;
    minutes = elapsed/60;
    QString str;
    ui->timeLabel->setText(str.asprintf("%02d:%02d.%01d", minutes, seconds, millis/100));
    if(trainFuture.isFinished()){
        timer.stop();
        ui->tabWidget->setCurrentIndex(trainingTab);
        this->setEnabled(true);
    }
#ifdef __linux__
    QString filename="/proc/"+QString::number(qApp->applicationPid())+"/statm";
    QFile file(filename);
    QString statm;
    if(file.exists()){
        file.open(QIODevice::ReadOnly);
        statm=file.readAll();
        //qDebug()<<statm<<statm.section(" ",5,5);
        long long usedMem=statm.section(" ",5,5).toLongLong()*4;
        struct sysinfo info;
        sysinfo(&info);
        long long totalMem=info.totalram>>10;
        ui->memLbl->setText(QString::number(usedMem)+"/"+QString::number(totalMem)+" kB");
        if(usedMem>totalMem){
            QPalette pal=ui->memLbl->palette();
            pal.setColor(QPalette::WindowText,Qt::red);
            ui->memLbl->setPalette(pal);
        } else {
            QPalette pal=ui->memLbl->palette();
            pal.setColor(QPalette::WindowText,Qt::black);
            ui->memLbl->setPalette(pal);
        }
    }
#endif
#ifdef _WIN32
    unsigned long long totalMem;
    unsigned long long usedMem=0;
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof (statex);
    GlobalMemoryStatusEx (&statex);
    totalMem=statex.ullTotalPhys >> 10;
    HANDLE hProcess = OpenProcess(  PROCESS_QUERY_INFORMATION |
                                        PROCESS_VM_READ,
                                        FALSE, qApp->applicationPid() );
    if(!hProcess){
        qDebug()<<"can't open process handle";
    }
    PROCESS_MEMORY_COUNTERS pmc;
    if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) ){
        usedMem=pmc.WorkingSetSize>>10;
    }

    ui->memLbl->setText(QString::number(usedMem)+"/"+QString::number(totalMem)+" kB");
    if(usedMem>totalMem){
        QPalette pal=ui->memLbl->palette();
        pal.setColor(QPalette::WindowText,Qt::red);
        ui->memLbl->setPalette(pal);
    } else {
        QPalette pal=ui->memLbl->palette();
        pal.setColor(QPalette::WindowText,Qt::black);
        ui->memLbl->setPalette(pal);
    }
#endif
}

void CompDefDialog::on_applyColorBtn_clicked()
{
    compDef->colorValidator[0][1]=ui->cvSizeSpin->value()/100;
    //double r, g, b, h, s, v;
    //double rSigma, gSigma, bSigma, hSigma, sSigma, vSigma;
    AverageColorResult res;
    //compDef->calcAverageColor(r, rSigma, g, gSigma, b, bSigma, h, hSigma, s, sSigma, v, vSigma);
    compDef->calcAverageColor(res);
    ui->rValLbl->setText(QString::number(res.r));
    ui->gValLbl->setText(QString::number(res.g));
    ui->bValLbl->setText(QString::number(res.b));
    ui->hValLbl->setText(QString::number(res.h));
    ui->sValLbl->setText(QString::number(res.s));
    ui->vValLbl->setText(QString::number(res.v));
    ui->rDevLbl->setText(QString::number(res.rSigma));
    ui->gDevLbl->setText(QString::number(res.gSigma));
    ui->bDevLbl->setText(QString::number(res.bSigma));
    ui->hDevLbl->setText(QString::number(res.hSigma));
    ui->sDevLbl->setText(QString::number(res.sSigma));
    ui->vDevLbl->setText(QString::number(res.vSigma));
    QImage img(100,1,QImage::Format_RGB888);
    showR();
    showG();
    showB();
    showH();
    showS();
    showV();
}

void CompDefDialog::on_saveDatasetBtn_clicked()
{
    if(compDef==nullptr)
        return;
    QString fileName=QFileDialog::getSaveFileName(this,tr("Nume fișier unde se va salva setul de date"),compDef->name+".xml",
                                 tr("fișiere xml (*.xml);;Toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    compDef->saveDlibDataset(fileName);
}

bool CompDefDialog::setValueSpin(QString text, QDoubleSpinBox *spinBox)
{
    text.remove('&');
    bool ok=false;
    double value=text.toDouble(&ok);
    qDebug()<<text<<text.toDouble();
    if(ok)
        spinBox->setValue(value);
    return ok;
}

bool CompDefDialog::setDeviationSpin(QString text, QDoubleSpinBox *spinBox)
{
    text.remove('&');
    bool ok=false;
    double value=text.toDouble(&ok);
    qDebug()<<text<<text.toDouble();
    if(ok){
        // the spinbox automatically clips the value, so no adjustment is needed
        spinBox->setValue(value*4);
    }
    return ok;
}

void CompDefDialog::loadBoarListDNN(QComboBox *comboBox, long long selectedId)
{
    comboBox->blockSignals(true);
    comboBox->clear();
    QSqlQuery query;
    QString sql;
    sql="SELECT name, id FROM compdefs WHERE traineddnn<>0 ORDER BY name";
    qDebug()<<"sql"<<sql;
    query.exec(sql);
    if(!query.exec(sql)){
        qDebug()<<"error"<<query.executedQuery()<<query.lastError();
        QSqlDatabase defaultdb=QSqlDatabase::database();
        defaultdb.close();
        defaultdb.open();
        if(defaultdb.isOpen())
            query.exec(sql);
        else
            return;
    }
    long long idx=-1;
    while(query.next()){
        QString name=query.value(0/*"name"*/).toString();
        long long id=query.value(1/*"id"*/).toLongLong();
        comboBox->addItem(name, id);
        if(id==selectedId)
            idx=comboBox->count()-1;
        qDebug()<<"component"<<name<<id;
    }
    if(idx>=0)
        comboBox->setCurrentIndex(idx);
    comboBox->blockSignals(false);

}


void CompDefDialog::on_vValLbl_clicked()
{
    setValueSpin(ui->vValLbl->text(), ui->vValSpin);
}

void CompDefDialog::on_sValLbl_clicked()
{
    setValueSpin(ui->sValLbl->text(), ui->sValSpin);
}

void CompDefDialog::on_hValLbl_clicked()
{
    setValueSpin(ui->hValLbl->text(), ui->hValSpin);
}

void CompDefDialog::on_bValLbl_clicked()
{
    setValueSpin(ui->bValLbl->text(), ui->bValSpin);
}

void CompDefDialog::on_gValLbl_clicked()
{
    setValueSpin(ui->gValLbl->text(), ui->gValSpin);
}

void CompDefDialog::on_rValLbl_clicked()
{
    setValueSpin(ui->rValLbl->text(), ui->rValSpin);
}

void CompDefDialog::on_rDevLbl_clicked()
{
    setDeviationSpin(ui->rDevLbl->text(), ui->rDevSpin);
}

void CompDefDialog::on_gDevLbl_clicked()
{
    setDeviationSpin(ui->gDevLbl->text(), ui->gDevSpin);
}

void CompDefDialog::on_bDevLbl_clicked()
{
    setDeviationSpin(ui->bDevLbl->text(), ui->bDevSpin);
}

void CompDefDialog::on_hDevLbl_clicked()
{
    setDeviationSpin(ui->hDevLbl->text(), ui->hDevSpin);
}

void CompDefDialog::on_sDevLbl_clicked()
{
    setDeviationSpin(ui->sDevLbl->text(), ui->sDevSpin);
}

void CompDefDialog::on_vDevLbl_clicked()
{
    setDeviationSpin(ui->vDevLbl->text(), ui->vDevSpin);
}

void CompDefDialog::showR()
{
    QImage img(100,1,QImage::Format_RGB888);
    double r=ui->rValSpin->value();
    double g=ui->gValSpin->value();
    double b=ui->bValSpin->value();
    double dev=ui->rDevSpin->value();
    if(dev>0){
        double rmin=r-dev;
        double rmax=r+dev;
        for(int i=0; i<100; i++){
            QColor color;
            double r2=rmin+(rmax-rmin)*i/99;
            r2=std::min(std::max(r2,0.0),255.0);
            color.setRgb(r2, g, b);
            img.setPixel(i,0,color.rgb());
        }
        ui->rColorLbl->setPixmap(QPixmap::fromImage(img));
    } else {
        ui->rColorLbl->clear();
    }
}

void CompDefDialog::showG()
{
    QImage img(100,1,QImage::Format_RGB888);
    double r=ui->rValSpin->value();
    double g=ui->gValSpin->value();
    double b=ui->bValSpin->value();
    double dev=ui->gDevSpin->value();
    if(dev>0){
        double gmin=g-dev;
        double gmax=g+dev;
        for(int i=0; i<100; i++){
            QColor color;
            double g2=gmin+(gmax-gmin)*i/99;
            g2=std::min(std::max(g2,0.0),255.0);
            color.setRgb(r, g2, b);
            img.setPixel(i,0,color.rgb());
        }
        ui->gColorLbl->setPixmap(QPixmap::fromImage(img));
    } else {
        ui->gColorLbl->clear();
    }

}

void CompDefDialog::showB()
{
    QImage img(100,1,QImage::Format_RGB888);
    double r=ui->rValSpin->value();
    double g=ui->gValSpin->value();
    double b=ui->bValSpin->value();
    double dev=ui->bDevSpin->value();
    if(dev>0){
        double bmin=b-dev;
        double bmax=b+dev;
        for(int i=0; i<100; i++){
            QColor color;
            double b2=bmin+(bmax-bmin)*i/99;
            b2=std::min(std::max(b2,0.0),255.0);
            color.setRgb(r, g, b2);
            img.setPixel(i,0,color.rgb());
        }
        ui->bColorLbl->setPixmap(QPixmap::fromImage(img));
    } else {
        ui->bColorLbl->clear();
    }
}

void CompDefDialog::showH()
{
    QImage img(100,1,QImage::Format_RGB888);
    double h=ui->hValSpin->value();
    double s=ui->sValSpin->value();
    double v=ui->vValSpin->value();
    double dev=ui->hDevSpin->value();
    if(dev>0){
        double hmin=h-dev;
        double hmax=h+dev;
        for(int i=0; i<100; i++){
            QColor color;
            double h2=hmin+(hmax-hmin)*i/99;
            if(h2<0)
                h2+=1;
            if(h2>1)
                h2-=1;
            color.setHsvF(h2, s, v);
            img.setPixel(i,0,color.rgb());
        }
        ui->hColorLbl->setPixmap(QPixmap::fromImage(img));
    } else {
        ui->hColorLbl->clear();
    }
}

void CompDefDialog::showS()
{
    QImage img(100,1,QImage::Format_RGB888);
    double h=ui->hValSpin->value();
    double s=ui->sValSpin->value();
    double v=ui->vValSpin->value();
    double dev=ui->sDevSpin->value();
    if(dev>0){
        double smin=s-dev;
        double smax=s+dev;
        for(int i=0; i<100; i++){
            QColor color;
            double s2=smin+(smax-smin)*i/99;
            s2=std::min(std::max(s2,0.0),1.0);
            color.setHsvF(h, s2, v);
            img.setPixel(i,0,color.rgb());
        }
        ui->sColorLbl->setPixmap(QPixmap::fromImage(img));
    } else {
        ui->sColorLbl->clear();
    }
}

void CompDefDialog::showV()
{
    QImage img(100,1,QImage::Format_RGB888);
    double h=ui->hValSpin->value();
    double s=ui->sValSpin->value();
    double v=ui->vValSpin->value();
    double dev=ui->vDevSpin->value();
    if(dev>0){
        double vmin=v-dev;
        double vmax=v+dev;
        for(int i=0; i<100; i++){
            QColor color;
            double v2=vmin+(vmax-vmin)*i/99;
            v2=std::min(std::max(v2,0.0),1.0);
            color.setHsvF(h, s, v2);
            img.setPixel(i,0,color.rgb());
        }
        ui->vColorLbl->setPixmap(QPixmap::fromImage(img));
    } else {
        ui->vColorLbl->clear();
    }

}

void CompDefDialog::on_rValSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showR();
}

void CompDefDialog::on_gValSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showG();
}

void CompDefDialog::on_bValSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showB();
}

void CompDefDialog::on_hValSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showH();
}

void CompDefDialog::on_sValSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showS();
}

void CompDefDialog::on_vValSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showV();
}

void CompDefDialog::on_rDevSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showR();
}

void CompDefDialog::on_gDevSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showG();
}

void CompDefDialog::on_bDevSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showB();
}

void CompDefDialog::on_hDevSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showH();
}

void CompDefDialog::on_sDevSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showS();
}

void CompDefDialog::on_vDevSpin_valueChanged(double arg1)
{
    Q_UNUSED(arg1);
    showV();
}

void CompDefDialog::on_spTrainBtn_clicked()
{
    storeConfig();
    QSqlDatabase defaultdb=QSqlDatabase::database();
    QString databaseName=defaultdb.databaseName();
    compDef->prepareToRun();
    trainingTab=ui->tabWidget->currentIndex();
    trainFuture = QtConcurrent::run(
                compDef, &CompDef::trainShapePredictor, databaseName);
    timeRunning.start();
    timer.start(1000);
    ui->tabWidget->setCurrentIndex(5);
    this->setEnabled(false);
}

void CompDefDialog::on_dnnTrainBtn_clicked()
{
    storeConfig();
    if(!compDef->compDNNParams.enabled){
        auto answer=QMessageBox::warning(this,tr("clasificator FHOG"), tr("Învățarea nu are sens, deoarece DNN nu e activat. Continuați?"), QMessageBox::Yes, QMessageBox::No);
        if(QMessageBox::Yes!=answer)
            return;
    }

    QSqlDatabase defaultdb=QSqlDatabase::database();
    QString databaseName=defaultdb.databaseName();
    compDef->prepareToRun();
    trainingTab=ui->tabWidget->currentIndex();
    trainFuture = QtConcurrent::run(
                compDef, &CompDef::trainDNN, databaseName);
    timeRunning.start();
    timer.start(1000);
    ui->tabWidget->setCurrentIndex(5);
    this->setEnabled(false);

}

void CompDefDialog::on_importDNNBtn_clicked()
{
    QString fileName=QFileDialog:: getOpenFileName(this,tr("Nume fișier de încărcat"),QString(),
                                 tr("fișiere dat (*.dat);;Toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    if(!compDef->importDNN(fileName)){
        QMessageBox::information(this, tr("eroare"), tr("Nu s-a putut încărca rețeaua neuronala din fișierul ")+fileName);
    }
}

void CompDefDialog::on_exportDNNBtn_clicked()
{
    QString fileName=QFileDialog:: getSaveFileName(this,tr("Nume fișier pentru salvare rețea neuronală"),QString(),
                                 tr("fișiere dat (*.dat);;Toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    if(!compDef->exportDNN(fileName)){
        QMessageBox::information(this, tr("eroare"), tr("Nu s-a putut salva rețeaua neuronala în fișierul ")+fileName);
    }
}

void CompDefDialog::on_deleteDnnBtn_clicked()
{
    assert(compDef!=nullptr);
    compDef->clearDNN();
    ui->dnnEnableCB->setChecked(false);
}

void CompDefDialog::on_deleteShapeBtn_clicked()
{
    assert(compDef!=nullptr);
    compDef->clearShapePredictor();
    ui->spEnableCB->setChecked(false);
}

void CompDefDialog::on_exportFHOGBtm_clicked()
{
    QString fileName=QFileDialog:: getSaveFileName(this,tr("Nume fișier pentru salvare clasificator FHOG"),QString(),
                                 tr("fișiere dat (*.dat);;Toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    if(!compDef->exportFHOG(fileName)){
        QMessageBox::information(this, tr("eroare"), tr("Nu s-a putut salva clasificatorul FHOG în fișierul ")+fileName);
    }
}

void CompDefDialog::on_deleteFHOGBtn_clicked()
{
    assert(compDef!=nullptr);
    compDef->clearFHOG();
}

void CompDefDialog::on_exportShapeBtn_clicked()
{
    QString fileName=QFileDialog:: getSaveFileName(this,tr("Nume fișier pentru salvare instrument poziționare"),QString(),
                                 tr("fișiere dat (*.dat);;Toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    if(!compDef->exportShapePredictor(fileName)){
        QMessageBox::information(this, tr("eroare"), tr("Nu s-a putut salva intrumentul de poziționare în fișierul ")+fileName);
    }
}

void CompDefDialog::on_importFHOGBtn_clicked()
{
    QString fileName=QFileDialog:: getOpenFileName(this,tr("Nume fișier FHOG de încărcat"),QString(),
                                 tr("fișiere dat (*.dat);;Toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    if(!compDef->importFHOG(fileName)){
        QMessageBox::information(this, tr("eroare"), tr("Nu s-a putut încărca clasificatorul FHOG din fișierul ")+fileName);
    }
}

void CompDefDialog::on_importShapeBtn_clicked()
{
    QString fileName=QFileDialog:: getOpenFileName(this,tr("Nume fișier instrument poziționare de încărcat"),QString(),
                                 tr("fișiere dat (*.dat);;Toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    if(!compDef->importShapePredictor(fileName)){
        QMessageBox::information(this, tr("eroare"), tr("Nu s-a putut încărca instrumentrul de poziționare din fișierul ")+fileName);
    }
}

void CompDefDialog::on_trainingIOUThreshCB_clicked()
{
    ui->trainingIOUThreshSpin->setEnabled( ! ui->trainingIOUThreshCB->isChecked());
}

