/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barcode.h"
#include "regextester.h"

const QStringList Barcode::bcTypes=QStringList()<<"toate"<<"datamatrix"<<"QR code"<<"code39"<<"code128";
#ifdef DMTX
const int Barcode::DMTXTIMEOUT;
#endif
Barcode::Barcode()
{

}

QString Barcode::decodeBC(const QImage image, const QString validator, int & errorCode, Barcode::BCType symbology)
{
    qDebug()<<"decodeBC"<<symbology;
    QString result=decodeBC1(image, validator, errorCode, symbology);
    for(int offset=1; (offset<4)&&(result.isEmpty()); offset++){
        QImage img=image.copy(offset, offset,image.width()-offset, image.height()-offset);
        result=decodeBC1(img, validator, errorCode, symbology);
    }
    return result;
}

QString Barcode::decodeBC1(const QImage image, const QString validator, int &errorCode, BCType symbology)
{
    int width4;
    QString result;
#ifdef ZXING
    //adjust width in order to be a multiple of 4, because QImage has 32 bits aligned scanlines, and libdmtx is not using this padding.
    QImage image2=image.scaled(2*image.width(), 2*image.height(), Qt::KeepAspectRatio,Qt::SmoothTransformation);
    width4=((image2.width()+3)/4)*4;
    QImage tmp2=image2.convertToFormat(QImage::Format_Grayscale8).copy(0,0,width4,image2.height());
    tmp2.invertPixels();
    uchar * dataPtr2=tmp2.bits();
    //image2.save("/tmp/images/1.png");
    //tmp2.save("/tmp/images/2.png");
    result=decodeBCZXing(dataPtr2, tmp2.width(), tmp2.height(), symbology, errorCode);
    if(!result.isEmpty()){
        QString dummy;
        if(RegExTester::validateSn(result, validator, dummy))
            return result;
        else {
            result.clear();
            errorCode=-22;
        }
    }
#endif
    errorCode=0;
    //adjust width in order to be a multiple of 4, because QImage has 32 bits aligned scanlines, and libdmtx is not using this padding.
    width4=((image.width()+3)/4)*4;
    QImage tmp=image.convertToFormat(QImage::Format_Grayscale8).copy(0,0,width4,image.height());
    uchar * dataPtr=tmp.bits();
#ifdef ZBAR
    if(symbology!=DATAMATRIX){
        result=decodeBCZbar(dataPtr, tmp.width(), tmp.height(), tmp.byteCount(), symbology, errorCode);
        if(!result.isEmpty()){
            QString dummy;
            if(RegExTester::validateSn(result, validator, dummy))
                return result;
            else {
                result.clear();
                errorCode=-22;
            }
        }
    }
#endif
    errorCode=-21;
#ifdef DMTX
    if((symbology==DATAMATRIX)||(symbology==ALLCODES)){
        result=decodeBCDmtx(dataPtr, tmp.width(), tmp.height(), errorCode);
        //result=decodeBCDmtx(dataPtr2, tmp2.width(), tmp2.height(), errorCode);
        //if(result.isEmpty())
        //    result=decodeBCDmtx(dataPtr2, tmp2.width(), tmp2.height(), errorCode);
    }
#endif
    return result;
}

#ifdef ZBAR
QString Barcode::decodeBCZbar(const uchar *dataPtr, const int w, const int h, const int l, const Barcode::BCType symbology, int &errorCode)
{
    errorCode=-1;
    zbar::ImageScanner scanner;
    //enable all symbologies
    auto zbarSymbology=zbar::ZBAR_NONE;
    QString expectedType;
    switch(symbology){
    case QRCODE:
        zbarSymbology=zbar::ZBAR_QRCODE;
        break;
    case CODE39:
        zbarSymbology=zbar::ZBAR_CODE39;
        break;
    case CODE128:
        zbarSymbology=zbar::ZBAR_CODE128;
        expectedType="CODE-128";
        break;
    default:
        //enable all
        zbarSymbology=zbar::ZBAR_NONE;
    }
    scanner.set_config(zbar::ZBAR_NONE, zbar::ZBAR_CFG_ENABLE, 0);
    scanner.set_config(zbarSymbology, zbar::ZBAR_CFG_ENABLE, 1);
    zbar::Image img(w, h, "Y800",dataPtr, l);
    qDebug()<<l<<dataPtr<<w<<h<<
              img.get_data_length()<<img.get_width()<<img.get_height()<<QString::number(img.get_format(),16);
    scanner.scan(img);
    QString msg;
    for(zbar::Image::SymbolIterator symbol = img.symbol_begin();
        symbol != img.symbol_end();
        ++symbol) {

        // do something useful with results
        QString symbology=QString::fromStdString( symbol->get_type_name());
        qDebug() << "decoded " << symbology
             << " symbol " << QString::fromStdString(symbol->get_data());
        msg=QString::fromStdString(symbol->get_data());
        if(!msg.isEmpty()){
            if((expectedType.isEmpty()) || (expectedType.compare(symbology)==0)){
                //use this result
                qDebug()<<"using"<<symbology<<"expected"<<expectedType;
                errorCode=0;
                break;
            } else {
                //ignore this result
                qDebug()<<"ignoring"<<symbology<<expectedType;
                msg.clear();
            }
        }
//        if(!msg.isEmpty())
//            msg+="\n";
//        msg += QString::fromStdString( symbol->get_type_name())+":"+QString::fromStdString(symbol->get_data());

    }
    img.set_data(nullptr, 0);
    return msg;
}
#endif


#ifdef DMTX
QString Barcode::decodeBCDmtx(uchar *dataPtr, const int w, const int h, int &errorCode)
{
    QString result;
    DmtxImage * dmtxImage = dmtxImageCreate(dataPtr, w, h, DmtxPack8bppK);
    if(!dmtxImage){
        qDebug()<<"can't create image";
        errorCode=-11;
        return "";
    }
    //dmtxImageSetProp(dmtxImage,DmtxPropImageFlip,DmtxFlipY);
    qDebug()<<dmtxImage->imageFlip<<dmtxImage->channelCount<<dmtxImage->bytesPerPixel<<dmtxImage->rowPadBytes;
    DmtxDecode * dec= dmtxDecodeCreate(dmtxImage,1);
    if(!dec){
        qDebug()<<"can't create dmtx decoder";
        errorCode=-12;
        return "";
    }
    DmtxTime dmtx_timeout;
    dmtx_timeout=dmtxTimeAdd(dmtxTimeNow(), DMTXTIMEOUT);
    DmtxRegion * reg = dmtxRegionFindNext(dec, &dmtx_timeout);
    if(reg != NULL) {
        DmtxMessage * message = dmtxDecodeMatrixRegion(dec, reg, DmtxUndefined);
        //DmtxMessage * message = dmtxDecodeMosaicRegion(dec, reg, DmtxUndefined);
       if(message != NULL) {
          //fputs("output: \"", stdout);
          //fwrite(message->output, sizeof(unsigned char), message->outputIdx, stdout);
          //fputs("\"\n", stdout);
          result=QString((const char *)(message->output));
          qDebug()<<"dmtx:"<<result;
          dmtxMessageDestroy(&message);
          errorCode=0;
       } else {
           errorCode=-14;
       }
       dmtxRegionDestroy(&reg);
    } else {
        qDebug()<<"no region found";
        errorCode=-13;
    }

    dmtxImageDestroy(&dmtxImage);
    return result;
}
#endif

#ifdef ZXING
#if ZXING_VERSION_MAJOR<2
QString Barcode::decodeBCZXing(uchar *dataPtr, const int w, const int h, const Barcode::BCType symbology, int &errorCode)
{
    errorCode=-1;
    //enable all symbologies
    auto zxingSymbology=ZXing::BarcodeFormat::NONE;
    QString expectedType;
    switch(symbology){
    case QRCODE:
        zxingSymbology=ZXing::BarcodeFormat::QR_CODE;
        break;
    case CODE39:
        zxingSymbology=ZXing::BarcodeFormat::CODE_39;
        break;
    case CODE128:
        zxingSymbology=ZXing::BarcodeFormat::CODE_128;
        //expectedType="CODE-128";
        break;
    case DATAMATRIX:
        zxingSymbology=ZXing::BarcodeFormat::DATA_MATRIX;
        //expectedType="DATAMATRIX";
        break;
    default:
        //enable all
        zxingSymbology=ZXing::BarcodeFormat::NONE;
    }
    ZXing::DecodeHints hints;
    hints.setTryHarder(true);
    hints.setTryRotate(true);
    hints.setFormats(zxingSymbology);
    const ZXing::Result& zxresult = ZXing::ReadBarcode({dataPtr, w, h, ZXing::ImageFormat::Lum}, hints);
    qDebug()<<"ZXing status"<<static_cast<int>(zxresult.status())<<QString::fromStdWString(zxresult.text());
    if(zxresult.status()==ZXing::DecodeStatus::NoError){
        QString result=QString::fromStdWString(zxresult.text());
        return result;
    }
    errorCode=-static_cast<int>(zxresult.status())-30;
    return QString();
}
#else
QString Barcode::decodeBCZXing(uchar *dataPtr, const int w, const int h, const Barcode::BCType symbology, int &errorCode)
{
    errorCode=-1;
    //enable all symbologies
    auto zxingSymbology=ZXing::BarcodeFormat::None;
    QString expectedType;
    switch(symbology){
    case QRCODE:
        zxingSymbology=ZXing::BarcodeFormat::QRCode;
        break;
    case CODE39:
        zxingSymbology=ZXing::BarcodeFormat::Code39;
        break;
    case CODE128:
        zxingSymbology=ZXing::BarcodeFormat::Code128;
        //expectedType="CODE-128";
        break;
    case DATAMATRIX:
        zxingSymbology=ZXing::BarcodeFormat::DataMatrix;
        //expectedType="DATAMATRIX";
        break;
    default:
        //enable all
        zxingSymbology=ZXing::BarcodeFormat::None;
    }
    ZXing::ReaderOptions hints;
    hints.setTryHarder(true);
    hints.setTryRotate(true);
    hints.setFormats(zxingSymbology);
    auto zxresults = ZXing::ReadBarcodes({dataPtr, w, h, ZXing::ImageFormat::Lum}, hints);
    qDebug()<<"ZXing results"<<zxresults.size();
    if(zxresults.size()>0){
        QString result=QString::fromStdString(zxresults[0].text());
        return result;
    }
    errorCode=-30;
    return QString();
}
#endif
#endif
