/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MYCAMERA_H
#define MYCAMERA_H
#include <QObject>
#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include <QPixmap>
#include <QSettings>
#include "ocvhelper.h"


class MyCamera : public QObject
{
    Q_OBJECT
public:
    explicit MyCamera(QSettings * settings, QObject *parent = nullptr);
    QList<QString> getAvailableCameras(void);
    QList<QSize> getResolutions();
    bool setResolution(QSize size);
    QString getDeviceName(void);
    QSize resolution();
    int getNImages(void);
    int getNIgnores(void);
    const int MAXNIMAGES=8;
private:
    QString deviceName;
    QSize deviceResolution;
    QCamera * camera=nullptr;
    QCameraImageCapture * imageCapture=nullptr;
    bool pictureRequested=false;
    int nImages=1; ///< number of images to be averaged
    int nIgnores=1; ///< number of images to be ignored for camera warm-up
    int imgIdx=0; ///< counter for captured images, to count up to nIgnores+nImages. Only emit imageCaptured when all images have been capured
    //QList<QImage> images; ///< temporary store for images
    cv::Mat sum;

signals:
    //void imageCaptured(QPixmap image);
    void imageCaptured(QImage image);
    void captureError(QString errorMessage);

public slots:
    void setCamera(QString deviceName, bool enabled=true);
    void setNImages(int n);
    void setNIgnores(int n);
    void capture(bool firstImage=true);
private slots:
    void cameraIsReady(bool ready);
    void getImage(int id, const QImage &img);
};

#endif // MYCAMERA_H
