#ifndef OCVHELPER_H
#define OCVHELPER_H

#include <QObject>
#include <QRect>
#include <QImage>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

class OcvHelper : public QObject
{
    Q_OBJECT
public:
    explicit OcvHelper(QObject *parent = nullptr);
    static cv::Mat qImage2Mat(const QImage& src, bool switchRedBlue=true, bool grayscale=false);
    ///convert opencv Mat to QImage without allocating new memory for the pixel data.
    ///Assumes mat is of the format CV_8UC3 and requires the initial Mat to be valid as long as the QImage is used
    ///The resulting QImage will have the same byte order as the Mat (BGR or RGB)
    static QImage mat2QImage(cv::Mat& mat);
    static double imageSharpness(const QImage& img, const QRect r=QRect());

signals:

};

#endif // OCVHELPER_H
