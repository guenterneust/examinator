/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARDDEF_H
#define BOARDDEF_H
#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#ifndef _MSC_VER
#include <unistd.h>
#endif
#include <QList>
#include <QImage>
#include "compdef.h"
#include "boardcomptype.h"
#include "detectedcomponent.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <QThreadPool>
#include <QFuture>
#include "logger.h"

#include <QDebug>

#ifndef CV_BGR2RGB
#define CV_BGR2RGB cv::COLOR_BGR2RGB
#endif

class Features{
public:
    cv::Mat descriptors;
    std::vector<cv::KeyPoint> keypoints;
    bool isEmpty(void);
};

/**
 * @brief The ImagesAndLights class is a helper class used inside the board definition
 * The class is used inside a list, each instance representig an image to be taken, typically without moving the camera or the board, but often by changing the lights
 * Therefore, the board alignment can be done only once, but different components can be examined with different illumination.
 */
class ImagesAndLights{
public:
    ImagesAndLights(int leds=0){
        for(int i=0; i<leds; i++)
            lightData.append('0');
    }
    bool lightsEnabled=false; ///<boolean telling the system if to try to to urn on he lights as configured. If false, the lights will be left as they are.
    QByteArray lightData; ///< configuration of the lights, as a string of 3 bytes, each byte being either '0' or '1'.
    bool imageUsed=false; ///< boolean telling if the image at which this instance referes is used. This is calculated on the fly when the board is loaded, and therefore not saved.
    int delay=0;///< delay between setting the lighs and starting image acquisition. The special value -1 means that n external trigger is used.
    friend void serialize (const ImagesAndLights& item, std::ostream& out);
    friend void deserialize (ImagesAndLights& item, std::istream& in);
};
Q_DECLARE_METATYPE(ImagesAndLights)

inline void serialize(const ImagesAndLights &item, std::ostream &out)
{
    int version = 2;
    dlib::serialize(version, out);
    dlib::serialize(item.lightsEnabled, out);
    dlib::serialize(item.lightData.toStdString(), out);
    dlib::serialize(item.delay, out);
    qDebug()<<"serialize ImagesAndLights";
}

inline void deserialize (ImagesAndLights& item, std::istream& in)
{
    //qDebug()<<"deserialize CompDefRect";
    int version = 0;
    dlib::deserialize(version, in);
    if ((version != 1)&&(version != 2))
        throw dlib::serialization_error("Unexpected version found while deserializing Lights.");
    dlib::deserialize(item.lightsEnabled, in);
    std::string str;
    dlib::deserialize(str, in);
    item.lightData=QByteArray::fromStdString(str);
    if(version>1){
        dlib::deserialize(item.delay, in);
    }
    qDebug()<<"deserialize ImagesAndLights"<<item.lightData<<"\n";
}

class BoardDef : public QObject
{
    Q_OBJECT
public:
    enum AlignMethod {FIDUCIAL=0, AKAZE=1, ORB=2};
    enum TestStartType {STARTIMMEDIATE=0, STARTDELAYED=1, STARTONCLICK=2};
    enum DetailType {ALIGNFIDUCIAL=0,/* 1,2...9=AKAZE, ORB...*/ STARTMETHOD=10, LIGHTS=11};
    BoardDef(QWidget *testInterface=nullptr);
    QString name;
    QString refImagePath;
    CompDef refClassifier;
    void saveToDB(void);
    long long saveToDBAs(QString newName);
    bool loadFromDB(const long long id, bool loadForTest=false);
    void removeFromDB(void);
    bool dirty;
    CompDefRect refRect;
    //used to initialize
    BoardComp recentComp;
    /// active component type
    BoardCompType * compType;
    QList<BoardCompType> compTypes;
    long long id;
    int minArea;
    int maxArea;
    AlignMethod alignMethod=FIDUCIAL;
    QString snValidator;
    QString testCodeOverride;
    /**
     * @brief detectComponents search for components recognized by the comp classifier in an array of images
     * @param img array of up to 4 images, representing the same image, rotated in increments of 90 degrees
     * @param comp component definition, containing the classifier (FHOG or DNN) to be used to search the images
     * @param scale
     * @param displaySearchFrames
     * @param adjustThreshold
     * @return list of rectangles as bounding boxes for the detected components
     */
    QList<DetectedComponent> detectComponents(QVector<QImage> img, CompDef * comp,
                                              double scale, bool displaySearchFrames, double adjustThreshold);

    QList<DetectedComponent> detectComponentsImg(const QImage img, CompDef * comp,
                                              bool displaySearchFrames, double adjustThreshold);
    /**
     * @brief run the test for the entire board, as defined by this board definition, using the part of the provided image specified previously detected board rectangle as ROI
     * @param img image to be tested
     * @param boardRect prevously detected board rectangle, to be used as ROI
     * @param displaySearchFrames boolean specifying if the user wants to be shown the search ROI's for each component, for debug purposes
     * @return the list of board components, detected or not.
     */
    QList<DetectedComponent> testBoard(const QList<QImage> &img, const DetectedComponent &boardRect, bool displaySearchFrames, bool rotateComponents);
    void adjustDetectedComponents(QList<DetectedComponent> & dets, double chipwidth, double chipheight,
                                  int angle, double xoffset=0, double yoffset=0, double scale=1);
    /**
     * @brief check if detections overlap and remove the components with the lower detection score
     * @param components - list of components to be checked
     * @return the number of removed components
     */
    int removeOverlaps(QList<DetectedComponent> & components);
    /**
     storage for keypoints and the corresponding keypoint descriptors for the reference image
     these keypoints will be stored to the database, in order to be used for image alignment
     */
    Features refFeatures;
    void binaryDescriptors(Features & features, cv::Mat image, const CompDefRect &roi);
    void saveBinaryFeatures(void);
    void loadBinaryFeatures(void);
    int testStartType=0;
    int testStartDelay=2000;
    bool testStartExtDisabled=false;
    ///check if a barcode location for the serial number has been defined
    bool hasBarcodeSN(void);
    /// run the board definition on the given image
    /// return status in testOk, and the actual results as list of DetectedComponents, one for each
    /// instance of the board found
    /// if resultsFile is given, the results are also saved into the file with this name. If oldResults
    /// is also provided, the decisins from the oldResult ar merged with the current result
    QList<DetectedComponent> runOnImage(const QList<QImage> &imageList, bool displaySearchFrames,
                                        bool &testOk, bool rotateComponents, QString resultsFile=QString(),
                                        const QList<DetectedComponent> &oldResults=QList<DetectedComponent>(),
                                        int *finalDecision=nullptr);
    /// get classifiers from boardComp id
    /// also put the name of the component type into compTypeName
    const QList<BoardCompTypeClassifier> classifierFromComponent(long long component, QString &compTypeName);
    static int calculateDecision(QList<DetectedComponent> &boards);
    /// List holding the information concerning the lights to be turned on for an image.
    /// Besides the information concerning the lights, this list also defines the number of images to be taken of one board:
    /// the number of images is equal to the number of list entries.
    QList<ImagesAndLights> lights;
    /**
     * @brief This function makes sure that the lights list contains at least one item.
     * Then, it checks that every component has at least one of the images activated, by activating the first image if none is active.
     * While doing this, it remembers which of the defined images is really used for any of the components.
     * Finally the imageUsed flag for each of the images in the lights list is set according to the usage of the lights
     */
    void fixImagesAndLights(void);
    ///check the status of all parts of the board and set the status of the board accordingly.
    /// return if the board is OK
    bool setBoardStatus(DetectedComponent &boardRect);
    static QStringList imageNames(const QString rootImage);
    static QList<QImage> loadBoardImages(const QString rootImage);
    /// set the pointer to the flag from the main window, in order to know if to wait for future images
    void setImageAcquisitionDone(bool * ptr){
        imageAcquisitionDone=ptr;
    }
    ///minimum number of boards to be tested in an image set
    /// if less boards are found, the global result will be FAIL, irrespective of the status of the boards found
    int numberOfBoards=1;
    bool isValid(){return valid;}
    unsigned lastsave=0;
    unsigned lastupdate=0;
    unsigned getLastSaved(QSqlDatabase &db);
    unsigned getLastUpdated(QSqlDatabase &db);
private:
    /// pointer to the test interface. Only used in order to connect to the gotNewImage signal
    QWidget *parent=nullptr;
    QThreadPool dnnThreadPool;
    /// pointer to the flag telling if all images have been acquired
    bool * imageAcquisitionDone=nullptr;
    //double x, y, w, h;
    //MemCompDefRect::Orientation orientation;
    /**
     * @brief findBoardComponents find all fiducials or all other componente corresponding to a board in the image
     * @param img image to be analyzed
     * @param transform trannsform to be applied to get the expected components coordinates from the defined component coordinates
     * @param boardRect detected board rectangle
     * @param compType value specifying what type of components to find: fiducial, regular components and barcodes, or subcomponents
     * @param displaySearchFrames boolean specifying if to show the ROIs for each component.
     * @param masterOffsets offsets for the found master components, to be filled in when the normal components are found, and used when subcomponents are searched for
     * @param doneComponents list of components which don't need any further processing
     * @return returns the list of found components. If a component is not found, a failing rectangle with the expected position is returned.
     */
    QList<DetectedComponent> findBoardComponents(const QImage &img, const cv::Mat & transform,
                                                 const DetectedComponent & boardRect, BoardCompType::ComponentType compType,
                                                 bool displaySearchFrames,
                                                 QMap<long long, QPointF> &masterOffsets, QSet<long long> &doneComponents, int imageIndex, bool rotateComponents);
    /**
     * @brief retrieveOneFoundComponent helper function used in findBoardComponents to retrieve one component searched for using findOneBoardComponent
     * @param result The result of findOneBoardComponent
     * @param foundComponents list of the already found components, to add the current component to
     * @param masterOffsets masterOffsets offsets for the found master components, to be filled in when the normal components are found, and used when subcomponents are searched for
     * @param doneComponents list of components which don't need any further processing
     */
    void retrieveOneFoundComponent(QFuture<DetectedComponent> result,  BoardCompType::ComponentType compType, QList<DetectedComponent> &foundComponents,
                                   QMap<long long, QPointF>& masterOffsets, QSet<long long> &doneComponents);
    /**
     * @brief findOneBoardComponent
     * @param comp a dummy component filled with the data for the expected position of the component, to be returned when the component is not found
     * @param c reference to the board component which is to be found
     * @param xoffset offset of the chip relative to the entire image, used to bring back the found component to image coordinates
     * @param yoffset offset of the chip relative to the entire image, used to bring back the found component to image coordinates
     * @param angle rotation angle of the chip in degrees, used to bring back the found component to image coordinates
     * @param rot90 boolean telling if the rotation angle is 90 or 270 degrees, cases when widht and height are switched for some uses
     * @param wChip width of the image chip
     * @param hChip height of the image chip
     * @param chipScale scale of the image chip, used to bring back the found component to image coordinates
     * @param cT reference to the component type, used to get the classifiers used to search for the component
     * @param compType type of the component to be searched for, used to set the type of the found component
     * @param qChip image chip to be searched for the component
     * @param displaySearchFrames boolean used to enable displaying the image chips for debug purposes
     * @return the found component, with all properties set to image coordinates, or the expeced component, as provided by the comp parameter if nothing acceptable was found
     */
    DetectedComponent findOneBoardComponent(DetectedComponent comp, const BoardComp & c, double xoffset, double yoffset, int angle, int rot90,
                                            double wChip, double hChip, double chipScale, const BoardCompType &cT,
                                            BoardCompType::ComponentType compType, const QImage qChip, bool displaySearchFrames, const int imageIndex);
    /**
     * @brief return all master components in the board definition as a QMap using the component id as key, and a QPointF as null point, to be used as offset of the master component when searching for subcomponents
     * @return
     */
    QMap<long long, QPointF> findParentComponents();
    // from https://stackoverflow.com/questions/17127762/cvmat-to-qimage-and-back
    inline QImage mat2QImage(cv::Mat const& src)
    {
         cv::Mat temp; // make the same cv::Mat
         cv::cvtColor(src, temp,CV_BGR2RGB); // cvtColor Makes a copy, that's what we need
         QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
         dest.bits(); // enforce deep copy, see documentation
         // of QImage::QImage ( const uchar * data, int width, int height, Format format )
         return dest;
    }

    void qImage2array2d(QImage const& src, dlib::array2d<dlib::rgb_pixel> &dst);
    /**
     * @brief helper function to convert the vertices of a component rect to a vector of cv::Point2f
     * @param rect - input rectangle
     * @return returns a vector of four points, in clockwise order starting top-left
     */
    std::vector<cv::Point2f> rectPointList(const CompDefRect & rect);
    /**
     * @brief padding - constant used to define extra padding, besides the specified tollerances, for the image chips on which co
     */
    const double padding=0.5;
    /// attempt alignment of the scene using ORB/AKAZE features. The reference features are loaded from the database.
    /// this function extracts features from the scene and attempts to match them to the reference features.
    /// If a match is found, the transformation matrix gets updated, otherwise it remains untouched
    bool alignBinaryFeatures(const QImage img, const DetectedComponent & boardRect, cv::Mat &transform);
    //QList<DetectedComponent> testBoard(const QImage &img, DetectedComponent & boardRect);
    /// calculate the total error in the destination space by adding up the distances between the destination points and
    /// the points obtained by applying the provided transformation onto  the source points
    double transformError(std::vector<cv::Point2f>srcPts, std::vector<cv::Point2f>dstPts,cv::Mat transform);
    /// merge the decision from the oldResults with the results
    /// returns the global resulting decision and updates the decisions for the found components
    int mergeResults(QList<DetectedComponent> &results, const QList<DetectedComponent> &oldResults);
    bool valid=true;
    float averageRotation(const cv::Mat & transform, const int orientationDifference);
    QImage extractRotatedRegion(const QImage &image, int x, int y, int w, int h, float alpha, float scale);
signals:
    void logMsg(QString message, LOGVERBOSITY verbosity);
};


#endif // BOARDDEF_H
