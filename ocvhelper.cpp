#include "ocvhelper.h"
#include "QElapsedTimer"
#include <QDebug>

#ifndef CV_BGR2RGB
#define CV_BGR2RGB cv::COLOR_BGR2RGB
#endif

OcvHelper::OcvHelper(QObject *parent) : QObject(parent)
{

}

cv::Mat OcvHelper::qImage2Mat(const QImage &src, bool switchRedBlue, bool grayscale)
{
#if 1
    if(grayscale){
        if(src.format()!=QImage::Format_Grayscale8){
            return qImage2Mat(src.convertToFormat(QImage::Format_Grayscale8),switchRedBlue, grayscale);
        }
        qDebug()<<"qImage2Mat"<<src.height() << src.width()<< src.bytesPerLine() << src.byteCount() << src.bytesPerLine()*src.height() << src.format();
        cv::Mat tmp=cv::Mat(src.height(),src.width(),CV_8UC1,(uchar*)src.bits(),src.bytesPerLine()).clone();
        qDebug()<<"tmp created"<<tmp.rows<<tmp.cols<<tmp.elemSize();
        qDebug()<<"tmp ok?";
        return tmp;
    } else {
        if(src.format()!=QImage::Format_RGB888){
            return qImage2Mat(src.convertToFormat(QImage::Format_RGB888),switchRedBlue, grayscale);
        }
        //qDebug()<<"qImage2Mat"<<src.height() << src.width()<< src.bytesPerLine() << src.byteCount() << src.bytesPerLine()*src.height() << src.format();
        cv::Mat tmp=cv::Mat(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine()).clone();
        //qDebug()<<"tmp created"<<tmp.rows<<tmp.cols<<tmp.elemSize();
        //qDebug()<<"tmp ok?";
        if(switchRedBlue)
            cvtColor(tmp, tmp,CV_BGR2RGB);
        else {
            /*nothing*/;
        }
        return tmp;
    }
#else
    qDebug()<<"qImage2Mat"<<src.height() << src.width()<< src.bytesPerLine() << src.byteCount() << src.bytesPerLine()*src.height() << src.format();
    //cv::Mat tmp=cv::Mat(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine()).clone();
    dlib::array2d<dlib::rgb_pixel> arr;
    dlib::assign_image(arr, dlib::mat((const dlib::rgb_pixel *)src.bits(), src.width(), src.height()));
    usleep(5000000);
    cv::Mat tmp(1,1,CV_8UC3);
    usleep(5000000);
    qDebug()<<"tmp created"<<tmp.rows<<tmp.cols<<tmp.elemSize();

    return tmp;
    tmp=cv::Mat(src.height(),src.width(),CV_8UC3);
    memcpy(tmp.data, src.constBits(), src.byteCount());
    qDebug()<<"tmp ok?";
    if(switchRedBlue)
        cvtColor(tmp, tmp,CV_BGR2RGB);
    else {
        /*nothing*/;
    }
    return tmp;
#endif
}

QImage OcvHelper::mat2QImage(cv::Mat &mat)
{
    assert(mat.type()==CV_8UC3);
    const uchar *qImageBuffer = (const uchar*)mat.data;
    // Create QImage with same dimensions as input Mat
    QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
    return img;
}


double OcvHelper::imageSharpness(const QImage &img, const QRect r)
{
    QRect chipRect;
    if(img.isNull())
        return 0;
    if(r.isEmpty())
        chipRect=img.rect();
    else
        chipRect=r.intersected(img.rect());
    if(chipRect.isEmpty())
        return 0;
    QImage tmp=img.convertToFormat(QImage::Format_Grayscale8).copy(chipRect);
    cv::Mat chip=qImage2Mat(tmp, false, true);
    cv::Mat dst;
    cv::Scalar mu, sigma;
    cv::Laplacian(chip, dst, CV_64F, 1);
    cv::meanStdDev(dst, mu, sigma);
    double focusMeasure1 = sigma.val[0] * sigma.val[0];
    cv::Laplacian(chip, dst, CV_64F, 3);
    cv::meanStdDev(dst, mu, sigma);
    double focusMeasure3 = sigma.val[0] * sigma.val[0];
    cv::Laplacian(chip, dst, CV_64F, 5);
    cv::meanStdDev(dst, mu, sigma);
    double focusMeasure5 = sigma.val[0] * sigma.val[0];
    cv::Laplacian(chip, dst, CV_64F, 7);
    cv::meanStdDev(dst, mu, sigma);
    double focusMeasure7 = sigma.val[0] * sigma.val[0];
    qDebug()<<"focus"<<focusMeasure1<<focusMeasure3<<focusMeasure5<<focusMeasure7;
    cv::meanStdDev(chip, mu, sigma);
    double adj=sigma.val[0]*sigma.val[0];
    qDebug()<<"focus"<<focusMeasure1/adj<<focusMeasure3/adj<<focusMeasure5/adj<<focusMeasure7/adj<<sigma.val[0]<<chipRect;
    dst=qImage2Mat(img);
    QElapsedTimer t;
    t.start();
    mu=cv::mean(dst);
    qDebug()<<"average"<<mu[0]<<mu[1]<<mu[2]<<"duration"<<t.elapsed();
    return focusMeasure3;
}
