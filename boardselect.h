/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARDSELECT_H
#define BOARDSELECT_H

#include <QDialog>
#include <QComboBox>
#include <QSettings>

namespace Ui {
class BoardSelect;
}

class BoardSelect : public QDialog
{
    Q_OBJECT

public:
    explicit BoardSelect(bool displaySearchFrames, bool showFiducials, int saveOption, QString saveDir,
                         QSettings * settings, QWidget *parent = 0);
    ~BoardSelect();
    QComboBox * getBoardCombo();
    long long getSelectedBoard();
    long long getId(QString boardName);
    bool getDisplaySearchFrames();
    //void setDisplaySearchFrames(bool value);

    bool getShowFiducials();
    int getSaveOption();
    QString getSaveDir();
private slots:
    void on_settingsCombo_currentIndexChanged(int index);

    void on_settingsEdit_editingFinished();

    void on_boardSearchEdit_textChanged(const QString &arg1);

private:
    Ui::BoardSelect *ui;
    QSettings * settings=nullptr;
};

#endif // BOARDSELECT_H
