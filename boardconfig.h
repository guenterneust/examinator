/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARDCONFIG_H
#define BOARDCONFIG_H

#include <QDialog>
#include <QComboBox>
#include "boarddef.h"

namespace Ui {
class BoardConfig;
}

class BoardConfig : public QDialog
{
    Q_OBJECT

public:
    explicit BoardConfig(const QString &name, int alignMethod, QString testCode, const QList<ImagesAndLights> &lights, const int numberOfBoards, QWidget *parent = 0);
    ~BoardConfig();
    QComboBox * getClassifierCombo(void);
    QString getName(void);
    long long getClassifier(void);
    long long getAlignMethod(void);
    QString getTestCodeOverride(void);
    int getNumberOfBoards(void);
    QList<ImagesAndLights> getLights();


private slots:
    void on_BoardConfig_accepted();

    void on_imageNumSpin_valueChanged(int arg1);
    void on_imageUpBtn_clicked();
    void updateLights();

    void on_imageDownBtn_clicked();

    void on_imageDelaySpin_valueChanged(int arg1);

    void on_extTriggerCB_clicked();

private:
    QList<ImagesAndLights> lights;
    void showImageAndLights(const ImagesAndLights &light);
    int currentImage;

public:
    Ui::BoardConfig *ui;
};

#endif // BOARDCONFIG_H
