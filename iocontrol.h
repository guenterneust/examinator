/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IOCONTROL_H
#define IOCONTROL_H

#include <QSerialPort>
#include <QTimer>
#include "boarddef.h"
/**
 @brief The IOControl class
 Class used primarily to control the lights for the examinator programm by using an arduino, connected trough a serial connection.
 The system is intended to be used, when possible with controlled light, comming from LED strips, called upper, central and lower light.
 When used as intended (LED strips controlled by the arduino trough FET's, the strips should be on or off in less time than it takes to send the command to the arduino.
 Nonetheless, usually some delay needs to be added when using different lights for the images, because the autoexposure of the camera needs some time to adjust.
 Besides of controlling the lights, this class is also used for a simple remote control of the program.
 For now, the following messages are defined
 "T": input signal for examinator, triggers the start of the optical inspection, or the capture of the next image when a multiimage test is executed and the image is defined to use an external trigger
 "TN": input signal for examinator, triggers the capture of the next image when a multiimage test is executed and the image is defined to use an external trigger
 "ST": output signal for examinator, sent always after a new test is started
 "SP": output signal for examinator, sent after a test is started finished and the result is PASS
 "SF": output signal for examinator, sent after a test is started finished and the result is FAIL
 "PING": input signal for examinator, used to check if the program is alive
 "PONG": output signal for examinator, sent immediately after receiving PING
 "L": input signal for examinator, confirms that he lights have been set.
 "P": input signal for examinator, informing that he lights can not be set, typically because of missing power for the LED strips.
 */
class IOControl : public QObject
{
    Q_OBJECT
public:
    explicit IOControl(QObject *parent = nullptr);
    bool setPort(QString portName);
    QByteArray getLights();
    void setWarnMissingLights(bool doWarn){
        warnMissingLights=doWarn;
    };
    inline void emitStart(void){
        emit start();
    }
private:
    QSerialPort port;
    QByteArray readBuffer;
    bool warnMissingLights=true;
    const int maxNoResponseWarn=3;
    int dontWarn=0;
    bool stressTestRunning=false;
    ImagesAndLights stressLights;
    volatile int pendingStressAnswers=0;
    QTimer watchdog;

private slots:
    void timeoutSlot(void);
    void triggerSlot(void);

signals:
    void start(void);
    void triggerNext(void);
    void initiateImageCapture(void);
    void newData(QByteArray data);
    void stressLightsSignal(const ImagesAndLights & lights, bool);
public slots:
    void readData();
    bool setLights(const ImagesAndLights &lights, bool startCapture=false);//receives 3 bytes, which should consist of 3 digits, '0' meaning off for each byte.
    void sendStatus(int status);
    void sendMessage(QByteArray msg);
    void stressTest(const ImagesAndLights &lights, bool start);
};

#endif // IOCONTROL_H
