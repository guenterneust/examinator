#include "brdinfo.h"
#include "ui_brdinfo.h"
#include <QDebug>
#include <QAbstractTextDocumentLayout>
#include <QDateTime>
#include <QTextTable>
#include <QTextTableCell>
#include <QTextFrameFormat>
#include <QTextEdit>
#include <QTextCursor>
#include <QMessageBox>
#include <QShortcut>

#define DPI 1200

BrdInfo::BrdInfo(BoardDef *brdDef, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BrdInfo)
{
    ui->setupUi(this);
    int rows=8;
    if(brdDef!=nullptr)
        rows=8+brdDef->lights.count();
    ui->brdPropertiesTbl->setRowCount(rows);
    ui->brdPropertiesTbl->setColumnCount(2);
    ui->brdPropertiesTbl->setItem(0,0,new QTableWidgetItem(tr("placa")));
    ui->brdPropertiesTbl->setItem(1,0,new QTableWidgetItem(tr("clasificator")));
    ui->brdPropertiesTbl->setItem(2,0,new QTableWidgetItem(tr("suprafață clasificator")));
    ui->brdPropertiesTbl->setItem(3,0,new QTableWidgetItem(tr("dnn")));
    ui->brdPropertiesTbl->setItem(4,0,new QTableWidgetItem(tr("poziționare")));
    ui->brdPropertiesTbl->setItem(5,0,new QTableWidgetItem(tr("validator culoare")));
    ui->brdPropertiesTbl->setItem(6,0,new QTableWidgetItem(tr("validator nr. Serie")));
    ui->brdPropertiesTbl->setItem(7,0,new QTableWidgetItem(tr("nr. imagini")));
    if(brdDef!=nullptr){
        ui->brdPropertiesTbl->setItem(0,1,new QTableWidgetItem(brdDef->name));
        ui->brdPropertiesTbl->setItem(1,1,new QTableWidgetItem(brdDef->refClassifier.name));
        ui->brdPropertiesTbl->setItem(2,1,new QTableWidgetItem(QString::number(brdDef->refClassifier.minArea)));
        auto item=new QTableWidgetItem();
        item->setCheckState(brdDef->refClassifier.compDNNParams.enabled ? Qt::Checked : Qt::Unchecked);
        item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        ui->brdPropertiesTbl->setItem(3,1,item);
        item=new QTableWidgetItem();
        item->setCheckState(brdDef->refClassifier.compSpParams.enabled ? Qt::Checked : Qt::Unchecked);
        item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        ui->brdPropertiesTbl->setItem(4,1, item);
        item=new QTableWidgetItem();
        item->setCheckState((brdDef->refClassifier.colorValidator[0][0]!=0) ? Qt::Checked : Qt::Unchecked);
        item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        ui->brdPropertiesTbl->setItem(5,1,item);
        ui->brdPropertiesTbl->setItem(6,1,new QTableWidgetItem(brdDef->snValidator));
        ui->brdPropertiesTbl->setItem(7,1,new QTableWidgetItem(QString::number(brdDef->lights.count())));
        for(int i=0; i<brdDef->lights.count(); i++){
            ui->brdPropertiesTbl->setItem(8+i,0,new QTableWidgetItem(tr("lumini")+QString::number(i+1)));
            QString lights=tr("nemodificat");
            if(brdDef->lights[i].lightsEnabled)
                lights=QString::fromLatin1(brdDef->lights[i].lightData);
            ui->brdPropertiesTbl->setItem(8+i,1,new QTableWidgetItem(lights));
        }
    }

    //qDebug()<<ui->brdPropertiesTbl->verticalHeader()->height()<<ui->brdPropertiesTbl->verticalHeader()->width()<<ui->brdPropertiesTbl->verticalHeader()->length();
    ui->brdPropertiesTbl->resizeColumnsToContents();
    ui->brdPropertiesTbl->setFixedHeight(ui->brdPropertiesTbl->verticalHeader()->length()+4);
    ui->brdPropertiesTbl->setFixedWidth(ui->brdPropertiesTbl->horizontalHeader()->length()+4);

    compInfoModel.setContent(brdDef);
    ui->compPropTblView->setModel(&compInfoModel);
    ui->compPropTblView->resizeColumnsToContents();
    ui->compPropTblView->resizeRowsToContents();
    ui->compPropTblView->setFixedHeight(ui->compPropTblView->verticalHeader()->length()+ui->compPropTblView->horizontalHeader()->height()+4);
    ui->compPropTblView->setFixedWidth(ui->compPropTblView->horizontalHeader()->length()+4);

    //Adjust board image
    ui->graphicsView->setFrameStyle(0); //borderless
    ui->graphicsView->setFixedSize(compInfoModel.mainScene.bg.width()/4, compInfoModel.mainScene.bg.height()/4);
    ui->graphicsView->setTransform(QTransform::fromScale(0.25f, 0.25f));
    ui->graphicsView->setScene(&compInfoModel.mainScene);

    ui->horizontalSlider->setToolTip(QString::number(ui->horizontalSlider->value())+"%");

    //print
    QObject::connect(new QShortcut(QKeySequence::Print, this), &QShortcut::activated, this, &BrdInfo::printPreview);
    QObject::connect(ui->printButton, SIGNAL(clicked()), this, SLOT(printPreview()));
}

void BrdInfo::printPreview()
{
    //Because printPreviewDialog does not use a default printer, the output will be blank, so we check first
    QPrinterInfo defaultPrinter = QPrinterInfo::defaultPrinter();
    if (defaultPrinter.isNull() || defaultPrinter.printerName().isEmpty())
    {
        if (QPrinterInfo::availablePrinters().isEmpty())
            QMessageBox::critical(this, tr("Print error"), tr("Nu a fost gasită nici o imprimantă."), QMessageBox::Ok);
        else defaultPrinter = QPrinterInfo::availablePrinters().first();
    }

    printer = new QPrinter(defaultPrinter, QPrinter::HighResolution); //max 1200 DPI(size 9400 x 12400)
    printer->setPaperSize(QPrinter::PaperSize::A4);
    printer->setResolution(DPI); //1200
    printer->setPageMargins(10, 10, 10, 10, QPrinter::Millimeter);
    printer->setColorMode(QPrinter::Color);
    printer->setOrientation(QPrinter::Landscape);
    printer->setOutputFormat(QPrinter::PdfFormat);
    //printer->setFullPage(false);

    printPreviewDialog = new QPrintPreviewDialog(printer, this, Qt::Window);
    QObject::connect(printPreviewDialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(writeDocument(QPrinter*)));

    printPreviewDialog->exec();
}

void BrdInfo::writeDocument(QPrinter* printer)
{
    if (!printer)
        return;
    printPreviewDialog->setModal(true);

    //settings
    const qreal cellPadding = 5, cellSpacing = 0;
    QSizeF dscaleLargeImg(2, 2);
    QSizeF pageSize = printer->pageRect(QPrinter::DevicePixel).size(); //QPrinter::Point
    //QSizeF pageSize = printer->pageRect().size();
    const QFont defaultFont = QFont("Times New Roman", 10, QFont::Thin);
    const QSize imageSize = compInfoModel.mainScene.bg.size();

    //increase/decrease sceneSize
    if (imageSize.width() > 1920 && imageSize.height() > 1080)
        dscaleLargeImg = QSizeF(8, 8);

    QTextDocument* textDocument = new QTextDocument(printPreviewDialog);
    QTextCursor cursor(textDocument);

    //format configuration
    QTextCharFormat textFormat;
    QTextTableFormat tableFormat;
    QTextTableCellFormat cellFormat, headerCellFormat;
    QTextBlockFormat blockFormat, cellBlockFormat;

    blockFormat.setAlignment(Qt::AlignmentFlag::AlignCenter); //allignHcenter
    blockFormat.setPageBreakPolicy(QTextFormat::PageBreak_AlwaysAfter);

    //QTextDocument uses by default the screen DPI(~96) and the QPrinter's painter at HighResolution has 1200,
    //so it will affect the fonts and images.
    //Applying a QAbstractTextDocumentLayout that configures the QPrinter painting settings will do the trick.
    textDocument->documentLayout()->setPaintDevice(printer);
    textDocument->setUseDesignMetrics(true);
    textDocument->setPageSize(pageSize);
    textDocument->setDocumentMargin(5);

    textFormat.setFont(defaultFont);

    headerCellFormat.setPadding(2.5);
    headerCellFormat.setProperty(QTextFormat::BlockAlignment, Qt::AlignCenter);
    headerCellFormat.setFont(QFont("Times New Roman", 14, QFont::Bold));

    cellFormat.setPadding(2.5);
    cellFormat.setProperty(QTextFormat::BlockAlignment, Qt::AlignCenter);
    cellFormat.setFont(QFont("Times New Roman", 12, QFont::Thin));

    tableFormat.setBorder(6);
    tableFormat.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
    tableFormat.setBorderBrush(QBrush(QColor(Qt::black)));
    tableFormat.setAlignment(Qt::AlignLeft);
    tableFormat.setWidth(QTextLength(QTextLength::VariableLength, 100));
    tableFormat.setCellSpacing(cellSpacing);
    tableFormat.setCellPadding(cellPadding);

    /*----------BoardSettings Table--------*/
    cursor.insertBlock();
    cursor.movePosition(QTextCursor::Start);
    cursor.beginEditBlock();
    QTextTable* boardTable = cursor.insertTable(ui->brdPropertiesTbl->rowCount(), 2, tableFormat);
    for (int row = 0; row < boardTable->rows(); row++) {
        for (int col = 0; col < boardTable->columns(); col++) {
            QString txt = ui->brdPropertiesTbl->item(row, col)->text();
            if (txt.isEmpty())
            {
                if (Qt::Checked == ui->brdPropertiesTbl->item(row, col)->checkState())
                    txt = "true";
                else if (Qt::Unchecked == ui->brdPropertiesTbl->item(row, col)->checkState())
                    txt = "false";
            }
            boardTable->cellAt(row, col).firstCursorPosition().insertText(txt, textFormat);
            cursor.movePosition(QTextCursor::NextCell, QTextCursor::MoveAnchor, col);
        }
        cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, row);
    }
    cursor.endEditBlock();
    cursor.movePosition(QTextCursor::NextBlock);

    //spacing line
    cursor.insertBlock();
    cursor.beginEditBlock();
    cursor.insertText(" "); cursor.movePosition(QTextCursor::NextRow);
    cursor.endEditBlock(); cursor.movePosition(QTextCursor::NextBlock);

    /*--------------Scene Image------------*/
    //Insert Block
    cursor.insertBlock(blockFormat);
    cursor.movePosition(QTextCursor::StartOfBlock);
    cursor.beginEditBlock();
    //draw image
    QImage sceneRender = QImage(imageSize, QImage::Format_ARGB32_Premultiplied);
    QPainter painter(&sceneRender);
    painter.begin(printer);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
    compInfoModel.mainScene.render(&painter);
    painter.end();

    //add QTextImageFormat in QTextDocument resource cache to preserve the original resolution
    textDocument->addResource(QTextDocument::ImageResource, QUrl("sceneRender"), sceneRender);
    QTextImageFormat imgFormat;
    imgFormat.setName("sceneRender");
    imgFormat.setWidth(sceneRender.width() / dscaleLargeImg.width());
    imgFormat.setHeight(sceneRender.height() / dscaleLargeImg.height());

    cursor.insertImage(imgFormat);
    cursor.endEditBlock();
    cursor.movePosition(QTextCursor::EndOfBlock);
    cursor.movePosition(QTextCursor::NextBlock);

    /*----------Components Table----------*/
    // Insert Block
    cellBlockFormat.setAlignment(Qt::AlignCenter);
    cursor.insertBlock(blockFormat);
    cursor.beginEditBlock();
    tableFormat.setAlignment(Qt::AlignCenter);
    textFormat.setFont(QFont("Times New Roman", 12, QFont::Thin));
    tableFormat.setWidth(QTextLength(QTextLength::VariableLength, 160));
    QTextTable* componentsTable = cursor.insertTable(compInfoModel.rowCount() + 1, compInfoModel.columnCount(), tableFormat);

    //header
    for (int col = 0; col < componentsTable->columns(); col++) {
        QTextTableCell cell = componentsTable->cellAt(0, col);
        cell.setFormat(cellFormat);
        cell.firstCursorPosition().insertText(compInfoModel.headerData(col, Qt::Horizontal).toString(), headerCellFormat);
        cursor.movePosition(QTextCursor::NextCell, QTextCursor::MoveAnchor, col);
    }

    //cells
    for (int row = 1; row < componentsTable->rows(); row++) {
        for (int col = 0; col < componentsTable->columns(); col++) {
            QTextTableCell cell = componentsTable->cellAt(row, col);
            cell.setFormat(cellFormat);
            if (col != 9) {
                if (col == 4)
                    cell.firstCursorPosition().insertText(QString::number(compInfoModel.data(compInfoModel.index(row - 1, col)).toDouble()), cell.format());
                else
                    cell.firstCursorPosition().insertText(compInfoModel.data(compInfoModel.index(row - 1, col)).toString(), cell.format());
            }
            else {
                QImage sample = compInfoModel.data(compInfoModel.index(row - 1, col), Qt::DecorationRole).value<QImage>();
                if (!sample.isNull() && cell.isValid()) {

                    //enhance resolution
                    textDocument->addResource(QTextDocument::ImageResource, QUrl(QString::number(row)), sample);
                    QTextImageFormat cellImgFormat;
                    cellImgFormat.setName(QString::number(row));

                    //center image
                    QTextBlockFormat sampleFormat;
                    sampleFormat.setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                    cell.firstCursorPosition().setBlockFormat(sampleFormat);
                    cell.firstCursorPosition().insertImage(cellImgFormat);
                }
                cursor.movePosition(QTextCursor::NextCell, QTextCursor::MoveAnchor, col);
            }
        }
        cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, row);
    }
    cursor.endEditBlock();

    //document end
    cursor.movePosition(QTextCursor::End);
    textDocument->print(printer);
    delete textDocument;
}

void BrdInfo::on_horizontalSlider_valueChanged(int value)
{
    ui->graphicsView->setTransform(QTransform::fromScale(value/100.0,value/100.0));
    ui->horizontalSlider->setToolTip(QString::number(value)+"%");
}

BrdInfo::~BrdInfo()
{
    delete ui;
    delete printer;
    delete printPreviewDialog;
}

void CompInfoModel::setContent(BoardDef *boardDef)
{
    mainScene.setBackgroundImage(boardDef->refImagePath);
    compInfo.clear();
    if(nullptr==boardDef)
        return;
    QStringList classes=BoardCompType::componentTypeStrings();
    foreach (auto &cT, boardDef->compTypes) {
        QString compTypeName=cT.name;
        foreach (auto &comp, cT.components) {
            foreach (auto &c, cT.classifiers) {
                CompInfo info;
                info.comp=comp.name;
                info.compType=compTypeName;
                info.className=classes[cT.componentType];
                info.classifier=c.compDefPtr->name;
                info.threshold=c.compDefPtr->fhogThreshold;
                info.area=c.compDefPtr->minArea;
                info.dnn=c.compDefPtr->compDNNParams.enabled;
                info.sp=c.compDefPtr->compSpParams.enabled;
                info.colorValidator=(c.compDefPtr->colorValidator[0][0]!=0);
                //info.sample=QPixmap(":/icons/icons/46x48/icon_inspector.png");
                info.sample=c.compDefPtr->getPrimaryRect(false);
                if(info.sample.width()>info.sample.height()){
                    info.sample=info.sample.scaledToWidth(50, Qt::SmoothTransformation);
                } else {
                    info.sample=info.sample.scaledToHeight(50,Qt::SmoothTransformation);
                }
                qDebug()<<"pixmap"<<info.sample.height()<<info.sample.width();

                //save cell for print
                compInfo.append(info);

                //draw rect
                ComponentRectItem * item = new ComponentRectItem(const_cast<BoardComp&>(comp));
                mainScene.addItem(item);
            }
        }
    }
}
