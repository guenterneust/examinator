/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QComboBox>
#include "mscene.h"
#include <QImage>
#include <boarddef.h>
#include <QChart>
#include "filedeleter.h"
#include "regextester.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0, QWidget * testInterface = 0);
    ~Widget();

    /**
     * @brief load the available classifier list from the database, and populate the provided combobox with these classifier names
     * @param comboBox a pointer to the combobox to be populated with the classifier list
     * @param id item to be set as current item in the combobox
     * @param aspectRatio if positive, this parameter limits the classifiers to those having the aspectRatio indicated by this parameter
     * @return returns true if the item has been found, and false otherwise
     */
    bool loadCompList(QComboBox * comboBox, long long id=-1, double aspectRatio=-1);
    /**
     * @brief load the list of boards from the database into the provided combobox
     * @param comboBox pointer to the combobox to be populated with the board list from the database
     * @param selectedId database ID of the item currently selected
     */
    static void loadBoardList(QComboBox * comboBox, long long selectedId=-1);
    /**
     * @brief Method used to interactively try the classifier. It runs a classifier, or a grup of classifiers, on he entire image, for all 4 possible orientations
     * @param compDef pointer to the component definition used as classifier
     * @param image QImage to be analyzed
     * @param detected output list of detected components
     * @param scale
     * @param adjustThreshold threshold to be used to reject the detections
     */
    void runClassifierOnImage(CompDef *compDef, QImage & image, QList<DetectedComponent> &detected, double scale, double adjustThreshold);

    MScene compDefScene;
    FileDeleter fileDeleter;
private:
    class CompStatModel *compStatModel;
    Ui::Widget *ui;
    MScene brdDefScene;
    BoardDef * boardDef;
    QtCharts::QChart *compStatChart, *compGlobalStatChart;
    void changeCompImage(QString imgName);
    /**
     * @brief load the list of already defined components
     *
     * This function can be used only after createDatabase() has been executed, as it depends on the database.
     */
    void loadCompDefList(long long desiredId=-1);
    /**
     * @brief load the list of already defined boards
     *
     * This function can be used only after createDatabase() has been executed, as it depends on the database.
     */
    void loadBoardDefList(void);
    /**
     * @brief load a component definition from the database, and display it on the editor
     * @param id identifier from the database
     */
    void activateCompDef(long long id);
    /**
     * @brief load a board definition from the database, and display it on the editor
     * @param id identifier from the database
     */
    void activateBoardDef(long long id);
    void activateLoadedBoardDef(void);
    /**
     * @brief save current component definition if necessary, and clear the associated data structures
     */
    void clearCompDef(bool autosave=false);
    /**
     * @brief save current board definition if necessary, and clear the associated data structures
     */
    void clearBoardDef(void);
    void evalClassifierStats(const QModelIndex &idx);
    void showCompStats(const QModelIndex &idx);
    void addCompImage(const QString fileName);
    void showBoardWorker(long long boardId);
    ///recalculate component statistics for one image, show the image and the stats
    void compStatRecalc(const QModelIndex &idx);
    ///flag indicating that the editor is busy and should not respond to requests to change the current component being edited
    bool busy=false;
    QWidget * testInterface;
public slots:
    void addToClassifier(long long classifierId, const QString &imageName);
    void showBoard(long long boardId);
private slots:
    /** @brief function used to create a new rectangle by mouseclick
     *
     * this function is called by the event handler mousePressEvent(); using the mouse position and default parameters.
    */
    void addCompDefRect(double x, double y);
    void addBrdCompRect(double x, double y);
    void setScale(int scale);

    void on_scaleSpinBox_valueChanged(int arg1);
    void on_addCompImageBtn_clicked();
    /// slot used to update the controls and component image when the user selects or alters the rectangle
    void compRectChanged(ComponentRectItem *compRect, bool selected);
    void brdRectChanged(ComponentRectItem *compRect, bool selected);
    void applyCompSettings(void);
    void on_compOrientCombo_currentIndexChanged(int index);
    void on_compImageCombo_currentIndexChanged(int index);
    void on_deleteCompRect_clicked();
    void on_delCompImage_clicked();
    void on_compSaveBtn_clicked();
    void on_newCompBtn_clicked();
    void on_compNameCombo_currentIndexChanged(int index);
    void on_rectTypeCombo_currentIndexChanged(int index);
    void on_configBtn_clicked();
    void on_implicitNegativeCB_stateChanged(int arg1);
    void refreshCompDef();
    void on_modeTab_currentChanged(int index);
    void on_newBoardBtn_clicked();
    void on_brdSaveBtn_clicked();
    void on_refImgBtn_clicked();
    void on_brdCfgBtn_clicked();
    void on_delCompBtn_clicked();
    void on_brdNameCombo_currentIndexChanged(int index);
    void on_brdOrientCombo_currentIndexChanged(int index);
    void on_boardToolbox_currentChanged(int index);
    void on_brdNewCompTypeBtn_clicked();
    void on_brdTypeNameEdit_editingFinished();
    void on_brdCompTypeCombo_currentIndexChanged(int index);
    void on_brdCompTypeClassCombo_currentIndexChanged(int index);
    void on_brdCompAltBtn_clicked();
    void on_brdCompAltDelBtn_clicked();
    void on_brdCompTypeDelBtn_clicked();
    void on_brdDelBtn_clicked();
    void on_brdCompDelBtn_clicked();
    void on_brdCompOrientCombo_currentIndexChanged(int index);
    void on_brdCompNameEdit_editingFinished();
    void on_brdXTolSpin_valueChanged(double arg1);
    void on_brdYTolSpin_valueChanged(double arg1);
    void on_brdSizeTolSpin_valueChanged(double arg1);
    void on_brdMinAreaSpin_valueChanged(int arg1);
    void on_brdMaxAreaSpin_valueChanged(int arg1);
    void on_compToolBox_currentChanged(int index);
    void on_snValidatorEdit_editingFinished();
    void on_groupBox_clicked();
    void on_brdStartCombo_currentIndexChanged(int index);
    void on_brdDelaySpin_editingFinished();
    void on_newBoardBtn_triggered(QAction *action);
    void on_newCompBtn_triggered(QAction *arg1);
    void on_symbologyCombo_currentIndexChanged(int index);
    void compDefKeypressed(QKeyEvent *event);
    void brdDefKeypressed(QKeyEvent *event);
    void on_compStatImgView_doubleClicked(const QModelIndex &index);
    void on_compStatBtn_clicked();
    void on_compStatAllBtn_clicked();
    void on_modeTab_tabBarDoubleClicked(int index);
    void on_brdExtStartDisableCB_stateChanged(int arg1);

    /**
     * @brief setCurrentComboItemByUserdata sets the current item of the provided combobox to the first item which has the Userdata equal to the provided value
     * @param box pointer to already populated combobox
     * @param data value to be searched for
     */
    void setCurrentComboItemByUserdata(QComboBox * box, const QVariant data);
    /**
     * @brief populateParentCompCombo populate the parent component combobox with all the components defined on the current board. Only the components
     * which have a valid id are added to the combobox. Aditionally an "ignore" pseudocomponent is added as first item.
     */
    void populateParentCompCombo();
    /**
     * @brief populate ui->imagesCB
     */
    void populateBrdCompImagesCombo();

    void on_parentCompCombo_currentIndexChanged(int index);
    void on_brdCompImagesBtn_triggered(QAction *arg1);
    void on_brdCompAltShowBtn_clicked();
    void on_brdCompAltEdit_editingFinished();
    void on_brdCompAltCombo_currentIndexChanged(int index);
    void on_brdCompTypeAbsenceCB_stateChanged(int arg1);
    void on_includeDatasetCombo_currentIndexChanged(int index);
    void on_compAspectCombo_currentIndexChanged(int index);
    void on_brdCfgBtn_triggered(QAction *arg1);
    void on_snValidatorEdit_returnPressed();
    void on_compAngleSpin_valueChanged(int arg1);
    void on_brdAngleSpin_valueChanged(int arg1);
};

#endif // WIDGET_H
