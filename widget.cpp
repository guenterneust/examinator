/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QFileDialog>
#include <assert.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMenu>
#include <cmath>
#include <limits>

#include <QMessageBox>

#include "compdefdialog.h"
#include "boardcompaltdialog.h"
#include "barcode.h"
#include "compstatmodel.h"
#include "brdinfo.h"
#include "ocvhelper.h"
#include "boardconfig.h"

Widget::Widget(QWidget *parent, QWidget * testInterface) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    this->testInterface=testInterface;
    boardDef=nullptr;
    ui->setupUi(this);
    ui->modeTab->setCurrentIndex(0);
    ui->boardToolbox->setCurrentIndex(0);
    ui->compToolBox->setCurrentIndex(0);
    if(ui->modeTab->currentWidget()->objectName().compare("placa",Qt::CaseInsensitive)==0){
        ui->graphicsView->setScene(&brdDefScene);
    } else {
        ui->graphicsView->setScene(&compDefScene);
    }
    QMenu *menu = new QMenu();
    QAction *copyAction = new QAction(tr("copiază placă"), this);
    menu->addAction(copyAction);
    ui->newBoardBtn->setMenu(menu);

    menu = new QMenu();
    copyAction = new QAction(tr("copiază componenta"), this);
    menu->addAction(copyAction);
    ui->newCompBtn->setMenu(menu);

    menu = new QMenu();
    copyAction = new QAction(tr("componente testate"), this);
    copyAction->setObjectName("testedComps");
    menu->addAction(copyAction);
    copyAction = new QAction(tr("clasificator placă"), this);
    copyAction->setObjectName("brdClassifier");
    menu->addAction(copyAction);
    ui->brdCfgBtn->setMenu(menu);

    //ui->graphicsView->setStyleSheet("background-image:url(/home/guenter/source/cvdemo.dir/Front_of_Raspberry_Pi.jpg);background-repeat: no-repeat");
    //ui->graphicsView->setStyleSheet("background-image:/home/guenter/source/cvdemo.dir/Front_of_Raspberry_Pi.jpg");
    ui->graphicsView->scale(0.5,0.5);
    connect(ui->scaleSlider,SIGNAL(valueChanged(int)),this, SLOT(setScale(int)));
    setScale(ui->scaleSlider->value());
    ui->compAspectCombo->addItem("1:6",1.0/6);
    ui->compAspectCombo->addItem("1:4",1.0/4);
    ui->compAspectCombo->addItem("1:3",1.0/3);
    ui->compAspectCombo->addItem("1:2.5",1.0/2.5);
    ui->compAspectCombo->addItem("1:2",1.0/2);
    ui->compAspectCombo->addItem("2:3",2.0/3);
    ui->compAspectCombo->addItem("3:4",3.0/4);
    ui->compAspectCombo->addItem("1:1",1.0);
    ui->compAspectCombo->addItem("4:3",4.0/3);
    ui->compAspectCombo->addItem("3:2",3.0/2);
    ui->compAspectCombo->addItem("2:1",2.0/1);
    ui->compAspectCombo->addItem("2.5:1",2.5);
    ui->compAspectCombo->addItem("3:1",3.0);
    ui->compAspectCombo->addItem("4:1",4.0);
    ui->compAspectCombo->addItem("6:1",6.0);
    ui->compAspectCombo->setCurrentIndex(7);
    ui->orientationModeCombo->addItem(tr("indiferent"),CompDefRect::ANY);
    ui->orientationModeCombo->addItem(tr("N-S,E-V"),CompDefRect::H_V);
    ui->orientationModeCombo->addItem(tr("N,E,S,V"),CompDefRect::N_E_S_V);
    ui->orientationModeCombo->setCurrentIndex(2);
    ui->rectTypeCombo->clear();
    ui->rectTypeCombo->addItem(tr("Pozitiv"),CompDefRect::POSITIVE);
    ui->rectTypeCombo->addItem(tr("Negativ"),CompDefRect::NEGATIVE);
    ui->rectTypeCombo->addItem(tr("Ignorare"),CompDefRect::IGNORERECT);
    ui->rectTypeCombo->setCurrentIndex(0);
    //ordinea trebuie sa fie corecta, nu se face cautare in brdCompTypeClassCombo
    {
        QStringList types=BoardCompType::componentTypeStrings();
        for(int i=0; i<types.count(); i++){
#ifndef BARCODEREAD
            if(i==BoardCompType::BARCODE)
                continue;
#endif
            ui->brdCompTypeClassCombo->addItem(types[i], i);
        }

    }
    ui->brdCompOrientCombo->addItem("N",CompDefRect::N);
    ui->brdCompOrientCombo->addItem("E",CompDefRect::E);
    ui->brdCompOrientCombo->addItem("S",CompDefRect::S);
    ui->brdCompOrientCombo->addItem("V",CompDefRect::W);
    this->applyCompSettings();
    connect(&compDefScene,SIGNAL(rectChanged(ComponentRectItem*, bool)),this,SLOT(compRectChanged(ComponentRectItem*, bool)));
    connect(&brdDefScene,SIGNAL(rectChanged(ComponentRectItem*, bool)),this,SLOT(brdRectChanged(ComponentRectItem*, bool)));
    //connect(ui->compAspectCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(applyCompSettings()));
    connect(ui->orientationModeCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(applyCompSettings()));
    connect(&compDefScene,SIGNAL(middleMousePressed(double,double)),this,SLOT(addCompDefRect(double,double)));
    connect(&brdDefScene,SIGNAL(middleMousePressed(double,double)),this,SLOT(addBrdCompRect(double,double)));
    connect(&compDefScene,SIGNAL(sceneKeyEvent(QKeyEvent*)),this, SLOT(compDefKeypressed(QKeyEvent*)));
    connect(&brdDefScene,SIGNAL(sceneKeyEvent(QKeyEvent*)),this, SLOT(brdDefKeypressed(QKeyEvent*)));
    //createDatabase();
    compStatModel=new CompStatModel(this);
    loadCompDefList();
    loadBoardDefList();
#ifdef BARCODEREAD
    ui->symbologyCombo->clear();
    foreach (auto name, Barcode::bcTypes) {
       ui->symbologyCombo->addItem(name);
    }
    qDebug()<<Barcode::bcTypes;
#endif
    ui->compRGBWidget->setVisible(false);
    auto sizes=ui->splitter->sizes();
    sizes[1]=0;
    ui->splitter->setSizes(sizes);
    //ui->compStatImgView->setModel(ui->compImageCombo->model());
    ui->compStatImgView->setModel(compStatModel);
    ui->compStatImgView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    {
        compStatChart = new QChart();
        /*
        QLineSeries *series = new QLineSeries();
        series->append(0,1);
        series->append(1,3);
        series->append(2,3);
        series->append(3,2);
        series->append(4,4);
        series->setName("corect");
        QLineSeries *series2 = new QLineSeries();
        series2->append(0,11);
        series2->append(1,13);
        series2->append(2,13);
        series2->append(3,12);
        series2->append(4,14);
        series2->setName("orientare");
        compStatChart->addSeries(series);
        compStatChart->addSeries(series2);
        QLineSeries *series3 = new QLineSeries();
        series3->append(series2->points());
        compStatChart->removeAllSeries();
        series3->append(0,1.5);
        series3->append(1,3.5);
        series3->append(2,3.5);
        series3->append(3,2.5);
        series3->append(4,4.5);
        compStatChart->addSeries(series3);
        compStatChart->createDefaultAxes();
        */
        compStatChart->layout()->setContentsMargins(0, 0, 0, 0);
        compStatChart->setBackgroundRoundness(0);
        compStatChart->setMargins(QMargins(0,0,0,0));
        ui->compScoreChart->setChart(compStatChart);
        compGlobalStatChart = new QChart();
        compGlobalStatChart->layout()->setContentsMargins(0, 0, 0, 0);
        //compGlobalStatChart->setBackgroundRoundness(0);
        compGlobalStatChart->setMargins(QMargins(0,0,0,0));
        compGlobalStatChart->legend()->hide();
        ui->compGlobalScoreChart->setChart(compGlobalStatChart);
    }
    connect(&fileDeleter, SIGNAL(requestClassifier(long long, QString)), this, SLOT(addToClassifier(long long, QString)));
}

Widget::~Widget()
{
    qDebug()<<"widget destructor";
    delete ui;
}

void Widget::loadCompDefList(long long desiredId)
{
    ui->compNameCombo->blockSignals(true);
    loadCompList(ui->compNameCombo);
    if(ui->compNameCombo->count()>0){
        int idx=0;
        if(desiredId>=0){
            for(int i=0; i<ui->compNameCombo->count(); i++){
                long long data=ui->compNameCombo->itemData(i).toLongLong();
                //qDebug()<<"desired"<<desiredId<<"current"<<data;
                if(data==desiredId){
                    idx=i;
                    break;
                }
            }
        }
        //database is not empty, load a component
        ui->compNameCombo->setCurrentIndex(idx);
        long long id=ui->compNameCombo->itemData(ui->compNameCombo->currentIndex()).toLongLong();
        activateCompDef(id);
    } else {
        // add the new, empty component definition to the combobox
        ui->compNameCombo->addItem(compDefScene.compDef->name,-1);
    }
    ui->compNameCombo->blockSignals(false);
}

void Widget::loadBoardDefList()
{
    ui->brdNameCombo->blockSignals(true);
    loadBoardList(ui->brdNameCombo);
    if(ui->brdNameCombo->count()>0){
        //database is not empty, load the first component
        ui->brdNameCombo->setCurrentIndex(0);
        long long id=ui->brdNameCombo->itemData(ui->brdNameCombo->currentIndex()).toLongLong();
        activateBoardDef(id);
    } else {
        // add the new, empty board definition to the combobox
        if(boardDef==nullptr)
            boardDef=new BoardDef;
        ui->brdNameCombo->addItem(boardDef->name,-1);
    }
    ui->brdNameCombo->blockSignals(false);
}

void Widget::loadBoardList(QComboBox *comboBox, long long selectedId)
{
    comboBox->blockSignals(true);
    comboBox->clear();
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    if(!defaultdb.isOpen())
        qDebug()<<"database not open";
    QSqlQuery query;
    QString sql;
    sql="SELECT name, id FROM boards ORDER BY name";
    if(!query.exec(sql)){
        qDebug()<<"error"<<query.executedQuery()<<query.lastError();
        defaultdb.close();
        defaultdb.open();
        if(defaultdb.isOpen())
            query.exec(sql);
        else
            return;
    }
    long long idx=-1;
    while(query.next()){
        QString name=query.value(0/*"name"*/).toString();
        long long id=query.value(1/*"id"*/).toLongLong();
        comboBox->addItem(name, id);
        if(id==selectedId)
            idx=comboBox->count()-1;
    }
    if(idx>=0)
        comboBox->setCurrentIndex(idx);
    comboBox->blockSignals(false);

}

void Widget::runClassifierOnImage(CompDef *compDef, QImage &image, QList<DetectedComponent> &detected, double scale, double adjustThreshold)
{
    QVector<QImage> img(4);
    img[0]=image;
    {
        QTransform t2;
        QTransform t=t2.rotate(90);
        img[1]=img[0].transformed(t);
    }
    {
        QTransform t2;
        QTransform t=t2.rotate(180);
        img[2]=img[0].transformed(t);
    }
    {
        QTransform t2;
        QTransform t=t2.rotate(270);
        img[3]=img[0].transformed(t);
    }
    compDefScene.compDef->prepareToRun();
    detected=boardDef->detectComponents(img, compDef,scale, false, adjustThreshold);

}

bool Widget::loadCompList(QComboBox *comboBox, long long id, double aspectRatio)
{
    comboBox->blockSignals(true);
    comboBox->clear();
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    sql="SELECT name, id, aspectratio FROM compdefs ORDER BY name";
    query.exec(sql);
    int idx=-1;
    while(query.next()){
        QString name=query.value(0/*"name"*/).toString();
        long long itemid=query.value(1/*"id"*/).toLongLong();
        if(aspectRatio>0){
            double ratio=query.value("aspectratio").toDouble();
            if(std::abs(aspectRatio-ratio)>0.05){
                //wrong aspect ratio, forget about this item
                continue;
            }
        }
        if(itemid==id)
            idx=comboBox->count();
        comboBox->addItem(name, itemid);
    }
    if(idx>=0)
        comboBox->setCurrentIndex(idx);
    comboBox->blockSignals(false);
    return idx>=0;
}

void Widget::activateCompDef(long long id)
{
    qDebug()<<"activateCompDef"<<id;
    if(!compDefScene.compDef){
        compDefScene.compDef=new CompDef;
        //connect(compDefScene.compDef,SIGNAL(updated()),this,SLOT(refreshCompDef()));
    }
    compStatModel->prepareForUpdate();
    compDefScene.compDef->loadFromDB(id, true, true);
    compStatModel->updateFinished();
    {   //set aspect ratio combo
        ui->compAspectCombo->blockSignals(true);
        double bestfit=std::numeric_limits<double>::max();
        for(int i=0; i<ui->compAspectCombo->count(); i++){
            double fit=fabs(compDefScene.compDef->aspectRatio - ui->compAspectCombo->itemData(i).toDouble());
            if(fit<bestfit){
                bestfit=fit;
                ui->compAspectCombo->setCurrentIndex(i);
            }
        }
        ui->compAspectCombo->blockSignals(false);
    }
    {   //set orientation mode combo
        ui->orientationModeCombo->blockSignals(true);
        for(int i=0; i<ui->orientationModeCombo->count(); i++){
            if(compDefScene.compDef->orientationMode==ui->orientationModeCombo->itemData(i).toInt()){
                ui->orientationModeCombo->setCurrentIndex(i);
                break;
            }
        }
        ui->orientationModeCombo->blockSignals(false);
        //update all components by applying the current settings to them
        applyCompSettings();
    }
    int nImages=compDefScene.compDef->images.count();
    int nIncludedImages=compDefScene.compDef->includedImages.count();
    //qDebug()<<"activateCompDef"<<id<<nImages;
    if(nImages+nIncludedImages>0){
        //populate image combobox
        ui->compImageCombo->blockSignals(true);
        ui->compImageCombo->clear();
        for(int i=0; i<nImages; i++){
            QString fileName=compDefScene.compDef->images[i].imagePath;
            QFileInfo info(fileName);
            //qDebug()<<fileName;
            ui->compImageCombo->addItem(info.fileName(),fileName);
        }
        for(int i=0; i<nIncludedImages; i++){
            QString fileName=compDefScene.compDef->includedImages[i].imagePath;
            QFileInfo info(fileName);
            //qDebug()<<fileName;
            ui->compImageCombo->addItem(info.fileName(),fileName);
        }
        ui->compImageCombo->setCurrentIndex(0);
        ui->compImageCombo->blockSignals(false);
        on_compImageCombo_currentIndexChanged(0);
    }
    bool found=loadCompList(ui->includeDatasetCombo, compDefScene.compDef->getIncludeDataset(), compDefScene.compDef->aspectRatio);
    ui->includeDatasetCombo->blockSignals(true);
    ui->includeDatasetCombo->insertItem(0, tr("nu include"), -1);
    if(!found){
        ui->includeDatasetCombo->setCurrentIndex(0);
        compDefScene.compDef->setIncludeDataset(-1);
    }
    ui->includeDatasetCombo->blockSignals(false);
    this->setWindowTitle(tr("componenta: ")+compDefScene.compDef->name);
}

void Widget::activateBoardDef(long long id)
{
    clearBoardDef();
    if(boardDef==nullptr)
        boardDef=new BoardDef;
    boardDef->loadFromDB(id);
    this->setWindowTitle(tr("placa: ")+boardDef->name);
    activateLoadedBoardDef();
}

void Widget::activateLoadedBoardDef()
{
    brdDefScene.setBackgroundImage(boardDef->refImagePath);
    QFileInfo info(boardDef->refImagePath);
    ui->refImgLbl->setText(info.baseName());
    ui->refImgLbl->setToolTip(boardDef->refImagePath);
    if(0 >= boardDef->refRect.w){
        boardDef->refRect.w=150;
        boardDef->refRect.h=150;
    }
    ComponentRectItem *item=new ComponentRectItem(boardDef->refRect);
    //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
    brdDefScene.addItem(item);
    item->setAcceptedMouseButtons(Qt::LeftButton);
    item->setFlag(QGraphicsItem::ItemIsMovable);
    item->setFlag(QGraphicsItem::ItemIsSelectable);
    ui->brdOrientCombo->blockSignals(true);
    ui->brdOrientCombo->clear();
    ui->brdOrientCombo->addItem("N",CompDefRect::N);
    ui->brdOrientCombo->addItem("E",CompDefRect::E);
    ui->brdOrientCombo->addItem("S",CompDefRect::S);
    ui->brdOrientCombo->addItem("V",CompDefRect::W);

    int idx;
    switch(boardDef->refRect.orientation){
    case CompDefRect::N:
    default:
        idx=ui->brdOrientCombo->findText("N");
        ui->brdOrientCombo->setCurrentIndex(idx);
        break;
    case CompDefRect::E:
        idx=ui->brdOrientCombo->findText("E");
        ui->brdOrientCombo->setCurrentIndex(idx);
        break;
    case CompDefRect::S:
        idx=ui->brdOrientCombo->findText("S");
        ui->brdOrientCombo->setCurrentIndex(idx);
        break;
    case CompDefRect::W:
        idx=ui->brdOrientCombo->findText("V");
        ui->brdOrientCombo->setCurrentIndex(idx);
        break;
    }
    ui->brdOrientCombo->blockSignals(false);
    //setValue sems to trigger valueChanged at least for some daabases?!?
    ui->brdAngleSpin->blockSignals(true);
    ui->brdAngleSpin->setValue(boardDef->refRect.angle*1000);
    ui->brdAngleSpin->blockSignals(false);
    ui->snValidatorEdit->setText(boardDef->snValidator);
    boardDef->refRect.applySettings(boardDef->refRect.orientation,CompDefRect::N_E_S_V,
                                    boardDef->refClassifier.aspectRatio,
                                    boardDef->refRect.rectangleType);
    int minArea=boardDef->minArea;
    if(boardDef->refClassifier.minArea>minArea)
        minArea=boardDef->refClassifier.minArea;
    ui->brdMinAreaSpin->setValue(minArea);
    ui->brdMaxAreaSpin->setValue(std::max( boardDef->maxArea, minArea) );
    ui->brdCompTypeCombo->blockSignals(true);
    ui->brdCompTypeCombo->clear();
    for(int i=0; i<boardDef->compTypes.count(); i++){
        ui->brdCompTypeCombo->addItem(boardDef->compTypes[i].name,
                                      boardDef->compTypes[i].id);
    }
    ui->brdCompTypeCombo->blockSignals(false);
    if(boardDef->compTypes.count()>0){
        on_brdCompTypeCombo_currentIndexChanged(0);
    } else {
        ui->brdTypeNameEdit->clear();
        ui->brdCompTypeClassCombo->setCurrentIndex(BoardCompType::COMPONENT);
        ui->brdCompAltCombo->clear();
    }
    //enable/disable component selection by mouse
    on_boardToolbox_currentChanged(ui->boardToolbox->currentIndex());
    //set start method
    ui->brdStartCombo->blockSignals(true);
    ui->brdStartCombo->setCurrentIndex(boardDef->testStartType);
    ui->brdDelaySpin->setValue(boardDef->testStartDelay);
    ui->brdExtStartDisableCB->setChecked(boardDef->testStartExtDisabled);
    ui->brdStartCombo->blockSignals(false);
}

void Widget::clearCompDef(bool autosave)
{
    ui->compImgLabel->clear();
    ui->compRGBWidget->setVisible(false);
    if(compDefScene.compDef){
        if(compDefScene.compDef->dirty){
            int btn;
            if(autosave){
                btn=QMessageBox::Yes;
                QSqlDatabase db=QSqlDatabase::database();
                unsigned lastsaved=compDefScene.compDef->getLastSaved(db);
                if(lastsaved>compDefScene.compDef->lastsave){
                    btn=QMessageBox::warning(this,
                                             tr("componentă modificată"),
                                             tr("componenta încărcată e de la ")+
                                             QDateTime::fromSecsSinceEpoch(compDefScene.compDef->lastsave).toString("HH:mm:ss dd/MM/yyyy")+
                                             tr(" dar cea din baza de date e de la ")+
                                             QDateTime::fromSecsSinceEpoch(lastsaved).toString("HH:mm:ss dd/MM/yyyy")+
                                             tr("\nDoriți să continuați salvarea?"),
                                             QMessageBox::Yes,QMessageBox::No
                                             );
                }
            } else {
                btn=QMessageBox::warning(this, tr("avertisment salvare definitie"),
                                         tr("Definiția a fost modificată. Doriți salvarea acesteia?"),
                                         QMessageBox::Yes,QMessageBox::No);
            }
            if(btn==QMessageBox::Yes){
                compDefScene.compDef->saveToDB();
            }
        }
        compDefScene.clear();
        delete compDefScene.compDef;
        compDefScene.compDef=NULL;
        ui->compImageCombo->blockSignals(true);
        ui->compImageCombo->clear();
        ui->compImageCombo->blockSignals(false);
    }

}

void Widget::clearBoardDef()
{
    ui->refImgLbl->clear();
    if(boardDef){
        if(boardDef->dirty){
            int btn;
            btn=QMessageBox::warning(this, tr("avertisment salvare definitie"),
                                     tr("Definitia a fost modificata. Doriti salvarea acesteia?"),
                                     QMessageBox::Yes,QMessageBox::No);
            if(btn==QMessageBox::Yes){
                boardDef->saveToDB();
            }
        }
        brdDefScene.clear();
        delete boardDef;
        boardDef=NULL;
        ui->brdCompTypeCombo->blockSignals(true);
        ui->brdCompTypeCombo->clear();
        ui->brdCompTypeCombo->blockSignals(false);
    }
}

void Widget::evalClassifierStats(const QModelIndex &idx)
{
    compStatModel->stats[idx.row()]=CompStat();
    //run classifier on image and calculate stats
    QImage image;
    image.load(compStatModel->data(idx).toString());
    if(image.isNull()){
        qDebug()<<"unable to load image"<<compStatModel->data(idx).toString();
        return;
    }
    double scale=1;
    if(compDefScene.compDef->upsample>=1){
        image=image.scaled(image.width()*2,image.height()*2);
        scale=0.5;
    }
    try{
        auto & lst=compStatModel->stats[idx.row()].detections;
        /* lower the threshold by -1 in order to be able to see how the classifier behaves also below the threshold */
        runClassifierOnImage(compDefScene.compDef, image, lst, scale, -1);
        compStatModel->processImageDetections(compDefScene.compDef, idx);
        compStatModel->setValid(idx.row(),true);
    } catch(...){
        qDebug()<<"exception while processing"<<idx.row();
    }
}

void Widget::showCompStats(const QModelIndex &idx)
{
    if(!idx.isValid())
        return;
    ui->imgCorrectLbl->setNum(compStatModel->stats[idx.row()].values.correct);
    ui->imgOrientLbl->setNum(compStatModel->stats[idx.row()].values.orientation);
    ui->imgExtraLbl->setNum(compStatModel->stats[idx.row()].values.extra);
    ui->imgMissingLbl->setNum(compStatModel->stats[idx.row()].values.missing);
    ui->totalCorrectLbl->setNum(compStatModel->globalStat.values.correct);
    ui->totalOrientLbl->setNum(compStatModel->globalStat.values.orientation);
    ui->totalExtraLbl->setNum(compStatModel->globalStat.values.extra);
    ui->totalMissingLbl->setNum(compStatModel->globalStat.values.missing);
}

void Widget::addCompImage(const QString fileName)
{
    if(compDefScene.compDef->usesImage(fileName)){
        QMessageBox::information(this,"info", tr("imaginea ")+fileName+tr(" este deja folosită"));
        return;
    }
    if(compDefScene.compDefRect){
        compDefScene.compDefRect->updateCompDefRect();
    }
    if(!compDefScene.setBackgroundImage(fileName)){
        qDebug()<<"unable to load"<<fileName;
        return;
    }
    compStatModel->prepareForUpdate();
    compDefScene.compDefImage=compDefScene.compDef->addImage(fileName);
    QFileInfo info(fileName);
    ui->compImageCombo->blockSignals(true);
    int nImages=compDefScene.compDef->images.count()-1;
    ui->compImageCombo->insertItem(nImages, info.fileName(),fileName);
    ui->compImageCombo->setCurrentIndex(nImages);
    ui->compImageCombo->setToolTip(fileName);
    ui->compImageCombo->blockSignals(false);
    compStatModel->updateFinished();
    ui->implicitNegativeCB->setChecked(compDefScene.compDefImage->implicitNegative);
    compDefScene.compDef->dirty=true;
    compDefScene.compDefRect=nullptr;

}

void Widget::showBoardWorker(long long boardId)
{
    clearBoardDef(/*ui->brdNameCombo->currentIndex()==index*/);
    loadBoardList(ui->brdNameCombo, boardId);
    activateBoardDef(boardId);
}

void Widget::compStatRecalc(const QModelIndex &idx)
{
    //clear the rectangles from the scene before updating the detection list, because the rectangles have pointers to the old list!
    if(ui->compImageCombo->currentIndex()==idx.row()){
        compDefScene.clearDetectedRects();
    } else {
        ui->compImageCombo->setCurrentIndex(idx.row());
    }
    evalClassifierStats(idx);
    compStatModel->calculateGlobalStat();
    compStatModel->statToChart(compStatModel->stats[idx.row()], *compStatChart, compDefScene.compDef->fhogThreshold);
    compStatModel->statToChart(compStatModel->globalStat, *compGlobalStatChart, compDefScene.compDef->fhogThreshold);
    showCompStats(idx);
    compDefScene.showDetections(compStatModel->stats[idx.row()].detections, compDefScene.compDef->fhogThreshold);
}

void Widget::addToClassifier(long long classifierId, const QString &imageName)
{
    if(busy){
        qDebug()<<"editor busy -> not adding image to classifier";
        return;
    }
    //qDebug()<<"addToClassifeir";
    if(this->isHidden())
        this->show();
    this->setWindowState((this->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    this->raise();
    //this->activateWindow();
    if(ui->modeTab->currentIndex()!=1)
        ui->modeTab->setCurrentIndex(1);
    int i;
    bool found=false;
    for(i=0; i<ui->compNameCombo->count(); i++){
        long long id=ui->compNameCombo->itemData(i).toLongLong();
        if(id==classifierId){
            found=true;
            break;
        }
    }
    if(!found){
        //reload the component list, maybe it's a new component
        loadCompDefList(classifierId);
        if(ui->compNameCombo->count()>0){
            long long id=ui->compNameCombo->itemData(i).toLongLong();
            if(id==classifierId){
                found=true;
            }
        }
        if(!found)
            return;
    } else {
        ui->compNameCombo->setCurrentIndex(i);
    }
    if(imageName.isEmpty())
        return;
    if(ui->compToolBox->currentIndex()!=1)
        ui->compToolBox->setCurrentIndex(1);
    if(QFile::exists(imageName))
        addCompImage(imageName);

}

void Widget::showBoard(long long boardId)
{
    if(this->isHidden())
        this->show();
    this->setWindowState((this->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    this->raise();
    //this->activateWindow();
    if(ui->modeTab->currentIndex()!=0)
        ui->modeTab->setCurrentIndex(0);
    if(ui->boardToolbox->currentIndex()!=0)
        ui->boardToolbox->setCurrentIndex(0);
    showBoardWorker(boardId);
}

/// Add new rect to the list of rectangles for the current image, and return a reference to it
void Widget::addCompDefRect(double x, double y)
{
    double w=sqrt(compDefScene.compDef->minArea*compDefScene.compDef->aspectRatio);
    if(compDefScene.compDef->upsample>0){
        w/=2;
        if(compDefScene.compDef->upsample>1){
            w/=2;
        }
    }
    double h=w;
    bool enforceAspectRatio=false;
    if(compDefScene.compDef->recentRect.rectangleType==CompDefRect::NEGATIVE)
        enforceAspectRatio=false;
    if(compDefScene.compDef->recentRect.rectangleType==CompDefRect::IGNORERECT)
        enforceAspectRatio=false;
    if((w<compDefScene.compDef->recentRect.w)||(!enforceAspectRatio)){
        //last used width is larger than minimum width, so keep the same rectangle size
        w=compDefScene.compDef->recentRect.w;
        h=compDefScene.compDef->recentRect.h;
        if((w>compDefScene.bg.width())&&enforceAspectRatio){
            w=compDefScene.bg.width();
            if(compDefScene.compDef->aspectRatio>0){
                h=w/compDefScene.compDef->aspectRatio;
            }
        }
    } else {
        // last used width is smaller than minimum width
        if(compDefScene.compDef->aspectRatio>0){
            h=w/compDefScene.compDef->aspectRatio;
        }
    }
    if(w<=0)
        w=sqrt(compDefScene.compDef->minArea*compDefScene.compDef->aspectRatio);
    if(h<0)
        h=w/compDefScene.compDef->aspectRatio;
    CompDefRect * rect=
            compDefScene.compDefImage->addNewRect(x, y, w, h,
                                                  compDefScene.compDef->recentRect.orientation);
    if(rect){
        rect->applySettings(rect->orientation,
                            compDefScene.compDef->orientationMode,
                            compDefScene.compDef->aspectRatio,
                            compDefScene.compDef->recentRect.rectangleType);
        ComponentRectItem *item=new ComponentRectItem(*rect);
        //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
        compDefScene.addItem(item);
        item->setAcceptedMouseButtons(Qt::LeftButton);
        item->setFlag(QGraphicsItem::ItemIsMovable);
        item->setFlag(QGraphicsItem::ItemIsSelectable);
        //item->setFlag(QGraphicsItem::ItemSendsGeometryChanges);//nu merge!!!
    }
    compDefScene.compDef->dirty=true;

}

void Widget::addBrdCompRect(double x, double y)
{
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    if(boardDef->compType->classifiers.count()<=0){
        qDebug()<<"boardDef->compType->classifiers.count()<=0";
        return;
    }
    if(ui->boardToolbox->currentIndex()!=3){
        qDebug()<<"ui->boardToolbox->currentIndex()!=3";
        return;
    }
    double ratio=boardDef->compType->aspectRatio();
    if(ratio<0)
        ratio=1;
    double w=sqrt(boardDef->compType->minArea()*ratio);
    double h=w/ratio;
    if(w>brdDefScene.compDef->recentRect.w){
        brdDefScene.compDef->recentRect.w=w;
        brdDefScene.compDef->recentRect.h=h;
    }
    BoardComp * comp = boardDef->compType->addNewComp(x, y, boardDef->recentComp);
    boardDef->dirty=true;
    if(comp){
        if(comp->w * comp->h < boardDef->compType->minArea()){
            comp->w=w;
            comp->h=h;
        }
        comp->applySettings(comp->orientation,
                            CompDefRect::N_E_S_V,
                            boardDef->compType->aspectRatio(),
                            CompDefRect::BOARDCOMP);
        ComponentRectItem *item=new ComponentRectItem(*comp);
        //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
        brdDefScene.addItem(item);
        item->setAcceptedMouseButtons(Qt::LeftButton);
        item->setFlag(QGraphicsItem::ItemIsMovable);
        item->setFlag(QGraphicsItem::ItemIsSelectable);
        //item->setFlag(QGraphicsItem::ItemSendsGeometryChanges);//nu merge!!!
    }
}

void Widget::applyCompSettings(void)
{
    bool symmetrical=true;
    if((ui->compAspectCombo->currentData().toDouble()>1.01)||
            (ui->compAspectCombo->currentData().toDouble()<0.99)){
        symmetrical=false;
    }
    compDefScene.compDef->orientationMode=static_cast<CompDefRect::OrientationMode>(ui->orientationModeCombo->itemData(ui->orientationModeCombo->currentIndex()).toInt());
    //display available orientations in combo box
    ui->compOrientCombo->blockSignals(true);
    ui->compOrientCombo->clear();
    //switch(ui->orientationModeCombo->currentData().toInt()){//nu merge inainte de QT 5.2
    switch(ui->orientationModeCombo->itemData(ui->orientationModeCombo->currentIndex()).toInt()){
    /* all rectangles can have any of the 4 orientations, in order to make it easyer to create good classifiers
    case CompDefRect::ANY:
        if(symmetrical)
            break;
        //else enforce nonsymmetrical orientation mode and fall trough
        ui->orientationModeCombo->setCurrentIndex(1);
        [[fallthrough]];
    case CompDefRect::H_V:
        ui->compOrientCombo->addItem("N-S",CompDefRect::NS);
        ui->compOrientCombo->addItem("E-V",CompDefRect::EW);
        break;
    */
    default:
    case CompDefRect::N_E_S_V:
        ui->compOrientCombo->addItem("N",CompDefRect::N);
        ui->compOrientCombo->addItem("E",CompDefRect::E);
        ui->compOrientCombo->addItem("S",CompDefRect::S);
        ui->compOrientCombo->addItem("V",CompDefRect::W);
        break;
    }
    ui->compOrientCombo->blockSignals(false);
    //s.compDef->aspectRatio=ui->compAspectCombo->currentData().toReal();
    compDefScene.compDef->aspectRatio=ui->compAspectCombo->itemData(ui->compAspectCombo->currentIndex()).toReal();
    for(int i=0; i<compDefScene.compDef->images.count(); i++){
        for(int r=0; r<compDefScene.compDef->images[i].rect.count(); r++){
            CompDefRect & compRect=compDefScene.compDef->images[i].rect[r];
            compRect.applySettings(compRect.orientation,
                                   compDefScene.compDef->orientationMode,compDefScene.compDef->aspectRatio,
                                   compRect.rectangleType);
        }
    }
    //update items in current image
    foreach(QGraphicsItem * item, compDefScene.items()){
        ComponentRectItem * comp = dynamic_cast<ComponentRectItem *>(item);
        if(!comp)
            continue;
        //qDebug()<<comp->type();
        /*
        comp->compDefRect->applySettings(comp->compDefRect->orientation,
                                         s.compDef->orientationMode,s.compDef->aspectRatio,
                                         comp->compDefRect->rectangleType);
*/
        comp->updateFromCompDef();
    }

}

void Widget::setScale(int scale){
    //qDebug()<<scale;
    ui->scaleSpinBox->setValue(scale);
    ui->graphicsView->setTransform(QTransform::fromScale(scale/100.0,scale/100.0));
    MScene * s=dynamic_cast<MScene *>( ui->graphicsView->scene() );
    if(s)
        s->scale=scale/100.0;
}

void Widget::on_scaleSpinBox_valueChanged(int arg1)
{
    ui->scaleSlider->setValue(arg1);
}

void Widget::on_addCompImageBtn_clicked()
{
    qDebug()<<"add clicked";
    /*static*/ QString fileName;
    QString name = QFileDialog::getOpenFileName(this,
                                            tr("Imagine"),
                                            fileName,
                                            tr("Fisiere Imagine(*.png *.jpg *.bmp *.jpeg);;Toate fișierele(*.*)"));
    if(name.isEmpty())
        return;
    fileName=name;
    qDebug()<<"addCompImageBtn"<<fileName;
    addCompImage(fileName);
}

void Widget::compRectChanged(ComponentRectItem *compRect, bool selected)
{
    if((!compDefScene.bg)||(!compRect)||(!compRect->compDefRect)||(!selected)){
        ui->compImgLabel->clear();
        ui->compRGBWidget->setVisible(false);
        compDefScene.compDef->recentRect=*(compRect->compDefRect);
        return;
    }

    int idx=ui->compOrientCombo->findData(compRect->compDefRect->orientation);
    ui->compOrientCombo->setCurrentIndex(idx);
    ui->compAngleSpin->setValue(compRect->compDefRect->angle*1000);
    idx=ui->rectTypeCombo->findData(compRect->compDefRect->rectangleType);
    ui->rectTypeCombo->setCurrentIndex(idx);
    //QPixmap compPixmap=compDefScene.getCompChip(compRect->compDefRect);
    QPixmap compPixmap=CompDef::getCompChip(compDefScene.bg, compRect->compDefRect);
#if 0
    double w,h;
    if((compRect->compDefRect->orientation==CompDefRect::E)||
            (compRect->compDefRect->orientation==CompDefRect::W)||
            (compRect->compDefRect->orientation==CompDefRect::EW)){
        w=compRect->compDefRect->h;
        h=compRect->compDefRect->w;
    } else {
        w=compRect->compDefRect->w;
        h=compRect->compDefRect->h;
    }
    //qDebug()<<"copy from"<<compRect->compDefRect->x<<compRect->compDefRect->y<<w<<h;
    QPixmap compPixmap=compDefScene.bg.copy(compRect->compDefRect->x,compRect->compDefRect->y, w, h);
    int rotation=0;
    switch(compRect->compDefRect->orientation){
    case CompDefRect::E:
    case CompDefRect::EW:
        rotation=270;
        break;
    case CompDefRect::S:
        rotation=180;
        break;
    case CompDefRect::W:
        rotation=90;
        break;
    default:
        break;
    }
    if(rotation){
        QTransform t2;
        QTransform t=t2.rotate(rotation);

        //qDebug()<<"orientation"<<compRect->compDefRect->orientation<<"rotation"<<rotation<<t;
        ui->compImgLabel->setPixmap(compPixmap.transformed(t).scaledToWidth(124));
    } else {
        //qDebug()<<"orientation"<<compRect->compDefRect->orientation<<"rotation"<<rotation;
        ui->compImgLabel->setPixmap(compPixmap.scaledToWidth(124));
    }
#endif
    ui->compImgLabel->setPixmap(compPixmap.scaledToWidth(124));
    compDefScene.compDef->recentRect=*(compRect->compDefRect);
    if((compDefScene.compDef->colorValidator[0][0]!=0)&&(compRect->compDefRect->rectangleType==CompDefRect::POSITIVE)){
        DetectedComponent dc;
        QImage img=compPixmap.toImage();
        bool ok=compDefScene.compDef->validateColor(img, dc);
        if(!ok)
            qDebug()<<"validator failed"<<dc.red<<dc.green<<dc.blue<<dc.hue<<dc.sat<<dc.val;
        ui->compRGBWidget->setVisible(true);
        ui->compRGBWidget->setStyleSheet("background-color:rgb("+QString::number(dc.red)+","+QString::number(dc.green)+","+QString::number(dc.blue)+");");
        ui->compRLbl->setText(QString::number(dc.red,'f',1)+" ("+QString::number(compDefScene.compDef->colorValidator[1][0])+","+QString::number(compDefScene.compDef->colorValidator[1][1])+")" );
        ui->compGLbl->setText(QString::number(dc.green,'f',1)+" ("+QString::number(compDefScene.compDef->colorValidator[2][0])+","+QString::number(compDefScene.compDef->colorValidator[2][1])+")" );
        ui->compBLbl->setText(QString::number(dc.blue,'f',1)+" ("+QString::number(compDefScene.compDef->colorValidator[3][0])+","+QString::number(compDefScene.compDef->colorValidator[3][1])+")" );
        ui->compHLbl->setText(QString::number(dc.hue,'f',3)+" ("+QString::number(compDefScene.compDef->colorValidator[4][0],'f',3)+","+QString::number(compDefScene.compDef->colorValidator[4][1],'f',3)+")" );
        ui->compSLbl->setText(QString::number(dc.sat,'f',3)+" ("+QString::number(compDefScene.compDef->colorValidator[5][0],'f',3)+","+QString::number(compDefScene.compDef->colorValidator[5][1],'f',3)+")" );
        ui->compVLbl->setText(QString::number(dc.val,'f',3)+" ("+QString::number(compDefScene.compDef->colorValidator[6][0],'f',3)+","+QString::number(compDefScene.compDef->colorValidator[6][1],'f',3)+")" );
    }
}

void Widget::brdRectChanged(ComponentRectItem *compRect, bool selected)
{
    if((!compRect)||(!compRect->compDefRect)||(!selected)){
        //ui->compImgLabel->clear();
        if(!((!compRect)||
             (!compRect->compDefRect)||
             (compRect->compDefRect->rectangleType!=CompDefRect::BOARDCOMP))){
            BoardComp * comp=static_cast<BoardComp *>(compRect->compDefRect);
            boardDef->recentComp=*(comp);
        }
        //brdDefScene.compDef->recentRect=*(compRect->compDefRect);
        return;
    }
    if(compRect->compDefRect->rectangleType==CompDefRect::BOARDRECT){
        long area=compRect->compDefRect->w*compRect->compDefRect->h;
        ui->markedAreaLbl->setText(QString::number(area));
        ui->brdAngleSpin->setValue(boardDef->refRect.angle*1000);
    }
    if(compRect->compDefRect->rectangleType!=CompDefRect::BOARDCOMP)
        return;
    /// @todo unselect all other rectangles
    BoardComp * comp=static_cast<BoardComp *>(compRect->compDefRect);
    boardDef->recentComp=*(comp);
    ui->brdCompNameEdit->setText(comp->name);
    ui->brdXTolSpin->setValue(comp->xTol);
    ui->brdYTolSpin->setValue(comp->yTol);
    ui->brdSizeTolSpin->setValue(comp->sizeTol);
    int idx=ui->brdCompOrientCombo->findData(comp->orientation);
    ui->brdCompOrientCombo->setCurrentIndex(idx);
    ui->symbologyCombo->setCurrentIndex(comp->symbology);
    setCurrentComboItemByUserdata(ui->parentCompCombo, comp->parentComponent);
    auto lst=ui->brdCompImagesBtn->menu()->actions();
    //qDebug()<<"set images to"<<comp->images.to_ulong()<<QString::fromStdString(comp->images.to_string());
    QString txt;
    for(int i=0; i<boardDef->lights.count(); i++){
        if(comp->images[i])
            txt.append("1");
        else
            txt.append("0");
        if(lst.count()>i)
            lst[i]->setChecked( comp->images[i] );
        else
            qDebug()<<"wrong number of images in actions list"<<"i"<<i<<"lighs"<<boardDef->lights.count()<<"actions"<<lst.count();
    }
    ui->brdCompImagesBtn->setText(txt);
}

void Widget::on_compOrientCombo_currentIndexChanged(int index)
{

    qDebug()<<"comp orient changed?"<<index<<
              ui->compOrientCombo->itemData(ui->compOrientCombo->currentIndex())<<ui->compOrientCombo->currentIndex();
    if(!compDefScene.compDefRect)
        return;
    /// @todo - actualizat modificarile inainte de rotire
    compDefScene.compDefRect->updateCompDefRect();
    CompDefRect::Orientation orientation=
            static_cast<CompDefRect::Orientation>(ui->compOrientCombo->itemData(
                                                         ui->compOrientCombo->currentIndex()).toInt());
    qDebug()<<"comp orient changed"<<orientation;
    compDefScene.compDefRect->compDefRect->applySettings(orientation,compDefScene.compDef->orientationMode,compDefScene.compDef->aspectRatio,
                                                         compDefScene.compDefRect->compDefRect->rectangleType);
    qDebug()<<"updateFromCompDef";
    compDefScene.compDefRect->updateFromCompDef();
    this->compRectChanged(compDefScene.compDefRect,true);
}


void Widget::on_compImageCombo_currentIndexChanged(int index)
{
    if(compDefScene.compDefRect){
        qDebug()<<"on_compImageCombo_currentIndexChanged";
        compDefScene.compDefRect->updateCompDefRect();
    }
    compDefScene.changeImage(index);
    ui->implicitNegativeCB->setChecked(compDefScene.compDefImage->implicitNegative);
    ui->compImgLabel->clear();
    ui->compRGBWidget->setVisible(false);
    ui->compImageCombo->setToolTip(compDefScene.compDefImage->imagePath);
}

void Widget::on_deleteCompRect_clicked()
{
    if(!compDefScene.compDefRect)
        return;
    assert(compDefScene.compDefImage);
    CompDefRect * r=compDefScene.compDefRect->compDefRect;
    assert(r);
    for(int i=0; i<compDefScene.compDefImage->rect.count(); i++){
        if(&(compDefScene.compDefImage->rect[i])==r){
            compDefScene.compDefImage->rect.removeAt(i);
            compDefScene.removeItem(compDefScene.compDefRect);
            delete(compDefScene.compDefRect);
            compDefScene.compDefRect=NULL;
            ui->compImgLabel->clear();
            ui->compRGBWidget->setVisible(false);
            return;
        }
    }
}

void Widget::on_delCompImage_clicked()
{
    if(!compDefScene.compDefImage)
        return;
    int result=QMessageBox::question(this, "Stergere placa", "Sunteti sigur ca doriti sa stergeti imaginea?");
    if(result!=QMessageBox::Yes)
        return;
    assert(compDefScene.compDef);
    CompDefImage * imgPtr = compDefScene.compDefImage;
    QString img=compDefScene.compDefImage->imagePath;
    //for(int i=0; i<ui->compImageCombo->count(); i++){
    //only search in images for current classifier
    for(int i=0; i<compDefScene.compDef->images.count(); i++){
        if(ui->compImageCombo->itemData(i).toString()==img){
            //found combobox item
            compDefScene.compDef->dirty=true;
            compDefScene.compDefRect=NULL;
            ui->compImgLabel->clear();
            ui->compRGBWidget->setVisible(false);
            //update graphics scene
            if(i+1<ui->compImageCombo->count()){
                //not the last image, make the next image the current image
                on_compImageCombo_currentIndexChanged(i+1);
            } else if(i>0){
                on_compImageCombo_currentIndexChanged(0);
            } else {
                compDefScene.clear();
                compDefScene.compDefImage=NULL;
            }
            //remove combobox item

            ui->compImageCombo->blockSignals(true);
            ui->compImageCombo->removeItem(i);
            ui->compImageCombo->blockSignals(false);
            //remove data from memory
            for(int j=0; j<compDefScene.compDef->images.count(); j++){
                if(&(compDefScene.compDef->images[j])==imgPtr){
                    qDebug()<<"removing image"<<compDefScene.compDef->images[j].imagePath;
                    compStatModel->prepareForUpdate();
                    compDefScene.compDef->images.removeAt(j);
                    compStatModel->updateFinished();
                    return;
                }
            }
            qDebug()<<"can't find image";
            return;
        }
    }
    qDebug()<<"Can't find image";
}

void Widget::on_compSaveBtn_clicked()
{
    if(compDefScene.compDefRect){
        compDefScene.compDefRect->updateCompDefRect();
    }
    QSqlDatabase db=QSqlDatabase::database();
    unsigned lastsaved=compDefScene.compDef->getLastSaved(db);
    if(lastsaved>compDefScene.compDef->lastsave){
        auto btn=QMessageBox::warning(this,
                              tr("componentă modificată"),
                              tr("componenta încărcată e de la ")+
                              QDateTime::fromSecsSinceEpoch(compDefScene.compDef->lastsave).toString("HH:mm:ss dd/MM/yyyy")+
                              tr(" dar cea din baza de date e de la ")+
                              QDateTime::fromSecsSinceEpoch(lastsaved).toString("HH:mm:ss dd/MM/yyyy")+
                              tr("\nDoriți să continuați?"),
                              QMessageBox::Yes,QMessageBox::No
                              );
        if(btn!=QMessageBox::Yes)
            return;
    }
    QString oldName=compDefScene.compDef->name;
    compDefScene.compDef->saveToDB();
    if(ui->compNameCombo->currentData().toLongLong()<0){
        int idx=ui->compNameCombo->currentIndex();
        ui->compNameCombo->setItemData(idx,compDefScene.compDef->id);
    }
    if(oldName!=compDefScene.compDef->name){
        loadCompDefList(compDefScene.compDef->id);
    }
}


void Widget::on_newCompBtn_clicked()
{
    clearCompDef();
    compDefScene.compDef=new CompDef;
    ui->compNameCombo->addItem(compDefScene.compDef->name,-1);
    ui->compNameCombo->setCurrentIndex(ui->compNameCombo->count()-1);
}

void Widget::on_compNameCombo_currentIndexChanged(int index)
{
    if(compDefScene.compDefRect){
        qDebug()<<"on_compImageCombo_currentIndexChanged";
        compDefScene.compDefRect->updateCompDefRect();
    }
    long long id=ui->compNameCombo->itemData(index).toLongLong();
    qDebug()<<"compNameCombo_currentIndexChanged"<<index<<id;
    clearCompDef(ui->compNameCombo->currentIndex()==index);
    activateCompDef(id);
    //s.changeCompDef();
}

void Widget::on_rectTypeCombo_currentIndexChanged(int index)
{
    qDebug()<<"rectangleType?"<<index;
    if(!compDefScene.compDefRect)
        return;
    if(compDefScene.compDefRect){
        compDefScene.compDefRect->updateCompDefRect();
    }

    CompDefRect::RectangleType rectangleType=
            static_cast<CompDefRect::RectangleType>(ui->rectTypeCombo->itemData(
                                                           ui->rectTypeCombo->currentIndex()).toInt());
    qDebug()<<"rtype"<<rectangleType;
    compDefScene.compDefRect->compDefRect->applySettings(compDefScene.compDefRect->compDefRect->orientation,compDefScene.compDef->orientationMode,
                                                         compDefScene.compDef->aspectRatio, rectangleType);
    qDebug()<<"updateFromCompDef";
    compDefScene.compDefRect->updateFromCompDef();
    this->compRectChanged(compDefScene.compDefRect,true);
}

void Widget::on_configBtn_clicked()
{
    QSqlDatabase db=QSqlDatabase::database();
    unsigned lastsaved=compDefScene.compDef->getLastSaved(db);
    if(lastsaved>compDefScene.compDef->lastsave){
        auto btn=QMessageBox::warning(this,
                              tr("componentă modificată"),
                              tr("componenta încărcată e de la ")+
                              QDateTime::fromSecsSinceEpoch(compDefScene.compDef->lastsave).toString("HH:mm:ss dd/MM/yyyy")+
                              tr(" dar cea din baza de date e de la ")+
                              QDateTime::fromSecsSinceEpoch(lastsaved).toString("HH:mm:ss dd/MM/yyyy")+
                              tr("\nDoriți să continuați?"),
                              QMessageBox::Yes,QMessageBox::No
                              );
        if(btn!=QMessageBox::Yes)
            return;
    }
    CompDefDialog dlg(this);
    dlg.setWindowFlags(Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint);
    dlg.setWindowModality(Qt::WindowModal);
    dlg.setCompDef(compDefScene.compDef);
    busy=true;
    int result=dlg.exec();
    busy=false;
    //qDebug()<<"dialog result"<<result;
    if(result==0){
        //save clicked
        ui->compNameCombo->setItemText(ui->compNameCombo->currentIndex(), compDefScene.compDef->name);
        this->setWindowTitle(tr("componenta: ")+compDefScene.compDef->name);
    } else if(result==IMPORTAI){
        //refresh compdef after importing component from autoinspect
        refreshCompDef();
    }
}

void Widget::on_implicitNegativeCB_stateChanged(int arg1)
{
    compDefScene.compDefImage->implicitNegative=arg1;
}

void Widget::on_modeTab_currentChanged(int index)
{
    if(index==0){
        ui->graphicsView->setScene(&brdDefScene);
        this->setWindowTitle(tr("placa: ")+ui->brdNameCombo->currentText());
    } else {
        ui->graphicsView->setScene(&compDefScene);
        this->setWindowTitle(tr("componenta: ")+ui->compNameCombo->currentText());
    }
    ui->compStatWidget->setVisible((index==1)&&(ui->compToolBox->currentIndex()==1));
    setScale(ui->scaleSlider->value());
}

void Widget::refreshCompDef()
{
    qDebug()<<"refreshCompDef";
    on_compNameCombo_currentIndexChanged(ui->compNameCombo->currentIndex());
}

void Widget::on_newBoardBtn_clicked()
{
    QString boardName;
    BoardConfig cfg(boardName, 0, QString(), QList<ImagesAndLights>(), 1, this);
    loadCompList(cfg.getClassifierCombo());
    if(cfg.exec()==QDialog::Accepted){
        /// @todo save old board definition
        clearBoardDef();
        boardDef=new BoardDef;
        boardDef->name=cfg.getName().trimmed();
        boardDef->alignMethod=static_cast<BoardDef::AlignMethod>(cfg.getAlignMethod());
        boardDef->refClassifier.loadFromDB(cfg.getClassifier(), false, false);
        boardDef->lights=cfg.getLights();
        boardDef->numberOfBoards=cfg.getNumberOfBoards();
        ui->brdNameCombo->blockSignals(true);
        ui->brdNameCombo->addItem(boardDef->name,-1);
        ui->brdNameCombo->setCurrentIndex(ui->brdNameCombo->count()-1);
        ui->brdNameCombo->blockSignals(false);
        brdDefScene.clear();
        activateLoadedBoardDef();
        on_brdOrientCombo_currentIndexChanged(ui->brdOrientCombo->currentIndex());
    }
    return;
}

void Widget::on_brdSaveBtn_clicked()
{
    if(boardDef==nullptr){
        qDebug()<<"no active board definition loaded";
        return;
    }
    QSqlDatabase db=QSqlDatabase::database();
    unsigned lastsaved=boardDef->getLastSaved(db);
    if(lastsaved>boardDef->lastsave){
        auto btn=QMessageBox::warning(this,
                              tr("placă modificată"),
                              tr("placa încărcată e de la ")+
                              QDateTime::fromSecsSinceEpoch(boardDef->lastsave).toString("HH:mm:ss dd/MM/yyyy")+
                              tr(" dar cea din baza de date e de la ")+
                              QDateTime::fromSecsSinceEpoch(lastsaved).toString("HH:mm:ss dd/MM/yyyy")+
                              tr("\nDoriți să continuați?"),
                              QMessageBox::Yes,QMessageBox::No
                              );
        if(btn!=QMessageBox::Yes)
            return;
    }
    if(brdDefScene.bg.isNull()){
        qDebug()<<"no board image";
        boardDef->refFeatures.keypoints.clear();
        boardDef->refFeatures.descriptors=cv::Mat();
    } else {
        if((boardDef->alignMethod==BoardDef::ORB)||
                (boardDef->alignMethod==BoardDef::AKAZE)){
            QImage img=brdDefScene.bg.toImage();
            qDebug()<<"brdSave"<<brdDefScene.bg.width()<<brdDefScene.bg.height()<<img.width()<<img.height();
            cv::Mat mat=OcvHelper::qImage2Mat(img);
            qDebug()<<"mat"<<mat.rows<<mat.cols;
            boardDef->binaryDescriptors(boardDef->refFeatures,mat, boardDef->refRect);
            qDebug()<<"orbDescriptors"<<boardDef->refFeatures.keypoints.size()<<boardDef->refFeatures.descriptors.cols<<boardDef->refFeatures.descriptors.rows;
        }
    }
    /// @todo make name unique
    QMultiMap<QString, long long> names;
    QString name=boardDef->name;
    QString newName=name;
    for(int i=0; i<ui->brdNameCombo->count(); i++){
        names.insert(ui->brdNameCombo->itemText(i), ui->brdNameCombo->itemData(i).toLongLong());
    }
    if(names.contains(name)){
        auto vals=names.values(name);
        if(vals.count()>1){
            //duplicate board name, rename the board
            std::sort(vals.begin(), vals.end());
            int i=2;
            for(;;){
                newName=name+"_"+QString::number(i);
                i++;
                if(!names.contains(newName))
                    break;
            }
            boardDef->name=newName;
            ui->brdNameCombo->setItemText(ui->brdNameCombo->currentIndex(),boardDef->name);
            this->setWindowTitle("placa: "+boardDef->name);
            activateLoadedBoardDef();
            qDebug()<<"duplicate board name found. Changed board name to"<<newName;
        }
    }
    boardDef->saveToDB();
    if((boardDef->alignMethod==BoardDef::ORB)||
            (boardDef->alignMethod==BoardDef::AKAZE)){
        if(!boardDef->refFeatures.isEmpty())
            boardDef->saveBinaryFeatures();
    }
    if(ui->brdNameCombo->currentData().toLongLong()<0)
        ui->brdNameCombo->setItemData(ui->brdNameCombo->currentIndex(),boardDef->id);
    //refresh
    on_brdCompTypeCombo_currentIndexChanged(ui->brdCompTypeCombo->currentIndex());
}

void Widget::on_refImgBtn_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    "Imagine", "", tr("Fisiere Imagine (*.png *.jpg *.bmp *.jpeg);;toate fișierele (*.*)"));
    if(fileName.isEmpty())
        return;
    if(!brdDefScene.setBackgroundImage(fileName)){
        qDebug()<<"unable to load"<<fileName;
        return;
    }
    QFileInfo info(fileName);
    boardDef->refImagePath=info.absoluteFilePath();
    ui->refImgLbl->setText(info.baseName());
    activateLoadedBoardDef();
}

void Widget::on_brdCfgBtn_clicked()
{
    if(boardDef==nullptr){
        on_newBoardBtn_clicked();
        return;
    }
    //qDebug()<<"show boardconfig"<<boardDef->testCodeOverride;
    BoardConfig cfg(boardDef->name, boardDef->alignMethod, boardDef->testCodeOverride, boardDef->lights, boardDef->numberOfBoards, this);
    loadCompList(cfg.getClassifierCombo(),boardDef->refClassifier.id);
    if(cfg.exec()==QDialog::Accepted){
        boardDef->name=cfg.getName().trimmed();
        boardDef->alignMethod=static_cast<BoardDef::AlignMethod>(cfg.getAlignMethod());
        boardDef->testCodeOverride=cfg.getTestCodeOverride();
        boardDef->refClassifier.loadFromDB(cfg.getClassifier(), false, false);
        boardDef->lights=cfg.getLights();
        boardDef->numberOfBoards=cfg.getNumberOfBoards();
        brdDefScene.clear();
        ui->brdNameCombo->setItemText(ui->brdNameCombo->currentIndex(),boardDef->name);
        this->setWindowTitle("placa: "+boardDef->name);
        activateLoadedBoardDef();
    }
    return;
}

void Widget::on_delCompBtn_clicked()
{
    int result=QMessageBox::question(this, tr("Ștergere componentă"), tr("Sunteți sigur că doriți să ștergeți tipul de componentă?"));
    if(result!=QMessageBox::Yes)
        return;

    result=compDefScene.compDef->removeFromDB();
    if(!result){
        QMessageBox::information(this, tr("Ștergere eșuată"), tr("nu s-a putut șterge componenta!"));
        return;
    }
    clearCompDef();
    ui->compNameCombo->blockSignals(true);
    ui->compNameCombo->removeItem(ui->compNameCombo->currentIndex());
    if(ui->compNameCombo->count()>0){
        long long id=ui->compNameCombo->currentData().toLongLong();
        activateCompDef(id);
    }
    ui->compNameCombo->blockSignals(false);
}

void Widget::on_brdNameCombo_currentIndexChanged(int index)
{
    long long id=ui->brdNameCombo->itemData(index).toLongLong();
    qDebug()<<"brdNameCombo_currentIndexChanged"<<index<<id;
    showBoardWorker(id);
}

void Widget::on_brdOrientCombo_currentIndexChanged(int index)
{
    if(brdDefScene.compDefRect){
        brdDefScene.compDefRect->updateCompDefRect();
    }
    CompDefRect::Orientation orientation=
            static_cast<CompDefRect::Orientation>(ui->brdOrientCombo->itemData(index).toInt());
    qDebug()<<"board orient changed"<<orientation;
    boardDef->refRect.applySettings(orientation,CompDefRect::N_E_S_V,boardDef->refClassifier.aspectRatio,
                                                         boardDef->refRect.rectangleType);
    qDebug()<<"updateFromCompDef";
    QList<QGraphicsItem *> items=brdDefScene.items();
    foreach(QGraphicsItem * item, items){
        ComponentRectItem * r=dynamic_cast<ComponentRectItem *>(item);
        if(r==nullptr)
            continue;
        if(r->compDefRect->rectangleType==CompDefRect::BOARDRECT){
            r->updateFromCompDef();
            break;
        }
    }

}

void Widget::on_boardToolbox_currentChanged(int index)
{
    QList<QGraphicsItem *>items=brdDefScene.items();
    QGraphicsItem * item;
    ComponentRectItem * r;
    long area=0;
    foreach(item, items){
        r=dynamic_cast<ComponentRectItem*>(item);
        if(r==nullptr)
            continue;
        switch(index){
        case 1:
            //edit board rect
            if(r->compDefRect->rectangleType==CompDefRect::BOARDRECT){
                r->setFlag(QGraphicsItem::ItemIsMovable);
                r->setFlag(QGraphicsItem::ItemIsSelectable);
                area=r->compDefRect->w*r->compDefRect->h;
                ui->markedAreaLbl->setText(QString::number(area));
                qDebug()<<"area"<<area;
            } else {
                r->setFlag(QGraphicsItem::ItemIsMovable,false);
                r->setFlag(QGraphicsItem::ItemIsSelectable,false);
            }
            break;
        case 3:
            //edit board components
            if(r->compDefRect->rectangleType==CompDefRect::BOARDCOMP){
                r->setFlag(QGraphicsItem::ItemIsMovable);
                r->setFlag(QGraphicsItem::ItemIsSelectable);
            } else {
                r->setFlag(QGraphicsItem::ItemIsMovable,false);
                r->setFlag(QGraphicsItem::ItemIsSelectable,false);
            }
            break;
        default:
            r->setFlag(QGraphicsItem::ItemIsMovable,false);
            r->setFlag(QGraphicsItem::ItemIsSelectable,false);
            break;
        }
    }
    if(index==3){
        populateParentCompCombo();
        populateBrdCompImagesCombo();
    }
}

void Widget::on_brdNewCompTypeBtn_clicked()
{
    boardDef->dirty=true;
    //delete all items of type BOARDCOMP
    QList<QGraphicsItem*>items;
    QGraphicsItem * item;
    foreach(item, items){
        ComponentRectItem * r;
        r=dynamic_cast<ComponentRectItem*>(item);
        if(r==nullptr)
            continue;
        if(r->compDefRect->rectangleType==CompDefRect::BOARDCOMP){
            brdDefScene.removeItem(r);
            delete r;
        }
    }
    BoardCompType t;
    boardDef->compTypes.append(t);
    boardDef->compType=&(boardDef->compTypes[boardDef->compTypes.count()-1]);
    //ui->brdCompTypeCombo->blockSignals(true);
    ui->brdCompTypeCombo->addItem(t.name,-1);
    // component type is activated by brdCompTypeCombo slot
    ui->brdCompTypeCombo->setCurrentIndex(ui->brdCompTypeCombo->count()-1);
    //ui->brdCompTypeCombo->blockSignals(false);
    //ui->brdTypeNameEdit->setText(t.name);
}



void Widget::on_brdTypeNameEdit_editingFinished()
{
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    QString name=ui->brdTypeNameEdit->text();
    boardDef->compType->name=name;
    ui->brdCompTypeCombo->setItemText(ui->brdCompTypeCombo->currentIndex(),name);
    boardDef->dirty=true;
}


void Widget::on_brdCompTypeCombo_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    if(ui->brdCompTypeCombo->count()<=0){
        //no component type loaded
        return;
    }
    if(boardDef==nullptr){
        //no board definition active
        return;
    }
    assert(ui->brdCompTypeCombo->count()==boardDef->compTypes.count());
    ui->brdCompAltCombo->clear();
    /// @todo clear components
    QGraphicsItem * item;
    foreach(item, brdDefScene.items()){
        ComponentRectItem * r=dynamic_cast<ComponentRectItem *>(item);
        if(r==nullptr)
            continue;
        if(r->compDefRect==nullptr)
            continue;
        CompDefRect * comp=r->compDefRect;
        if(comp==nullptr)
            continue;
        if((comp->rectangleType!=CompDefRect::BOARDCOMP) &&
                (comp->rectangleType!=CompDefRect::FOUNDCOMP))
            continue;
        brdDefScene.removeItem(item);
    }
    if(boardDef->compTypes.count()<=0){

        return;
    }
    long long id=ui->brdCompTypeCombo->currentData().toLongLong();
    //find the corresponding BoardCompType object
    int idx=0;
    if(id<0){
        QString name=ui->brdCompTypeCombo->currentText();
        //no id available, search by name
        for(int i=0; i<boardDef->compTypes.count(); i++){
            if(boardDef->compTypes.at(i).name==name){
                idx=i;
            }

        }
    } else {
        for(int i=0; i<boardDef->compTypes.count(); i++){
            if(boardDef->compTypes.at(i).id==id){
                idx=i;
            }
        }
    }
    boardDef->compType=&(boardDef->compTypes[idx]);
    ui->brdTypeNameEdit->setText(boardDef->compType->name);
    ui->brdCompTypeAbsenceCB->setChecked(boardDef->compType->testAbsence);
    bool found=false;
    for(int i=0; i<BoardCompType::MAXCOMPONENTTYPE; i++){
        qDebug()<<"searching"<<ui->brdCompTypeClassCombo->itemData(i).toInt()<<boardDef->compType->componentType;
        if(ui->brdCompTypeClassCombo->itemData(i).toInt()==boardDef->compType->componentType){
            ui->brdCompTypeClassCombo->setCurrentIndex(i);
            found=true;
            qDebug()<<"ui->brdCompTypeClassCombo->setCurrentIndex(i)"<<i;
        }
    }
    if(!found)
        ui->brdCompTypeClassCombo->setCurrentIndex(0);
    //ui->brdCompTypeClassCombo->setCurrentIndex(boardDef->compType->componentType);
    //populate alternatives combobox and remember minarea
    QList<BoardCompTypeClassifier>classifiers=boardDef->compType->classifiers.values();
    for(int i=0; i<classifiers.count(); i++){
        qDebug()<<classifiers.at(i).compDefPtr->name<<classifiers.at(i).name;
        ui->brdCompAltCombo->addItem(classifiers.at(i).compDefPtr->name,classifiers.at(i).compDefPtr->id);
        if((i==0)&&(brdDefScene.compDef!=nullptr)){
            //compdef is not a pointer to a real component loaded from database, it's just a dummy
            brdDefScene.compDef->minArea=classifiers.at(0).compDefPtr->minArea;
        }
    }
    on_brdCompAltCombo_currentIndexChanged(-1);
    //show components for the selected component type in scene
    for(int i=0; i<boardDef->compType->components.count();i++) {
        BoardComp &r = boardDef->compType->components[i];
        qDebug()<<"restore"<<r.x<<r.y<<r.w<<r.h<<r.orientation;
        ComponentRectItem *item=new ComponentRectItem(r);
        //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
        brdDefScene.addItem(item);
        item->setAcceptedMouseButtons(Qt::LeftButton);
        //item->setFlag(QGraphicsItem::ItemIsMovable);
        //item->setFlag(QGraphicsItem::ItemIsSelectable);
    }
    brdDefScene.update();
    on_boardToolbox_currentChanged(ui->boardToolbox->currentIndex());
    if(boardDef->compType->componentType==BoardCompType::BARCODE){
        //qDebug()<<"show symbology";
        ui->symbologyLbl->setVisible(true);
        ui->symbologyCombo->setVisible(true);
    } else {
        //qDebug()<<"hide symbology";
        ui->symbologyLbl->setVisible(false);
        ui->symbologyCombo->setVisible(false);
    }
    if(boardDef->compType->componentType==BoardCompType::SUBCOMPONENT){
        //qDebug()<<"show parent";
        ui->parentCompLbl->setVisible(true);
        ui->parentCompCombo->setVisible(true);
    } else {
        //qDebug()<<"hide parent";
        ui->parentCompLbl->setVisible(false);
        ui->parentCompCombo->setVisible(false);
    }

}

void Widget::on_brdCompTypeClassCombo_currentIndexChanged(int index)
{
    if((boardDef==nullptr)||(boardDef->compType==nullptr))
        return;
    int intType=ui->brdCompTypeClassCombo->itemData(index).toInt();
    BoardCompType::ComponentType newType=static_cast<BoardCompType::ComponentType>(intType);
    qDebug()<<"setting type class"<<index<<intType;
    if(boardDef->compType->componentType!=newType){
        boardDef->compType->componentType=newType;
        boardDef->dirty=true;
    }
    if(BoardCompType::BARCODE==newType){
        qDebug()<<"show symbology";
        ui->symbologyLbl->setVisible(true);
        ui->symbologyCombo->setVisible(true);
    } else {
        qDebug()<<"hide symbology";
        ui->symbologyLbl->setVisible(false);
        ui->symbologyCombo->setVisible(false);
    }
}

/// add new alternative classifier
void Widget::on_brdCompAltBtn_clicked()
{
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    BoardCompAltDialog dlg;
    double aspectRatio=-1;
    if(boardDef->compType->classifiers.count()>0)
        aspectRatio=boardDef->compType->classifiers.first().compDefPtr->aspectRatio;
    loadCompList( dlg.getClassifierCombo(), -1, aspectRatio);
    int result=dlg.exec();
    if(result==QDialog::Accepted){
        qDebug()<<"accepted";
        long long id=dlg.getClassifier();
        foreach(const BoardCompTypeClassifier &c, boardDef->compType->classifiers){
            if(c.compDefPtr->id==id){
                //duplicate
                QMessageBox::information(this,tr("Duplicat"), tr("Adăugare ignorată, deoarece s-a folosit deja această alternativă"));
                return;
            }
        }

        QSharedPointer<CompDef> comp(new CompDef);
        comp->loadFromDB(id, false, false);
        boardDef->compType->classifiers.insert(-1,comp);
        ui->brdCompAltCombo->addItem(comp->name, id);
    } else {
        //the user has chosen not to add any classifier
        //in this case, we want to try the classifiers already added to see what they find
        if(boardDef->compType->classifiers.count()>0){

            static QList<DetectedComponent> detected;
            QVector<QImage> img(4);
            img[0]=brdDefScene.bg.toImage().convertToFormat(QImage::Format_RGB888);
            {
                QTransform t2;
                QTransform t=t2.rotate(90);
                img[1]=img[0].transformed(t);
            }
            {
                QTransform t2;
                QTransform t=t2.rotate(180);
                img[2]=img[0].transformed(t);
            }
            {
                QTransform t2;
                QTransform t=t2.rotate(270);
                img[3]=img[0].transformed(t);
            }
            detected=boardDef->detectComponents(img, nullptr, 1, false, 0);
            for(int i=0; i<detected.count(); i++){
                DetectedComponent & comp = detected[i];
                qDebug()<<"detected"<<comp.x<<comp.y<<comp.w<<comp.h<<comp.score<<comp.rectangleType;
                ComponentRectItem *item=new ComponentRectItem(comp);
                //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
                brdDefScene.addItem(item);
            }
        }
        qDebug()<<"rejected";
    }
}

void Widget::on_brdCompAltDelBtn_clicked()
{
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    if(ui->brdCompAltCombo->count()<=0)
        return;
    QString name=ui->brdCompAltCombo->itemText(ui->brdCompAltCombo->currentIndex());
    ui->brdCompAltCombo->removeItem(ui->brdCompAltCombo->currentIndex());
    for(auto it=boardDef->compType->classifiers.begin(); it!=boardDef->compType->classifiers.end(); it++){
        if(it.value().compDefPtr->name==name){
            boardDef->compType->classifiers.erase(it);
            break;
        }
    }
}

void Widget::on_brdCompTypeDelBtn_clicked()
{
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    int result=QMessageBox::question(this, tr("Ștergere tip componentă"), tr("Sunteți sigur că doriți să ștergeți tipul de componentă?"));
    if(result!=QMessageBox::Yes)
        return;
    for(int i=0; i<boardDef->compTypes.count(); i++){
        if(&(boardDef->compTypes[i])==boardDef->compType){
            qDebug()<<"removeAt"<<i;
            boardDef->compTypes.removeAt(i);
            ui->brdCompTypeCombo->removeItem(ui->brdCompTypeCombo->currentIndex());
            if(boardDef->compTypes.count()<=0){
                boardDef->compType=nullptr;
                ui->brdTypeNameEdit->clear();
                ui->brdCompTypeClassCombo->setCurrentIndex(BoardCompType::COMPONENT);
                ui->brdCompAltCombo->clear();
            }
            break;
        }
    }
}

void Widget::on_brdDelBtn_clicked()
{
    if(boardDef==nullptr)
        return;
    int result=QMessageBox::question(this, tr("Ștergere placă"), tr("Sunteți sigur că doriți să ștergeți placa?"));
    if(result!=QMessageBox::Yes)
        return;
    if(boardDef->id>0){
        boardDef->removeFromDB();
    }
    boardDef=nullptr;
    ui->brdNameCombo->removeItem(ui->brdNameCombo->currentIndex());
    if(ui->brdNameCombo->count()<=0){
        /// @todo clear everything
    }
}

void Widget::on_brdCompDelBtn_clicked()
{
    if(!brdDefScene.compDefRect)
        return;
    assert(boardDef->compType);
    CompDefRect * r=brdDefScene.compDefRect->compDefRect;
    assert(r);
    for(int i=0; i<boardDef->compType->components.count(); i++){
        if(&(boardDef->compType->components[i])==r){
            boardDef->compType->components.removeAt(i);
            brdDefScene.removeItem(brdDefScene.compDefRect);
            delete(brdDefScene.compDefRect);
            brdDefScene.compDefRect=NULL;
            //ui->compImgLabel->clear();
            /// @todo clear fields
            return;
        }
    }

}

void Widget::on_brdCompOrientCombo_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    if(brdDefScene.compDefRect==nullptr)
        return;
    brdDefScene.compDefRect->updateCompDefRect();

    brdDefScene.compDefRect->compDefRect->orientation=
            static_cast<CompDefRect::Orientation>(ui->brdCompOrientCombo->currentData().toInt());
    brdDefScene.compDefRect->updateFromCompDef();
}

void Widget::on_brdCompNameEdit_editingFinished()
{
    if(brdDefScene.compDefRect==nullptr)
        return;
    assert(brdDefScene.compDefRect->compDefRect->rectangleType==CompDefRect::BOARDCOMP);
    BoardComp * comp=static_cast<BoardComp*>(brdDefScene.compDefRect->compDefRect);
    /// @todo
    comp->name=ui->brdCompNameEdit->text();
}

void Widget::on_brdXTolSpin_valueChanged(double arg1)
{
    if(brdDefScene.compDefRect==nullptr)
        return;
    assert(brdDefScene.compDefRect->compDefRect->rectangleType==CompDefRect::BOARDCOMP);
    BoardComp * comp=static_cast<BoardComp*>(brdDefScene.compDefRect->compDefRect);
    comp->xTol=arg1;
}

void Widget::on_brdYTolSpin_valueChanged(double arg1)
{
    if(brdDefScene.compDefRect==nullptr)
        return;
    assert(brdDefScene.compDefRect->compDefRect->rectangleType==CompDefRect::BOARDCOMP);
    BoardComp * comp=static_cast<BoardComp*>(brdDefScene.compDefRect->compDefRect);
    comp->yTol=arg1;
}

void Widget::on_brdSizeTolSpin_valueChanged(double arg1)
{
    if(brdDefScene.compDefRect==nullptr)
        return;
    assert(brdDefScene.compDefRect->compDefRect->rectangleType==CompDefRect::BOARDCOMP);
    BoardComp * comp=static_cast<BoardComp*>(brdDefScene.compDefRect->compDefRect);
    comp->sizeTol=arg1;
}

void Widget::on_brdMinAreaSpin_valueChanged(int arg1)
{
    if(arg1<boardDef->refClassifier.minArea)
        arg1=boardDef->refClassifier.minArea;
    if(arg1==boardDef->minArea)
        return;
    if(arg1<=boardDef->maxArea)
        boardDef->minArea=arg1;
    else {
        boardDef->minArea=boardDef->maxArea;
    }
    ui->brdMinAreaSpin->setValue(boardDef->minArea);
    boardDef->dirty=true;
}

void Widget::on_brdMaxAreaSpin_valueChanged(int arg1)
{
    if(arg1<boardDef->refClassifier.minArea)
        arg1=boardDef->refClassifier.minArea;
    if(arg1==boardDef->maxArea)
        return;
    if(arg1>=boardDef->minArea)
        boardDef->maxArea=arg1;
    else {
        boardDef->maxArea=boardDef->minArea;
    }
    ui->brdMaxAreaSpin->setValue(boardDef->maxArea);
    boardDef->dirty=true;
}

void Widget::on_compToolBox_currentChanged(int index)
{
    Q_UNUSED(index);
    //unselect all items
    auto items=compDefScene.selectedItems();
    for(int i=0; i<items.count(); i++){
        items[i]->setSelected(false);
    }
    ui->compStatWidget->setVisible(index==1);
}

void Widget::on_snValidatorEdit_editingFinished()
{
    boardDef->snValidator=ui->snValidatorEdit->text();
}

void Widget::on_groupBox_clicked()
{
    if(!ui->groupBox->isChecked())
        ui->groupBox->setChecked(true);
    else
        return;
    qDebug()<<"proba detector";
    compDefScene.clearDetectedRects();
    static QList<DetectedComponent> detected;
    QImage image=compDefScene.bg.toImage().convertToFormat(QImage::Format_RGB888);
    if(image.isNull())
        return;
    double scale=1;
    if(compDefScene.compDef->upsample==1){
        image=image.scaled(image.width()*2,image.height()*2);
        scale=0.5;
    }
    runClassifierOnImage(compDefScene.compDef, image, detected, scale, 0);
    compDefScene.showDetections(detected, compDefScene.compDef->fhogThreshold);
}

void Widget::on_brdStartCombo_currentIndexChanged(int index)
{
    boardDef->testStartType=index;
}

void Widget::on_brdDelaySpin_editingFinished()
{
    boardDef->testStartDelay=ui->brdDelaySpin->value();
}

void Widget::on_newBoardBtn_triggered(QAction *action)
{
    //only one action is implemented
    Q_UNUSED(action);
    if(!boardDef)
        return;
    QString name=boardDef->name;
    QString newName;
    qDebug()<<"copy board";
    int i=2;
    QStringList names;
    for(int i=0; i<ui->brdNameCombo->count(); i++){
        names<<ui->brdNameCombo->itemText(i);
    }
    for(;;){
        newName=name+"_"+QString::number(i);
        i++;
        if(!names.contains(newName))
            break;
    }
    long long newId=boardDef->saveToDBAs(newName);
    if(newId<0){
        newId=boardDef->saveToDBAs(newName);
    }
    clearBoardDef(/*ui->brdNameCombo->currentIndex()==index*/);
    loadBoardList(ui->brdNameCombo, newId);
    activateBoardDef(newId);

}

void Widget::on_newCompBtn_triggered(QAction *arg1)
{
    ///only one action is implemented
    Q_UNUSED(arg1);
    if(!compDefScene.compDef)
        return;
    QString name=compDefScene.compDef->name;
    QString newName;
    qDebug()<<"copy component";
    int i=2;
    QStringList names;
    for(int i=0; i<ui->compNameCombo->count(); i++){
        names<<ui->compNameCombo->itemText(i);
    }
    for(;;){
        newName=name+"_"+QString::number(i);
        i++;
        if(!names.contains(newName))
            break;
    }
    long long newId=compDefScene.compDef->saveToDBAs(newName);
    if(newId>0){
        loadCompDefList(newId);
    }

}

void Widget::on_symbologyCombo_currentIndexChanged(int index)
{
    if(brdDefScene.compDefRect==nullptr)
        return;
    assert(brdDefScene.compDefRect->compDefRect->rectangleType==CompDefRect::BOARDCOMP);
    BoardComp * comp=static_cast<BoardComp*>(brdDefScene.compDefRect->compDefRect);
    comp->symbology=index;

}

void Widget::compDefKeypressed(QKeyEvent *event)
{
    switch(ui->compToolBox->currentIndex()){
    case 1:
        switch (event->key()) {
        case Qt::Key_Delete:
        case Qt::Key_Backspace:
            qDebug()<<"deleteCompRect";
            ui->deleteCompRect->click();
            break;
        case Qt::Key_P:
            ui->rectTypeCombo->setCurrentIndex(0);
            break;
        case Qt::Key_N:
            ui->rectTypeCombo->setCurrentIndex(1);
            break;
        case Qt::Key_I:
            ui->rectTypeCombo->setCurrentIndex(2);
            break;
        case Qt::Key_R:
        if(ui->compOrientCombo->count()>1){
            auto orient=ui->compOrientCombo->currentIndex();
            orient=(orient+1)%ui->compOrientCombo->count();
            ui->compOrientCombo->setCurrentIndex(orient);
        }
            break;
        default:
            qDebug()<<"ignore";
            break;
        }
        break;
    default:
        /*do nothing*/
        ;
    }

}

void Widget::brdDefKeypressed(QKeyEvent *event)
{
    switch(ui->boardToolbox->currentIndex()){
    case 3:
        switch (event->key()) {
        case Qt::Key_Delete:
        case Qt::Key_Backspace:
            qDebug()<<"deleteCompRect";
            ui->brdCompDelBtn->click();
            break;
        case Qt::Key_R:
        {
            auto orient=ui->brdCompOrientCombo->currentIndex();
            orient=(orient+1)%ui->brdCompOrientCombo->count();
            ui->brdCompOrientCombo->setCurrentIndex(orient);
        }
            break;
        default:
            qDebug()<<"ignore";
            break;
        }
        break;
    default:
        /*do nothing*/
        ;
    }
}

void Widget::on_compStatImgView_doubleClicked(const QModelIndex &index)
{
    //qDebug()<<"index data"<<index.data(Qt::DisplayRole)<<index.data(Qt::UserRole);
    if(ui->compImageCombo->currentIndex()!=index.row()){
        ui->compImageCombo->setCurrentIndex(index.row());
    } else {
        compDefScene.clearDetectedRects();
    }
    if(!compStatModel->stats[index.row()].valid){
        //calculate the stats for the image
        on_compStatBtn_clicked();
    } else {
        if(std::abs(compStatModel->oldThreshold - compDefScene.compDef->fhogThreshold)>0.001){
            compStatModel->processImageDetections(compDefScene.compDef, index);
        }

        compStatModel->statToChart(compStatModel->stats[index.row()], *compStatChart, compDefScene.compDef->fhogThreshold);
        compStatModel->statToChart(compStatModel->globalStat, *compGlobalStatChart, compDefScene.compDef->fhogThreshold);
        showCompStats(index);
    }
    if(compStatModel->stats[index.row()].valid){
        qDebug()<<"stored detections"<<compStatModel->stats[index.row()].detections.count();
        compDefScene.showDetections(compStatModel->stats[index.row()].detections,compDefScene.compDef->fhogThreshold);
    }
}

void Widget::on_compStatBtn_clicked()
{
    auto lst=ui->compStatImgView->selectionModel()->selectedIndexes();
    if(lst.isEmpty()){
        QModelIndex idx=ui->compStatImgView->currentIndex();
        if(!idx.isValid()){
            qDebug()<<"index not valid";
        } else {
            lst<<idx;
        }
    }
    foreach(const auto &idx, lst){
        try{
        compStatRecalc(idx);
        } catch(...){
            qDebug()<<"exception while processing"<<idx.row();
        }

        qApp->processEvents();
    }
}

void Widget::on_compStatAllBtn_clicked()
{
    compDefScene.clearDetectedRects();
    compStatModel->prepareForUpdate();
    compStatModel->updateFinished();

    int rowCount=compStatModel->rowCount();
    for(int i=0; i<rowCount; i++){
        QModelIndex idx=compStatModel->index(i);
        evalClassifierStats(idx);
        qApp->processEvents();
    }
    compStatModel->calculateGlobalStat();
    QModelIndex idx=ui->compStatImgView->currentIndex();
    if(idx.isValid())
        compStatModel->statToChart(compStatModel->stats[idx.row()], *compStatChart, compDefScene.compDef->fhogThreshold);
    compStatModel->statToChart(compStatModel->globalStat, *compGlobalStatChart, compDefScene.compDef->fhogThreshold);
    showCompStats(idx);
}

void Widget::on_modeTab_tabBarDoubleClicked(int index)
{
    qDebug()<<"doubleclick"<<index;
    if(index==0){
        if(nullptr!=testInterface)
            testInterface->show();
    } else
    if(index==1){
        fileDeleter.show();
    }
}

void Widget::on_brdExtStartDisableCB_stateChanged(int arg1)
{
    if(boardDef!=nullptr){
        boardDef->testStartExtDisabled = (arg1!=0);
        qDebug()<<"on_brdExtStartDisableCB_stateChanged"<<arg1<<ui->brdExtStartDisableCB->isChecked();
    }
}

void Widget::setCurrentComboItemByUserdata(QComboBox *box, const QVariant data)
{
    for(int i=0; i<box->count(); i++){
        if(box->itemData(i)==data){
            box->setCurrentIndex(i);
            return;
        }
    }
    //nothing found, default to first item
    if(box->count()>0)
        box->setCurrentIndex(0);
    return;
}

void Widget::populateParentCompCombo()
{
    ui->parentCompCombo->blockSignals(true);
    ui->parentCompCombo->clear();
    ui->parentCompCombo->addItem(tr("ignoră"), -1);
    if(boardDef==nullptr){
        ui->parentCompCombo->blockSignals(false);
        return;
    }

    for(int t=0; t<boardDef->compTypes.count(); t++){
        const auto& cT=boardDef->compTypes[t];
        if(cT.componentType != BoardCompType::COMPONENT)
            continue;
        for(int c=0; c<cT.components.count(); c++){
            const auto& component=cT.components[c];
            if(component.id<=0){
                //skip components without id
                continue;
            }
            ui->parentCompCombo->addItem(component.name, component.id);
        }
    }
    ui->parentCompCombo->blockSignals(false);
}

void Widget::populateBrdCompImagesCombo()
{
    if(boardDef==nullptr){
        ui->brdCompImagesBtn->menu()->clear();
        return;
    }
    QMenu *menu = new QMenu();
    auto images=boardDef->lights.count();
    for(int i=1; i<=images; i++){
        QAction *act = new QAction(QString::number(i), this);
        act->setObjectName(QString::number(i));
        act->setCheckable(true);
        menu->addAction(act);
    }
    ui->brdCompImagesBtn->setMenu(menu);
    qDebug()<<"populateBrdCompImagesCombo"<<menu->actions().count();
}

void Widget::on_parentCompCombo_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    if(brdDefScene.compDefRect==nullptr)
        return;
    assert(brdDefScene.compDefRect->compDefRect->rectangleType==CompDefRect::BOARDCOMP);
    BoardComp * comp=static_cast<BoardComp*>(brdDefScene.compDefRect->compDefRect);
    long long id=ui->parentCompCombo->currentData().toLongLong();
    comp->parentComponent=id;
}



void Widget::on_brdCompImagesBtn_triggered(QAction *arg1)
{
    qDebug()<<"triggered"<<arg1->objectName();
    //check if a rectangle is currently selected.
    if(brdDefScene.compDefRect==nullptr)
        return;
    //retrieve the current board component
    BoardComp * comp=static_cast<BoardComp*>(brdDefScene.compDefRect->compDefRect);
    //get the index of the current action item (stored as object name) and store the current check state
    int idx=arg1->objectName().toInt()-1;
    if((idx<boardDef->lights.count()) && (idx>=0)){
        comp->images[idx]=arg1->isChecked();
    }
    //update the text on the buton
    QString txt;
    for(int i=0; i<boardDef->lights.count(); i++){
        if(comp->images[i])
            txt.append("1");
        else
            txt.append("0");
    }
    ui->brdCompImagesBtn->setText(txt);
}

void Widget::on_brdCompAltShowBtn_clicked()
{
    if(ui->brdCompAltCombo->count()<=0)
        return;
    int idx=ui->brdCompAltCombo->currentIndex();
    long long id=ui->brdCompAltCombo->itemData(idx).toLongLong();
    qDebug()<<"on_brdCompAltShowBtn_clicked"<<id;
    addToClassifier(id,"");
}

void Widget::on_brdCompAltEdit_editingFinished()
{
    //qDebug()<<"on_brdCompAltEdit_editingFinished";
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    if(ui->brdCompAltCombo->count()<=0)
        return;
    long long classifierId=ui->brdCompAltCombo->currentData().toLongLong();
    for(auto i=boardDef->compType->classifiers.begin(); i!=boardDef->compType->classifiers.end(); ++i){
        if(i.value().compDefPtr->id != classifierId)
            continue;
        i.value().name=ui->brdCompAltEdit->text();
        break;
    }
}

void Widget::on_brdCompAltCombo_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    //qDebug()<<"on_brdCompAltCombo_currentIndexChanged";
    ui->brdCompAltEdit->clear();
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    if(ui->brdCompAltCombo->count()<=0)
        return;
    long long classifierId=ui->brdCompAltCombo->currentData().toLongLong();
    for(auto i=boardDef->compType->classifiers.begin(); i!=boardDef->compType->classifiers.end(); ++i){
        if(i.value().compDefPtr->id != classifierId)
            continue;
        ui->brdCompAltEdit->setText(i.value().name);
        break;
    }
}

void Widget::on_brdCompTypeAbsenceCB_stateChanged(int arg1)
{
    if(boardDef==nullptr)
        return;
    if(boardDef->compType==nullptr)
        return;
    boardDef->compType->testAbsence=(arg1==0)?false:true;
    boardDef->dirty=true;
}


void Widget::on_includeDatasetCombo_currentIndexChanged(int index)
{
    ///@todo update image list in detail page
    Q_UNUSED(index);
    long long itemId=ui->includeDatasetCombo->currentData().toLongLong();
    compStatModel->prepareForUpdate();
    compDefScene.compDef->setIncludeDataset(itemId);
    compStatModel->updateFinished();
    qDebug()<<"on_includeDatasetCombo_currentIndexChanged(int index)"<<itemId<<ui->includeDatasetCombo->itemData(index);
    //copy from activateCompDef
    int nImages=compDefScene.compDef->images.count();
    int nIncludedImages=compDefScene.compDef->includedImages.count();
    //qDebug()<<"activateCompDef"<<id<<nImages;
    if(nImages+nIncludedImages>0){
        //populate image combobox
        ui->compImageCombo->blockSignals(true);
        ui->compImageCombo->clear();
        for(int i=0; i<nImages; i++){
            QString fileName=compDefScene.compDef->images[i].imagePath;
            QFileInfo info(fileName);
            //qDebug()<<fileName;
            ui->compImageCombo->addItem(info.fileName(),fileName);
        }
        for(int i=0; i<nIncludedImages; i++){
            QString fileName=compDefScene.compDef->includedImages[i].imagePath;
            QFileInfo info(fileName);
            //qDebug()<<fileName;
            ui->compImageCombo->addItem(info.fileName(),fileName);
        }
        ui->compImageCombo->setCurrentIndex(0);
        ui->compImageCombo->blockSignals(false);
        on_compImageCombo_currentIndexChanged(0);
    }
    compDefScene.compDef->dirty=true;
}


void Widget::on_compAspectCombo_currentIndexChanged(int index)
{
    Q_UNUSED(index)
    //forced seting the aspect ratio needed to load the component list. This will be set also in applyCompSettings
    compDefScene.compDef->aspectRatio=ui->compAspectCombo->itemData(ui->compAspectCombo->currentIndex()).toReal();
    bool found=loadCompList(ui->includeDatasetCombo, compDefScene.compDef->getIncludeDataset(), compDefScene.compDef->aspectRatio);
    ui->includeDatasetCombo->blockSignals(true);
    ui->includeDatasetCombo->insertItem(0, tr("nu include"), -1);
    if(!found){
        ui->includeDatasetCombo->setCurrentIndex(0);
        compDefScene.compDef->setIncludeDataset(-1);
        compDefScene.compDef->includedImages.clear();
    }
    ui->includeDatasetCombo->blockSignals(false);

    applyCompSettings();
}


void Widget::on_brdCfgBtn_triggered(QAction *arg1)
{
    if(boardDef==nullptr)
        return;
    if(arg1->objectName()=="testedComps"){
        BrdInfo info(boardDef);
        info.exec();
    }
    if(arg1->objectName()=="brdClassifier"){
        long long id=boardDef->refClassifier.id;
        addToClassifier(id, "");
    }
}


void Widget::on_snValidatorEdit_returnPressed()
{
    RegExTester tester;
    tester.setValidator(ui->snValidatorEdit->text());
    if(tester.exec()){
        ui->snValidatorEdit->setText(tester.getValidator());
    }
}


void Widget::on_compAngleSpin_valueChanged(int arg1)
{
    if(!compDefScene.compDefRect)
        return;
    compDefScene.compDefRect->compDefRect->angle=arg1*0.001;
}


void Widget::on_brdAngleSpin_valueChanged(int arg1)
{
    if(brdDefScene.compDefRect){
        brdDefScene.compDefRect->updateCompDefRect();
    }
    boardDef->refRect.angle=arg1*0.001;
    qDebug()<<"update refrect angle"<<boardDef->refRect.angle;
    QList<QGraphicsItem *> items=brdDefScene.items();
    foreach(QGraphicsItem * item, items){
        ComponentRectItem * r=dynamic_cast<ComponentRectItem *>(item);
        if(r==nullptr)
            continue;
        if(r->compDefRect->rectangleType==CompDefRect::BOARDRECT){
            r->updateFromCompDef();
            break;
        }
    }
    boardDef->dirty=true;
}

