#include "raspicvcam.h"
#include <QSettings>
#include <QApplication>
#include <QDebug>
#include <QElapsedTimer>

RaspiCvCam::RaspiCvCam(QObject *parent) : QObject(parent)
{
    QSettings settings(QSettings::IniFormat,QSettings::UserScope, qApp->organizationName(),qApp->applicationName());
    if(settings.contains("raspicamresolution")){
        resolution=settings.value("raspicamresolution").toSize();
    }
    nImages=settings.value("raspicamnimages",1).toInt();
    nIgnore=settings.value("raspicamignores",1).toInt();
#ifndef USERASPIYUV
    if(cap.isOpened()){
        qDebug()<<"capture opened by default?!?";
        cap.release();
    }
#endif
}

QList<QSize> RaspiCvCam::getResolutions()
{
    QList<QSize> lst;
    QList<QSize> commonResolutions({{160,120},{320,240},{639,360},{640,480},{640,640},{1280,720},{1284,720},{1440,720},
                                    {1440,1080},{1920,1080},{1920,1280},{1920,1440},{2592,1458},
                                    {2592,1458},{2592,1728},{2592,1944},{2888,1440},{3264,1836},
                                    {3264,2176},{3264,2448},{3280,2464},{4056,3040},{4128,3096},{4128,2322},{5256,2952},{5256,3936},{10000,10000}});

    lst.append(QSize(640,480));
    lst.append(QSize(2592,1944));
    lst.append(QSize(3264,2464));
    //lst.append(QSize(3280,2464));
    lst.append(QSize(4032,3040));
    //lst.append(QSize(4056,3040));
    //lst.append(QSize(4064,3040));
    return lst;
    /* raspicam accepts arbitrary resolutions, so trying what is accepted does not work
    if(cap.isOpened()){
        cap.release();
    }

    cap.open();
    if(!cap.isOpened())
        return lst;
    foreach (QSize s, commonResolutions) {
        //try to set resolution to s and check if it worked
        /// @todo
        cap.set(cv::CAP_PROP_FRAME_WIDTH, s.width());
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, s.height());
        QSize newSize;
        newSize.setWidth(cap.get(cv::CAP_PROP_FRAME_WIDTH));
        newSize.setHeight(cap.get(cv::CAP_PROP_FRAME_HEIGHT));
        if(!lst.contains(newSize)){
            lst.append(newSize);
        }
        qDebug()<<s<<"->"<<newSize;
        int ex = static_cast<int>(cap.get(cv::CAP_PROP_FOURCC));
        // Transform from int to char via Bitwise operators
        char EXT[] = {(char)(ex & 0XFF),(char)((ex & 0XFF00) >> 8),(char)((ex & 0XFF0000) >> 16),(char)((ex & 0XFF000000) >> 24),0};

        qDebug()<<EXT<<
                  cap.get(cv::CAP_PROP_FPS);
    }
    cap.release();
    return lst;
    */
}

QSize RaspiCvCam::getResolution()
{
    return resolution;
}



int RaspiCvCam::getNImages() const
{
    return nImages;
}

void RaspiCvCam::setNImages(int value)
{
    nImages=value;
}

int RaspiCvCam::getNIgnore() const
{
    return nIgnore;
}

void RaspiCvCam::setNIgnore(int value)
{
    nIgnore=value;
}

#ifdef USERASPIYUV
void RaspiCvCam::setResolution(const QSize &value)
{
    qDebug()<<"setResolution"<<value;
    resolution = value;
    if(cap.state()!=QProcess::NotRunning){
        setEnabled(false);
    }
}

bool RaspiCvCam::reopen()
{
    cap.start("raspiyuv -n -t 0 -k -o - -rgb -w "+
                       QString::number(resolution.width())+" -h "+
                       QString::number(resolution.height()));
    cap.waitForStarted(1000);
    return isOpen();
}

bool RaspiCvCam::isOpen(){
    return cap.state()==QProcess::Running;
}

void RaspiCvCam::capture()
{
    qDebug()<<"raspi capture";
    if(!isOpen())
        reopen();
    if(!isOpen()){
        emit captureError("device is not open");
        return;
    }
    cap.readAll();
    cap.write("\n");
    QCoreApplication::processEvents();
    QByteArray imageData;
    int dataSize=3*resolution.width()*resolution.height();
    while(imageData.size()<dataSize){
        if(!cap.waitForReadyRead(10000))
            break;
        imageData.append(cap.readAll());
    }
    if(imageData.size()<dataSize){
        emit captureError("insufficient data received");
        qDebug()<<"read error"<<imageData.size()<<cap.program()<<cap.arguments()<<cap.state();
        return;
    }
    QImage image(resolution.width(), resolution.height(), QImage::Format_RGB888);
    int lineSize=resolution.width()*3;
    for(int i=0; i<resolution.height(); i++){
        memcpy(image.scanLine(i), imageData.constData()+i*lineSize, lineSize);
    }
    emit imageCaptured( image );
    return;

}

bool RaspiCvCam::setEnabled(bool enabled)
{
    if(enabled){
        if(isOpen()){
            setEnabled(false);
        }
        reopen();
        return isOpen();
    }
    if(isOpen()){
        cap.write("X\n");
        cap.waitForFinished(2000);
    }
    return true;
}

#else
void RaspiCvCam::setResolution(const QSize &value)
{
    qDebug()<<"setResolution"<<value;
    resolution = value;
    if(cap.isOpened()){
        cap.set(cv::CAP_PROP_FRAME_WIDTH, resolution.width());
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, resolution.height());
    }
    //cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('Y','U','Y','2'));
    //cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('M','J','P','G'));
    //open windows camera settings dialog
    //cap.set(cv::CAP_PROP_SETTINGS,1);
    qDebug()<<"exposure"<<cap.get(cv::CAP_PROP_EXPOSURE)
           <<"gain"<<cap.get(cv::CAP_PROP_GAIN)
          <<"autoexposure"<<cap.get(cv::CAP_PROP_AUTO_EXPOSURE);
}

void RaspiCvCam::capture()
{
    if(!cap.isOpened())
        reopen();
    if(!cap.isOpened()){
        emit captureError("device is not open");
        return;
    }
    qint64 minCaptureTime=50;//50 ms capture time default value
    double fps=cap.get(cv::CAP_PROP_FPS);
    if(fps>0){
        minCaptureTime=1000/(2*fps);
    }
    qDebug()<<"FPS"<<cap.get(cv::CAP_PROP_FPS);
    cv::Mat m;
    for(int i=0; i<nIgnore; i++){
        cap.grab();
    }
    QElapsedTimer timer;
    int count=0;
    qint64 e=0;
    for(timer.start(); ((e=timer.restart())<minCaptureTime)&&(count<nIgnore);count++){
        cap.grab();
        cap.retrieve(m);
    }
    qDebug()<<"elapsed"<<e<<minCaptureTime;
    if(m.empty()){
        qDebug()<<"capture error"<<cap.isOpened();
    } else
    if(nImages>1){
        cv::Mat sum;
        m.convertTo(sum,CV_16UC3);
        for(int i=1; i<nImages; i++){
            cap.grab();
            cap.retrieve(m);
            cv::add(sum,m,sum,cv::noArray(),CV_16UC3);
            qDebug()<<"capture"<<i<<sum.at<cv::Vec<uint16_t, 3>>(0,0)[0]<<"elapsed"<<timer.elapsed();
        }
        sum /= nImages;
        sum.convertTo(m, CV_8UC3);
    }
    if(m.type()==CV_8UC3){
        //mat2QImage
        const uchar *qImageBuffer = (const uchar*)m.data;
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, m.cols, m.rows, m.step, QImage::Format_RGB888);
        emit imageCaptured( img.rgbSwapped() );
        return;
    }
    emit captureError("unexpected image type");
    return;

}

bool RaspiCvCam::reopen()
{
    bool opened=false;
    opened = cap.open();
    if(!opened)
        return false;
    cap.set(cv::CAP_PROP_FRAME_WIDTH, resolution.width());
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, resolution.height());
    cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('Y','U','Y','2'));
    //cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('B','G','R','A'));
    qDebug()<<"buffer"<<cap.get(cv::CAP_PROP_BUFFERSIZE);
    return opened;
}

bool RaspiCvCam::setEnabled(bool enabled)
{
    if(enabled){
        if(cap.isOpened())
            cap.release();
        cap.open();
        return cap.isOpened();
    }
    if(cap.isOpened())
        cap.release();
    return true;
}

bool RaspiCvCam::isOpened(){
    return cap.isOpened();
}
#endif //USERASPIYUV
