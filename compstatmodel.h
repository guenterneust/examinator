/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPSTATMODEL_H
#define COMPSTATMODEL_H

#include <QAbstractListModel>
#include <QChart>
#include <QLineSeries>
#include "widget.h"

class CompStatElement{
public:
    int correct=0;
    int extra=0;
    int orientation=0;
};

class CompStatElementGlobal: public CompStatElement{
public:
    int missing=0;
};

class CompStat{
public:
    QList<DetectedComponent> detections;
    CompStatElementGlobal values;
    static const int histSteps=61;
    QVector<CompStatElement> hist=QVector<CompStatElement>(histSteps);
    bool valid=false;
    constexpr static const double histStep=0.05;
    /// delete all the statistics for this image, without touching the stored detections
    inline void clearStat(){
        values=CompStatElementGlobal();
        for (unsigned i=0; i<histSteps; i++){
            hist[i]=CompStatElement();
        }
        valid=false;
    }
    void accumultateStat(const CompStat &other){
        values.missing += other.values.missing;
        values.extra += other.values.extra;
        values.correct += other.values.correct;
        values.orientation += other.values.orientation;
        for(int i=0; i<histSteps; i++){
            hist[i].extra += other.hist[i].extra;
            hist[i].correct += other.hist[i].correct;
            hist[i].orientation += other.hist[i].orientation;
        }
    }
    inline void addCorrect(double value, double thresh){
        int idx=.5+(value/histStep +(histSteps/2));
        qDebug()<<"addCorrect()"<<value<<thresh<<idx;//<<(int)value/histStep<<(int)(value/histStep +(histSteps/2));
        idx=std::min(std::max(idx,0),histSteps-1);
        hist[idx].correct++;
        if(value>=thresh)
            values.correct++;
        else {
            //do nothing. There is anothere step which counts missing detections
            //values.missing++;
        }
    }
    inline void addExtra(double value, double thresh){
        int idx=.5+(value/histStep +(histSteps/2));
        idx=std::min(std::max(idx,0),histSteps-1);
        hist[idx].extra++;
        if(value>=thresh){
            qDebug()<<"addExtra()"<<value<<thresh<<idx;//<<(int)value/histStep<<(int)(value/histStep +(histSteps/2));
            values.extra++;
        } else {
            //do nothing
        }
    }
    inline void addOrientation(double value, double thresh){
        int idx=.5+(value/histStep +(histSteps/2));
        idx=std::min(std::max(idx,0),histSteps-1);
        hist[idx].orientation++;
        if(value>=thresh) {
            qDebug()<<"addOrientation()"<<value<<thresh<<idx;//<<(int)value/histStep<<(int)(value/histStep +(histSteps/2));
            values.orientation++;
        } else {
            //do nothing
        }
    }
    inline double histPosition(int index) const{
        return (index-histSteps/2)*histStep;
    }
};

class CompStatModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit CompStatModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void prepareForUpdate(void);
    void updateFinished(void);
    QVector<CompStat> stats;
    CompStat globalStat;
    void setValid(int row, bool valid){
        auto idx=index(row);
        stats[row].valid=valid;
        emit dataChanged(idx, idx);
    }
    /**
     * @brief This method analises the detected rectangles in an image by comparing them with the markings in the same image, and calculates the
     * number of missed objects, correct objects, wrongly oriented objects and objects detected where no object marking is. It also fills the bins
     * for the histogram for correct, wrong orientation and extra objects.
     * Before calling this function, the detected rectangles have to have been stored in the CompStat structure at the position given by index.row(),
     * and the image definition has to be at the position given by index.row() in the image list of the component definition.
     * @param compDef
     * @param index
     */
    void processImageDetections(const CompDef * compDef, const QModelIndex &index);
    //accumulate the stats of the individual images into the global stat
    void calculateGlobalStat(void);
    void statToChart(const CompStat &stat, QtCharts::QChart &chart, double threshold);
    double oldThreshold=0;
private:
    Widget * w=nullptr;
};

#endif // COMPSTATMODEL_H
