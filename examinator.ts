<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="ro_RO">
<context>
    <name>BarcodeCorrection</name>
    <message>
        <location filename="barcodecorrection.ui" line="14"/>
        <source>Corectare numar serie</source>
        <translation>Serial number correction</translation>
    </message>
    <message>
        <location filename="barcodecorrection.ui" line="23"/>
        <source>Eroare</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="barcodecorrection.ui" line="43"/>
        <source>Introduceți numărul de serie corect:</source>
        <translation>Insert the correct serial number:</translation>
    </message>
    <message>
        <location filename="barcodecorrection.ui" line="53"/>
        <source>Acceptă</source>
        <translation>Accept</translation>
    </message>
    <message>
        <location filename="barcodecorrection.ui" line="60"/>
        <source>Renunță</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>BoardCompAltDialog</name>
    <message>
        <location filename="boardcompaltdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="boardcompaltdialog.ui" line="44"/>
        <source>Clasificator componenta</source>
        <translation>Component classifier</translation>
    </message>
</context>
<context>
    <name>BoardCompTypeConfig</name>
    <message>
        <location filename="boardcomptypeconfig.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="20"/>
        <source>Denumire tip componenta</source>
        <translation>Component type name</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="30"/>
        <source>Clasa componenta</source>
        <translation>Component class</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="44"/>
        <source>Componenta</source>
        <translation>Component</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="49"/>
        <source>Fiducial</source>
        <translation>Fiducial</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="72"/>
        <source>Salveaza</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="82"/>
        <source>Renunta</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="85"/>
        <source>Esc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BoardConfig</name>
    <message>
        <location filename="boardconfig.ui" line="14"/>
        <source>Definire caracteristici placa</source>
        <translation>Define board specifics</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="23"/>
        <source>&amp;Lumini</source>
        <translation>&amp;Lights</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="318"/>
        <source>&amp;Întârziere</source>
        <oldsource>Întârzier&amp;e</oldsource>
        <translation>D&amp;elay</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="127"/>
        <source>Număr imagini folosite pentru testarea plăcii</source>
        <translation>Number of images used to test the board</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="168"/>
        <location filename="boardconfig.ui" line="218"/>
        <source>Selecție imagine curentă</source>
        <translation>Select current image</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="171"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="190"/>
        <source>Imagine curenta</source>
        <translation>Current image</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="199"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="221"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="230"/>
        <source>Denu&amp;mire placa</source>
        <oldsource>Denumire placa</oldsource>
        <translation>Board na&amp;me</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="240"/>
        <source>Nr. Placi</source>
        <translation>Nr. of boards</translation>
    </message>
    <message>
        <source>&amp;Clasificator</source>
        <translation type="vanished">&amp;Classifier</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="112"/>
        <source>&amp;Aliniere</source>
        <translation>&amp;Alingment</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="346"/>
        <source>trigger extern</source>
        <translation>external trigger</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="404"/>
        <source>Alegeți clasificatorul pentru găsirea plăcii</source>
        <translation>Choose the classifier used to find the board</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="330"/>
        <source>Întârziere între setarea luminilor și capturarea imaginii</source>
        <translation>Delay between setting the lights and capturing the image</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="333"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="59"/>
        <source>&amp;Imagini</source>
        <translation>&amp;Images</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="69"/>
        <source>Cod &amp;test</source>
        <translation>Test&amp;code</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="33"/>
        <source>Completați dacă codul de test e diferit de cel standard din fișierul de configurare</source>
        <translation>Fill in the test code if a different a different test code from the one defined in the configuration file is needed</translation>
    </message>
    <message>
        <source>Lumini</source>
        <translation type="vanished">Lights</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="263"/>
        <source>jos</source>
        <translation>down</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="293"/>
        <source>sus</source>
        <translation>up</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="273"/>
        <source>mijloc</source>
        <translation>center</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="283"/>
        <source>activare</source>
        <translation>enable</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="247"/>
        <source>Salvează</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="254"/>
        <source>Renunță</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="92"/>
        <source>Clasificator</source>
        <translation>Classifier</translation>
    </message>
    <message>
        <source>Aliniere</source>
        <translation type="vanished">Alignment</translation>
    </message>
    <message>
        <source>Salveaza</source>
        <translation type="vanished">Save</translation>
    </message>
    <message>
        <source>Renunta</source>
        <translation type="vanished">Cancel</translation>
    </message>
</context>
<context>
    <name>BoardSelect</name>
    <message>
        <location filename="boardselect.ui" line="14"/>
        <source>Configurare </source>
        <translation>Configure </translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="46"/>
        <source>Alege &amp;placa</source>
        <translation>Select &amp;board</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="144"/>
        <source>Afișeaza ferestrele de căutare</source>
        <translation>Display search windows</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="63"/>
        <source>fiducial</source>
        <translation>fiducal</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="27"/>
        <source>Caută</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="66"/>
        <source>Afișează puncte reper aliniere</source>
        <translation>Show fiducials</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="93"/>
        <source>Sal&amp;vare automată imagini</source>
        <translation>Autimaticaly sa&amp;ve images</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="104"/>
        <source>Nu</source>
        <translation>none</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="109"/>
        <source>Doar cele fail</source>
        <translation>failed images</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="114"/>
        <source>Toate</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="126"/>
        <source>&amp;în directorul</source>
        <translation>&amp;into folder</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="151"/>
        <source>Setari</source>
        <translation>Settings</translation>
    </message>
</context>
<context>
    <name>BrdInfo</name>
    <message>
        <location filename="brdinfo.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="brdinfo.ui" line="79"/>
        <source>Zoom Slider</source>
        <translation>Zoom Slider</translation>
    </message>
    <message>
        <location filename="brdinfo.ui" line="157"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="26"/>
        <source>placa</source>
        <translation>board</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="27"/>
        <source>clasificator</source>
        <translation>classifier</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="28"/>
        <source>suprafață clasificator</source>
        <translation>classifier area</translation>
    </message>
    <message>
        <source>DNN</source>
        <translation type="vanished">DNN</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="29"/>
        <source>dnn</source>
        <translation>dnn</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="30"/>
        <source>poziționare</source>
        <translation>location</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="31"/>
        <source>validator culoare</source>
        <translation>color validator</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="32"/>
        <source>validator nr. Serie</source>
        <translation></translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="33"/>
        <source>nr. imagini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="53"/>
        <source>lumini</source>
        <translation>lights</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="54"/>
        <source>nemodificat</source>
        <translation>unchanged</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="93"/>
        <source>Print error</source>
        <translation>Print error</translation>
    </message>
    <message>
        <location filename="brdinfo.cpp" line="93"/>
        <source>Nu a fost gasită nici o imprimantă.</source>
        <translation>No printer found.</translation>
    </message>
</context>
<context>
    <name>CameraConfig</name>
    <message>
        <location filename="cameraconfig.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="332"/>
        <source>&amp;Tip Camera</source>
        <translation>Camera &amp;Type</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="306"/>
        <source>Imagine</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="53"/>
        <location filename="cameraconfig.ui" line="140"/>
        <source>Ca&amp;mera</source>
        <translation>Ca&amp;mera</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="30"/>
        <location filename="cameraconfig.ui" line="153"/>
        <location filename="cameraconfig.ui" line="286"/>
        <source>Re&amp;zoluție</source>
        <translation>Re&amp;solution</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="40"/>
        <location filename="cameraconfig.ui" line="198"/>
        <location filename="cameraconfig.ui" line="256"/>
        <source>&amp;Nr. Imagini</source>
        <translation>Images &amp;qty.</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="80"/>
        <location filename="cameraconfig.ui" line="218"/>
        <source>Nr. img. i&amp;gnorate</source>
        <translation>I&amp;gnored images</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="93"/>
        <source>Adresa</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="100"/>
        <source>http://192.168.43.1:8080/photo.jpg</source>
        <translation>http://192.168.43.1:8080/photo.jpg</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="113"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aceasta nu e o cameră reală.&lt;/p&gt;&lt;p&gt;E folosită pentru încercări, imaginile fiind incărcate de pe disc.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is not a real camera.&lt;/p&gt;&lt;p&gt;It&apos;s used for trials, the images are loaded from disk.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="235"/>
        <source>&amp;setare manuală</source>
        <translation>manual &amp;setting</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="246"/>
        <source>Nr. img. &amp;ignorate</source>
        <translation>&amp;Ignored img. qty.</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="130"/>
        <source>API</source>
        <translation>API</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="342"/>
        <source>Încearcă</source>
        <translation>Try</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="71"/>
        <source>camera web</source>
        <translation>Webcam</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="72"/>
        <source>IP Webcam</source>
        <translation>Webcam IP</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="73"/>
        <source>imagini disc</source>
        <translation>Disk images</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="75"/>
        <source>camera OPENCV</source>
        <translation>OPENCV camera</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="78"/>
        <source>RASPICAM</source>
        <translation>RASPICAM</translation>
    </message>
</context>
<context>
    <name>ClassifierPicker</name>
    <message>
        <location filename="classifierpicker.ui" line="14"/>
        <source>Alegeți clasificatorul</source>
        <translation>Pick the classifier</translation>
    </message>
    <message>
        <location filename="classifierpicker.ui" line="23"/>
        <source>Componentă</source>
        <translation>Component</translation>
    </message>
    <message>
        <location filename="classifierpicker.ui" line="40"/>
        <source>Tip componentă</source>
        <translation>Component type</translation>
    </message>
    <message>
        <location filename="classifierpicker.ui" line="57"/>
        <source>Clasificator</source>
        <translation>Classifier</translation>
    </message>
    <message>
        <location filename="classifierpicker.ui" line="82"/>
        <source>Acceptă</source>
        <translation>Accept</translation>
    </message>
    <message>
        <location filename="classifierpicker.ui" line="92"/>
        <source>Renunță</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>CompDef</name>
    <message>
        <location filename="compdef.cpp" line="1902"/>
        <location filename="compdef.cpp" line="1957"/>
        <location filename="compdef.cpp" line="2184"/>
        <source>eroare</source>
        <translation>error</translation>
    </message>
    <message>
        <location filename="compdef.cpp" line="1902"/>
        <location filename="compdef.cpp" line="1957"/>
        <location filename="compdef.cpp" line="2184"/>
        <source>nu se poate deschide baza de date </source>
        <translation>can&apos;t open the database </translation>
    </message>
</context>
<context>
    <name>CompDefDialog</name>
    <message>
        <location filename="compdefdialog.ui" line="14"/>
        <source>Definire parametri componenta</source>
        <translation>Define component parameters</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1800"/>
        <source>Oglindire orizontala exemple pozitive</source>
        <translation>positive samples horizontal mirroring</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="55"/>
        <source>Oglindire verticala exemple pozitive</source>
        <translation>positive samples vertical mirroring</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="75"/>
        <source>Suprafata &amp;minima</source>
        <oldsource>Suprafata minima</oldsource>
        <translation>&amp;minimal area</translation>
    </message>
    <message>
        <source>Dublare rezolutie/interpolare</source>
        <translation type="vanished">resolution doubling/interpolation</translation>
    </message>
    <message>
        <source>Denumire Componenta</source>
        <translation type="vanished">componant name</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="96"/>
        <source>FHOG</source>
        <translation>FHOG</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="99"/>
        <source>Histogram of oriented gradients</source>
        <translation>Histogram of oriented gradients</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="117"/>
        <source>Invatare clasificator</source>
        <translation>Train classifier</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="124"/>
        <source>Prag acceptare</source>
        <translation>acceptance threshold</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="166"/>
        <source>Parametru &amp;C pentru SVM</source>
        <translation>SVM &amp;C parameter</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="176"/>
        <source>valori mai mici fac clasificatorul mai permisiv</source>
        <translation>lower values make the classifier more permissive</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="195"/>
        <source>Parametru &amp;eps pentru SVM</source>
        <translation>SVM &amp;eps parameter</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="224"/>
        <source>Numar fire e&amp;xecutie SVM</source>
        <translation>&amp;number of training threads</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="261"/>
        <source>DNN</source>
        <translation>DNN</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="264"/>
        <source>Deep Neural Network</source>
        <translation>Deep Neural Network</translation>
    </message>
    <message>
        <source>pasi fără progres</source>
        <translation type="vanished">steps without progress</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="417"/>
        <location filename="compdefdialog.ui" line="487"/>
        <source>număr mostre de imagine pentru un pas de învățare</source>
        <translation>minibatch size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="346"/>
        <source>rată învățare</source>
        <translation>learning rate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="465"/>
        <location filename="compdefdialog.ui" line="579"/>
        <source>număr pași de învățare după care se modifică numărul de mostre pentru un pas</source>
        <oldsource>număr pași de învățare pentru care se după care se modifică numărul de mostre pentru un pas</oldsource>
        <translation>number of steps after which the minibatch size is mofified</translation>
    </message>
    <message>
        <source>dim. lot</source>
        <translation type="vanished">minibatch size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="391"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;număr mostre de imagine pentru un pas de învățare&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>number of steps after which the minibatch size is mofified</translation>
    </message>
    <message>
        <source>dim. mostra</source>
        <translation type="vanished">image chip size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="663"/>
        <source>de la zero</source>
        <translation>from scratch</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="668"/>
        <source>reînvâțare</source>
        <translation>relearn</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="673"/>
        <source>folosind</source>
        <translation>using</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;dimensiune orizontală a mostrelor de imagine folosite la învățare&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>image chip width</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="322"/>
        <location filename="compdefdialog.ui" line="1710"/>
        <source>învățare</source>
        <translation>train</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="285"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;dimensiune verticală a mostrelor de imagine folosite la învățare&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>image chip height</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="617"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;factor de multiplicare pentru reducerea ratei de învățare după efectuarea numărului de pași făra progres&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>learning rate shrink factor</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="455"/>
        <source>rotație</source>
        <translation>rotation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="381"/>
        <source>modificare culori</source>
        <translation>disturb colors</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="356"/>
        <source>număr de pași fără progres după care se reduce rata de învățare</source>
        <translation>training steps without progress after which the learning rate is shrunken</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="235"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="240"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="245"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="253"/>
        <source>Număr orientări</source>
        <translation>Number of orientations</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="336"/>
        <source>dimensiune mostră</source>
        <translation>sample size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="375"/>
        <source>alterare aleatorie culoare mostre pentru crestere varietate</source>
        <translation>randomly alter sample colors to increase variety</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="407"/>
        <source>dimensiune lot</source>
        <translation>batch size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="433"/>
        <source>rată de învățare inițială</source>
        <translation>initial learning rate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="506"/>
        <source>rată de învățare minimă</source>
        <translation>minumal learning rate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="546"/>
        <source>salvare set date</source>
        <translation>save dataset</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="553"/>
        <source>rotire aleatorie mostre pentru crestere varietate</source>
        <translation>max random rotation angle to increase variety</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="569"/>
        <source>pași fără progres</source>
        <translation>steps without progress</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="636"/>
        <source>dezechilibru</source>
        <translation>bias</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="646"/>
        <source>Favorizare exemple pozitive. Uneori e nevoie de aceasta pentru a face posibilă învățarea</source>
        <translation>Favor positive samples. Sometimes this is necessary in order to obtain convergence</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="678"/>
        <source>continuare</source>
        <translation>continue</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="686"/>
        <source>mod învățare</source>
        <translation>learning mode</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="696"/>
        <source>Poziționare mai exactă, îngreunează învățarea</source>
        <translation>Learn bounding box regression - makes learning harder</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="706"/>
        <source>Număr filtre</source>
        <translation>Filters</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="714"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="719"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="724"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="729"/>
        <source>40</source>
        <translation>40</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="734"/>
        <source>48</source>
        <translation>48</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="739"/>
        <source>56</source>
        <translation>56</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="744"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="753"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="778"/>
        <source>export DNN</source>
        <translation>export DNN</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="785"/>
        <source>Import Autoinspect</source>
        <translation>Import Autoinspect</translation>
    </message>
    <message>
        <source>import retea neuronala</source>
        <translation type="vanished">Import neural network</translation>
    </message>
    <message>
        <source>export retea neuronala</source>
        <translation type="vanished">Export neural network</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="875"/>
        <source>culoare</source>
        <translation>color</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="899"/>
        <location filename="compdefdialog.ui" line="928"/>
        <location filename="compdefdialog.ui" line="969"/>
        <location filename="compdefdialog.ui" line="1001"/>
        <location filename="compdefdialog.ui" line="1169"/>
        <location filename="compdefdialog.ui" line="1315"/>
        <source>toleranță</source>
        <translation>tolerance</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="912"/>
        <location filename="compdefdialog.ui" line="1242"/>
        <source>Nuanță</source>
        <translation>Hue</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="988"/>
        <location filename="compdefdialog.ui" line="1023"/>
        <location filename="compdefdialog.ui" line="1045"/>
        <location filename="compdefdialog.ui" line="1080"/>
        <location filename="compdefdialog.ui" line="1137"/>
        <location filename="compdefdialog.ui" line="1191"/>
        <source>deviație standard</source>
        <translation>standard deviation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1058"/>
        <location filename="compdefdialog.ui" line="1223"/>
        <source>Intensitate</source>
        <translation>value</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1118"/>
        <location filename="compdefdialog.ui" line="1447"/>
        <source>Verde</source>
        <translation>Green</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1156"/>
        <location filename="compdefdialog.ui" line="1274"/>
        <source>Albastru</source>
        <translation>Blue</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1204"/>
        <location filename="compdefdialog.ui" line="1375"/>
        <source>Roșu</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1261"/>
        <location filename="compdefdialog.ui" line="1353"/>
        <source>Saturație</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1289"/>
        <source>Activare validator</source>
        <translation>Enable validator</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1296"/>
        <source>Procent din dimensiunea componentei</source>
        <translation>percentage of component size</translation>
    </message>
    <message>
        <source>Aplică</source>
        <translation type="vanished">Apply</translation>
    </message>
    <message>
        <source>R</source>
        <translation type="vanished">R</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1378"/>
        <source>RVAL</source>
        <translation>RVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1026"/>
        <source>RDEV</source>
        <translation>RDEV</translation>
    </message>
    <message>
        <source>G</source>
        <translation type="vanished">G</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1450"/>
        <source>GVAL</source>
        <translation>GVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1083"/>
        <source>GDEV</source>
        <translation>GDEV</translation>
    </message>
    <message>
        <source>B</source>
        <translation type="vanished">B</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1159"/>
        <source>BVAL</source>
        <translation>BVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1194"/>
        <source>BDEV</source>
        <translation>BDEV</translation>
    </message>
    <message>
        <source>H</source>
        <translation type="vanished">H</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1245"/>
        <source>HVAL</source>
        <translation>HVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1140"/>
        <source>HDEV</source>
        <translation>HDEV</translation>
    </message>
    <message>
        <source>S</source>
        <translation type="vanished">S</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1264"/>
        <source>SVAL</source>
        <translation>SVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="991"/>
        <source>SDEV</source>
        <translation>SDEV</translation>
    </message>
    <message>
        <source>V</source>
        <translation type="vanished">V</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1226"/>
        <source>VVAL</source>
        <translation>VVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1048"/>
        <source>VDEV</source>
        <translation>VDEV</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1780"/>
        <source>De&amp;numire Componenta</source>
        <translation>Component &amp;name</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="29"/>
        <source>Dublare re&amp;zolutie/interpolare</source>
        <translation>resolution &amp;doubling/interpolation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1306"/>
        <source>Calculează</source>
        <translation>Calculate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="699"/>
        <location filename="compdefdialog.ui" line="1492"/>
        <source>poziționare</source>
        <translation>location</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="792"/>
        <source>ștergere DNN</source>
        <translation>delete DNN</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="799"/>
        <source>export poziționare</source>
        <translation>export location</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="806"/>
        <source>import FHOG</source>
        <translation>import FHOG</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="813"/>
        <source>export FHOG</source>
        <translation>export FHOG</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="820"/>
        <source>ștergere FHOG</source>
        <translation>delete FHOG</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="827"/>
        <source>ștergere poziționare</source>
        <translation>delete location</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="834"/>
        <source>import poziționare</source>
        <translation>import location</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="854"/>
        <source>import DNN</source>
        <translation>import DNN</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1524"/>
        <source>tree depth</source>
        <translation>tree depth</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1544"/>
        <source>nu</source>
        <translation>nu</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1570"/>
        <source>cascade depth</source>
        <translation>cascade depth</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1577"/>
        <source>recomandat 6-18</source>
        <translation>reccomended 6-18</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1593"/>
        <source>feature poolsize</source>
        <translation>feature poolsize</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1600"/>
        <source>viteză vs acuratețe</source>
        <translation>speed vs accuracy</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1619"/>
        <source>num test splits</source>
        <translation>num test splits</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1626"/>
        <source>viteză vs acuratețe la învățare</source>
        <translation>speed vs accuracy while training</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1645"/>
        <source>oversampling amount</source>
        <translation>oversampling amount</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1665"/>
        <source>oversampling translation jitter</source>
        <translation>oversampling translation jitter</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="329"/>
        <location filename="compdefdialog.ui" line="1690"/>
        <source>activare</source>
        <translation>enable</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1718"/>
        <source>ocupat</source>
        <translation>busy</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1736"/>
        <source>timeLabel</source>
        <translation>timeLabel</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1754"/>
        <source>Memorie date folosita</source>
        <translation>used memory</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="211"/>
        <source>clasificator DNN</source>
        <translation>DNN classifier</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="211"/>
        <source>Învățarea nu are sens, deoarece DNN e activat. Continuați?</source>
        <translation>Training is useless, because DNN is not enabled. Continue?</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="234"/>
        <source>director clasificator autoinspect</source>
        <translation>autoinspect classifier folder</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="342"/>
        <source>Nume fișier unde se va salva setul de date</source>
        <translation>dataset filename</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="343"/>
        <source>fișiere xml (*.xml);;Toate fișierele (*.*)</source>
        <translation>xml files (*.xml);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="702"/>
        <source>clasificator FHOG</source>
        <translation>FHOG classifier</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="702"/>
        <source>Învățarea nu are sens, deoarece DNN nu e activat. Continuați?</source>
        <translation>Training is useless, because DNN is not enabled. Continue?</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="722"/>
        <source>Nume fișier de încărcat</source>
        <translation>File to be loaded</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="723"/>
        <location filename="compdefdialog.cpp" line="734"/>
        <location filename="compdefdialog.cpp" line="759"/>
        <location filename="compdefdialog.cpp" line="776"/>
        <location filename="compdefdialog.cpp" line="787"/>
        <location filename="compdefdialog.cpp" line="798"/>
        <source>fișiere dat (*.dat);;Toate fișierele (*.*)</source>
        <translation>dat files (*.dat);;All files (*.*</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="727"/>
        <location filename="compdefdialog.cpp" line="738"/>
        <location filename="compdefdialog.cpp" line="763"/>
        <location filename="compdefdialog.cpp" line="780"/>
        <location filename="compdefdialog.cpp" line="791"/>
        <location filename="compdefdialog.cpp" line="802"/>
        <source>eroare</source>
        <translation>error</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="727"/>
        <source>Nu s-a putut încărca rețeaua neuronala din fișierul </source>
        <translation>Unable to load he neural netwok from he file </translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="733"/>
        <source>Nume fișier pentru salvare rețea neuronală</source>
        <translation>File name to save the neual network</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="738"/>
        <source>Nu s-a putut salva rețeaua neuronala în fișierul </source>
        <translation>Unable to save the neural network to the file </translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="758"/>
        <source>Nume fișier pentru salvare clasificator FHOG</source>
        <translation>File name for FHOG classifier</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="763"/>
        <source>Nu s-a putut salva clasificatorul FHOG în fișierul </source>
        <translation>Unable to save the FHOG classifier to the file </translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="775"/>
        <source>Nume fișier pentru salvare instrument poziționare</source>
        <translation>File name to save the the location tool to</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="780"/>
        <source>Nu s-a putut salva intrumentul de poziționare în fișierul </source>
        <translation>Unable to save the location tool to the file </translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="786"/>
        <source>Nume fișier FHOG de încărcat</source>
        <translation>Filename of FHOG classifier to be loaded</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="791"/>
        <source>Nu s-a putut încărca clasificatorul FHOG din fișierul </source>
        <translation>Unable to lod FHOG classifier from the file </translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="797"/>
        <source>Nume fișier instrument poziționare de încărcat</source>
        <translation>Filename for location tool to be loaded</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="802"/>
        <source>Nu s-a putut încărca instrumentrul de poziționare din fișierul </source>
        <translation>Unable to load the location tool from the file </translation>
    </message>
</context>
<context>
    <name>CompInfoModel</name>
    <message>
        <location filename="brdinfo.h" line="96"/>
        <source>componenta</source>
        <translation>component</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="98"/>
        <source>tip componenta</source>
        <translation>component type</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="100"/>
        <source>clasificator</source>
        <translation>classifier</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="102"/>
        <source>clasă</source>
        <translation>class</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="104"/>
        <source>prag</source>
        <translation>threshold</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="106"/>
        <source>suprafața</source>
        <translation>area</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="108"/>
        <source>dnn</source>
        <translation>dnn</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="110"/>
        <source>poziționare</source>
        <translation>location</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="112"/>
        <source>validator culoare</source>
        <translation>color validator</translation>
    </message>
    <message>
        <location filename="brdinfo.h" line="114"/>
        <source>mostră</source>
        <translation>sample</translation>
    </message>
</context>
<context>
    <name>CompStatModel</name>
    <message>
        <location filename="compstatmodel.cpp" line="264"/>
        <source>corect</source>
        <translation>correct</translation>
    </message>
    <message>
        <location filename="compstatmodel.cpp" line="265"/>
        <source>orientare</source>
        <translation>orientation</translation>
    </message>
    <message>
        <location filename="compstatmodel.cpp" line="266"/>
        <source>suplimentar</source>
        <translation>extra</translation>
    </message>
    <message>
        <location filename="compstatmodel.cpp" line="267"/>
        <source>prag</source>
        <translation>threshold</translation>
    </message>
</context>
<context>
    <name>ComponentPathCorrection</name>
    <message>
        <location filename="componentpathcorrection.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="22"/>
        <source>Cale inițială</source>
        <translation>Initial path</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="36"/>
        <source>Cale nouă</source>
        <translation>New path</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="53"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="81"/>
        <source>Actualizează</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="88"/>
        <source>Renunță</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.cpp" line="96"/>
        <source>Fisiere Imagine(*.png *.jpg *.bmp *.jpeg);;Toate fișierele(*.*)</source>
        <translation>Image files(*.png *.jpg *.bmp *.jpeg);;All files(*.*)</translation>
    </message>
</context>
<context>
    <name>FileDeleter</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Form</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="14"/>
        <source>Ștergere fișiere</source>
        <translation>Delete Files</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="39"/>
        <source>imagini</source>
        <translation>images</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="57"/>
        <source>&amp;Director</source>
        <translation>Fol&amp;der</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="70"/>
        <source>Selectare director cu imagini de analizat</source>
        <translation>Select the folder with images to be analyzed</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="76"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="94"/>
        <location filename="filedeleter.ui" line="208"/>
        <source>Șterge</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="114"/>
        <source>0/0</source>
        <translation>0/0</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="134"/>
        <location filename="filedeleter.ui" line="228"/>
        <source>Închide</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="142"/>
        <source>clasificatori</source>
        <translation>classifiers</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="170"/>
        <source>Plăcu unde se folosește clasificatorul</source>
        <translation>Boards using the classifier</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="173"/>
        <source>Plăci</source>
        <translation>Boards</translation>
    </message>
    <message>
        <location filename="filedeleter.cpp" line="69"/>
        <source>director de analizat</source>
        <translation>Folder to be analyzed</translation>
    </message>
    <message>
        <location filename="filedeleter.cpp" line="202"/>
        <source>imagine folosită</source>
        <translation>used image</translation>
    </message>
    <message>
        <location filename="filedeleter.cpp" line="202"/>
        <source>Imaginea </source>
        <translation>The image </translation>
    </message>
    <message>
        <location filename="filedeleter.cpp" line="202"/>
        <source> e folosită. Sigur doriți ștergerea imaginii?</source>
        <translation> is in use. Are you sureyou want to delete the image?</translation>
    </message>
</context>
<context>
    <name>IOControl</name>
    <message>
        <location filename="iocontrol.cpp" line="110"/>
        <location filename="iocontrol.cpp" line="147"/>
        <source>Lumini</source>
        <translation>Lights</translation>
    </message>
    <message>
        <location filename="iocontrol.cpp" line="110"/>
        <source>Acest program e configurat pentru aprinderea automată a luminilor, dar acest lucru nu funcționează.
Verificați conexiunea USB pentru controlul luminilor.
Verificați să nu fie pornită altă aplicație care folosește portul pentru controlul luminilor.</source>
        <translation>This program has been configured to automatically turn on the lights, but this is not working.\nCheck the USB connection for lights control.\nCheck for other applications which might use the lights control port.</translation>
    </message>
    <message>
        <location filename="iocontrol.cpp" line="147"/>
        <source>Acest program e configurat pentru aprinderea automată a luminilor, dar acest lucru nu funcționează.
Verificați conexiunea USB pentru controlul luminilor.
Verificați alimentarea sistemului de iluminare.</source>
        <translation>This program has been configured to automatically turn on the lights, but this is not working.\nCheck the USB connection for lights control.\nCheck the power for the lighting system.</translation>
    </message>
</context>
<context>
    <name>ImageStatModel</name>
    <message>
        <location filename="testdebugwidget.h" line="209"/>
        <source>nume</source>
        <translation>name</translation>
    </message>
    <message>
        <location filename="testdebugwidget.h" line="211"/>
        <source>scor</source>
        <translation>score</translation>
    </message>
    <message>
        <location filename="testdebugwidget.h" line="213"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="testdebugwidget.h" line="215"/>
        <source>y</source>
        <translation>y</translation>
    </message>
    <message>
        <location filename="testdebugwidget.h" line="217"/>
        <source>scala</source>
        <translation>scale</translation>
    </message>
    <message>
        <location filename="testdebugwidget.h" line="219"/>
        <source>alternativa</source>
        <translation>alternative</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="40"/>
        <source>Numele programului de inspecție optică de încărcat</source>
        <translation>Name of the visual inspection program to be loaded</translation>
    </message>
    <message>
        <location filename="main.cpp" line="41"/>
        <source>Fișier bază de date sqlite de deschis</source>
        <translation>Sqlite database file to be opened</translation>
    </message>
    <message>
        <location filename="main.cpp" line="43"/>
        <source>Limbă folosită pentru interfața programului</source>
        <translation>User interface language</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <source>Fișier de configurare de deschis</source>
        <translation>Configuration file to be opened</translation>
    </message>
    <message>
        <location filename="boardcomptype.cpp" line="295"/>
        <source>Componenta</source>
        <translation>Component</translation>
    </message>
    <message>
        <location filename="boardcomptype.cpp" line="296"/>
        <source>Fiducial</source>
        <translation>Fiducial</translation>
    </message>
    <message>
        <location filename="boardcomptype.cpp" line="297"/>
        <source>Cod Bare</source>
        <translation>Barcode</translation>
    </message>
    <message>
        <location filename="boardcomptype.cpp" line="298"/>
        <source>Subcomponenta</source>
        <translation>Subcomponent</translation>
    </message>
    <message>
        <location filename="boardcomptype.cpp" line="299"/>
        <source>Grup</source>
        <translation>Group</translation>
    </message>
</context>
<context>
    <name>RegExTester</name>
    <message>
        <location filename="regextester.ui" line="14"/>
        <source>RegExpTester</source>
        <translation>RegExpTester</translation>
    </message>
    <message>
        <location filename="regextester.ui" line="28"/>
        <source>validator</source>
        <translation>validator</translation>
    </message>
    <message>
        <location filename="regextester.ui" line="44"/>
        <source>Exemplu (w{6})____$1</source>
        <translation>Example (w{6})____$1</translation>
    </message>
    <message>
        <location filename="regextester.ui" line="57"/>
        <source>nr. serie</source>
        <translation>serial number</translation>
    </message>
    <message>
        <location filename="regextester.ui" line="88"/>
        <source>Acceptă</source>
        <translation>Accept</translation>
    </message>
    <message>
        <location filename="regextester.ui" line="108"/>
        <source>Renunță</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="regextester.cpp" line="55"/>
        <source>validator: </source>
        <translation>validator: </translation>
    </message>
    <message>
        <location filename="regextester.cpp" line="56"/>
        <source>extractor: </source>
        <translation>extractor: </translation>
    </message>
    <message>
        <location filename="regextester.cpp" line="58"/>
        <source>extractor invalid!!!</source>
        <translation>extractor not valid!!!</translation>
    </message>
    <message>
        <location filename="regextester.cpp" line="59"/>
        <source>potrivire: </source>
        <translation>match: </translation>
    </message>
    <message>
        <location filename="regextester.cpp" line="59"/>
        <source>da</source>
        <translation>yes</translation>
    </message>
    <message>
        <location filename="regextester.cpp" line="59"/>
        <source>nu</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="regextester.cpp" line="62"/>
        <source>grup</source>
        <translation>group</translation>
    </message>
</context>
<context>
    <name>TestDebugWidget</name>
    <message>
        <source>componenta</source>
        <translation type="vanished">component</translation>
    </message>
    <message>
        <source>scor</source>
        <translation type="vanished">score</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="83"/>
        <source>director cu imagini de analizat</source>
        <translation>folder with images to be analyzed</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="531"/>
        <source>Confirmare ștergere fișier</source>
        <translation>Acknowledge file deletion</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="531"/>
        <source>Sigur doriți ștergerea definitivă a fișierului </source>
        <translation>Are you sure you want to permenently delete the file </translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="406"/>
        <location filename="testdebugwidget.cpp" line="900"/>
        <source>Salvare copie imagine. Apăsați CANCEL pentru a nu copia nimic</source>
        <translation>Save a copy of the image. Press CANCEL to not copy anything</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="65"/>
        <source>adaugă la placă</source>
        <translation>Add to board</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="70"/>
        <source>încarcă program placă</source>
        <translation>Load program for board</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="386"/>
        <location filename="testdebugwidget.cpp" line="397"/>
        <location filename="testdebugwidget.cpp" line="401"/>
        <source>clasificator neidentificat</source>
        <translation>Can&apos;t identify classifier</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="407"/>
        <location filename="testdebugwidget.cpp" line="901"/>
        <source>Imagini (*.png *.jpg *.jpeg);; Toate fișierele (*.*)</source>
        <translation>Images (*.png *.jpg *.jpeg);; All files (*.*)</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="413"/>
        <location filename="testdebugwidget.cpp" line="858"/>
        <source>copiere abandonată</source>
        <translation>copy aborted</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="413"/>
        <source>Nu se poate copia fișierul, deoarece există deja un fișier cu același nume</source>
        <translation>The file can not pe copied, because a file with the same name already exists</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="417"/>
        <location filename="testdebugwidget.cpp" line="911"/>
        <source>copiere nereușită</source>
        <translation>copy failed</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="417"/>
        <source>Nu s-a putut copia fișierul</source>
        <translation>The file could not be copied</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="858"/>
        <source>Nu se poate identifica clasificatorul</source>
        <translation>Unable to identify the classifier</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="886"/>
        <source>salvare nereușită</source>
        <translation>failure to save</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="886"/>
        <source>Nu se poate salva fișierul, deoarece componenta e in afara imaginii</source>
        <translation>Can&apos;t save the file, because the component is outside the image</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="907"/>
        <source>salvare abandonată</source>
        <translation>saving cancelled</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="907"/>
        <source>Nu se poate salva fișierul, deoarece există deja un fișier cu același nume. Va fi folosit fișierul existent!</source>
        <translation>Can&apos;t save the file because a file with the same name already exists. The existing file will be used!</translation>
    </message>
    <message>
        <location filename="testdebugwidget.cpp" line="911"/>
        <source>Nu s-a putut salva fișierul</source>
        <translation>Can&apos;t save the file</translation>
    </message>
    <message>
        <source>Nu se poate copia fișierul, deoarece există deja un fișier cu același nume. Va fi folosit fișierul existent!</source>
        <translation type="vanished">The file can&apos;t be copied, because a file with the same name already exists. The existing file will be used!</translation>
    </message>
</context>
<context>
    <name>TestInterface</name>
    <message>
        <location filename="testinterface.ui" line="14"/>
        <source>Alegeti placa de inspectat</source>
        <translation>select board to be inspected</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="833"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="432"/>
        <source>13</source>
        <translation>13</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="714"/>
        <source>19</source>
        <translation>19</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="479"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="349"/>
        <source>PushButton</source>
        <translation>Button</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="529"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="922"/>
        <source>Placa</source>
        <translation>Board</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="868"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="912"/>
        <source>17</source>
        <translation>17</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="761"/>
        <source>23</source>
        <translation>23</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="645"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="551"/>
        <source>21</source>
        <translation>21</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="890"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="667"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="808"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="976"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="620"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="783"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="454"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="689"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="598"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="736"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="407"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="573"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="504"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="954"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="100"/>
        <source>editor plăci și componente</source>
        <translation>board and component editor</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="106"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="117"/>
        <source>încarcă imagine</source>
        <translation>Load image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="123"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="134"/>
        <source>salvează imagine</source>
        <translation>Save image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="140"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="151"/>
        <source>achiziționează imagine</source>
        <translation>acquire image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="157"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="171"/>
        <source>analiză imagine</source>
        <translation>analyze image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="177"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="188"/>
        <source>alegere placă</source>
        <translation>select board</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="194"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="218"/>
        <source>PASS</source>
        <translation>PASS</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="238"/>
        <source>Serie</source>
        <translation>Serial number</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="261"/>
        <source>Execută test</source>
        <translation>Run test</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="73"/>
        <source>configurează camera</source>
        <oldsource>configureaza camera</oldsource>
        <translation>configure camera</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="76"/>
        <source>achiziție continuă</source>
        <translation>Continuous acquire</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="80"/>
        <source>test arduino</source>
        <translation>Arduino test</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="239"/>
        <source>Fișiere Imagine (*.png *.jpg *.jpeg *.bmp);;Toate fișierele (*.*)</source>
        <translation>Image files(*.png *.jpg *.bmp *.jpeg);;All files(*.*)</translation>
    </message>
    <message>
        <source>Numele programului de inspecție optică de încărcat</source>
        <translation type="vanished">Name of the visual inspection program to be loaded</translation>
    </message>
    <message>
        <source>Fișier bază de date sqlite de deschis</source>
        <translation type="vanished">Sqlite database file to be opened</translation>
    </message>
    <message>
        <source>Limbă folosită pentru interfața programului</source>
        <translation type="vanished">User interface language</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="239"/>
        <source>Imagine</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="314"/>
        <source>fișier inexistent</source>
        <translation>File missing</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="315"/>
        <source>fișierul </source>
        <translation>the file </translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="316"/>
        <source> nu există. Se va folosi locația implicită pentru baza de date:</source>
        <translation> does not exist. The default location for the database will be used:</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="657"/>
        <source>Program cu citire număr serie din imagine. Orice introduceți aici va fi ignorat!</source>
        <translation>This program reads the serial number form the image. This field will be ignored!</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="943"/>
        <source>Aplicație veche</source>
        <translation>Old application</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="943"/>
        <source>Atenție, pentru aceste program există o versiune mai nouă. Rezultatele obținute ar putea fi eronate.</source>
        <translation>Warning, a new version of this programm exists! The results of running this programm might be wrong.</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1001"/>
        <source>Actualizare program</source>
        <translation>Update program</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1002"/>
        <source>Există o versiune nouă a programului de inspectie. Doriți reîncărcarea programului?</source>
        <translation>A new version of the inspection program is available. Do you want to reload?</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1179"/>
        <source>nu e incărcat nici un program</source>
        <translation>no program loaded</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1180"/>
        <location filename="testinterface.cpp" line="1189"/>
        <source>dacă doriți să verificați plăci, incărcați întâi un program de inspecție</source>
        <translation></translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1188"/>
        <source>incărcare program nereușită</source>
        <translation>program loading failed</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1341"/>
        <source>Codul de bare nu s-a putut citi</source>
        <translation>Unable to read the barcode</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1347"/>
        <location filename="testinterface.cpp" line="1377"/>
        <source>Numărul de serie </source>
        <translation>The serial number </translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1347"/>
        <location filename="testinterface.cpp" line="1377"/>
        <source> e incompatibil cu validatorul </source>
        <translation> does not match the validator </translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1354"/>
        <location filename="testinterface.cpp" line="1382"/>
        <source>număr serie invalid, eroare </source>
        <translation>invalid serial number, error </translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1371"/>
        <source>Codul de bare introdus este gol</source>
        <translation>The provided serial number is empty</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1388"/>
        <source>număr serie invalid, rezultatul pentru placa nu se va salva</source>
        <translation>invalid serial number, board result will not be saved</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1517"/>
        <source>Activare rapidă senzor</source>
        <translation>Unusual short sensor activation interval</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1517"/>
        <source>sigur doriți pornirea unui test?</source>
        <translation>Do you really want to start a test?</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="1595"/>
        <location filename="testinterface.cpp" line="1597"/>
        <source>eroare achiziție imagine</source>
        <translation>image acquisition error</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="widget.ui" line="20"/>
        <source>Widget</source>
        <translation>widget</translation>
    </message>
    <message>
        <location filename="widget.ui" line="93"/>
        <source>placa</source>
        <translation>board</translation>
    </message>
    <message>
        <location filename="widget.ui" line="147"/>
        <source>&amp;administrare</source>
        <translation>m&amp;anagement</translation>
    </message>
    <message>
        <location filename="widget.ui" line="207"/>
        <source>&amp;Sterge placa</source>
        <translation>&amp;Delete board</translation>
    </message>
    <message>
        <location filename="widget.ui" line="331"/>
        <source>&amp;Configurare</source>
        <translation>&amp;Configure</translation>
    </message>
    <message>
        <location filename="widget.ui" line="291"/>
        <source>&amp;Validator 
nr. serie</source>
        <translation>Serial number
&amp;Validator</translation>
    </message>
    <message>
        <source>Expresie regulată pentru validarea numerelor de serie, cum ar fi \w{6}|(?i)TESTA</source>
        <translation type="vanished">Regular expression for serial number validator, like \w{6}|(?i)TESTA</translation>
    </message>
    <message>
        <location filename="widget.ui" line="232"/>
        <source>&amp;Pornire
test</source>
        <translation>&amp;Start
test</translation>
    </message>
    <message>
        <location filename="widget.ui" line="254"/>
        <source>Imediat</source>
        <translation>Immediate</translation>
    </message>
    <message>
        <location filename="widget.ui" line="259"/>
        <source>Întârziat</source>
        <translation>Delayed</translation>
    </message>
    <message>
        <location filename="widget.ui" line="264"/>
        <source>Apăsare RUN</source>
        <translation>Press RUN</translation>
    </message>
    <message>
        <location filename="widget.ui" line="243"/>
        <source>&amp;Întârziere</source>
        <translation>&amp;Delay</translation>
    </message>
    <message>
        <location filename="widget.ui" line="318"/>
        <source>S&amp;alvare</source>
        <translation>S&amp;ave</translation>
    </message>
    <message>
        <location filename="widget.ui" line="197"/>
        <source>Placă &amp;nouă</source>
        <translation>&amp;New board</translation>
    </message>
    <message>
        <location filename="widget.ui" line="272"/>
        <source>Dezactivare
senzor start</source>
        <translation>Disable
start sensor</translation>
    </message>
    <message>
        <location filename="widget.ui" line="356"/>
        <source>&amp;referinta</source>
        <translation>&amp;reference</translation>
    </message>
    <message>
        <location filename="widget.ui" line="396"/>
        <source>&amp;Imagine</source>
        <translation>&amp;Image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="432"/>
        <source>&amp;Minima</source>
        <translation>&amp;Minumum</translation>
    </message>
    <message>
        <location filename="widget.ui" line="442"/>
        <source>Suprafata</source>
        <translation>Area</translation>
    </message>
    <message>
        <location filename="widget.ui" line="454"/>
        <location filename="widget.ui" line="1064"/>
        <location filename="widget.ui" line="1359"/>
        <source>Orientare</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="465"/>
        <location filename="widget.ui" line="1373"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="widget.ui" line="470"/>
        <location filename="widget.ui" line="1378"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="widget.ui" line="475"/>
        <location filename="widget.ui" line="1383"/>
        <location filename="widget.ui" line="1478"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="widget.ui" line="480"/>
        <location filename="widget.ui" line="1388"/>
        <location filename="widget.ui" line="1445"/>
        <source>V</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="widget.ui" line="490"/>
        <source>Ma&amp;xima</source>
        <translation>Ma&amp;ximum</translation>
    </message>
    <message>
        <location filename="widget.ui" line="500"/>
        <source>NumeImagine</source>
        <translation>ImageName</translation>
    </message>
    <message>
        <location filename="widget.ui" line="507"/>
        <source>Marcată</source>
        <translation>Current</translation>
    </message>
    <message>
        <location filename="widget.ui" line="536"/>
        <source>&amp;tip comp. </source>
        <translation>component &amp;type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="557"/>
        <source>Tip &amp;nou</source>
        <translation>&amp;new type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="567"/>
        <source>&amp;Sterge</source>
        <translation>&amp;Delete</translation>
    </message>
    <message>
        <location filename="widget.ui" line="574"/>
        <source>&amp;Nume Tip Componenta</source>
        <translation>Component type &amp;name</translation>
    </message>
    <message>
        <location filename="widget.ui" line="592"/>
        <source>C&amp;lasa</source>
        <translation>C&amp;lass</translation>
    </message>
    <message>
        <location filename="widget.ui" line="623"/>
        <source>Alternativa</source>
        <translation>Alternative</translation>
    </message>
    <message>
        <location filename="widget.ui" line="654"/>
        <source>&amp;Adauga</source>
        <translation>&amp;Add</translation>
    </message>
    <message>
        <location filename="widget.ui" line="661"/>
        <location filename="widget.ui" line="800"/>
        <source>Ster&amp;ge</source>
        <translation>&amp;Delete</translation>
    </message>
    <message>
        <location filename="widget.ui" line="700"/>
        <source>&amp;componente</source>
        <translation>&amp;components</translation>
    </message>
    <message>
        <location filename="widget.ui" line="836"/>
        <source>&amp;Nume</source>
        <translation>&amp;Name</translation>
    </message>
    <message>
        <location filename="widget.ui" line="771"/>
        <source>Tol. &amp;X</source>
        <translation>&amp;X tol.</translation>
    </message>
    <message>
        <location filename="widget.ui" line="740"/>
        <source>Toleranta pozitie orizontala, in procente</source>
        <translation>Horizontal position tollerance as percentage</translation>
    </message>
    <message>
        <location filename="widget.ui" line="886"/>
        <source>Tol. &amp;Y</source>
        <translation>&amp;Y tol.</translation>
    </message>
    <message>
        <location filename="widget.ui" line="873"/>
        <source>Toleranta pozitie verticala, in procente</source>
        <translation>Vertical position tollerance as percentage</translation>
    </message>
    <message>
        <location filename="widget.ui" line="753"/>
        <source>Tol. &amp;Scala</source>
        <translation>&amp;scale tol.</translation>
    </message>
    <message>
        <location filename="widget.ui" line="721"/>
        <source>Toleranta dimensiune, in procente</source>
        <translation>Scale tollerance, as percentage</translation>
    </message>
    <message>
        <location filename="widget.ui" line="826"/>
        <source>&amp;Orientare</source>
        <translation>&amp;Orientation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="803"/>
        <location filename="widget.ui" line="1312"/>
        <source>Del, Backspace</source>
        <translation>Del, Backspace</translation>
    </message>
    <message>
        <location filename="widget.ui" line="644"/>
        <source>Arată</source>
        <translation>Sho&amp;w</translation>
    </message>
    <message>
        <location filename="widget.ui" line="763"/>
        <source>Tip cod
bare</source>
        <translation>barcode
type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="856"/>
        <source>Comp.
părinte</source>
        <translation>Comp.
părinte</translation>
    </message>
    <message>
        <location filename="widget.ui" line="896"/>
        <source>Imagini</source>
        <translation>Images</translation>
    </message>
    <message>
        <location filename="widget.ui" line="909"/>
        <source>0000</source>
        <translation>0000</translation>
    </message>
    <message>
        <location filename="widget.ui" line="930"/>
        <source>componenta</source>
        <translation>component</translation>
    </message>
    <message>
        <location filename="widget.ui" line="933"/>
        <source>Dublu click pentru analiză utilizare imagini și clasificatori</source>
        <translation>Doubleclick to analyze image and classifier usage</translation>
    </message>
    <message>
        <location filename="widget.ui" line="978"/>
        <source>tip componenta</source>
        <translation>component type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1088"/>
        <source>Componentă nouă</source>
        <translation>new component</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1006"/>
        <source>Sterge componenta</source>
        <translation>Delete component</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1098"/>
        <source>Configurare</source>
        <translation>Configure</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1028"/>
        <source>aspect ratio</source>
        <translation>aspect ratio</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1041"/>
        <source>Raport latime/inaltime</source>
        <translation>width/height ratio</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1013"/>
        <source>Salvează</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="widget.ui" line="96"/>
        <source>Dublu click pentru afișare fereastră principală</source>
        <translation>Double click to show main window</translation>
    </message>
    <message>
        <location filename="widget.ui" line="308"/>
        <source>Expresie regulată pentru validarea numerelor de serie, cum ar fi \w{6}|(?i)TESTA
Apăsați ENTER pentru incercarea validatorului</source>
        <translation>Regular expression to validate srial numbers, likei \w{6}|(?i)TESTA\nPressi ENTER to try the validator</translation>
    </message>
    <message>
        <location filename="widget.ui" line="668"/>
        <source>nume alternativa</source>
        <translation>alternative name</translation>
    </message>
    <message>
        <location filename="widget.ui" line="678"/>
        <source>Test absență</source>
        <translation>Missing test</translation>
    </message>
    <message>
        <location filename="widget.ui" line="996"/>
        <source>Include date de la</source>
        <translation>Include data from</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1168"/>
        <source>varianta</source>
        <translation>items</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1194"/>
        <source>Tip marcaj</source>
        <translation>Marking type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1202"/>
        <location filename="widget.cpp" line="102"/>
        <source>Pozitiv</source>
        <translation>Positive</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1207"/>
        <location filename="widget.cpp" line="103"/>
        <source>Negativ</source>
        <translation>Negative</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1212"/>
        <location filename="widget.cpp" line="104"/>
        <source>Ignorare</source>
        <translation>Ignore</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1228"/>
        <source>debifeaza pentru incercare clasificator</source>
        <translation>unselect to try the classifier</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1234"/>
        <location filename="widget.cpp" line="884"/>
        <source>Imagine</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1267"/>
        <source>Sterge imaginea</source>
        <translation>Delete image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1270"/>
        <location filename="widget.ui" line="1309"/>
        <source>Sterge</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1277"/>
        <source>Adauga o imagine noua</source>
        <translation>Add a new image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1280"/>
        <source>Adauga</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1287"/>
        <source>Intreaga imagine e folosita ca exemplu negativ</source>
        <translation>The entire image can be used to extract negative samples</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1296"/>
        <source>implicit negativ</source>
        <translation>implicit negative</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1306"/>
        <source>Sterge marcajul curent</source>
        <translation>Delete current marking</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1419"/>
        <source>albastru</source>
        <translation>blue</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1422"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1429"/>
        <source>verde</source>
        <translation>green</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1435"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1442"/>
        <source>intensitate</source>
        <translation>intensity</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1452"/>
        <source>roșu</source>
        <translation>red</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1458"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1465"/>
        <source>nunșă</source>
        <translation>hue</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1468"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1475"/>
        <source>saturație</source>
        <translation>saturation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1576"/>
        <source>Analizează</source>
        <translation>Analyze</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1583"/>
        <source>Caută în imagine</source>
        <translation>Search the image</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1628"/>
        <source>corect</source>
        <translation>correct</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1635"/>
        <source>orientare</source>
        <translation>orientation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1642"/>
        <source>suplimentar</source>
        <translation>extra</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1649"/>
        <source>lipsă</source>
        <translation>missing</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1656"/>
        <source>imagine</source>
        <translation>image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1663"/>
        <source>toate</source>
        <translation>all</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1670"/>
        <location filename="widget.ui" line="1677"/>
        <location filename="widget.ui" line="1684"/>
        <location filename="widget.ui" line="1691"/>
        <location filename="widget.ui" line="1698"/>
        <location filename="widget.ui" line="1705"/>
        <location filename="widget.ui" line="1712"/>
        <location filename="widget.ui" line="1719"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="58"/>
        <source>copiază placă</source>
        <translation>copy board</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="63"/>
        <source>copiază componenta</source>
        <translation>copy component</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="68"/>
        <source>componente testate</source>
        <translation>tested components</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="71"/>
        <source>clasificator placă</source>
        <translation>board classifier</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="97"/>
        <source>indiferent</source>
        <translation>indifferent</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="98"/>
        <source>N-S,E-V</source>
        <translation>N-S,E-W</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="99"/>
        <source>N,E,S,V</source>
        <translation>N,E,S,W</translation>
    </message>
    <message>
        <source>Componenta</source>
        <translation type="vanished">Component</translation>
    </message>
    <message>
        <source>Fiducial</source>
        <translation type="vanished">Fiducial</translation>
    </message>
    <message>
        <source>Cod Bare</source>
        <translation type="vanished">Barcode</translation>
    </message>
    <message>
        <source>Subcomponenta</source>
        <translation type="vanished">Subcomponent</translation>
    </message>
    <message>
        <source>Grup</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="394"/>
        <location filename="widget.cpp" line="2334"/>
        <source>nu include</source>
        <translation>do not include</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="400"/>
        <location filename="widget.cpp" line="1206"/>
        <location filename="widget.cpp" line="1225"/>
        <source>componenta: </source>
        <translation>Component:</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="409"/>
        <location filename="widget.cpp" line="1222"/>
        <source>placa: </source>
        <translation>Board:</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="503"/>
        <location filename="widget.cpp" line="1127"/>
        <source>componentă modificată</source>
        <translation>component changed</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="504"/>
        <location filename="widget.cpp" line="1128"/>
        <source>componenta încărcată e de la </source>
        <translation>the loaded component is from </translation>
    </message>
    <message>
        <location filename="widget.cpp" line="506"/>
        <location filename="widget.cpp" line="1130"/>
        <location filename="widget.cpp" line="1275"/>
        <source> dar cea din baza de date e de la </source>
        <translation>while the one from the databse is from </translation>
    </message>
    <message>
        <location filename="widget.cpp" line="508"/>
        <source>
Doriți să continuați salvarea?</source>
        <translation>\nDo you want to save anyway?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="513"/>
        <location filename="widget.cpp" line="537"/>
        <source>avertisment salvare definitie</source>
        <translation>component save warning
</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="514"/>
        <source>Definiția a fost modificată. Doriți salvarea acesteia?</source>
        <translation>The component definition has been changed. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="538"/>
        <source>Definitia a fost modificata. Doriti salvarea acesteia?</source>
        <translation>The component definition has been changed. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="596"/>
        <source>imaginea </source>
        <translation>The image </translation>
    </message>
    <message>
        <location filename="widget.cpp" line="596"/>
        <source> este deja folosită</source>
        <translation> is already used</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="886"/>
        <source>Fisiere Imagine(*.png *.jpg *.bmp *.jpeg);;Toate fișierele(*.*)</source>
        <translation>Image files (*.png *.jpg *.bmp *.jpeg);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1132"/>
        <location filename="widget.cpp" line="1277"/>
        <source>
Doriți să continuați?</source>
        <translation>\nContinue?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1272"/>
        <source>placă modificată</source>
        <translation>board changed</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1273"/>
        <source>placa încărcată e de la </source>
        <translation>The loaded board is from </translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1334"/>
        <source>Fisiere Imagine (*.png *.jpg *.bmp *.jpeg);;toate fișierele (*.*)</source>
        <translation>Image files (*.png *.jpg *.bmp *.jpeg);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1373"/>
        <source>Ștergere componentă</source>
        <translation>Delete component</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1373"/>
        <location filename="widget.cpp" line="1732"/>
        <source>Sunteți sigur că doriți să ștergeți tipul de componentă?</source>
        <translation>Are you sure you want to delete the component type?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1379"/>
        <source>Ștergere eșuată</source>
        <translation>Deletion failed</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1379"/>
        <source>nu s-a putut șterge componenta!</source>
        <translation>the component could not be deleted!</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1663"/>
        <source>Duplicat</source>
        <translation>Duplicate</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1663"/>
        <source>Adăugare ignorată, deoarece s-a folosit deja această alternativă</source>
        <translation>Failed to add, because the alternative has already been used</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1732"/>
        <source>Ștergere tip componentă</source>
        <translation>Delete component type</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1755"/>
        <source>Ștergere placă</source>
        <translation>Delete board</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1755"/>
        <source>Sunteți sigur că doriți să ștergeți placa?</source>
        <translation>Are you sure you want to delete the board?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="2154"/>
        <source>ignoră</source>
        <translation>ignore</translation>
    </message>
</context>
<context>
    <name>testDebugWidget</name>
    <message>
        <location filename="testdebugwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="64"/>
        <source>Selectare director cu imagini de analizat</source>
        <translation>Select the folder with images to be analyzed</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="67"/>
        <location filename="testdebugwidget.ui" line="136"/>
        <location filename="testdebugwidget.ui" line="163"/>
        <location filename="testdebugwidget.ui" line="177"/>
        <location filename="testdebugwidget.ui" line="197"/>
        <location filename="testdebugwidget.ui" line="224"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="88"/>
        <source>Analizează toate imaginile</source>
        <translation>Analyze all images</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="94"/>
        <source>Analizează</source>
        <translation>Analyze</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="101"/>
        <source>Analizează imaginea curentă</source>
        <translation>Analyze current image</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="133"/>
        <source>Ascunde rezultate</source>
        <translation>Hide results</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="160"/>
        <source>Deschidere definiție placă</source>
        <translation>Open board definition</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="174"/>
        <source>Copiere imagine și includere în clasificator</source>
        <translation>Copy image and add to classifier</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="194"/>
        <source>Copiere partială imagine și includere în clasificator</source>
        <translation>Save image crop and add to classifier</translation>
    </message>
    <message>
        <location filename="testdebugwidget.ui" line="221"/>
        <source>Șterge fișier imagine</source>
        <translation>Delete image file</translation>
    </message>
</context>
</TS>
