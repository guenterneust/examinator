#ifndef REGEXTESTER_H
#define REGEXTESTER_H

#include <QDialog>
#include <QRegExp>
#include <QDebug>

namespace Ui {
class RegExTester;
}

class RegExTester : public QDialog
{
    Q_OBJECT

public:
    explicit RegExTester(QWidget *parent = nullptr);
    ~RegExTester();
    void setValidator(const QString & validator);
    QString getValidator();
    /**
     * @brief validateSn check if the serial number provided as parameter matches the regular expression provided as validator.
     * If the validator provides an extractor part, given at the end of the string separated by ____, extractedSN is set by extracting the capture groups
     * as defined by the etraction part, otherwise the entire string is returned
     * @param sn
     * @param validator
     * @param extractedSN
     * @return
     */
    static bool validateSn(QString sn, QString validator, QString &extractedSN)
    {
        QStringList dummySerials;
        dummySerials << "TESTA"<<"TESTAA"<<"111111111"<<"11111111111";
        if(dummySerials.contains(sn,Qt::CaseInsensitive)){
            extractedSN=sn;
            return true;
        }
        QStringList lst=validator.split("____");
        QString val=validator;
        QString extractor;
        extractedSN=QString();
        if(lst.count()>1){
            val=lst[0];
            extractor=lst[1];
        }
        QRegExp regExp(val);
        bool ok=regExp.exactMatch(sn);
        qDebug()<<"checking"<<sn<<"validator"<<val<<"extractor"<<extractor<<"valid"<<regExp.isValid()<<"error"<<regExp.errorString()<<"result"<<regExp.capturedTexts();
        if(!ok){
            return false;
        }
        if(extractor.isEmpty()){
            extractedSN=sn;
        } else {
            lst=extractor.split('$');
            bool error=false;
            if(!lst[0].isEmpty()){
                qDebug()<<"invalid validator/extractor"<<val<<extractor;
                error=true;
                return false;
            }
            for(int i=1; i<lst.count(); i++){
                int grp=lst[i].toInt(&error);
                error=!error;
                if(error){
                    qDebug()<<"invalid validator/extractor"<<val<<extractor;
                    return false;
                }
                if((grp<0)||(grp>regExp.captureCount())){//captureCount is not including item 0!
                    qDebug()<<"invalid validator/extractor"<<val<<extractor<<i<<grp<<regExp.captureCount();
                    return false;
                }
                auto texts=regExp.capturedTexts();
                extractedSN += texts[grp];
            }
        }
        return true;
    }


private slots:
    void on_validatorEdit_textChanged(const QString &arg1);

    void on_exampleEdit_textChanged(const QString &arg1);

private:
    Ui::RegExTester *ui;
    QRegExp regExp;
    void analyze(const QString &validator, const QString &example);
};

#endif // REGEXTESTER_H
