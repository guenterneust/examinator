/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPDEF_H
#define COMPDEF_H
#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include "compdef_dnn.h"
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>
#include <QImage>
#include <QPixmap>
#include <QSqlDatabase>
#include "compdefimage.h"
#include "compdefrect.h"
#include "detectedcomponent.h"
#include "compspparams.h"
#include "compdnnparams.h"

#include <dlib/opencv/cv_image.h>
//#include <dlib/svm_threaded.h>
//#include <dlib/string.h>
//#include <dlib/gui_widgets.h>


#define PARALLELCOMPDET 1

const int scannerPyramid=8;

class CompDefImage;
class CompDefRect;
typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<scannerPyramid> > image_scanner_type;

//dlib::array<dlib::array2d<unsigned char> > images;
//std::vector<std::vector<dlib::rectangle> > object_locations, ignore;

/// auxiliary structure for average color calculation
class AverageColorResult {
public:
    double r=0, g=0, b=0, h=0, s=0, v=0;
    double rSigma=0, gSigma=0, bSigma=0, hSigma=0, sSigma=0, vSigma=0;
    void clear(void){
        r=0;
        g=0;
        b=0;
        h=0;
        s=0;
        v=0;
        rSigma=0;
        gSigma=0;
        bSigma=0;
        hSigma=0;
        sSigma=0;
        vSigma=0;
    }
};

class AverageColorData {
public:
    QVector<double> r, g, b, h, s, v;
};

/** class containing a component definition loaded into memory
 *
 * The component definition consists mainly of a list of annotated images, which together form the
 * dataset needed to train the classifier to recognize one component type.
 * Besides the dataset, the parameters used to train the classifiers are also stored in this class,
 * as well as the resulting classifiers.
*/
class CompDef : public QObject
{
    Q_OBJECT
public:
    CompDef();
    /// flag indicating that the component definition has been modified, and needs to be saved
    bool dirty;
    /// list of annotated images
    QList<CompDefImage> images, includedImages;
    /// method to add a new image to the list of annotated images, initially with empty annotation
    CompDefImage *addImage(QString filename);
    /// aspect ratio enforced on all the positive samples
    qreal aspectRatio;
    /// selector indicating the possible component orientations allowed for this definition
    CompDefRect::OrientationMode orientationMode;
    /// save component definition, including the annotated image list, to the database
    /// this will generate an component id if it's the first time this component is saved for this component
    void saveToDB();
    /// search in the database if the name used for this component is not already used, and alter the name of the current component if the name is in use
    /// this alteration is done only in memory, so the component needs to be saved in order to modify the name permanently
    QString makeNameUnique(void);
    /// save a copy of the component definition
    long long saveToDBAs(QString newName);
    /// remove component definition, including the annotated image list, from the database
    bool removeFromDB();
    /// load the component definition, with or without the associated classifiers, from the database
    /// this will just create the datastructures, without displaying anything
    void loadFromDB(const long long id, bool checkImageFiles=false, bool loadImageMetadata=true, bool loadClassifiers=false);
    /// component definition name
    QString name;
    /// component definition id, used in the database
    long long id;
    double C;
    double eps;
    /// minimum area for a component rectangle. If upsamplig is allowed, the minimum area is considered after upsampling
    int minArea;
    /// minimum area for background rectangles - multiplier applied to minArea
    const double backgroundMultiplier=10.0;
    /// training parameter used for both FHOG and DNN
    /// dlib calculates good values for these thresholds, but only if the training images have similar object neighborhoods
    /// with the images the classifiers will work on, and the neighboring components are available to the trainer.
    /// As the requirement above is not always met, he default is a fixed threshold of 50%
    /// The value is stored as integer as percentage, and negative values mean that the threshold calculated by dlib is to be used for training
    int trainingIOUThresh=50;
    int upsample;
    int numThreads;
    bool horizontalFlip;
    bool verticalFlip;
    bool trainedFHOG=false;
    bool oldFHOG=true;
    bool trainedDNN=false;
    bool oldDNN=true;
    double fhogThreshold=0;
    //number of orientations for which a fhog classifier will be trained. The actual value is 2**fhogNOrient.
    int fhogNOrient=0;
    long long getIncludeDataset(void){
        return includeDataset;
    };
    /**
     * @brief set the includeDataset property and load the corresponding image metadata if requested
     * @param id database id of the dataset to be included, -1 if nothing will be included
     * @param loadImageMetadata boolean specifying if the image metadata have to be loaded
     * @return true on success, or false if the image metadata cold not be loaded
     */
    bool setIncludeDataset(long long id, bool loadImageMetadata=true);
    /**
      [0] - enabled, and percentage of the rectangle width and height .
            If width==0, validation is not active.

      [1]...[6] - min and max value for R, G, B, H, S, V as calculated by qimage.
    */
    std::array<std::array<double, 2>,7>colorValidator={};
    CompDefRect recentRect;
    static unsigned maxDNNArea;
    void trainFHOG(QString databaseName);
    void trainShapePredictor(QString databaseName);
    void trainDNN(QString databaseName);
    ///load a trained dnn from disk into memory and store it in the database
    bool importDNN(QString fileName);
    bool importFHOG(QString fileName);
    bool importShapePredictor(QString fileName);
    bool exportDNN(QString fileName);
    bool exportFHOG(QString fileName);
    bool exportShapePredictor(QString fileName);
    //run the shape predictor on each image containing rectangles and report the average positional error, in pixels
    double testShapePredictor(const dlib::array<dlib::array2d<dlib::rgb_pixel> > &imageArray, const std::vector<std::vector<dlib::full_object_detection> > &fullObjects, int spIdx);
    bool importAutoinspect(QString dirName);
    /*template<typename image_type1>
    QList<DetectedComponent> detectObjects(const image_type1 &img);
    */
    ///
    /// \brief detectObjects - this function converts the opencv image to dlib::array2d and calls the array2d version of this function
    /// \param img
    /// \param displaySearchFrames
    /// \return list of detected components (of type DetectedComponent) ordered by score
    ///
    QList<DetectedComponent> detectObjects(const dlib::cv_image<dlib::rgb_pixel> &img, bool displaySearchFrames);
    ///
    /// \brief detectObjects - search for instances of the objects recognized by the classifier in the input image
    /// \param img - input image
    /// \param displaySearchFrames flag requesting the display of the search ROI's
    /// \param qImg copy of the input image, in QImage format, used for the color validator
    /// \param adjustThreshold additive threshold adjustment, used to lower the threshold for testing purposes in order to assess if the threshold is good
    /// \return
    ///
    QList<DetectedComponent> detectObjects(const dlib::array2d<dlib::rgb_pixel> &img, bool displaySearchFrames, const QImage qImg=QImage(), double adjustThreshold=0);
    QList<DetectedComponent> detectObjectsFHOG(const dlib::array2d<dlib::rgb_pixel> &img, const QImage & qImg, double adjustThreshold);
    QList<DetectedComponent> detectObjectsDNN(const dlib::array2d<dlib::rgb_pixel> &img, const QImage & qImg, double adjustThreshold);
    /// recursive function used to divide and conquer images that don't fit into the GPU memory
    /// for images that are bigger than what fits into the GPU memory, first the image is divided into two (if these already fit into memory) or four partially overlapping tiles.
    /// in order to get also the objects crossing the boundaries, the network is also run on the entire image, shrunken by a factor of two for each direction.
    /// the detections are simply concatenated into the result vector. Afterwards, non maxima supression is done in order to keep only the relevant detections.
    template <
            typename net_type
            >
    std::vector<dlib::mmod_rect> detectObjectsDNNBig(const dlib::array2d<dlib::rgb_pixel> &img, net_type &net, double threshold, bool toplevel);
    /// load fhog classifier and shape predictor if needed
    void prepareToRun(void);
    /// load the FHOG from the default database, for the current component based on the id which has to be set already
    void loadFHOGDetector(void);
    /**
     * helper function for loadFHOGDetector, used in order to avoid running multiple SQL querries when the data is already available
    */
    void setLoadedFHOGDetector(const QByteArray data);
    /// load the shape predictor from the default database, for the current component based on the id which has to be set already
    void loadShapePredictor(void);
    /**
     * helper function for loadShapePredictor, used in order to avoid running multiple SQL querries when the data is already available
    */
    void setLoadedShapePredictor(const QByteArray data);
    /// load the dnn from the default database, for the current component based on the id which has to be set already
    void loadDNN(void);
    /**
     * helper function for loadDNN, used in order to avoid running multiple SQL querries when the data is already available
    */
    void setLoadedDNN(const QByteArray data);
    void clearDNN(void);
    void clearShapePredictor(void);
    void clearFHOG(void);
    /**
     * calculate the average channel value for the RGB channels, and the HSV values corresponding to these average
     * values using QColor::getHsvF
    */
    void averageColor(const QImage img, double &r, double &g, double &b, double &h, double &s, double &v);
    /**
     * @brief calculate the average color of the central area of the image using averageColor, on the portion of
     * the image defined by the color validator for this component definition
     * The results are returned by putting the into the component variables provided as reference
     * @param img input image
     * @param r reference to red component
     * @param g reference to green component
     * @param b reference to blue component
     * @param h reference to hue component
     * @param s reference to saturation component
     * @param v reference to value component
     */
    void averageColorCenter(const QImage img, double &r, double &g, double &b, double &h, double &s, double &v);
    /**
     * @brief calculate the average color and standard deviation of the central area of all positive samples
     * @param res structure containing r g b h s v and their standard deviation. Will be used to pass the output of the calculation
     */
    void calcAverageColor( AverageColorResult &res);

    bool validateColor(QImage img, DetectedComponent &dc);
    template <
            typename pyramid_type,
            typename image_array_type
            >
    void conditionalUpsampleImageDataset(image_array_type &images,
                                         std::vector<std::vector<dlib::rectangle> > &objects,
                                         std::vector<std::vector<dlib::rectangle> > &objects2,
                                         unsigned long minRectArea,
                                         unsigned long max_image_size = std::numeric_limits<unsigned long>::max());
    template <
            typename pyramid_type,
            typename image_array_type
            >
    void conditionalDownsampleImageDataset(image_array_type &images,
                                         std::vector<std::vector<dlib::rectangle> > &objects,
                                         std::vector<std::vector<dlib::rectangle> > &objects2,
                                         unsigned long maxRectArea);
    bool usesImage(QString fileName);
    bool saveDlibDataset(QString fileName);
    CompSpParams compSpParams;
    CompDNNParams compDNNParams;
    inline unsigned numFHOGDetectors(){
        unsigned i=0;
        for(;i<fhogDetector.size(); i++){
            if(fhogDetector[i].num_detectors()==0)
                break;
        }
        return i;
    }
    inline unsigned numShapePredictors(){
        unsigned i=0;
        for(;i<shapePredictor.size(); i++){
            if(shapePredictor[i].num_parts()==0)
                break;
        }
        return i;
    }
    /**
     * @brief getLastImagePath  This is used mainly to decide where to save a new image to be added to the classifier.
     * @return return the path to the last added image.
     */
    QString getLastImagePath();
    /**
     * @brief extract the image area corresponding to the provided component definition rectangle, rotate it to be oriented northwards, and return it
     * @param bg pixmap for the entire image
     * @param rect component definitition rectangle, has to be defined for the provided image to make sense
     * @return the pixmap of the component area of the image, rotated northwards
     */
    static QPixmap getCompChip(const QPixmap &bg, const CompDefRect * rect);
    /**
     * @brief get the pixmap primaryRect. At the first call or if reload is true, it's loaded from the database and the filesystem, otherwise it's just passed over.
     * @param reload force the loading of the pixmp from disk, even if it's already loaded
     * @return the pixmap primaryRect.
     */
    QPixmap getPrimaryRect(bool reload);
    unsigned lastsave=0;
    ///read the savetime (unix timestamp) value from the database, usually in order to check if the data in memory is still valid
    unsigned getLastSaved(QSqlDatabase &db);
private:
    /// reference rectangle, for now the first positive sample is used. This is only initialized at the first call of getPrimaryRect
    QPixmap primaryRect;
    dlib::rectangle minRect();
    std::vector<dlib::object_detector<image_scanner_type>> fhogDetector;
    std::vector<dlib::shape_predictor> shapePredictor;
    anet_type1 dnnDetector;
    bool spLoaded=false;
    bool fhogLoaded=false;
    bool dnnLoaded=false;
    const double chipMargin=1; ///< margins around the actual component, as fraction of the component dimensions, used to create the training dataset.
    /**
     * for the current component definition, loaded into this object, generate a in-memory dlib training dataset, consisting of
     * a list of images as dlib arrays, a list of object locations for each image, and a list of ignores for each image.
     * As opposed to the way dlib usually work, this creates at most one object location per image, cropping the images around the
     * object markings with some margins.
     * Data augmentation by horizontal and vertical mirrong is applied as configured when the dataset is built, so during training no random flipping is used.
     * Negative markings generate images with object locations, only objects within the negative image having the proper orientation being marked.
     * This leads to duplicating these positive samples, but has been added in order to try to add some context around object margins,
     * because MMOD tends to reject objects placed close to each other if only trained without any context around the objects.
     */
    template <
        typename array_type
        >
    std::vector<std::vector<dlib::rectangle> >
    createDlibDataset(dlib::array<array_type >& imageArray,
                      std::vector<std::vector<dlib::rectangle> > &objectLocations,
                      std::vector<std::vector<float> > &angles,
                      bool growAndShrink);
    /**
    For one negative marking of an image from the list of images belonging to a component definition, extract all
    the parts to be ignored, either because labeled as positives, or as ignores.
    */
    template <
        typename array_type
        >
    std::vector<dlib::rectangle> createNegativeMarking(const CompDefImage & image,
                                                                     const array_type &bigImage, array_type &imageChip,
                                                                     double x, double y, double w, double h,
                                                       CompDefRect::Orientation orientation,
                                                       std::vector<dlib::rectangle> &objects,
                                                       std::vector<float> &angles);
    /// function based on the dlib function flip_image_dataset_left_right
    template<typename image_array_type>
    void flipImageDatasetUpDown(image_array_type &images, std::vector<std::vector<dlib::rectangle> > &objects, std::vector<std::vector<dlib::rectangle> > &objects2);
    /// helper function written based on the flip_image_left_right function in dlib
    template<typename image_type1, typename image_type2>
    dlib::point_transform_affine flipImageUpDown(const image_type1 &in_img, image_type2 &out_img);
    /// helper function based on flip_rect_left_right from dlib
    dlib::rectangle flipRectUpDown(const dlib::rectangle &rect, const dlib::rectangle &window);
    ///
    template<typename image_array_type, typename T, typename U>
    void addImageUpDownFlips(image_array_type &images, std::vector<std::vector<T> > &objects, std::vector<std::vector<U> > &objects2);
    /// function used immediately after training the classifier, in order to store the result of the training.
    /// It's also used when a classifier is imported from a file.
    bool saveFHOGDetector(QString databaseName);
    /// function used immediately after training the shape predictor, in order to store the result of the training.
    /// It's also used when a shape predictor is imported from a file.
    /// returns true on success
    bool saveShapePredictor(QString databaseName);
    /// function used immediately after training the classifier, in order to store the result of the training.
    /// It's also used when a classifier is imported from a file.
    bool saveDNN(QString databaseName, net_type1 net);
    /// helper function used when importing autoinspect files
    /// This function works as the QString::split function, but leaves portions surrounded by double quotes in one split
    QStringList quotedSplit(QString text);
    QString unquote(QString str);
    CompDefImage * getImage(const QString & imagePath);///< find the image object referring to the given image file, and return a pointer to it, or nullptr no match is found
    ///calculate ratio of rectangle r1 covered by r2
    static double percentCoveredBy(dlib::rectangle &r1, dlib::rectangle &r2);
    /// Load the neural network of another component, usually in order to use it as starting weights to learn the current component
    bool loadDNN(long long compId, net_type1 &net, QString databaseName);
    ///id of another component definition, to be included as training data for the current CompDef. -1 means don't include anything
    long long includeDataset=-1;
    bool loadIncludedImageMetadata(bool checkImageFiles);
    void prepareToRun(const QSqlQuery &query);
};

template <typename image_array_type>
void rotate_image_dataset (
    double angle,
    image_array_type& images,
    std::vector<std::vector<dlib::full_object_detection> >& objects
)
{
    // make sure requires clause is not broken
    DLIB_ASSERT( images.size() == objects.size(),
        "\t void rotate_image_dataset()"
        << "\n\t Invalid inputs were given to this function."
        << "\n\t images.size():   " << images.size()
        << "\n\t objects.size():  " << objects.size()
        );

    typename image_array_type::value_type temp;
    for (unsigned long i = 0; i < images.size(); ++i)
    {
        const dlib::point_transform_affine tran = dlib::rotate_image(images[i], temp, angle);
        swap(temp, images[i]);
        for (unsigned long j = 0; j < objects[i].size(); ++j)
        {
            dlib::full_object_detection d=dlib::impl::tform_object(tran,objects[i][j]);
            //const dlib::rectangle rect = objects[i][j];
            objects[i][j] = d;
        }
    }
}


#endif // COMPDEF_H
