/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CLASSIFIERPICKER_H
#define CLASSIFIERPICKER_H

#include <QDialog>
#include <QList>
#include "compdef.h"
#include "boardcomptype.h"

namespace Ui {
class ClassifierPicker;
}

///small dialog used to select the classifier to which an image is to be added from the TestDebugWidget
/// gets as input a QList of QSharedPointers to Classifiers, and has as result the index of the selected Classifier, or -1 if it's rejected;
class ClassifierPicker : public QDialog
{
    Q_OBJECT

public:
    explicit ClassifierPicker(QWidget *parent = 0);
    ~ClassifierPicker();
    ///load the working data into the dialog
    void setValues(const QString compName, const QString compTypeName, const QList<BoardCompTypeClassifier > &classifiers);
    ///get the selection result for the dialog after closing it - it's used instead of the return value of exec()
    ///needed because when the dialog is closed, the result is zero, which is a valid index
    int result(){
        return selection;
    }
private slots:

    void on_acceptBtn_clicked();

    void on_rejectBtn_clicked();

private:
    Ui::ClassifierPicker *ui;
    ///stores the result of the dialog, which is the index if the selected classifier
    int selection=-1;
};

#endif // CLASSIFIERPICKER_H
