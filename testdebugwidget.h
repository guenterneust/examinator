/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TESTDEBUGWIDGET_H
#define TESTDEBUGWIDGET_H

#include <QWidget>
#include <QDir>
#include "detectedcomponent.h"
#include <QList>
#include <QAbstractListModel>
#include "boarddef.h"
#include <QTimer>
#include <QSortFilterProxyModel>
namespace Ui {
class testDebugWidget;
}

class ImageResults{
public:
    QList<DetectedComponent> boards;
    QString imageName;
    QString resultsName;
    int decision=-1;
    bool unknown=true;
    bool dirty=false;
    void calculateDecision(void);
    void load(QString name=QString()){
        if(!name.isNull())
            resultsName=name;
        QFileInfo info(resultsName);
        if(info.isFile()){
            std::ifstream in((resultsName).toStdString(), std::ifstream::in|std::ifstream::binary);
            unknown=false;
            boards.clear();

            try {
                deserialize(boards,in);
                try {
                    dlib::deserialize(decision, in);
                } catch(...){
                    //no decision in file for board
                    calculateDecision();
                }
            } catch(...){
                qDebug()<<"deserialize of saved results failed";
                boards.clear();
                unknown=true;
            }
        }
    };
};

class BoardStatModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit BoardStatModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags (const QModelIndex& index) const override;
    void setImages(QList<ImageResults> * images);
    void setRequiredBoards(int nRequiredBoards){
        requiredBoards=nRequiredBoards;
    };
    void prepareForUpdate(void);
    void updateFinished(void);
    void update(const QModelIndex &index){
        emit dataChanged(index, index);
    }
    void update(const QModelIndex &index,const QModelIndex &index2){
        emit dataChanged(index, index2);
    }
    bool setData(const QModelIndex &index,
                                    const QVariant &value, int role) override{
        /* loop undecided -> checked==right -> unchecked==wrong */
        /* decision values:
         * 0 - wrong
         * 1 - right
         * -1 - undecided
        */
        if(!index.isValid() || role != Qt::CheckStateRole)
            return false;
        (*images)[index.row()].dirty=true;
        //qDebug()<<"setData"<<value<<(*images)[index.row()].decision;
        if(value.toInt() == Qt::Checked){
            /* Qt only returns checked or unchecked;
             * if old value was "wrong", new value will be "undecided", otherwise "right" */
            (*images)[index.row()].decision= ((*images)[index.row()].decision==0)?-1:1;
        }else if(value.toInt() == Qt::Unchecked){
            (*images)[index.row()].decision=0;
        } else {
            (*images)[index.row()].decision=-1;
        }
        if((*images)[index.row()].boards.count()>0){
            for(int b=0; b<(*images)[index.row()].boards.count(); b++){
                auto &components = (*images)[index.row()].boards[b].parts;
                if((*images)[index.row()].decision==1){
                    /* right */
                    (*images)[index.row()].boards[b].decision=1;
                    for(int i=0; i<components.count(); i++)
                        components[i].decision=1;
                } else if((*images)[index.row()].decision==0){
                    /* wrong */
                    bool ok=false;
                    for(int i=0; i<components.count(); i++)
                        if(components[i].decision!=1)
                            ok=true;
                    if(!ok){
                        /* all components were right, set them to undecided */
                        (*images)[index.row()].boards[b].decision=-1;
                        for(int i=0; i<components.count(); i++){
                            components[i].decision=-1;
                        }
                    }
                } else {
                    /* undecided */
                    bool ok=false;
                    for(int i=0; i<components.count(); i++){
                        if(components[i].decision==-1)
                            ok=true;
                        else if(components[i].decision==0){
                            // set all components from wrong to undecided
                            components[i].decision=-1;
                            ok=true;
                        }
                    }
                    if(!ok){
                        //if all components were right, set them to undecided
                        (*images)[index.row()].boards[b].decision=-1;
                        for(int i=0; i<components.count(); i++)
                            components[i].decision=-1;
                    }
                }
            }
        }
        emit dataChanged(index, index);
        emit dataModified(index.row());
        return true;
    }
private:
    QList<ImageResults> *images=nullptr;
    int requiredBoards=1;
signals:
    void dataModified(int position);
};

class ImageStatModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit ImageStatModel(QObject *parent = nullptr){
        Q_UNUSED(parent);
        rootItem=new DetectedComponent();
        rootItem->boardComp.name="rootItem";
    };

    // Header:
    //QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags (const QModelIndex& index) const override;
    void setImages(QList<ImageResults> * images);
    ///sets the current image to be shown and updates the model
    void prepareForUpdate(void);
    void updateFinished(void);
    void update(const QModelIndex &index){
        emit dataChanged(index, index);
    }
    void update(const QModelIndex &index,const QModelIndex &index2){
        emit dataChanged(index, index2);
    }
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override
    {
        Q_UNUSED(orientation);
       if (role != Qt::DisplayRole)
            return QVariant();
       switch(section){
       case 0:
           return tr("nume");
       case 1:
           return tr("scor");
       case 2:
           return tr("x");
       case 3:
           return tr("y");
       case 4:
           return tr("scala");
       case 5:
           return tr("alternativa");
       default:
           return QVariant();
       }
    }
    bool setData(const QModelIndex &index,
                                    const QVariant &value, int role) override{

        if(!index.isValid() || (role != Qt::CheckStateRole))
            return false;
        (*images)[imageNum].dirty=true;
        //auto &components=(*images)[imageNum].boards[0].parts;
        DetectedComponent *comp=static_cast<DetectedComponent *>(index.internalPointer());
        if(comp==nullptr){
            qDebug()<<"setdata null pointer";
            return false;
        }
        //qDebug()<<"setData before"<<value.toInt()<<comp->decision;
        if(value.toInt() == Qt::Checked){
            comp->decision= (comp->decision==0)?-1:1;
        }else if(value.toInt() == Qt::Unchecked){
            comp->decision=0;
        } else
            comp->decision=-1;
        //qDebug()<<"setData after"<<value.toInt()<<comp->decision;
        QModelIndex parentIdx=index.parent();
        if(parentIdx.isValid()){
            //ensure parent decision is compatible with current decision
            DetectedComponent *parent=static_cast<DetectedComponent *>(parentIdx.internalPointer());
            if(parent!=nullptr){
                if(comp->decision==0){
                    parent->decision=0;
                    emit dataChanged(parentIdx,parentIdx);
                } else if(comp->decision==-1){
                    if(parent->decision==1){
                        parent->decision=-1;
                        emit dataChanged(parentIdx,parentIdx);
                    }
                }
            }
        }
        if(comp->boardComp.id<0){
            beginResetModel();
            //this is a board, not a simple component
            //make sure that all components have a status compatible with the board status
            if(comp->decision==1){
                //all components must be correct
                for(int i=0; i<comp->parts.count(); i++){
                    comp->parts[i].decision=1;
                }
            } else if(comp->decision==0){
                //board is wrong. If all components are right, set all of them to undecided
                bool ok=false;
                for(int i=0; i<comp->parts.count(); i++)
                    if(comp->parts[i].decision!=1)
                        ok=true;
                if(!ok){
                    /* all components were right, set them to undecided */
                    for(int i=0; i<comp->parts.count(); i++){
                        comp->parts[i].decision=-1;
                    }
                }
            } else {
                //board is undecided. Set any component with wrong to undecided
                bool ok=false;
                for(int i=0; i<comp->parts.count(); i++){
                    if(comp->parts[i].decision==-1)
                        ok=true;
                    else if(comp->parts[i].decision==0){
                        // set all components from wrong to undecided
                        comp->parts[i].decision=-1;
                        ok=true;
                    }
                }
                if(!ok){
                    //if all components were right, set them to undecided
                    for(int i=0; i<comp->parts.count(); i++)
                        comp->parts[i].decision=-1;
                }
            }
            endResetModel();
        }
        if(comp->boardComp.id<0){
        } else {
            emit dataChanged(index, index);
        }
        emit dataModified(imageNum, comp->decision);
        return true;
    }
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    DetectedComponent * getRootItem(){
        return rootItem;
    }
private:
    ///
    /// \brief setParentAndRow recursively set the parent and row for all components and boards detected in the image
    /// \param parent current node to be handled
    ///
    void setParentAndRow(DetectedComponent *parent){
        for(int row=0; row<parent->parts.count(); row++){
            DetectedComponent * child=&(parent->parts[row]);
            child->row=row;
            child->parent=parent;
            setParentAndRow(child);
        }
    }
    int imageNum;
    QList<ImageResults> *images=nullptr;
    DetectedComponent * rootItem;
public slots:
    void setImage(int image);
signals:
    void dataModified(uint image, int decision);

};

class TestDebugWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TestDebugWidget(QWidget *parent = nullptr);
    ~TestDebugWidget();
    void setBoardDefPtr(BoardDef ** boardDefPtr);

    void setRotateComponents(bool newRotateComponents);

private slots:
    void on_loadDirBtn_clicked();

    void on_boardTestBtn_clicked();

    void on_analyzeBtn_clicked();

    void on_delBtn_clicked();

    void on_copyAndOpenBtn_clicked();
    void masterViewCurrentChanged(const QModelIndex&idxTo, const QModelIndex&idxFrom);

private:
    QList<ImageResults> images;
    QDir dir;
    Ui::testDebugWidget *ui;
    BoardStatModel boardStatModel;
    ImageStatModel imageStatModel;
    QSortFilterProxyModel *proxyModel;
    //this is only safe if the testDebugWidget does not lives longer than the
    //testInterface. This should not be a problem, as the widget is embedded into the testInterface
    BoardDef ** boardDefPtr=nullptr;
    /** run the currently loaded board test on one image and store the results
    returns PASS status
    */
    bool processOneImage(ImageResults & res);
    QTimer timer;
    //location of the lst copied image
    static QString destination;
    void copyAndOpenImage(bool boardClassifier);
    bool rotateComponents=false;
signals:
    void imageChanged(QString imageName, QList<DetectedComponent> & boardRects, bool imageOk);
    /// Request to add the image to the specified classifier
    /// Used usually for failing components in one image, the classifier being the one used for that component
    /// if the classifier can't be identified, for example if no component is selected or the wron board is loaded, ihe image is copied but not added to any classifier
    void imageForClassifier(long long classifierId, const QString & imagePah);
    void showBoard(long long boardId);
    void loadBoard(QString boardName);
    void centerAt(double x, double y);
    void clearRects();
private slots:
    void updateDetail(int position);
    void updateMaster(unsigned image, int decision);
    void expandDetailView();
    void on_openBoardBtn_clicked();
    void on_detailTreeView_doubleClicked(const QModelIndex &index);
    void on_cropAndOpenBtn_clicked();
    void on_clearBtn_clicked();
    void on_saveBtn_clicked();
    void on_copyAndOpenBtn_triggered(QAction *arg1);
    void on_loadDirBtn_triggered(QAction *arg1);
};



#endif // TESTDEBUGWIDGET_H
