/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "boardconfig.h"
#include "ui_boardconfig.h"
#include <QDebug>

BoardConfig::BoardConfig(const QString & name, int alignMethod, QString testCode, const QList<ImagesAndLights> &lights, const int numberOfBoards, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BoardConfig)
{
    ui->setupUi(this);
    ui->nameEdit->setText(name);
    ui->alignMethodCombo->setCurrentIndex(alignMethod);
    ui->testCodeEdit->setText(testCode);
    /*
    if(lights.length()>0){
        ui->enableLightCB->setChecked(lights[0].lightsEnabled);
        if(lights[0].lightData.length()>2){
            ui->upperLightCB->setChecked(lights[0].lightData.at(2)!='0');
            ui->centerLightCB->setChecked(lights[0].lightData.at(1)!='0');
            ui->lowerLightCB->setChecked(lights[0].lightData.at(0)!='0');
        }
        if(lights.length()>1){
            ui->enableLight2CB->setChecked(lights[1].lightsEnabled);
            if(lights[1].lightData.length()>2){
                ui->upperLight2CB->setChecked(lights[1].lightData.at(2)!='0');
                ui->centerLight2CB->setChecked(lights[1].lightData.at(1)!='0');
                ui->lowerLight2CB->setChecked(lights[1].lightData.at(0)!='0');
            }
        }
    }
    */
    this->lights=lights;
    ui->imageNumSpin->setValue(lights.count());
    currentImage=0;
    ui->imageLbl->setNum(currentImage+1);
    ui->numberOfBoardsSpin->setValue(numberOfBoards);
    if(lights.count()>0)
        showImageAndLights(lights[0]);
    connect(ui->upperLightCB, SIGNAL(stateChanged(int)), this, SLOT(updateLights()));
    connect(ui->centerLightCB, SIGNAL(stateChanged(int)), this, SLOT(updateLights()));
    connect(ui->lowerLightCB, SIGNAL(stateChanged(int)), this, SLOT(updateLights()));
    connect(ui->enableLightCB, SIGNAL(stateChanged(int)), this, SLOT(updateLights()));
    connect(ui->imageDelaySpin, SIGNAL(valueChanged(int)), this, SLOT(updateLights()));
    //ui->extTriggerCB->setEnabled(false);
}

BoardConfig::~BoardConfig()
{
    delete ui;
}

QComboBox *BoardConfig::getClassifierCombo()
{
    return ui->classifierCombo;
}

QString BoardConfig::getName()
{
    return ui->nameEdit->text();
}

long long BoardConfig::getClassifier()
{
    return ui->classifierCombo->currentData().toLongLong();
}

long long BoardConfig::getAlignMethod()
{
    return(ui->alignMethodCombo->currentIndex());
}

QString BoardConfig::getTestCodeOverride()
{
    //qDebug()<<"getTestCodeOverride"<<ui->testCodeEdit->text();
    return(ui->testCodeEdit->text());
}

int BoardConfig::getNumberOfBoards()
{
    return ui->numberOfBoardsSpin->value();
}

QList<ImagesAndLights> BoardConfig::getLights()
{
    ImagesAndLights l0;
    l0.lightsEnabled=ui->enableLightCB->isChecked();
    l0.lightData.append(ui->lowerLightCB->isChecked()?'1':'0');
    l0.lightData.append(ui->centerLightCB->isChecked()?'1':'0');
    l0.lightData.append(ui->upperLightCB->isChecked()?'1':'0');
    /*
    l1.lightsEnabled=ui->enableLight2CB->isChecked();
    l1.lightData.append(ui->lowerLight2CB->isChecked()?'1':'0');
    l1.lightData.append(ui->centerLight2CB->isChecked()?'1':'0');
    l1.lightData.append(ui->upperLight2CB->isChecked()?'1':'0');
    qDebug()<<"lights"<<l0.lightsEnabled<<l0.lightData<<l1.lightsEnabled<<l1.lightData;
    QList<ImagesAndLights> lights;
    lights<<l0<<l1;
    */
    return lights;
}

void BoardConfig::on_BoardConfig_accepted()
{

}

void BoardConfig::on_imageNumSpin_valueChanged(int arg1)
{
    Q_UNUSED(arg1)
    int nImages=ui->imageNumSpin->value();
    while(lights.count()>nImages){
        lights.removeLast();
    }
    while(lights.count()<nImages){
        lights.append(ImagesAndLights(3));
    }
    if(currentImage>=lights.count()){
        currentImage=lights.count()-1;
        ui->imageLbl->setNum(currentImage+1);
        showImageAndLights(lights.last());
    }

}

void BoardConfig::showImageAndLights(const ImagesAndLights &light)
{
    ui->enableLightCB->blockSignals(true);
    ui->upperLightCB->blockSignals(true);
    ui->centerLightCB->blockSignals(true);
    ui->lowerLightCB->blockSignals(true);
    ui->imageDelaySpin->blockSignals(true);
    ui->enableLightCB->setChecked(light.lightsEnabled);
    ui->lowerLightCB->setChecked(light.lightData.at(0)!='0');
    ui->centerLightCB->setChecked(light.lightData.at(1)!='0');
    ui->upperLightCB->setChecked(light.lightData.at(2)!='0');
    ui->imageDelaySpin->setValue(light.delay);
    ui->extTriggerCB->setChecked(light.delay<0);
    ui->enableLightCB->blockSignals(false);
    ui->upperLightCB->blockSignals(false);
    ui->centerLightCB->blockSignals(false);
    ui->lowerLightCB->blockSignals(false);
    ui->imageDelaySpin->blockSignals(false);
    qDebug()<<"showImageAndLights"<<currentImage<<lights[currentImage].lightData<<light.lightData<<lights[currentImage].lightsEnabled;
}

void BoardConfig::on_imageUpBtn_clicked()
{
    if(currentImage+1<lights.count()){
        currentImage++;
        showImageAndLights(lights[currentImage]);
        ui->imageLbl->setNum(currentImage+1);
    }
}

void BoardConfig::on_imageDownBtn_clicked()
{
    if(currentImage>0){
        currentImage--;
        showImageAndLights(lights[currentImage]);
        ui->imageLbl->setNum(currentImage+1);
    }
}

void BoardConfig::updateLights()
{
    if((currentImage>=0)&&(currentImage<lights.count())){
        lights[currentImage].lightsEnabled=ui->enableLightCB->isChecked();
        QByteArray lightData;
        lightData.append(ui->lowerLightCB->isChecked()?'1':'0');
        lightData.append(ui->centerLightCB->isChecked()?'1':'0');
        lightData.append(ui->upperLightCB->isChecked()?'1':'0');
        lights[currentImage].lightData=lightData;
        lights[currentImage].delay=ui->imageDelaySpin->value();
        qDebug()<<"updateLights"<<currentImage<<lights[currentImage].lightData<<lights[currentImage].lightsEnabled;
    }
}


void BoardConfig::on_imageDelaySpin_valueChanged(int arg1)
{
    ui->extTriggerCB->setChecked(arg1<0);
}


void BoardConfig::on_extTriggerCB_clicked()
{
    if(ui->extTriggerCB->isChecked())
        ui->imageDelaySpin->setValue(-1);
    else
        ui->imageDelaySpin->setValue(0);
}


