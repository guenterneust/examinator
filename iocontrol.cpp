/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "iocontrol.h"
#include <QSerialPortInfo>
#include <QDebug>
#include <QTimer>
#include <QMessageBox>
#include <QEventLoop>
#include <QApplication>

IOControl::IOControl(QObject *parent) : QObject(parent)
{
    connect(&port, SIGNAL(readyRead()), this, SLOT(readData()));
    qRegisterMetaType<ImagesAndLights>();
    connect(this, SIGNAL(stressLightsSignal(const ImagesAndLights &, bool)), this, SLOT(setLights(const ImagesAndLights &, bool)), Qt::QueuedConnection);
    watchdog.setInterval(1000);
    watchdog.setSingleShot(true);
    connect(&watchdog, &QTimer::timeout,this, &IOControl::timeoutSlot, Qt::QueuedConnection);
}

bool IOControl::setPort(QString portName)
{
    qDebug()<<"setPort"<<portName;
    auto lst=QSerialPortInfo::availablePorts();
    if(portName.isEmpty()){
        qDebug()<<"no port name provided, detected"<<lst.count()<<"ports";
        foreach (auto item, lst) {
           qDebug()<<"detected"<<item.portName()<<item.description()<<item.manufacturer()<<item .systemLocation();
        }
        return false;
    }
    port.setPortName(portName);
    bool ok = port.open(QIODevice::ReadWrite);
    if(!ok){
        foreach (auto item, lst) {
            if((item.portName().contains(portName, Qt::CaseInsensitive))||
                    (item.description().contains(portName, Qt::CaseInsensitive))||
                    (item.manufacturer().contains(portName, Qt::CaseInsensitive))||
                    (item.systemLocation().contains(portName, Qt::CaseInsensitive)))
            {
                qDebug()<<"using"<<item.portName()<<item.description()<<item.manufacturer()<<item .systemLocation();
                port.setPortName(item.portName());
                break;
            }
            qDebug()<<"not using"<<item.portName()<<item.description()<<item.manufacturer()<<item .systemLocation();
        }
        port.setBaudRate(QSerialPort::Baud9600);
        ok = port.open(QIODevice::ReadWrite);
        if(!ok)
            qDebug()<<"can't open port"<<port.portName()<<port.error()<<port.isOpen()<<port.baudRate()<<port.dataBits()<<port.openMode();
    }
    if(ok){
        //send a bunch of newlines in order to allow the arduino to recognize one in order to accept the baudrate
        for(int i=0; i<50; i++){
            port.write("\n");
            port.waitForBytesWritten();
        }
    }
    return ok;
}

void IOControl::timeoutSlot()
{
    qDebug()<<"timeout"<<stressTestRunning;
    qApp->processEvents();
    qDebug()<<"no answer from iocontrol board";
    if(maxNoResponseWarn != dontWarn){
        //try to revive port, maybe it's stuck
        if(port.isOpen())
            port.close();
        port.open(QIODevice::ReadWrite);
        if(port.isOpen()){
            //send a bunch of newlines in order to allow the arduino to recognize one in order to accept the baudrate
            for(int i=0; i<50; i++){
                port.write("\n");
                port.waitForBytesWritten();
            }
        }
    }
    if(stressTestRunning){
        if(pendingStressAnswers>0){
            //only send new message if the answer was pending
            pendingStressAnswers--;
            emit stressLightsSignal(stressLights, false);
        }
    }
    if(!warnMissingLights)
        return;
    if(dontWarn>0){
        dontWarn--;
        return;
    }
    QMessageBox::information(nullptr, tr("Lumini"), tr("Acest program e configurat pentru aprinderea automată a luminilor, dar acest lucru nu funcționează.\nVerificați conexiunea USB pentru controlul luminilor.\nVerificați să nu fie pornită altă aplicație care folosește portul pentru controlul luminilor."));
    dontWarn=maxNoResponseWarn;
}

void IOControl::triggerSlot()
{
    disconnect(this, nullptr, this, SLOT(triggerSlot()));
    qDebug()<<"initiateImageCapture";
    emit initiateImageCapture();
}

void IOControl::readData()
{
    if(port.bytesAvailable()<=0)
        return;
    QByteArray data;
    data=port.read(1);
    //qDebug()<<"readData"<<data;
    if(data=="\n"){
        qDebug()<<"readBuffer"<<readBuffer;
        dontWarn=maxNoResponseWarn;
        if(readBuffer=="T")
            emit start();
        else if(readBuffer=="L"){
            watchdog.stop();
            //qDebug()<<"Lights ok";
            if(stressTestRunning){
                if(pendingStressAnswers>0){
                    pendingStressAnswers--;
                    setLights(stressLights,false);
                }
            }
        }else if(readBuffer=="TN")
            emit triggerNext();
        else if(readBuffer=="PING")
            sendMessage("PONG");
        else if(readBuffer=="P"){
            QMessageBox::information(nullptr, tr("Lumini"), tr("Acest program e configurat pentru aprinderea automată a luminilor, dar acest lucru nu funcționează.\nVerificați conexiunea USB pentru controlul luminilor.\nVerificați alimentarea sistemului de iluminare."));
        } else
            emit newData(readBuffer);
        readBuffer.clear();
    } else {
        readBuffer.append(data);
    }
    //maybe there's more data in the buffer - retrieve it
    readData();
}

bool IOControl::setLights(const ImagesAndLights &lights, bool startCapture)
{
    int delay=lights.delay;
    bool result=true;
    if(lights.lightsEnabled){
        if(!port.isOpen()){
            qDebug()<<"port is not open - not setting lighs";
            port.open(QIODevice::ReadWrite);
            if(!port.isOpen()){
                //dont't complain about not being able to set lights when shutting down
                if(lights.lightData!="000"){
                    if(stressTestRunning)
                        pendingStressAnswers=1;
                    timeoutSlot();
                    //QTimer::singleShot(100, this, SLOT(timeoutSlot()));
                }
                result = false;
            }
        }
        if(port.isOpen()) {
            if((!stressTestRunning)||(pendingStressAnswers<=0)){
                //when stresstest is running, only send new message if no answer is pending
                if(stressTestRunning)
                    pendingStressAnswers=1;
                qDebug()<<"setting lights to"<<lights.lightData;
                port.write("L"+lights.lightData+"\n");
                port.waitForBytesWritten();
                port.flush();
                //when the main thread is busy doing a test, for some reason the single shot timer event comes before the answer from the iocontrol board gets processed
                //to solve this, only if some consecutive timeouts occure the warning will be issued, and the counter will be reset every time an answer is received.
                if(lights.lightData!="000"){
                    //QTimer::singleShot(1000, this, SLOT(timeoutSlot()));
                    //qDebug()<<"starting watchdog";
                    watchdog.start();
                }
            }
        }
    }
    if(startCapture){
        if(delay==-1){
            //delay=120000;//two minutes delay if no trigger is received
            connect(this, SIGNAL(start()), this, SLOT(triggerSlot()));
            connect(this, SIGNAL(triggerNext()), this, SLOT(triggerSlot()));
        } else if(0<delay){
            QTimer::singleShot(delay, this, SLOT(triggerSlot()));
        } else {
            //delay==0
            triggerSlot();
        }
    }
    return result;
}

void IOControl::sendStatus(int status)
{
    QByteArray msg;
    switch(status){
    case -1:
    default:
        msg="ST";//testing
        break;
    case 0:
        msg="SP";//pass
        break;
    case 1:
        msg="SF";//fail
        break;
    }
   sendMessage(msg);
}

void IOControl::sendMessage(QByteArray msg)
{
    if(!port.isOpen()){
        qDebug()<<"port is not open";
        port.open(QIODevice::ReadWrite);
    }
    if(!port.isOpen()){
        return;
    }
    port.write(msg+"\n");
}

void IOControl::stressTest(const ImagesAndLights &lights, bool start)
{
    if(!(stressTestRunning=start)){
       //stop the test
        return;
    }
    stressLights.lightData=lights.lightData;
    stressLights.lightsEnabled=lights.lightsEnabled;
    pendingStressAnswers=0;
    setLights(lights,false);
}
