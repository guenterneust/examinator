/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FILEDELETER_H
#define FILEDELETER_H

#include <QWidget>
#include <QVector>
#include <QString>
#include <QDir>
#include <QMultiMap>
#include <QListWidgetItem>

namespace Ui {
class FileDeleter;
}

class FileDeleter : public QWidget
{
    Q_OBJECT

public:
    explicit FileDeleter(QWidget *parent = nullptr);
    ~FileDeleter();
    void setPathConverter(QString combinedString);
    QString convertPath(QString input);

private slots:
    void on_dirSelButton_clicked();

    void on_filesList_itemSelectionChanged();

    void on_deleteBtn_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_classifierList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_classifierList_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::FileDeleter *ui;
    QVector<QString> pathConverterFrom, pathConverterTo;
    void addPathConverter(const QString from, const QString to);
    QDir dir;
    void updateStats(void);
    //update the files list, using the folder already stored in dir.
    void updateFilesList(void);
    void updateClassifierList(void);
    QMultiMap<long long, QString> classifierBoards;
signals:
    void requestClassifier(long long classifier, QString fileName=QString());
};

#endif // FILEDELETER_H
