/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "checksn.h"
#include <QList>
#include <QtConcurrentRun>
#include <QTcpSocket>

#define MAXRECORDS 200

QdmSNCheck::QdmSNCheck(QString settingsPath){
    dbSettings=new DbSettings(qApp->organizationName(),qApp->applicationName(), settingsPath);
    static QSqlDatabase qdmDb2;
    if(dbSettings->qdmDBType.compare("NONE",Qt::CaseInsensitive)!=0)
        qdmDb2=QSqlDatabase::addDatabase(dbSettings->qdmDBType,"qdmConnection");
    static QSqlDatabase localDb2=QSqlDatabase::addDatabase(dbSettings->localDBType,"localConnection");
    qdmDb = &qdmDb2;
    localDb = &localDb2;
    qDebug()<<"dbsettings are"<<dbSettings->qdmDBType<<dbSettings->qdmDBHost<<dbSettings->qdmConnString<<dbSettings->qdmDBUser<<dbSettings->qdmDBPassword;
    qdmDb->setHostName(dbSettings->qdmDBHost);
    qdmDb->setDatabaseName(dbSettings->qdmConnString);
    qdmDb->setUserName(dbSettings->qdmDBUser);
    qdmDb->setPassword(dbSettings->qdmDBPassword);
    if(dbSettings->useLocalDB){
        localDb = &localDb2;
        localDb->setHostName(dbSettings->localDBHost);
        localDb->setDatabaseName(dbSettings->localConnString);
        localDb->setUserName(dbSettings->localDBUser);
        localDb->setPassword(dbSettings->localDBPassword);
    }
    updating=false;
}

QdmSNCheck::~QdmSNCheck()
{
    delete dbSettings;
}

QString QdmSNCheck::getLastError(){
    return lastError;
}

/** Check one serial number
  If testCode is available, check also the previous steps, as defined in QDM
*/
bool QdmSNCheck::checkSN(const QString & serialNumber/**< serial number */,
                         const QString & mat/**< material number */,
                         const QString & testCode/**< test code or "" */,
                         const QString & validationPattern/**< regular expression used to validate the serial number */,
                         const QString & testPlaceType/**< string identifying the test place type as needed by QDM */,
                         const QString & testPlaceNumber/**< string of digits identifying the test place as needed by QDM */){
    lastError="";
    QString sn=serialNumber/*.toUpper()*/;
    QStringList dummySN=QStringList()<<"TESTA"<<"111111111"<<"TESTAA"<<"11111111111111";
    if(dummySN.contains(sn,Qt::CaseInsensitive)){//dummy barcode, no check needed
        emit warningMsg("dummy barcode!");
        return true;
    }
    QRegExp regExp(validationPattern);
    qDebug()<<"validationPattern"<<validationPattern<<"sn"<<sn;
    if(!regExp.exactMatch(sn)){
        lastError="Serial number "+sn+" does not match pattern \"" + validationPattern+ "\"";
        emit errorMsg(lastError);
        return false;
    }
    qDebug()<<"validation OK";
    qDebug()<<dbSettings;
    qDebug()<<dbSettings->qdmDBType;
    if(dbSettings->qdmDBType.compare("NONE", Qt::CaseInsensitive)!=0) {
        qDebug()<<"using qdm db";
        if(updating){
            lastError="updating local database. Using QDM database";
            emit warningMsg(lastError);
            /*switch(dbSettings->missingQDMError){
        case 1://warning
            qDebug()<<"warning"<<lastError;
            emit warningMsg(lastError);
        case 0://just accept
            return true;
        default://error
            emit errorMsg(lastError);
            return false;
        }
        */
        }
        bool ok;
        /* always use the QDM database when testcode is given */
        if((dbSettings->useLocalDB)&&(testCode=="")&&(!updating)){
            //localDb=QSqlDatabase::addDatabase(dbSettings->localDBType);
            localDb->setDatabaseName(dbSettings->localConnString);
            if(dbSettings->localDBHost!="")
                localDb->setHostName(dbSettings->localDBHost);
            localDb->setUserName(dbSettings->localDBUser);
            localDb->setPassword(dbSettings->localDBPassword);
            qDebug()<<"localQuery";
            ok = localDb->open();
            QSqlQuery localQuery(*localDb);
            QString sql="CREATE TABLE IF NOT EXISTS `EXEMPLAR` (`SERIENNUMMER` VARCHAR NOT NULL, "
                    "`AUFTRAGSNUMMER` VARCHAR NOT NULL,`MATERIAL_NR` VARCHAR NOT NULL, PRIMARY KEY (`SERIENNUMMER`))";
            if(ok){
                qDebug()<<dbSettings->localDBType<<dbSettings->localConnString;
                if(!localQuery.exec(sql)){
                    qDebug()<<"unable to create local database"<<localDb->lastError().text()<<dbSettings->localDBType<<dbSettings->localConnString<<
                              dbSettings->localDBHost<<dbSettings->localDBUser<<dbSettings->localDBPassword;
                    emit errorMsg(lastError="unable to create local database "+dbSettings->localDBType+", "+dbSettings->localConnString+
                            ", "+dbSettings->localDBHost+", "+dbSettings->localDBUser+", "+dbSettings->localDBPassword);
                } else {
                    //
                    localQuery.exec(QString("SELECT distinct SERIENNUMMER,  MATERIAL_NR ")+
                                    "FROM EXEMPLAR "+
                                    "WHERE SERIENNUMMER ='"+sn+"'");
                    if(localQuery.next()){
                        qDebug()<< localQuery.value(0) << localQuery.value(1);
                        if(mat!=localQuery.value(1).toString()){
                            QString err;
                            err="Info: serial number "+sn+" has material number " + localQuery.value(1).toString();
                            emit errorMsg(err);
                            //any error will be double checked in the QDM database
                        } else {
                            //serial number in local database is OK, just accept it
                            localDb->close();
                            return true;
                        }
                    }
                }
                localDb->close();
            }
        }
        QTime t1=QTime::currentTime();
        ok = qdmDb->open();
        qDebug() <<"database open:"<< ok;
#define STOREDPROCEDURE
#ifndef STOREDPROCEDURE
        if(ok){
            QSqlQuery q(*qdmDb);
            q.exec(QString("SELECT distinct EX.SERIENNUMMER,  EX.MATERIAL_NR, BD.BAUTEIL_MAT_BEZ, AUFTRAGSNUMMER ")+
                   "FROM EXEMPLAR EX, BAUTEILDATEN BD "+
                   "WHERE EX.MATERIAL_NR = BD.BAUTEIL_MAT_NR "+
                   "AND SERIENNUMMER ='"+sn+"'");
            if(q.next()){
                qDebug()<< q.value(0) << q.value(1) <<q.value(2);
                if(mat!=q.value(1).toString()){
                    lastError="Serial number "+sn+" has material number " + q.value(1).toString() + " (" + q.value(2).toString()+ ")";
                    //ui->MSGList->addItem(err);
                    //ui->MSGList->scrollToBottom();
                    //QMessageBox::warning(NULL, "error", err);
                    qdmDb->close();
                    emit errorMsg(lastError);
                    qDebug()<<QTime::currentTime().msecsTo(t1);
                    return(false);
                }
                //Serial number OK
                //QMessageBox::warning(this, "error", q.value(0).toString()+" apartine de "+ q.value(1).toString()+ " " + q.value(2).toString());
                if((dbSettings->useLocalDB)&&(testCode==""))
                    emit updateRequest(sn, mat, q.value(3).toString());
            } else {
                //no record found
                lastError="Serial number "+sn+" not found";
                //ui->MSGList->addItem(err);
                //ui->MSGList->scrollToBottom();
                //QMessageBox::warning(NULL, "error", err);
                emit errorMsg(lastError);
                qdmDb->close();
                qDebug()<<QTime::currentTime().msecsTo(t1);
                return(false);
            }
            qdmDb->close();
        } else {
            if( useSocketCheck ){
                return checkSNSocket(sn, mat, testCode);
            } else {
                lastError="Can't open QDM database";
                //ui->MSGList->addItem(err);
                //ui->MSGList->scrollToBottom();
                //QMessageBox::warning(NULL, "error", lastError);
                switch(dbSettings->missingQDMError){
                case 1://warning
                    qDebug()<<"warning"<<lastError;
                    emit errorMsg(lastError);
                case 0://just accept
                    return true;
                default://error
                    emit errorMsg(lastError);
                    return false;
                }
            }
        }
#else
        if(ok){//qdm database is open, check using stored procedure
            qDebug()<<"storedProc";
            QSqlQuery q(*qdmDb);
            QString sql="call work_exemplar.check_exemplar_ex(:exemplar , :mat_nr , :auf_nr , :hierarchie, :pruefcode, :result )";
            q.prepare(sql);
            qDebug()<<"err"<<q.lastError();
            QString result="res";
            result.reserve(100);
            q.bindValue(":exemplar", sn/*.toUpper()*/);
            //qDebug()<<"err"<<q.lastError()<<result;
            q.bindValue(":mat_nr", mat);
            //qDebug()<<"err"<<q.lastError();
            q.bindValue(":auf_nr", "");
            //qDebug()<<"err"<<q.lastError();
            q.bindValue(":hierarchie", "");
            //qDebug()<<"err"<<q.lastError();
            q.bindValue(":pruefcode", testCode);
            //qDebug()<<"err"<<q.lastError();
            q.bindValue(":result", result, QSql::Out);
            //qDebug()<<"err"<<q.lastError();
            q.exec();
            qDebug()<<q.lastError()<<q.lastQuery();
            //qDebug()<<"err2"<<q.lastError();
            result=q.boundValue(":result").toString();
            //qDebug()<<"result="<<result<<q.boundValue(":result");
            qDebug()<<t1.msecsTo(QTime::currentTime());
            if(result.compare("sn_ok",Qt::CaseInsensitive)==0){
                qdmDb->close();
                if((dbSettings->useLocalDB)&&(testCode==""))
                    emit updateRequest(sn, mat, "");
                return true;
            }
            QString err=result;
            if(result.startsWith("not_ok",Qt::CaseInsensitive)){
                q.exec(QString("SELECT distinct EX.SERIENNUMMER,  EX.MATERIAL_NR, BD.BAUTEIL_MAT_BEZ ")+
                       "FROM EXEMPLAR EX, BAUTEILDATEN BD "+
                       "WHERE EX.MATERIAL_NR = BD.BAUTEIL_MAT_NR "+
                       "AND SERIENNUMMER ='"+sn/*.toUpper()*/+"'");
                if(q.next()){
                    err="Serial number "+sn+" has material number " + q.value(1).toString() + " (" + q.value(2).toString()+ ")";
                } else {
                    err="Serial number "+sn+" not found";
                }
            } else
                if(result.startsWith("missing test",Qt::CaseInsensitive)){
                    //missing previous test
                    QString code=result.mid(13,3);
                    err="Test with Testcode " + code + " missing!!!";
                } else
                    if(result.startsWith("wrong sequence",Qt::CaseInsensitive)){
                        //test sequence wrong
                        QString code1=result.mid(15,3);
                        QString code2=result.mid(23,3);
                        err="Test sequence wrong! Found " + code1 + " instead of " + code2 + "!!!";
                    }
            qDebug()<<"dbErr"<<err;
            emit errorMsg(lastError=err);
            qdmDb->close();
            return false;
        } else {
            qDebug()<<"socketCheck";
            if( dbSettings->useSocketCheck ){
                return checkSNSocket(sn, mat, testCode, testPlaceType, testPlaceNumber);
            } else {
                lastError="Can't open QDM database";
                //ui->MSGList->addItem(err);
                //ui->MSGList->scrollToBottom();
                //QMessageBox::warning(NULL, "error", lastError);
                switch(dbSettings->missingQDMError){
                case 1://warning
                    qDebug()<<"warning"<<lastError;
                    emit errorMsg(lastError);
                    return true;
                case 0://just accept
                    return true;
                default://error
                    emit errorMsg(lastError);
                    return false;
                }
            }
        }
#endif
        qDebug()<<QTime::currentTime().msecsTo(t1);
    } else {
        qDebug()<<"socketCheck?";
        if( dbSettings->useSocketCheck )
            return checkSNSocket(sn, mat, testCode, testPlaceType, testPlaceNumber);
    }
    return true;

}
/**
  Store all serial numbers having the same order number and material number as the reference board to the local database.
  This function is only starting updateLocalDbRun, where the actual work is done, in a separate thread.
*/
void QdmSNCheck::updateLocalDb(QString sn, QString mat, QString op){
    QtConcurrent::run(this, &QdmSNCheck::updateLocalDbRun, sn, mat, op);
}
/**
  Store all serial numbers having the same order number and material number as the reference board to the local database.
  This function is normally run in a separate thread.
*/
void QdmSNCheck::updateLocalDbRun(QString sn, QString mat, QString op){
    sn=sn/*.toUpper()*/;
    qDebug()<<"updating"<<sn<<mat<<op;
    if(updating){
        qDebug()<<"busy updating";
        return;
    }
    updating=true;
    QSqlDatabase localDbU=QSqlDatabase::addDatabase(dbSettings->localDBType,"localUpdateConnection");;
    localDbU.setHostName(dbSettings->localDBHost);
    localDbU.setDatabaseName(dbSettings->localConnString);
    localDbU.setUserName(dbSettings->localDBUser);
    localDbU.setPassword(dbSettings->localDBPassword);
    QSqlDatabase qdmDbU=QSqlDatabase::addDatabase(dbSettings->qdmDBType,"qdmUpdateConnection");;
    qdmDbU.setHostName(dbSettings->qdmDBHost);
    qdmDbU.setDatabaseName(dbSettings->qdmConnString);
    qdmDbU.setUserName(dbSettings->qdmDBUser);
    qdmDbU.setPassword(dbSettings->qdmDBPassword);

    //if(localDbClone.open()){
    if(localDbU.open()){
        QSqlQuery localQuery(localDbU);
        QString sql="CREATE TABLE IF NOT EXISTS `EXEMPLAR` (`SERIENNUMMER` VARCHAR NOT NULL, "
                "`AUFTRAGSNUMMER` VARCHAR NOT NULL,`MATERIAL_NR` VARCHAR NOT NULL, PRIMARY KEY (`SERIENNUMMER`))";
        if(!localQuery.exec(sql)){
            qDebug()<<"unable to create local database"<<dbSettings->localDBType<<dbSettings->localConnString<<dbSettings->localDBHost<<dbSettings->localDBUser<<dbSettings->localDBPassword;
            emit errorMsg("unable to create local database "+dbSettings->localDBType+", "+dbSettings->localConnString+", "+dbSettings->localDBHost+", "+dbSettings->localDBUser+", "+dbSettings->localDBPassword);
            updating=false;
            return;
        }
        sql="SELECT AUFTRAGSNUMMER, MATERIAL_NR FROM EXEMPLAR WHERE SERIENNUMMER='"+sn+"'";
        localQuery.exec(sql);
        if(localQuery.next()){
            //serial number found in database
            qDebug()<< sql << "serial number found in local database";
            //emit infoMsg( sql + " serial number found in local database");
            updating=false;
            return;
        }
        {
            QList<QString> snList;
            //serial number not present in local database. Download from server
            //qDebug()<<"isOpen"<<qdmDb->isOpen()<<qdmDb->isOpenError();
            if(!qdmDbU.open()){
                // unable to open database

                qDebug()<<"unable to open QDM database"<<qdmDbU.isOpen()<<qdmDbU.isOpenError()<<qdmDbU.lastError();
                emit errorMsg("unable to open QDM database");
            } else {
                qDebug()<<"qdmDbU open";
                QSqlQuery qdmQuery(qdmDbU);
                qdmQuery.setForwardOnly(true);
                if((op=="")||(mat=="")){
                    sql="SELECT AUFTRAGSNUMMER, MATERIAL_NR FROM EXEMPLAR WHERE SERIENNUMMER='"+sn+"'";
                    qdmQuery.exec(sql);
                    if(!qdmQuery.next()){
                        //serial number not found in qdm database
                        qDebug()<< sql << "serial number not found in qdm database";
                        emit errorMsg(sql + ": serial number not found in qdm database");
                        goto wrap_up;
                    }
                    if(op=="")
                        op=qdmQuery.value(0).toString();
                    if(mat=="")
                        mat=qdmQuery.value(1).toString();
                }
                sql="SELECT SERIENNUMMER FROM EXEMPLAR WHERE AUFTRAGSNUMMER='"+op+"' AND MATERIAL_NR='"+mat+"'" ;
                qdmQuery.exec(sql);
                while(qdmQuery.next()){
                    QString s=qdmQuery.value(0).toString();
                    qDebug()<<s;
                    snList.append(s);
                }
                localQuery.exec("PRAGMA journal_mode = OFF ");
                localQuery.exec("BEGIN TRANSACTION");
                int record;
                record=0;
                foreach(QString s, snList){
                    sql="DELETE FROM EXEMPLAR WHERE SERIENNUMMER='"+s+"'";
                    localQuery.exec(sql);
                    sql="INSERT INTO EXEMPLAR (SERIENNUMMER, AUFTRAGSNUMMER, MATERIAL_NR) VALUES('"+
                            s +"','"+
                            op +"','"+
                            mat +"')";
                    //qDebug()<<sql;
                    if(!localQuery.exec(sql)){
                        //should not happen, it's a local database and duplicate serial numbers are previously deleted
                        qDebug()<<"Database error";
                        emit warningMsg("unable to execute "+sql);
                        emit warningMsg("disabling local database for this session");
                        emit warningMsg("call system administrator ");
                        dbSettings->useLocalDB=false;
                        break;
                    }
                    if(record++ > MAXRECORDS){
                        emit infoMsg(QString::number(record)+ "serial numbers saved to local database");
                        localQuery.exec("COMMIT");
                        localQuery.exec("BEGIN TRANSACTION");
                        record=0;
                    }
                }
                localQuery.exec("COMMIT");
wrap_up:
                ;

            }
            //qdmDb->close();
            qdmDbU.close();
        }
    } else {
        qDebug()<<"unable to open localdbclone";
        emit warningMsg("unable to open local database");
    }
    localDbU.close();
    updating=false;
}

QString QdmSNCheck::SNtoMaterial(QString sn,
                                 bool checkOrder){
    lastError="";
    /*sn=sn.toUpper();*/
    QStringList dummySN=QStringList()<<"TESTA"<<"111111111"<<"TESTAA"<<"11111111111111";
    if(dummySN.contains(sn)){//dummy barcode, no check needed
        emit warningMsg("dummy barcode!");
        return "";
    }
    if(updating){
        emit errorMsg(lastError="updating local database. QDM search only!");
    }
    bool ok;
    /* always use the QDM database when testcode is given */
    if((!updating)&&(dbSettings->useLocalDB)){
        //localDb=QSqlDatabase::addDatabase(dbSettings->localDBType);
        localDb->setDatabaseName(dbSettings->localConnString);
        if(dbSettings->localDBHost!="")
            localDb->setHostName(dbSettings->localDBHost);
        localDb->setUserName(dbSettings->localDBUser);
        localDb->setPassword(dbSettings->localDBPassword);
        ok = localDb->open();
        QSqlQuery localQuery(*localDb);
        QString sql="CREATE TABLE IF NOT EXISTS `EXEMPLAR` (`SERIENNUMMER` VARCHAR NOT NULL, "
                "`AUFTRAGSNUMMER` VARCHAR NOT NULL,`MATERIAL_NR` VARCHAR NOT NULL, PRIMARY KEY (`SERIENNUMMER`))";
        if(ok){
            qDebug()<<dbSettings->localDBType<<dbSettings->localConnString;
            if(!localQuery.exec(sql)){
                qDebug()<<"unable to create local database"<<dbSettings->localDBType<<dbSettings->localConnString<<
                          dbSettings->localDBHost<<dbSettings->localDBUser<<dbSettings->localDBPassword;
                emit errorMsg(lastError="unable to create local database "+dbSettings->localDBType+", "+dbSettings->localConnString+
                              ", "+dbSettings->localDBHost+", "+dbSettings->localDBUser+", "+dbSettings->localDBPassword);
            } else {
                //
                localQuery.exec(QString("SELECT distinct SERIENNUMMER,  MATERIAL_NR ")+
                                "FROM EXEMPLAR "+
                                "WHERE SERIENNUMMER ='"+sn+"'");
                if(localQuery.next()){
                    qDebug()<< localQuery.value(0) << localQuery.value(1);
                    //serial number in local database is OK, just accept it
                    localDb->close();
                    return localQuery.value(1).toString();
                } //if nothing is found, check the QDM database
            }
            localDb->close();
        }
    }
    /* only for speed measurement */
    QTime t1=QTime::currentTime();
    if(dbSettings->qdmDBType.compare("NONE",Qt::CaseInsensitive)!=0){
        ok = qdmDb->open();
        qDebug() <<"database open:"<< ok;
        if(ok){
            QSqlQuery q(*qdmDb);
            q.exec(QString("SELECT distinct EX.SERIENNUMMER,  EX.MATERIAL_NR, BD.BAUTEIL_MAT_BEZ, AUFTRAGSNUMMER ")+
                   "FROM EXEMPLAR EX, BAUTEILDATEN BD "+
                   "WHERE EX.MATERIAL_NR = BD.BAUTEIL_MAT_NR "+
                   "AND SERIENNUMMER ='"+sn+"'");
            if(q.next()){
                qDebug()<< q.value(0) << q.value(1) <<q.value(2);
                //Serial number OK
                //QMessageBox::warning(this, "error", q.value(0).toString()+" apartine de "+ q.value(1).toString()+ " " + q.value(2).toString());
                //serial number in local database is OK, just accept it
                qdmDb->close();
                return q.value(1).toString();
            } else {
                //no record found
                if(checkOrder){
                    q.exec("SELECT distinct MATERIAL_NR "
                                   "FROM AUFTRAG "
                                  "WHERE AUFTRAGSNUMMER ='"+sn+"'");

                    if(q.next()){
                        qDebug()<< q.value(0);
                        //Order found
                        //serial number in local database is OK, just accept it
                        qdmDb->close();
                        return q.value(0).toString();
                    }
                }
                lastError="Serial number "+sn+" not found";
                //ui->MSGList->addItem(err);
                //ui->MSGList->scrollToBottom();
                //QMessageBox::warning(NULL, "error", err);
                emit errorMsg(lastError);
                qdmDb->close();
                qDebug()<<QTime::currentTime().msecsTo(t1);
                return "";
            }
            qdmDb->close();
        } else {
            lastError="Can't open QDM database";
            //ui->MSGList->addItem(err);
            //ui->MSGList->scrollToBottom();
            //QMessageBox::warning(NULL, "error", lastError);
            emit errorMsg(lastError);
            return "";
        }
    } else {
        if(dbSettings->useSocketCheck){
            QString result=SNtoMaterialSocket(sn);
            if((result.length()<6) || (result.split(' ').count()!=1)){
                emit errorMsg("error: "+result);
                return "";
            }
            return result;
        }
    }
    qDebug()<<QTime::currentTime().msecsTo(t1);
    return "";

}

QString QdmSNCheck::SNtoMaterialSocket(const QString serialNumber)
{
    qDebug()<<"socketcheck"<<serialNumber<<dbSettings->socketCheckServer<<dbSettings->socketCheckPort.toInt();
    lastError="";
    QString sn=serialNumber/*.toUpper()*/;
    QString answer;
    //bool result=true;
    QTcpSocket * tcpSocket = new QTcpSocket(this);
    tcpSocket->connectToHost(dbSettings->socketCheckServer,dbSettings->socketCheckPort.toInt());
    tcpSocket->waitForConnected(200);
    if(tcpSocket->state()==QAbstractSocket::ConnectedState){
        QString str=sn+"    ";
        tcpSocket->write((str+"\n").toLatin1().constData());
        tcpSocket->waitForBytesWritten(200);
        tcpSocket->waitForReadyRead(3000);
        answer=tcpSocket->readAll();
        qDebug()<<"got"<<answer;
        tcpSocket->close();
        answer=answer.simplified();
    } else {
        answer=lastError="Can't open connection to " + dbSettings->socketCheckServer;
    }
    tcpSocket->deleteLater();
    return answer;
}

bool QdmSNCheck::checkSNSocket(const QString &serialNumber/**< serial number */,
                         const QString & mat/**< material number */,
                         const QString &testCode/**< test code or "" */,
                               const QString &testPlaceType/**<  */,
                               const QString & testPlaceNumber/**<  */){
    qDebug()<<"socketcheck"<<serialNumber<<mat<<testCode<<dbSettings->socketCheckServer<<dbSettings->socketCheckPort.toInt();
    lastError="";
    QString sn=serialNumber/*.toUpper()*/;
    QStringList dummySN=QStringList()<<"TESTA"<<"111111111"<<"TESTAA"<<"11111111111111";
    if(dummySN.contains(sn, Qt::CaseInsensitive)){//dummy barcode, no check needed
        emit warningMsg("dummy barcode!");
        return true;
    }
    QString answer;
    bool result=true;
    QTcpSocket * tcpSocket = new QTcpSocket(this);
    tcpSocket->connectToHost(dbSettings->socketCheckServer,dbSettings->socketCheckPort.toInt());
    tcpSocket->waitForConnected(200);
    if(tcpSocket->state()==QAbstractSocket::ConnectedState){
        QString str=sn+" "+mat;
        //if(testCode!="")
        str += " " + testCode;
        str += " "; //+ auftrag="";
        str += " " + testPlaceType + testPlaceNumber;
        if(testPlaceNumber.length()==7)
            str += " " + testPlaceNumber;
        else
            str += " " + testPlaceType;
        tcpSocket->write((str+"\n").toLatin1().constData());
        tcpSocket->waitForBytesWritten(200);
        bool ok=tcpSocket->waitForReadyRead(3000);
        if(!ok){
            qDebug()<<"waitForReadyRead failed";
            ok=tcpSocket->waitForReadyRead(3000);
            if(!ok){
                qDebug()<<"waitForReadyRead failed again";
                ok=tcpSocket->waitForReadyRead(3000);
                if(!ok){
                    qDebug()<<"waitForReadyRead failed again";
                }
            }
        }
        answer=tcpSocket->readAll();
        qDebug()<<"got"<<answer;
        tcpSocket->close();
        answer=answer.simplified();
        if(!answer.startsWith("OK")){
            result=false;
            lastError="answer \""+answer+"\"";
            lastError += "\nrequest was \""+str+"\"";
            emit errorMsg(lastError);
        }
    } else {
        lastError="Can't open connection to " + dbSettings->socketCheckServer;
        switch(dbSettings->missingQDMError){
        case 1://warning
            qDebug()<<"warning"<<lastError;
            emit errorMsg(lastError);
            break;
        case 0://just accept
            break;
        default://error
            emit errorMsg(lastError);
            result=false;
        }
    }
    tcpSocket->deleteLater();
    return result;
}

bool QdmSNCheck::fastReportSNStatusSocket(const QString &serialNumber/**< serial number */,
                         const QString & mat/**< material number */,
                         const QString &testCode/**< test code or "" */,
                               const QString &testPlaceType/**<  */,
                               const QString & testPlaceNumber/**<  */,
                               const int status){
    lastError="";
    if(!dbSettings->fastReportSNStatus)
        return false;//just report failure, makeProtocol will make the leika file without marking the protocol with nostatus
    qDebug()<<"fastReportSNStatusSocket"<<serialNumber<<mat<<testCode<<dbSettings->socketCheckServer<<dbSettings->socketCheckPort.toInt()<<status;
    QString sn=serialNumber/*.toUpper()*/;
    QString answer;
    bool result=true;
    QTcpSocket * tcpSocket = new QTcpSocket(this);
    tcpSocket->connectToHost(dbSettings->socketCheckServer,dbSettings->socketCheckPort.toInt());
    tcpSocket->waitForConnected(200);
    if(tcpSocket->state()==QAbstractSocket::ConnectedState){
        QString str=sn+" "+mat;
        //if(testCode!="")
        str += " " + QString::number(status);
        str += " " + testCode;
        str += " "; //+ auftrag="";
        str += " " + testPlaceType + testPlaceNumber;
//        if(testPlaceNumber.length()==7)
//            str += " " + testPlaceNumber;
//        else
//            str += " " + testPlaceType;
        tcpSocket->write((str+"\n").toLatin1().constData());
        tcpSocket->waitForBytesWritten(200);
        bool ok=tcpSocket->waitForReadyRead(3000);
        if(!ok){
            qDebug()<<"waitForReadyRead failed";
            ok=tcpSocket->waitForReadyRead(3000);
            if(!ok){
                qDebug()<<"waitForReadyRead failed again";
                ok=tcpSocket->waitForReadyRead(3000);
                if(!ok){
                    qDebug()<<"waitForReadyRead failed again";
                }
            }
        }
        answer=tcpSocket->readAll();
        qDebug()<<"got"<<answer;
        tcpSocket->close();
        answer=answer.simplified();
        if(!answer.startsWith("OK")){
            result=false;
            lastError="answer \""+answer+"\"";
            lastError += "\nrequest was \""+str+"\"";
            emit errorMsg(lastError);
        }
    } else {
        lastError="Can't open connection to " + dbSettings->socketCheckServer;
        switch(dbSettings->missingQDMError){
        case 1://warning
        default://don't signal error, result will be transmitted as regular leika file
            qDebug()<<"warning"<<lastError;
            emit errorMsg(lastError);
            break;
        case 0://just accept
            break;
        }
    }
    tcpSocket->deleteLater();
    return result;
}
