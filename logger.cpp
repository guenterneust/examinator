/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "logger.h"
#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <QDateTime>

Logger::Logger(QObject *parent) : QObject(parent)
{

}

Logger::~Logger()
{
    if(logFile.isOpen())
        logFile.close();
}

bool Logger::setLogFile(QString fileName)
{
    qDebug()<<"logFile"<<fileName;
    QFileInfo info(fileName);
    info.absoluteDir().mkpath(info.absolutePath());
    if(info.exists()){
        if(info.size()>MAXLOGSIZE){
            //file is quite big, create backup and use a new file
            //search for an unused file name
            for(int i=1; i<10000; i++){
                QString backupName=info.absolutePath()+"/"+info.completeBaseName()+"_"+QString::number(i)+"."+info.suffix();
                QFileInfo info2(backupName);
                if(!info2.exists()){
                    //unused filename found, rename the old log file to the new name
                    QFile backupFile(info.absoluteFilePath());
                    backupFile.rename(info2.absoluteFilePath());
                    break;
                }
                //try the next name
            }
        }
    }
    logFile.setFileName(info.absoluteFilePath());
    if(logFile.exists()){
        return true;
    }
    bool result=logFile.open(QIODevice::WriteOnly|QIODevice::Text);

    return result;
}

void Logger::setLogVerbosity(LOGVERBOSITY verbosity)
{
    logVerbosity=verbosity;
}

void Logger::logMessage(QString message, LOGVERBOSITY criticality)
{
    static bool loggingOK=true;
    if(criticality<=logVerbosity){
        qDebug()<<"logMessage"<<message<<criticality<<logVerbosity;
        if(!logFile.isOpen()){
            logFile.open(QIODevice::WriteOnly|QIODevice::Text|QIODevice::Append);
        }
        if(!logFile.isOpen()){
            qDebug()<<"unable to log message"<<message<<criticality;
            if(loggingOK==true){
                QMessageBox::warning(NULL, "Eroare deschidere fisier "+logFile.fileName(),
                                     "Nu se poate inregistra mesajul "+message+
                                     " "+QString::number(criticality));
                loggingOK=false;
                return;
            }
        }
        qDebug()<<"write"<<QDateTime::currentDateTime().toString("dd-MM-yy HH:mm:ss ")<<message;
        logFile.write((QDateTime::currentDateTime().toString("dd-MM-yy HH:mm:ss ")+message+"\n").toLatin1());
        logFile.close();
    }
    loggingOK=true;
}
