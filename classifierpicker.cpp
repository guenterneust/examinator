/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "classifierpicker.h"
#include "ui_classifierpicker.h"

ClassifierPicker::ClassifierPicker(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClassifierPicker)
{
    ui->setupUi(this);
}

ClassifierPicker::~ClassifierPicker()
{
    delete ui;
}

void ClassifierPicker::setValues(const QString compName, const QString compTypeName, const QList<BoardCompTypeClassifier > &classifiers)
{
    ui->compEdit->setText(compName);
    ui->compTypeEdit->setText(compTypeName);
    for(int i=0; i<classifiers.count(); i++){
        ui->classifierCombo->addItem(classifiers[i].compDefPtr->name, classifiers[i].compDefPtr->id);
    }
}


void ClassifierPicker::on_acceptBtn_clicked()
{
    if(ui->classifierCombo->count()<=0){
        this->done(0);
        return;
    }
    //int id=ui->classifierCombo->currentData().toInt();
    selection=ui->classifierCombo->currentIndex();
    //qDebug()<<"currentIndex"<<ui->classifierCombo->currentIndex()<<this->result();
    this->done(selection);
}

void ClassifierPicker::on_rejectBtn_clicked()
{
    selection=-1;
    this->done(selection);
}
