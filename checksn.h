/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHECKSN_H
#define CHECKSN_H

#include <QString>
#include <QSettings>
#include <QApplication>
#include <QtSql>
#include <QMessageBox>
#include <assert.h>
#include <QDebug>

class DbSettings{
public:
    QString qdmDBType;///< QDM database type, usually QOCI or QODBC
    QString qdmConnString;///< connection string to database, or database name
    QString qdmDBHost;///< host of the QDM database
    QString qdmDBUser;///< user name for qdm database access
    QString qdmDBPassword;///< password for qdm database access
    QString localDBType;///< local database type, currently QSQLITE is used
    QString localConnString;///< local database connection string or database name. For QSQLITE use the filename, including the path!
    QString localDBHost;///< host for the local database. For QSQLITE leave this empty.
    QString localDBUser;///< user name for the local database. For QSQLITE this field is ignored
    QString localDBPassword;///< password for the local database. For QSQLITE this field is ignored
    bool useLocalDB;///< enable/disable use of the local storage. If set to false, all serial number checks are done using the stored procedure in QDM
    int missingQDMError;///< defines behaviour if QDM database is unreachable: 0 - accept, 1 - accept with warning, 2 - reject
    //bool delayedDownload;///< if true, the serial numbers of the current order will be downloaded by TRI2LEIKA, else by FCBarCode
    bool useSocketCheck;
    QString socketCheckServer;
    QString socketCheckPort;
    bool fastReportSNStatus;
    inline DbSettings(QString org, QString app, QString settingsPath){
        QSettings::setDefaultFormat(QSettings::IniFormat);
        QSettings * settingsPtr=NULL;
        if(settingsPath.length()>0){
            settingsPtr=new QSettings(settingsPath, QSettings::IniFormat);
            if(settingsPtr==NULL)
                settingsPtr = new QSettings(QSettings::IniFormat,QSettings::SystemScope,org,app);
        } else {
            settingsPtr = new QSettings(QSettings::IniFormat,QSettings::SystemScope,org,app);
            if(!settingsPtr->isWritable()){
                QString f=settingsPtr->fileName();
                qDebug()<<"settings not writable, exists:"<<QFile::exists(f);
                if(!QFile::exists(f)){
                    //not writable and does not exists -> change to user scope
                    delete settingsPtr;
                    settingsPtr=new QSettings(QSettings::IniFormat,QSettings::UserScope,qApp->organizationName(),qApp->applicationName());
                }
            }
        }
        assert(settingsPtr!=NULL);
        QSettings & settings = *settingsPtr;
        qDebug()<<"dbsettings"<<settings.fileName();
        bool updateIni=false;
        if(!settings.contains("global/qdmDBType"))
            updateIni=true;
        //two different default values. Normally overwritten by values from the ini file.
#if 0
        qdmDBType=settings.value("global/qdmDBType","QODBC").toString();
        qdmConnString=settings.value("global/qdmConnString","\"Driver={Microsoft ODBC for Oracle};CONNECTSTRING=QDM32;uid=QDMPRO;pwd=INFO;\"").toString();
        qdmDBHost=settings.value("global/qdmDBHost","").toString();
#else
        qdmDBType=settings.value("global/qdmDBType","NONE").toString();
        qdmConnString=settings.value("global/qdmConnString","qdm").toString();
        qdmDBHost=settings.value("global/qdmDBHost","10.99.254.101").toString();
#endif
        qdmDBUser=settings.value("global/qdmDBUser","QDMPRO").toString();
        qdmDBPassword=settings.value("global/qdmDBPassword","INFO").toString();
        localDBType=settings.value("global/localDBType","QSQLITE").toString();
        localConnString=settings.value("global/localConnString","localdb.sqlite").toString();
        localDBHost=settings.value("global/localDBHost","").toString();
        localDBUser=settings.value("global/localDBUser","QDMPRO").toString();
        localDBPassword=settings.value("global/localDBPassword","INFO").toString();
        useLocalDB=settings.value("global/useLocalDB",false).toBool();
        missingQDMError=settings.value("global/missingQDMError",1).toInt();

        //delayedDownload=settings.value("global/delayedDownload",true).toBool();
        useSocketCheck=settings.value("global/useSocketCheck",true).toBool();
        socketCheckServer=settings.value("global/socketCheckServer","192.168.170.140").toString();
        socketCheckPort=settings.value("global/socketCheckPort","51111").toString();
        fastReportSNStatus=settings.value("global/fastReportSNStatus",false).toBool();
        if(updateIni){
            settings.setValue("global/qdmDBType",qdmDBType);
            settings.setValue("global/qdmConnString",qdmConnString);
            settings.setValue("global/qdmDBHost",qdmDBHost);
            settings.setValue("global/qdmDBUser",qdmDBUser);
            settings.setValue("global/qdmDBPassword",qdmDBPassword);
            settings.setValue("global/localDBType",localDBType);
            settings.setValue("global/localConnString",localConnString);
            settings.setValue("global/localDBHost",localDBHost);
            settings.setValue("global/localDBUser",localDBUser);
            settings.setValue("global/localDBPassword",localDBPassword);
            settings.setValue("global/useLocalDB",useLocalDB);
            settings.setValue("global/missingQDMError",missingQDMError);
            //settings.setValue("global/delayedDownload",delayedDownload);
            settings.setValue("global/useSocketCheck",useSocketCheck);
            settings.setValue("global/socketCheckServer",socketCheckServer);
            settings.setValue("global/socketCheckPort",socketCheckPort);
            settings.setValue("global/fastReportSNStatus",fastReportSNStatus);
        }
        delete settingsPtr;
    }
};

class QdmSNCheck:public QObject{
    Q_OBJECT
public:
    /**
     * @brief constructor
     * @param settingsPath - path to ini file containing the settings concerning the serial number check. If empty,
     * the default ini file for the application will be used, whith the fallback mechanism enabled.
     */
    QdmSNCheck(QString settingsPath="");
    ~QdmSNCheck();
    /**
     * @brief check if the test identified by testCode can be done on product serialNumber as product type mat.
     * If testCode is available, check also the previous steps, as defined in QDM
     * This method also check if the serial number matches the validatoion pattern, before asking the server
     * @param serialNumber - serial number of the product to be tested
     * @param mat - material number as which the product will be tested
     * @param testCode - test code for the test to be done
     * @param validationPattern - regular expression used to check validity of serial number when database check is not possible
     * @param testPlaceType - string identifying the test place type, like AOI
     * @param testPlaceNumber - number identifying the test place
     * @return true if test is allowed. The result message can be retrieved with getLastError.
     */
    bool checkSN(const QString  & serialNumber, const QString & mat, const QString & testCode,
                 const QString & validationPattern, const QString & testPlaceType, const QString & testPlaceNumber);
    /**
     * @brief check if the test identified by testCode can be done on product serialNumber as product type mat.
     * The method does not check the validation pattern, assuming this check has been done before.
     * If testCode is available, check also the previous steps, as defined in QDM\n
     * This function uses a tcp port to send the check request to the verification server, and receives the
     * result from the same port.
     * @param serialNumber - serial number of the product to be tested
     * @param mat - material number as which the product will be tested
     * @param testCode - test code for the test to be done
     * @param testPlaceType - string identifying the test place type, like AOI
     * @param testPlaceNumber - number identifying the test place
     * @return true if test is allowed. The result message can be retrieved with getLastError.
     */
    bool checkSNSocket(const QString & serialNumber, const QString &mat, const QString & testCode,
                       const QString & testPlaceType, const QString &testPlaceNumber);
    /**
     * @brief report to ITAC the status of the just run test.
     * No plausibility check are performed.
     * If testCode is available, ITAC will attempt to also update the satus to QDM\n
     * This function uses a tcp port to send the board status to the verification server, and receives the
     * result from the same port.
     * @param serialNumber - serial number of the product to be tested
     * @param mat - material number as which the product will be tested
     * @param testCode - test code for the test to be done
     * @param testPlaceType - string identifying the test place type, like AOI
     * @param testPlaceNumber - number identifying the test place
     * @param status - 0 = PASS, 1=FAIL,2=SCRAP, 3 = INPROCESS; only 0 and 1 are used
     * @return true if test the status has been set. The result message can be retrieved with getLastError.
     */
    bool fastReportSNStatusSocket(const QString & serialNumber, const QString &mat, const QString & testCode,
                       const QString & testPlaceType, const QString &testPlaceNumber, const int status);
    /** return material number for a given serial number or order number
    */
    QString SNtoMaterial(QString sn/**< serial number */, bool checkOrder=true/**< also check if sn is an order number */);
    /** return material number for a given serial number or order number
    */
    QString SNtoMaterialSocket(const QString serialNumber);
    /** get error message for the last executed serial number check. Should start with OK for acceptable
     * serial numbers
    */
    QString getLastError(void);
    inline bool getUseLocalDb(void){
        return dbSettings->useLocalDB;
    }

public slots:
    void updateLocalDb(QString sn, QString mat, QString op="");
private:
    QSqlDatabase *qdmDb;
    QSqlDatabase *localDb;
    class DbSettings *dbSettings;
    bool updating;
    void updateLocalDbRun(QString sn, QString mat, QString op);
    QString lastError;
signals:
    void editSettings(void);
    void errorMsg(QString msg);
    void warningMsg(QString msg);
    void infoMsg(QString msg);
    void updateRequest(QString sn, QString mat, QString op);
};




#endif // CHECKSN_H
