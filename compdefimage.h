/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPDEFIMAGE_H
#define COMPDEFIMAGE_H
#include <QString>
#include <QList>
#include <QSqlRecord>
#include "compdefrect.h"

class CompDefImage
{
private:
public:
    long long id;
    QString imagePath;
    bool implicitNegative;///< flag telling the trainer if the entire image area should be used implicit as source for negative samples
    CompDefImage();
    CompDefImage(QString filename);
    CompDefImage(QSqlRecord record, QString newPath=QString());
    QList<CompDefRect> rect;
    CompDefRect * addNewRect(double x, double y, double w, double h, CompDefRect::Orientation orientation);
    void saveToDB(long long compdef);
    //void loadFromDB();
};

#endif // COMPDEFIMAGE_H
