/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPSPPARAMS_H
#define COMPSPPARAMS_H
//shape predictor training parameters
#include <dlib/image_processing.h>


class CompSpParams
{
public:
    CompSpParams();
    double nu=0.1;///< from https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/, the dlib example has 0.05
                ///< smaller values need more samples!
    int treeDepth=4;///< from https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/
    int cascadeDepth=10;///< minumum from from https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/
    int featurePoolSize=400;///< from https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/
    int numTestSplits=50;///< from https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/
    int oversamplingAmount=100;///< 50 from https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/, dlib example uses 300
    double oversamplingTranslationJitter=0.1;///< from https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/
    bool enabled=false;
    friend void serialize (const CompSpParams& item, std::ostream& out);

    friend void deserialize (CompSpParams& item, std::istream& in);

};

inline void serialize (const CompSpParams& item, std::ostream& out)
{
    int version = 1;
    dlib::serialize(version, out);
    dlib::serialize(item.nu, out);
    dlib::serialize(item.treeDepth, out);
    dlib::serialize(item.cascadeDepth, out);
    dlib::serialize(item.featurePoolSize, out);
    dlib::serialize(item.numTestSplits, out);
    dlib::serialize(item.oversamplingAmount, out);
    dlib::serialize(item.oversamplingTranslationJitter, out);
    dlib::serialize(item.enabled, out);
}

inline void deserialize (CompSpParams& item, std::istream& in)
{
    int version = 0;
    dlib::deserialize(version, in);
    if (version != 1)
        throw dlib::serialization_error("Unexpected version found while deserializing CompSpParams.");
    dlib::deserialize(item.nu, in);
    dlib::deserialize(item.treeDepth, in);
    dlib::deserialize(item.cascadeDepth, in);
    dlib::deserialize(item.featurePoolSize, in);
    dlib::deserialize(item.numTestSplits, in);
    dlib::deserialize(item.oversamplingAmount, in);
    dlib::deserialize(item.oversamplingTranslationJitter, in);
    dlib::deserialize(item.enabled, in);
}


#endif // COMPSPPARAMS_H
