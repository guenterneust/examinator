cmake_minimum_required(VERSION 3.5)

set(-DCMAKE_VERBOSE_MAKEFILE ON)
add_definitions(-DAPP_VERSION="0.5")
add_definitions(-DDLIB_PNG_SUPPORT)
add_definitions(-DDLIB_JPEG_SUPPORT)

project(examinator LANGUAGES CXX)
set(CMAKE_VERBOSE_MAKEFILE ON)

if(WIN32)
    set(dlib_directory C:/source/dlib/dlib CACHE PATH "path to dlib")
else()
    set(dlib_directory ../dlib/dlib CACHE PATH "path to dlib")
endif()


option(OPENCVCAM "enable use of OpenCV camera" ON)
option(BARCODEREAD "enable barcode decoder" ON)
option(RASPICAM "enable use of RaspiCV camera" ON)

if(EXISTS ${dlib_directory})
    add_subdirectory(${dlib_directory} dlib_build)
    INCLUDE_DIRECTORIES("${dlib_directory}/..")
    INCLUDE_DIRECTORIES("${dlib_directory}/external/libpng")
    INCLUDE_DIRECTORIES("${dlib_directory}/external/libjpeg")
    message(STATUS "dlib: ${dlib_directory}")
endif()

if(WIN32)
    set(OpenCV_DIR C:/source/opencv/build/install CACHE PATH "path to opencv install dir")
endif(WIN32)

find_package(OpenCV REQUIRED)
if((EXISTS ${OpenCV_DIR}) OR UNIX)
    message(STATUS "Config: ${OpenCV_DIR}")
    message(STATUS "OpenCV version: ${OpenCV_VERSION}")
    message(STATUS "OpenCV libraries: ${OpenCV_LIBS}")
    message(STATUS "OpenCV include path: ${OpenCV_INCLUDE_DIRS}")
    INCLUDE_DIRECTORIES("${OpenCV_INCLUDE_DIRS}")
endif()

file(GLOB srcs *.cpp *.c)
file(GLOB hdrs *.hpp *.h)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check http://doc.qt.io/qt-5/deployment-android.html for more information.
# They need to be set before the find_package(Qt5 ...) call.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()
if(OPENCVCAM)
    add_definitions(-DOCVCAM)
    set(OCVCAM_SOURCES opencvcam.h opencvcam.cpp )
endif(OPENCVCAM)

if(BARCODEREAD)
    add_definitions(-DBARCODEREAD) #needs definition command for dobarcoderead
    set(BARCODEREAD_SOURCES barcode.h barcode.cpp )

    #pckconfig unix zbar dmtx zxing
    if(UNIX)
        #Steps: 1.Find package 2.If found include dir 3.link libraries 4.else print error
        find_package(PkgConfig)
        pkg_check_modules(PC_ZBAR zbar) #if found sets PC_ZBAR_FOUND=1 and also finds libs and dir
        pkg_check_modules(PC_DMTX libdmtx)
        pkg_check_modules(PC_ZXING zxing)

        #ZBAR
        if(PC_ZBAR_FOUND)
            add_definitions(-DZBAR)
            include_directories(${PC_ZBAR_INCLUDEDIR})
            link_directories(${PC_ZBAR_LIBDIR}) #for target
        else(PC_ZBAR_FOUND)
            message("ERROR: ZBAR COULD NOT BE FOUND")
        endif(PC_ZBAR_FOUND)
        #DMTX
        if(PC_DMTX_FOUND)
            add_definitions(-DDMTX)
            include_directories(${PC_DMTX_INCLUDEDIR})
            link_directories(${PC_DMTX_LIBDIR})
        else(PC_DMTX_FOUND)
            message("ERROR: DMTX COULD NOT BE FOUND")
        endif(PC_DMTX_FOUND)

        #ZXING
        if(PC_ZXING_FOUND)
            add_definitions(-DZXING)
            include_directories(${PC_ZXING_INCLUDEDIR})
            link_directories(${PC_ZXING_LIBDIR})
        else(PC_ZXING_FOUND)
            message("ZXNG COULD NOT BE FOUND")
        endif(PC_ZXING_FOUND)
    endif(UNIX)

    #windows -zbar dmtx zxing
    if(WIN32)

        #zbar
        set(ZBAR_DIR C:/source/zbar CACHE PATH "path to zbar"  )
        if(EXISTS ${ZBAR_DIR}/include/zbar.h)
            set(ZBAR_LIBDIR ${ZBAR_DIR}/zbar/.libs)
            message($ZBAR)
            add_definitions(-DZBAR)
            include_directories(${ZBAR_DIR}/include)
            link_directories(${ZBAR_LIBDIR})
        else()
            message("ZBAR not found")
        endif()

        #dmtx
        set(DMTX_DIR C:/source/libdmtx CACHE PATH "path to dmtx"  )
        if(EXISTS ${DMTX_DIR}/dmtx.h)
            set(DMTX_LIBDIR ${DMTX_DIR}/.libs)
            message($DMTX)
            add_definitions(-DDMTX)
            include_directories(${DMTX_DIR})
            link_directories(${DMTX_LIBDIR})
        else()
            message("DMTX not found")
        endif()

        #zxing
        set(ZXING_DIR C:/source/zxing-cpp-master/build CACHE PATH "path to zxing")
        if(EXISTS ${ZXING_DIR}/include/ZXing/ReadBarcode.h)
            #set(ZXING_LIBDIR ${ZXING_DIR}/core/${CMAKE_BUILD_TYPE})
            set(ZXING_LIBDIR ${ZXING_DIR}/core/release)
            message($ZXING)
            add_definitions(-DZXING)
            include_directories(${ZXING_DIR}/include)
            link_directories(${ZXING_LIBDIR})
        else()
            message("ZXING not found")
        endif()
    endif(WIN32)
endif(BARCODEREAD)

if(RASPICAM)
    add_definitions(-DRASPICAM)
    set(RASPICAM_SOURCES raspicvcam.h raspicvcam.cpp)
endif()

find_package(Qt5 COMPONENTS Widgets Multimedia SerialPort Sql Charts Network LinguistTools PrintSupport REQUIRED)
find_package(Qt5Sql)
find_package(Qt5Concurrent)

#Qt5LinguistTools-------------->Setting up translation
set(TS_FILES  examinator.ts)
find_package(Qt5 COMPONENTS Widgets LinguistTools REQUIRED)
configure_file(tr.qrc ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
qt5_create_translation(QM_FILES ${CMAKE_CURRENT_SOURCE_DIR} ${TS_FILES})

#Doxygen
option(BUILD_DOC "Build documentation" ON)
if(BUILD_DOC)
    #Doxygen unix
    if(UNIX)
        find_package(Doxygen)
        if (DOXYGEN_FOUND)
            # set input and output files
            set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile)
            set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile)
            # request to configure the file
            configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
            message("Doxygen build started")
            # note the option ALL which allows to build the docs together with the application
            add_custom_target(doc_doxygen ALL
                COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                COMMENT "Generating API documentation with Doxygen"
                VERBATIM)
        else (DOXYGEN_FOUND)
            message("Doxygen need to be installed to generate the doxygen documentation")
        endif (DOXYGEN_FOUND)
    endif(UNIX)

    #doxygen windows
    if(WIN32)
        set(DOXYGEN_DIR C:/source/doxygen CACHE PATH "path to doxygen" )
        if(EXISTS ${DOXYGEN_DIR})
            set(DOXYGEN_EXECUTABLE ${DOXYGEN_DIR}/bin)
            set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile)
            set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)
            include_directories(${DOXYGEN_DIR})
            message($$DOXYGEN)
            configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
            add_custom_target(doxygen
                COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                COMMENT "Generating API documentation with Doxygen"
                VERBATIM)
        else()
            message("Doxygen need to be installed to generate the doxygen documentation")
        endif()
    endif(WIN32)
endif(BUILD_DOC)

if(ANDROID)
    add_library(examinator SHARED
        ${dlib_directory}/all/source.cpp
        main.cpp
        filedeleter.cpp
        iocontrol.cpp
        widget.cpp
        mscene.cpp
        cornergrabber.cpp
        compdefdialog.cpp
        boarddef.cpp
        boardcomptype.cpp
        boardcomp.cpp
        boardconfig.cpp
        boardcomptypeconfig.cpp
        boardcompaltdialog.cpp
        detectedcomponent.cpp
        testinterface.cpp
        testdebugwidget.cpp
        boardselect.cpp
        compdefrect.cpp
        compdefimage.cpp
        compdef.cpp
        myftp.cpp
        checksn.cpp
        cameraconfig.cpp
        ipcamera.cpp
        mycamera.cpp
        componentpathcorrection.cpp
        logger.cpp
        compspparams.cpp
        compdnnparams.cpp
        compstatmodel.cpp
        classifierpicker.cpp
        barcodecorrection.cpp
        brdinfo.cpp
        ocvhelper.cpp
		regextester.cpp
        widget.h
        compdef_dnn.h
        filedeleter.h
        iocontrol.h
        mscene.h
        cornergrabber.h
        compdefdialog.h
        boarddef.h
        boardcomptype.h
        boardcomp.h
        boardconfig.h
        boardcomptypeconfig.h
        boardcompaltdialog.h
        detectedcomponent.h
        testinterface.h
        testdebugwidget.h
        boardselect.h
        compdefrect.h
        compdefimage.h
        compdef.h
        checksn.h
        myftp.h
        cameraconfig.h
        ipcamera.h
        mycamera.h
        componentpathcorrection.h
        logger.h
        compspparams.h
        compdnnparams.h
        compstatmodel.h
        brdinfo.h
        ocvhelper.h
        widget.ui
		regextester.h
        compdefdialog.ui
        boardconfig.ui
        boardcomptypeconfig.ui
        boardcompaltdialog.ui
        filedeleter.ui
        testinterface.ui
        testdebugwidget.ui
        boardselect.ui
        cameraconfig.ui
        componentpathcorrection.ui
        classifierpicker.ui
        barcodecorrection.ui
        brdinfo.ui
		regextester.ui
        icons.qrc
        ${OCVCAM_SOURCES}
        ${BARCODEREAD_SOURCES}
        ${RASPICAM_SOURCES}
        ${TS_FILES}
        ${QM_FILES}
        ${CMAKE_CURRENT_BINARY_DIR}/tr.qrc
        )
else()
    add_executable(examinator
        ${dlib_directory}/all/source.cpp
        main.cpp
        filedeleter.cpp
        iocontrol.cpp
        widget.cpp
        mscene.cpp
        cornergrabber.cpp
        compdefdialog.cpp
        boarddef.cpp
        boardcomptype.cpp
        boardcomp.cpp
        boardconfig.cpp
        boardcomptypeconfig.cpp
        boardcompaltdialog.cpp
        detectedcomponent.cpp
        testinterface.cpp
        testdebugwidget.cpp
        boardselect.cpp
        compdefrect.cpp
        compdefimage.cpp
        compdef.cpp
        myftp.cpp
        checksn.cpp
        cameraconfig.cpp
        ipcamera.cpp
        mycamera.cpp
        componentpathcorrection.cpp
        logger.cpp
        compspparams.cpp
        compdnnparams.cpp
        compstatmodel.cpp
        classifierpicker.cpp
        barcodecorrection.cpp
        brdinfo.cpp
        ocvhelper.cpp
		regextester.cpp
        widget.h
        compdef_dnn.h
        filedeleter.h
        iocontrol.h
        mscene.h
        cornergrabber.h
        compdefdialog.h
        boarddef.h
        boardcomptype.h
        boardcomp.h
        boardconfig.h
        boardcomptypeconfig.h
        boardcompaltdialog.h
        detectedcomponent.h
        testinterface.h
        testdebugwidget.h
        boardselect.h
        compdefrect.h
        compdefimage.h
        compdef.h
        checksn.h
        myftp.h
        cameraconfig.h
        ipcamera.h
        mycamera.h
        componentpathcorrection.h
        logger.h
        compspparams.h
        compdnnparams.h
        compstatmodel.h
        brdinfo.h
        ocvhelper.h
		regextester.h
        widget.ui
        compdefdialog.ui
        boardconfig.ui
        boardcomptypeconfig.ui
        boardcompaltdialog.ui
        filedeleter.ui
        testinterface.ui
        testdebugwidget.ui
        boardselect.ui
        cameraconfig.ui
        componentpathcorrection.ui
        classifierpicker.ui
        barcodecorrection.ui
        brdinfo.ui
		regextester.ui
        icons.qrc
        ${OCVCAM_SOURCES}
        ${BARCODEREAD_SOURCES}
        ${RASPICAM_SOURCES}
        ${TS_FILES}
        ${QM_FILES}
        ${CMAKE_CURRENT_BINARY_DIR}/tr.qrc
        )
endif()

#Windows application icon
#if (WIN32)
#  set(WINDOWS_RES_FILE ${CMAKE_CURRENT_BINARY_DIR}/resources.obj)
#  if (MSVC)
#    add_custom_command(OUTPUT ${WINDOWS_RES_FILE}
#      COMMAND rc.exe /fo ${WINDOWS_RES_FILE} resources.rc
#      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/cmake/win
#    )
#  else(MSVC)
#    add_custom_command(OUTPUT ${WINDOWS_RES_FILE}
#      COMMAND windres.exe resources.rc ${WINDOWS_RES_FILE}
#      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/cmake/win
#    )
#  endif(MSVC)
#endif(WIN32)

#FIND_PACKAGE ( Threads REQUIRED )
#find_package(PNG)
if(EXISTS ${dlib_directory})
    target_link_libraries(examinator PRIVATE Qt5::Widgets  Qt5::Sql Qt5::Concurrent Qt5::SerialPort Qt5::Charts Qt5::Network Qt5::Multimedia Qt5::PrintSupport dlib::dlib ${OpenCV_LIBS} )

    if(UNIX)
        #link libraries only if they are found
        if(EXISTS ${PC_ZBAR_LIBDIR})
            target_link_libraries(examinator PRIVATE ${PC_ZBAR_LIBRARIES} )
        endif()

        if(EXISTS ${PC_DMTX_LIBDIR})
            target_link_libraries(examinator PRIVATE ${PC_DMTX_LIBRARIES})
        endif()

        if(EXISTS ${PC_ZXING_LIBDIR})
            target_link_libraries(examinator PRIVATE ${PC_ZXING_LIBRARIES})
        endif()
    endif(UNIX)

    if(WIN32)
        target_link_libraries(examinator PRIVATE psapi)
        #check if exist, if not, don`t throw error
        if(EXISTS ${ZBAR_LIBDIR})
            target_link_libraries(examinator PRIVATE libzbar)
        endif()

        if(EXISTS ${DMTX_LIBDIR})
            target_link_libraries(examinator PRIVATE dmtx)
        endif()

        if(EXISTS ${ZXING_LIBDIR})
            target_link_libraries(examinator PRIVATE ZXing)
        endif()
    endif(WIN32)
endif()
