/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "filedeleter.h"
#include "ui_filedeleter.h"
#include <QFileDialog>
#include <QDebug>
#include <QSqlQuery>
#include <QMessageBox>

FileDeleter::FileDeleter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FileDeleter)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
}

FileDeleter::~FileDeleter()
{
    delete ui;
}

void FileDeleter::setPathConverter(QString combinedString)
{
    pathConverterTo.clear();
    pathConverterFrom.clear();
    QStringList converters=combinedString.split(";");
    foreach(QString conv, converters){
        QStringList lst=conv.split("=");
        if(lst.count()!=2){
            qDebug()<<"invalid conversion encountered"<<conv;
            continue;
        }
        addPathConverter(lst[0],lst[1]);
    }
}

QString FileDeleter::convertPath(QString input)
{
    QString tmp;
    tmp=input.replace('\\','/');//use the '/' directory separator everywhere
    for(int i=0; i<pathConverterFrom.count(); i++){
        if(tmp.startsWith(pathConverterFrom[i])){
            return tmp.replace(0,pathConverterFrom[i].length(),pathConverterTo[i]);
        }
    }
    return tmp;
}

void FileDeleter::on_dirSelButton_clicked()
{
    QString path=QFileDialog::getExistingDirectory(this, tr("director de analizat"), ui->dirEdit->text());
    if(path.isEmpty()){
        qDebug()<<"cancelled";
       return;
    }
    dir.setFilter(QDir::Files);
    path=convertPath(path);
    dir.setPath(path);
    ui->dirEdit->setText(path);
    updateFilesList();
}

void FileDeleter::addPathConverter(const QString from, const QString to)
{
    QString from2=from;
    from2=from2.replace('\\','/');//use the '/' directory separator everywhere
    QString to2=to;
    to2=to2.replace('\\','/');//use the '/' directory separator everywhere
    int idx=pathConverterFrom.indexOf(from2);
    if(idx>=0){
        if(pathConverterTo[idx].compare(to2)==0){
            //duplicate, just ignore
            return;
        } else {
            qDebug()<<"ignoring attempt to overwrite path converter with different value"<<from2<<to2;
            return;
        }
    }
    pathConverterFrom.append(from2);
    pathConverterTo.append(to2);
}

void FileDeleter::updateStats()
{
    unsigned selected = ui->filesList->selectedItems().count();
    unsigned files=ui->filesList->count();
    ui->statsLbl->setText(QString::number(selected)+"/"+QString::number(files));
}

void FileDeleter::updateFilesList()
{
    QString path=dir.absolutePath();
    qDebug()<<"first dir.absolutePath()"<<path;
    ui->filesList->clear();
    QSet<QString> usedImages;
    QSqlQuery query;
    QString sql="SELECT imagepath FROM compdefimages UNION SELECT refimagepath FROM boards";
    if(query.exec(sql)){
        while(query.next()){
            QString imagePath=convertPath(query.value(0).toString());
            if(!imagePath.startsWith(path,Qt::CaseInsensitive)){
                //ignore
                continue;
            } else {
                QStringList lst=imagePath.split("/", QString::SkipEmptyParts, Qt::CaseInsensitive);
                QString fname=lst.last();
                usedImages.insert(fname);
                qDebug()<< "in folder" << imagePath << fname;
            }
        }
    }
    qDebug()<<"dir.absolutePath()"<<dir.absolutePath();
    for(unsigned i=0; i<dir.count(); i++){
        qDebug()<<"dir[i]"<<dir[i];
        QString fname=dir[i];
        QListWidgetItem *item=new QListWidgetItem(fname);
        if(usedImages.contains(fname)){
            //used
            item->setBackground(Qt::red);
            item->setData(Qt::UserRole,0);
        }else{
            //not used
            item->setBackground(Qt::green);
            item->setData(Qt::UserRole,1);
        }
        ui->filesList->addItem(item);
    }
    updateStats();

}

void FileDeleter::updateClassifierList()
{
    classifierBoards.clear();
    ui->classifierList->clear();
    QSqlQuery query;
    QString sql="SELECT compdefs.id as id, compdefs.name as name, boards.name as bname FROM compdefs, boards where compdefs.id=boards.refclassifier UNION "
            "SELECT compdefs.id as id, compdefs.name as name, bname FROM compdefs left join "
            "(SELECT classifier, boards.name as bname from boardcompclassifiers, boardcomptypes, boards WHERE "
            "boardcompclassifiers.comptype=boardcomptypes.id and boardcomptypes.board=boards.id) as tbl "
            "on compdefs.id=tbl.classifier ORDER BY compdefs.name, compdefs.id, bname DESC";
    if(query.exec(sql)){
        while(query.next()){
            auto id=query.value("id").toLongLong();
            auto name=query.value("name").toString();
            auto bname=query.value("bname").toString();
            qDebug()<<name<<id<<bname;
            if(!classifierBoards.contains(id)){
                QListWidgetItem *item=new QListWidgetItem(name);
                if(!bname.isEmpty()){
                    //used
                    item->setBackground(Qt::red);
                    //used classifiers have negative id
                    item->setData(Qt::UserRole,-id);
                }else{
                    //not used
                    item->setBackground(Qt::green);
                    item->setData(Qt::UserRole,id);
                }
                ui->classifierList->addItem(item);
            }
            if(!bname.isEmpty()){
                classifierBoards.insertMulti(id, bname);
            }
        }
    }

}

void FileDeleter::on_filesList_itemSelectionChanged()
{
    updateStats();
}

void FileDeleter::on_deleteBtn_clicked()
{
    auto items=ui->filesList->selectedItems();
    for(int i=0; i<items.count(); i++){
        QString fname=items[i]->text();
        qDebug()<<fname<<dir.exists(fname);
        bool fileNotUsed=items[i]->data(Qt::UserRole).toBool();
        if(!fileNotUsed)
        {
            auto btn=QMessageBox::question(this, tr("imagine folosită"), tr("Imaginea ") + fname + tr(" e folosită. Sigur doriți ștergerea imaginii?"));
            if(btn==QMessageBox::No)
                continue;
        }
        if(dir.exists(fname)){
            bool result=true;
            result=dir.remove(fname);
            qDebug()<<"remove"<<result;
        } else
            qDebug()<<"can't delete inexistent file"<<fname;
    }
    dir.setPath(dir.absolutePath());
    updateFilesList();
}

void FileDeleter::on_tabWidget_currentChanged(int index)
{
    if(index!=1)
        return;
    updateClassifierList();
}

void FileDeleter::on_classifierList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    ui->boardList->clear();
    if(current==nullptr){
        qDebug()<<"nullptr";
        return;
    }
    long long id=current->data(Qt::UserRole).toLongLong();
    qDebug()<<"on_classifierList_currentItemChanged"<<id;
    if(classifierBoards.contains(std::abs(id))){
        auto lst=classifierBoards.values(std::abs(id));
        foreach (auto element, lst) {
            qDebug()<<element;
            ui->boardList->addItem(element);
        }
    }
}

void FileDeleter::on_classifierList_itemDoubleClicked(QListWidgetItem *item)
{
    if(item==nullptr)
        return;
    long long id=item->data(Qt::UserRole).toLongLong();
    id=std::abs(id);
    emit requestClassifier(id);
    this->close();

}
