#ifndef RASPICVCAM_H
#define RASPICVCAM_H

#include <QObject>
#include <QImage>

#define USERASPIYUV
#ifdef USERASPIYUV
#include <QProcess>
#else
#include <raspicam/raspicam_still_cv.h>
#endif

class RaspiCvCam : public QObject
{
    Q_OBJECT
public:
    explicit RaspiCvCam(QObject *parent = nullptr);
    QList<QSize> getResolutions();
    void setResolution(const QSize &value);
    QSize getResolution(void);
    bool setEnabled(bool enabled);
    bool isOpened(void);

    int getNImages() const;
    void setNImages(int value);

    int getNIgnore() const;
    void setNIgnore(int value);


private:
#ifdef USERASPIYUV
    QProcess cap;
    bool isOpen(void);
#else
    raspicam::RaspiCam_Still_Cv cap;
#endif
    QSize resolution=QSize(4056,3040);
    ///number of images to average in order to improve the image quality
    int nImages=1;
    ///number of images to ignore in order tu flush the buffers
    int nIgnore=1;
    bool reopen();

signals:
    void imageCaptured(QImage image);
    void captureError(QString errorMessage);

public slots:
    void capture();
};

#endif // RASPICVCAM_H
