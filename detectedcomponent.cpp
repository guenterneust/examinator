/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "detectedcomponent.h"
#include "boardcomptype.h"

DetectedComponent::DetectedComponent()
{
    rectangleType=CompDefRect::FOUNDCOMP;
}

DetectedComponent::DetectedComponent(const BoardComp &c, const cv::Mat &transform,
                                     const QMap<long long, QPointF> &masterOffsets, const int orientationDiff,
                                     const int compType)
{
    //calculate scale factor for rectangle by measuring the length of two orthogonal
    //unit segments at the center of the object to be detected
    std::vector<cv::Point2f>center;
    cv::Point2f pt(c.xCenter(),c.yCenter());
    center.push_back(pt);
    center.push_back(cv::Point2f(pt.x+1,pt.y));
    center.push_back(cv::Point2f(pt.x,pt.y+1));
    cv::perspectiveTransform(center,center,transform);
    double scale=(norm(center[0]-center[1])+norm(center[0]-center[2]))/2;
    qDebug()<<"findcomp"<<c.name<<c.x<<c.y<<c.w<<c.h<<c.orientation<<
              c.xCenter()<<c.yCenter()<<"->"<<center[0].x<<center[0].y<<scale;
    //qDebug()<<center[0].x<<center[0].y<<center[1].x<<center[1].y<<norm(center[0]-center[1])<<norm(center[0]-center[2]);
    double width, height;
    width=c.w*scale;
    height=c.h*scale;
    orientation=c.orientation;
    addOrientationDifference(orientationDiff);
    //qDebug()<<"orientation"<<c.orientation<<comp.orientation<<orientationDiff;
    int rot90=orientationDiff%2;
    if((c.orientation==CompDefRect::E)||
            (c.orientation==CompDefRect::W)||
            (c.orientation==CompDefRect::EW)){
        rot90=(rot90+1)%2;
    }
    if(rot90){
        x=center[0].x-height/2;
        y=center[0].y-width/2;
    } else {
        x=center[0].x-width/2;
        y=center[0].y-height/2;
    }
    if(BoardCompType::SUBCOMPONENT == compType){
        auto parent=c.parentComponent;
        if(masterOffsets.contains(parent)){
            qDebug()<<"applying parent offset"<<masterOffsets[parent].x()<<masterOffsets[parent].y();
            x += masterOffsets[parent].x();
            y += masterOffsets[parent].y();
        } else {
            qDebug()<<"parent offset not found"<<parent<<masterOffsets;
        }
    }
    w=width;
    h=height;
}
