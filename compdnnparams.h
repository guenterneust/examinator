#ifndef COMPDNNPARAMS_H
#define COMPDNNPARAMS_H
//dnn training parameters
#include <dlib/image_processing.h>


class CompDNNParams
{
public:
    CompDNNParams();
public:
    double minLearningRate=0.00001;
    double maxLearningRate=0.05;
    unsigned batchSize1=4;
    unsigned bsSamples1=5000;
    unsigned batchSize2=8;
    unsigned bsSamples2=50000;
    unsigned batchSize3=20;
    unsigned chipWidth=200;
    unsigned chipHeight=200;
    bool disturbColors=true;
    double maxRotation=5;
    int stepsWihoutProgress=3000;
    double lrShrinkFactor=0.3;
    unsigned trainingMode=1;///< 0 - from scratch (random initialization); 1 - relearn (initialize with previously learned weights); 2 - initialize with weights from another net; 3 - continue from last time
    unsigned long long transferClassifier=0;
    unsigned networkType=1;///<maybe more than one network archiecture will be available
    bool enabled=false;
    double bias=0;
    bool bbRegression=false;
    int nFilter=40;//default number of filters

    friend void serialize (const CompDNNParams& item, std::ostream& out);

    friend void deserialize (CompDNNParams& item, std::istream& in);
};

inline void serialize(const CompDNNParams &item, std::ostream &out)
{
    int version = 1;
    dlib::serialize(version, out);
    dlib::serialize(item.minLearningRate, out);
    dlib::serialize(item.maxLearningRate, out);
    dlib::serialize(item.batchSize1, out);
    dlib::serialize(item.bsSamples1, out);
    dlib::serialize(item.batchSize2, out);
    dlib::serialize(item.bsSamples2, out);
    dlib::serialize(item.batchSize3, out);
    dlib::serialize(item.chipWidth, out);
    dlib::serialize(item.chipHeight, out);
    dlib::serialize(item.disturbColors, out);
    dlib::serialize(item.maxRotation, out);
    dlib::serialize(item.stepsWihoutProgress, out);
    dlib::serialize(item.lrShrinkFactor, out);
    dlib::serialize(item.trainingMode, out);
    dlib::serialize(item.transferClassifier, out);
    dlib::serialize(item.networkType, out);
    dlib::serialize(item.enabled, out);
    dlib::serialize(item.bias, out);
    dlib::serialize(item.bbRegression, out);
    dlib::serialize(item.nFilter, out);
}

inline void deserialize (CompDNNParams& item, std::istream& in)
{
    int version = 0;
    dlib::deserialize(version, in);
    if (version != 1)
        throw dlib::serialization_error("Unexpected version found while deserializing CompDNNParams.");
    dlib::deserialize(item.minLearningRate, in);
    dlib::deserialize(item.maxLearningRate, in);
    dlib::deserialize(item.batchSize1, in);
    dlib::deserialize(item.bsSamples1, in);
    dlib::deserialize(item.batchSize2, in);
    dlib::deserialize(item.bsSamples2, in);
    dlib::deserialize(item.batchSize3, in);
    dlib::deserialize(item.chipWidth, in);
    dlib::deserialize(item.chipHeight, in);
    dlib::deserialize(item.disturbColors, in);
    dlib::deserialize(item.maxRotation, in);
    dlib::deserialize(item.stepsWihoutProgress, in);
    dlib::deserialize(item.lrShrinkFactor, in);
    dlib::deserialize(item.trainingMode, in);
    dlib::deserialize(item.transferClassifier, in);
    dlib::deserialize(item.networkType, in);
    dlib::deserialize(item.enabled, in);
    dlib::deserialize(item.bias, in);
    dlib::deserialize(item.bbRegression, in);
    dlib::deserialize(item.nFilter, in);
    if(item.nFilter<16)
        item.nFilter=40;
}


#endif // COMPDNNPARAMS_H
