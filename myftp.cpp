/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "myftp.h"
#include <QNetworkReply>
#include <QApplication>
#include <QTimer>
#include <assert.h>
#include <QStandardPaths>
#include "logger.h"


#define SLEEPTIME 20000

/** prepare the ftp client, without doing any upload
  */
MyFtp::MyFtp(QObject * _parent, QString inifile, bool useWritableSettins) : QObject(_parent){
    qDebug()<<"initializing myFtp"<<_parent;
    parent=_parent;
    if(inifile==""){
        settings=new QSettings(QSettings::IniFormat,QSettings::SystemScope,qApp->organizationName(),qApp->applicationName());
        qDebug()<<settings->fileName()<<"writable"<<settings->isWritable()<<settings->status()<<"exists"<<QFile::exists(settings->fileName());
        if(!settings->isWritable()){
            qDebug()<<"settings not writable";
            QString f=settings->fileName();
            if((!QFile::exists(f)) || useWritableSettins){
                delete settings;
                settings=new QSettings(QSettings::IniFormat,QSettings::UserScope,qApp->organizationName(),qApp->applicationName());
            }//if file exists, assume that it contains the desired settings even if it's not writable
        }
    }else
        settings=new QSettings(inifile, QSettings::IniFormat);
    createConfigItem("global/ftpServer","hugo",settings);
    createConfigItem("global/ftpPath",".",settings);
    createConfigItem("global/ftpUser","qdmftp",settings);
    createConfigItem("global/ftpPassword","nopassword",settings);
    createConfigItem("testPlaceType","AOI",settings);
    createConfigItem("testPlaceNumber","3599999",settings);
    createConfigItem("testCode","G17",settings);
    createConfigItem("testMethod","P",settings);
    createConfigItem("localPath",QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/qdm/",settings);
    createConfigItem("localStoragePath",
                     QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/qdm/localstorage/",settings);
    createConfigItem("showResultLabel",0,settings);
    createConfigItem("backupLevel",2, settings);
    createConfigItem("backupFile","backupFile.txt", settings);
    createConfigItem("backupPath",QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/qdm/backup/", settings);
    createConfigItem("backupMode","W", settings);
    ftpServer=settings->value("global/ftpServer","hugo").toString();
    ftpPath=settings->value("global/ftpPath",".").toString();
    ftpUser=settings->value("global/ftpUser","qdmftp").toString();
    ftpPassword=settings->value("global/ftpPassword","nopassword").toString();
    //connect(this,SIGNAL(commandFinished(int,bool)),this,SLOT(cmdFinishedSlot(int, bool)));
    //connect(this,SIGNAL(stateChanged(int)),this,SLOT(stateChangedSlot(int)));

    //qDebug()<<settings->fileName();
    leikaFiles=QDir(settings->value("localPath").toString());
    leikaFiles.mkpath(settings->value("localPath").toString());
    leikaFiles.mkpath(settings->value("localStoragePath").toString());
    leikaFiles.setFilter(QDir::Files);
    leikaFiles.setNameFilters(QStringList("*."+settings->value("testPlaceType").toString()+
                                          settings->value("testPlaceNumber").toString()));
    qDebug()<<"count"<<leikaFiles.count()<<leikaFiles.nameFilters()<<leikaFiles.filter()<<leikaFiles.exists();
    startId=0;
    //connectToHost ( parent->ftpServer);
    if(ftpServer.isEmpty()){
        //don't initiate ftp if no server is provided
        return;
    }
    url=QUrl("ftp://"+ftpServer);
    //qDebug()<<url.toString()<<url.isValid();
    watchdog.start(3*SLEEPTIME);
    uploadTimer.start(SLEEPTIME);
    connect(&uploadTimer,SIGNAL(timeout()),this, SLOT(ftpUpload()));
    connect(&watchdog, SIGNAL(timeout()), this, SLOT(watchdogSlot()));
}


MyFtp::~MyFtp(){
    //delete leikaFiles;
    delete settings;
}

/*!
Here the actual work is done.\n
Actions:
- cleanup after old transfer
- initiate new transfer
- wait some time and restart upload when no file is left
 */
void MyFtp::cmdFinishedSlot(){
    //qDebug() << "id" << id << err << leikaFiles->count();
    //emit sendToLog("Signal 'cmdFinishedSlot' id="+QString::number(id),3,1);
    watchdog.start();//reset watchdog
    uploadTimer.start();
    int err;
    //qDebug()<<"cmdFinished"<<startId<<uploadTimer.remainingTime();
    if(file.isOpen())
        file.close();
    if((startId>0)&&(reply!=nullptr)){
        //not the first file - cleanup old file before processing next one
        qDebug()<<"file processed"<<reply->isFinished()<<reply->isOpen()<<"networkaccessible"<<reply->manager()->networkAccessible();
        if((err=reply->error())!=QNetworkReply::NoError){
            // leave the file untouched for the next iteration
            emit sendToLog("error " + QString::number(err) + " while uploading "+(leikaFiles)[startId-1],IMPORTANT);
            //qDebug()<<"error " + QString::number(err) + " while uploading "+(leikaFiles)[startId-1];
            if(reply->manager()->networkAccessible()==QNetworkAccessManager::NotAccessible){
                /// @todo add delay, network is not accessible at the moment
                reply->deleteLater();
                reply=nullptr;
                //qDebug()<<"reply=nullptr";
                uploadTimer.start();
                watchdog.start();
                busy=false;
                return;
            }
        } else {
            if(!reply->isOpen()){
                emit sendToLog("reply not open for "+file.fileName(),INFO);
            }
            emit uploadFinished( file.fileName() );
            cleanUpFile(file);
            emit sendToLog("upload succeeded for "+(leikaFiles)[startId-1],IMPORTANT);
        }
        reply->deleteLater();
        reply=nullptr;
        //qDebug()<<"reply=null";
    }
    //check if files available to be sent
    while(leikaFiles.count()>startId){
        url.setPath("/"+ftpPath+"/"+(leikaFiles)[startId]);
        url.setUserName(ftpUser);
        url.setPassword(ftpPassword);
        url.setPort(21);
        qDebug() << "sending" << (leikaFiles)[startId]<<"to"<<url.toString();
        emit sendToLog("Sending using FTP: " + (leikaFiles)[startId],INFO);

        file.setFileName(settings->value("localPath").toString()+
                         (leikaFiles)[startId]);
        if(file.size()<=0){
            emit sendToLog("found empty file: "+file.fileName(),CRITICAL);
        }
        if(file.open(QIODevice::ReadOnly)){
            reply = nam.put(QNetworkRequest(url), &file);
            startId++;
            //fara QueuedConnection se apela intai finished, apoi eroarea, si reply=0 nu ajungea la slotul de eroare!
            connect(reply, SIGNAL(finished()), SLOT(cmdFinishedSlot()),Qt::QueuedConnection);
            connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), SLOT(errorSlot()),Qt::QueuedConnection);
            //qDebug()<<"new reply connected";
            return;
        } else {
            //can't read file - skip
            emit sendToLog("can't open file "+file.fileName(),IMPORTANT);
            startId++;
            //busy=false;
            //QTimer::singleShot(SLEEPTIME, this, SLOT(ftpUpload()));
        }
    }
    // all files sent. Wait some time and try again.
    reply=nullptr;
    busy=false;
    //QTimer::singleShot(SLEEPTIME, this, SLOT(ftpUpload()));
}


/** initiate leika protocol upload from localPathLeika to the ftp server
  If successfull, clean up the leika protocol files.
  Restart the timer for the next upload
*/
void MyFtp::ftpUpload(void){
    if(ftpServer.isEmpty()){
        return;
    }
    //qDebug()<< "upload with ftp"<<uploadTimer.remainingTime()<<watchdog.remainingTime();
    //assert(parent);
    emit sendToLog("ftpUpload",VERBOSE);

    if(busy){
        qDebug()<<"busy";
        return;
    }
    busy=true;
    QString path=settings->value("localPath","/qdm").toString();
    leikaFiles.setPath(path);
    if(!leikaFiles.exists()){
        qDebug()<<"myftp: path does not exist"<<path;
    }
    //qDebug()<<"count"<<leikaFiles.path()<<leikaFiles.count()<<leikaFiles.nameFilters()<<leikaFiles.filter()<<leikaFiles.exists();
    startId=0;
    if(leikaFiles.count()>0){
        qDebug()<<"uploading"<<leikaFiles.count();
        cmdFinishedSlot();
    }
    else{
        //qDebug()<<"singleshot";
        busy=false;
        watchdog.start();//reset watchdog
        uploadTimer.start();
        //QTimer::singleShot(SLEEPTIME, this, SLOT(ftpUpload()));
    }
}

void MyFtp::watchdogSlot()
{
    //qDebug()<<"watchdog timeout";
    if(reply){
        if(reply->isRunning())
            reply->abort();
        reply->deleteLater();
    }
    reply=nullptr;
    emit sendToLog("Watchdog activated for FTP",INFO);
    busy=false;
    ftpUpload();
}

void MyFtp::errorSlot()
{
    if(reply!=nullptr){
        qDebug()<<"errorSlot";qDebug()<<reply;
        QString msg=reply->errorString();
        long err=reply->error();
        if(reply->isRunning())
            reply->abort();
        reply->deleteLater();
        emit sendToLog("Ftp error: "+msg + "("+ QString::number(err) +")", IMPORTANT);
    } else {
        emit sendToLog("error without active network reply", CRITICAL);
    }
    reply=nullptr;
    //qDebug()<<"reply=0";
    busy=false;
    //ftpUpload();
    uploadTimer.start();
}

void MyFtp::configItem(QString item, QString str)
{
    if(!settings)
        return;
    if(!settings->isWritable())
        return;
    QString value=settings->value(item).toString();
    if(value!=str)
        settings->setValue(item,str);
}

void MyFtp::createConfigItem(QString item, QString str, QSettings * mySettings)
{
    if(!mySettings)
        return;
    if(!mySettings->isWritable())
        return;
    if(mySettings->contains(item))
        return;
    settings->setValue(item,str);
}

void MyFtp::createConfigItem(QString item, int val, QSettings *mySettings)
{
    if(!mySettings)
        return;
    if(!mySettings->isWritable())
        return;
    if(mySettings->contains(item))
        return;
    settings->setValue(item,val);
}

void MyFtp::configItem(QString item, int val)
{
    if(!settings)
        return;
    if(!settings->isWritable())
        return;
    int value=settings->value(item).toInt();
    if(value!=val)
        settings->setValue(item,val);
}

void MyFtp::defaultConfig()
{
    //unused
    configItem("leikaFiles","/qdm/");
    configItem("ftpServer","hugo");
    configItem("ftpPath","./");
    configItem("ftpUser","qdmftp");
    configItem("ftpPassword","nopassword");
    configItem("testPlaceType","aoi");
    configItem("testPlaceNumber","999");
    configItem("testCode","G7");
    configItem("backupLevel",2);
    configItem("backupFile","backupFile.txt");
}

void MyFtp::cleanUpFile(QFile &file){
    int backupLevel=settings->value("backupLevel",2).toInt();
    QByteArray backupMode=settings->value("backupMode","").toByteArray();
    QString subDir="";
    QDate date=QDate::currentDate();
    for(int i=0; i<backupMode.length(); i++){
        if(backupMode[i]=='Y'){
            subDir+=date.toString("yyyy")+"/";
        } else if(backupMode[i]=='M'){
            subDir+=date.toString("yyyy.MM")+"/";
        } else if(backupMode[i]=='W'){
            subDir+=date.toString("yyyy")+".KW"+QString("%1").arg(date.weekNumber(),2)+"/";
        } else if(backupMode[i]=='D'){
            subDir+=date.toString("yyyy.MM.dd")+"/";
        }
    }
    QString backupFile=settings->value("backupPath").toString()+subDir+settings->value("backupFile").toString();
    qDebug()<<"backupLevel"<<backupLevel;
    if(backupLevel==0){
        file.remove();
    } else if(backupLevel==1){
        QFile::remove(backupFile);
        if(!file.rename(backupFile)){
            qDebug()<<"unable to rename " + file.fileName() + " to "+ backupFile;
            if(!file.remove()){
                qDebug()<<("unable to remove " + file.fileName());
            }
        }
    } else {
        // backup all files
        QDir dir;
        QFileInfo info(backupFile);
        QString newName=info.absolutePath();
        dir.mkpath(newName);
        newName += "/"+QFileInfo(file).fileName();
        qDebug()<<"new name:"<<newName;
        if(!file.rename(newName)){
            qDebug()<<("unable to rename " + file.fileName() + " to "+ newName);
            QFile::remove(backupFile);
            if(!file.rename(backupFile)){
                qDebug()<<("unable to rename " + file.fileName() + " to "+ backupFile);
                if(!file.remove()){
                    qDebug()<<("unable to remove " + file.fileName());
                }
            }
        }
    }
}

