/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPDEF_DNN_H
#define COMPDEF_DNN_H

#include <dlib/image_processing.h>
#include <dlib/data_io.h>
//#include <dlib/opencv/cv_image.h>
#include <dlib/dnn.h>
#include <variant>

/// A 5x5 conv layer that does 2x downsampling
template <long num_filters, typename SUBNET> using con5d = dlib::con<num_filters,5,5,2,2,SUBNET>;
// A 3x3 conv layer that doesn't do any downsampling
template <long num_filters, typename SUBNET> using con3  = dlib::con<num_filters,3,3,1,1,SUBNET>;

template <
    int N,
    template <typename> class BN,
    int stride,
    typename SUBNET
    >
using block  = BN<dlib::con<N,3,3,1,1,dlib::prelu<BN<dlib::con<N,3,3,stride,stride,SUBNET>>>>>;

template <
    template <int,template<typename>class,int,typename> class block,
    int N,
    template<typename>class BN,
    typename SUBNET
    >
using residual = dlib::add_prev1<block<N,BN,1,dlib::tag1<SUBNET>>>;
template <int N, typename SUBNET> using res       = dlib::prelu<residual<block,N,dlib::bn_con,SUBNET>>;
template <int N, typename SUBNET> using ares       = dlib::prelu<residual<block,N,dlib::affine,SUBNET>>;

// Now we can define the 8x downsampling block in terms of conv5d blocks.  We
// also use relu and batch normalization in the standard way.
//template <typename SUBNET> using downsampler  = relu<bn_con<con5d<32, relu<bn_con<con5d<32, relu<bn_con<con5d<32,SUBNET>>>>>>>>>;
template <int FILTERS, typename SUBNET> using downsampler  = dlib::prelu<dlib::bn_con<con5d<FILTERS, dlib::prelu<dlib::bn_con<
            con5d<32, dlib::prelu<dlib::bn_con<con5d<16,SUBNET>>>>>>>>>;
template <int FILTERS, typename SUBNET> using adownsampler  = dlib::prelu<dlib::affine<con5d<FILTERS, dlib::prelu<dlib::affine<
            con5d<32, dlib::prelu<dlib::affine<con5d<16,SUBNET>>>>>>>>>;

// The rest of the network will be 3x3 conv layers with batch normalization and
// relu.  So we define the 3x3 block we will use here.
//template <typename SUBNET> using rcon3  = dlib::prelu<dlib::bn_con<con3<32,SUBNET>>>;

// Finally, we define the entire network.   The special input_rgb_image_pyramid
// layer causes the network to operate over a spatial pyramid, making the detector
// scale invariant.
//using net_type  = loss_mmod<con<2,7,7,1,1,rcon3<rcon3<rcon3<downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;
//using net_type  = loss_mmod<con<1,3,3,1,1,res<32,res<32,res<32,downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;
template <int FILTERS> using net_typeN_2  =
    dlib::loss_mmod<dlib::con<1,7,7,1,1,res<FILTERS,res<FILTERS,downsampler<FILTERS, dlib::input_rgb_image_pyramid<dlib::pyramid_down<6>>>>>>>;
template <int FILTERS> using anet_typeN_2  =
    dlib::loss_mmod<dlib::con<1,7,7,1,1,ares<FILTERS,ares<FILTERS,adownsampler<FILTERS, dlib::input_rgb_image_pyramid<dlib::pyramid_down<6>>>>>>>;
//using anet_type1b  = dlib::loss_mmod<dlib::con<1,7,7,1,1,ares<40,ares<40,adownsampler<dlib::input_rgb_image>>>>>;

using net_type1 = net_typeN_2<40>;
using anet_type1 = anet_typeN_2<40>;


#endif // COMPDEF_DNN_H
