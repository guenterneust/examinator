/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CAMERACONFIG_H
#define CAMERACONFIG_H

#include <QDialog>
#include <QSettings>
#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include "ipcamera.h"
#include "mycamera.h"
#ifdef OCVCAM
#include "opencvcam.h"
#endif
#ifdef RASPICAM
#include "raspicvcam.h"
#endif

namespace Ui {
class CameraConfig;
}

class CameraConfig : public QDialog
{
    Q_OBJECT

public:
    explicit CameraConfig(QSettings * settings, MyCamera * myCamera, IpCamera * ipCamera,
#ifdef OCVCAM
                          OpenCVCam * ocvCamera,
#endif
#ifdef RASPICAM
                          RaspiCvCam * raspiCVCamera,
#endif
                          QWidget *parent = 0);
    ~CameraConfig();

private slots:

    void on_tryBtn_clicked();
    void showPixmap(QPixmap pixmap);
    void showImage(QImage image);

    void on_qCameraBox_currentIndexChanged(int index);

    void on_qCameraResolutionBox_currentIndexChanged(int index);

    void on_cameraTypeBox_currentIndexChanged(int index);

    void on_qNImagesSpin_valueChanged(int arg1);

    void on_ocvCameraBox_currentIndexChanged(int index);

    void on_ocvCameraResolutionBox_currentIndexChanged(int index);

    void on_ocvNIgnoreSpin_valueChanged(int arg1);

    void on_ocvNImagesSpin_valueChanged(int arg1);

    void activateRaspiCam(void);

    void on_raspiCamResolutionBox_currentIndexChanged(int index);

    void on_ocvCameraAPIBox_currentIndexChanged(int index);

    void on_qNIgnoresSpin_valueChanged(int arg1);

    void on_ocvManualResCB_clicked(bool checked);

    void on_ocvWidthBox_editingFinished();

    void on_ocvHeightBox_editingFinished();

private:
    Ui::CameraConfig *ui;
    QSettings * settings=nullptr;
    //QCamera * camera;
    QList<QString> cameras;
    IpCamera * ipCamera=nullptr;
    MyCamera * myCamera=nullptr;
    bool qCameraInitialized=false;
    bool qCameraInitialize();
#ifdef OCVCAM
    bool ocvCameraInitialized=false;
    bool ocvCameraInitialize();
    OpenCVCam * ocvCamera=nullptr;
#endif
#ifdef RASPICAM
    bool raspiCameraInitialized=false;
    bool raspiCameraInitialize();
    RaspiCvCam * raspiCVCamera=nullptr;
#endif
signals:
    void loadImageRequest(void);
};

#endif // CAMERACONFIG_H
