/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

///@todo add shortcut to autogenerate rotated markings from the selected marking
/// @todo add function to compare result of camera parameters, by calculating average and standard deviation for R, G, B, H, S, V over a region of interest
///@todo add function tu run a classifier or a group of classifiers to search for a cluster of components
#include "testinterface.h"
#include "ui_testinterface.h"
#include "boardselect.h"
#include <QFileDialog>
#include <QDebug>
#include <QDateTime>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMenu>
#include <QMessageBox>
#include "cameraconfig.h"
#include "logger.h"
#include <QtConcurrent>
#include "barcodecorrection.h"
#include "regextester.h"


//libgstreamer-plugins-bad1.0-dev
//libqt5gstreamer-1.0-0
//libqt5gstreamer-dev
//gstreamer1.0-plugins-bad

#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#else
#include <QDesktopServices>
#endif

QCommandLineParser parser;

TestInterface::TestInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestInterface)
{
    QString inspectionProgram=parseArguments();


    ui->setupUi(this);
    /*
    ui->editorBtn->setText("");
    ui->editorBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_FileIcon));
    ui->loadBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_DirIcon));
    ui->saveBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_DriveFDIcon));
    ui-> cameraBtn->setIcon(QIcon::fromTheme("camera-photo",ui->editorBtn->style()->standardIcon(QStyle::SP_ToolBarVerticalExtensionButton)));
    ui->playBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_MediaPlay));
    ui->boardBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_BrowserReload));
    */
    ui->testView->setScene(&testScene);
    ui->boardsButtonWidget->setVisible(false);
    QMenu *menu = new QMenu();
    QAction *testAction = new QAction(tr("configurează camera"), this);
    menu->addAction(testAction);
    ui->cameraBtn->setMenu(menu);
    QAction *continuousAcquireAction = new QAction(tr("achiziție continuă"), this);
    continuousAcquireAction->setObjectName("continuousAcquire");
    continuousAcquireAction->setCheckable(true);
    menu->addAction(continuousAcquireAction);
    QAction *stressTestArduinoAction = new QAction(tr("test arduino"), this);
    stressTestArduinoAction->setObjectName("stressTestArduino");
    stressTestArduinoAction->setCheckable(true);
    menu->addAction(stressTestArduinoAction);
    ui->cameraBtn->setMenu(menu);
    createDatabase();
    dbCheckAndSetAppBuildTime();
    if(parser.isSet("ini")){
        QString path=parser.value("ini");
        writableSettings=new QSettings(path, QSettings::IniFormat);
        if(!writableSettings->isWritable()){
            qDebug()<<"provided settings not writable:"<<writableSettings->fileName();
            delete writableSettings;
            writableSettings=nullptr;
        }
    }
    if(writableSettings==nullptr){
        writableSettings=new QSettings(QSettings::IniFormat,QSettings::SystemScope, qApp->organizationName(),qApp->applicationName());
        if(!writableSettings->isWritable()){
            qDebug()<<"system settings not writable:"<<writableSettings->fileName();
            delete writableSettings;
            writableSettings=new  QSettings(QSettings::IniFormat,QSettings::UserScope, qApp->organizationName(),qApp->applicationName());
            if(!writableSettings->isWritable()){
                qDebug()<<"settings not writable";
            }
        }
    }
    qDebug()<<"settings used:"<<writableSettings->fileName();
    myFtp=new MyFtp(this, writableSettings->fileName());
    snCheck=new QdmSNCheck(writableSettings->fileName());
    myCamera=new MyCamera(writableSettings, this);
    ocvCamera=new OpenCVCam(writableSettings, this);
    connect(myCamera,SIGNAL(imageCaptured(QImage)),this,SLOT(getImage(QImage)),Qt::QueuedConnection);
    connect(myCamera,SIGNAL(captureError(QString)),this,SLOT(captureError(QString)),Qt::QueuedConnection);
    connect(&ipCamera,SIGNAL(imageCaptured(QImage)),this,SLOT(getImage(QImage)),Qt::QueuedConnection);
    connect(&ipCamera,SIGNAL(captureError(QString)),this,SLOT(captureError(QString)),Qt::QueuedConnection);
    connect(this, SIGNAL(imageLoaded(QImage)), this, SLOT(getImage(QImage)));
#ifdef OCVCAM
    connect(ocvCamera,SIGNAL(imageCaptured(QImage)),this,SLOT(getImage(QImage)),Qt::QueuedConnection);
    connect(ocvCamera,SIGNAL(captureError(QString)),this,SLOT(captureError(QString)),Qt::QueuedConnection);
#endif
#ifdef RASPICAM
    connect(&raspiCvCamera,SIGNAL(imageCaptured(QImage)),this,SLOT(getImage(QImage)),Qt::QueuedConnection);
    connect(&raspiCvCamera,SIGNAL(captureError(QString)),this,SLOT(captureError(QString)),Qt::QueuedConnection);
#endif
    ipCamera.setUrl(writableSettings->value("ipwebcam").toString());
    if(writableSettings->value("cameratype").toString()!="qcamera"){
        //disable qCamera if something else is used
        myCamera->setCamera("",false);
    }
    displaySearchFrames=writableSettings->value("displaySearchFrames", false).toBool();
    autoSaveOption=writableSettings->value("autoSaveOption", 1).toInt();
    autoSaveDir=writableSettings->value("autoSaveDir").toString();
    showFiducials=writableSettings->value("showFiducials",false).toBool();
    rotateComponents=writableSettings->value("rotateComponents",true).toBool();
    if(!writableSettings->contains("rotateComponents"))
        writableSettings->setValue("rotateComponents",rotateComponents);
    QString dataLocation;
#if QT_VERSION >= 0x050400
    dataLocation=QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
#elif QT_VERSION >= 0x050000
    dataLocation=QStandardPaths::writableLocation(QStandardPaths::DataLocation);
#else
    dataLocation=QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif
    if(autoSaveDir.isEmpty()){
        autoSaveDir=dataLocation+"/failedImages";
    }
    if(!inspectionProgram.isEmpty()){
        qDebug()<<inspectionProgram;
        selectBoard(inspectionProgram);
    }
    ui->resultLbl->setText("");
    //ui->horizontalSlider->blockSignals(true);
    ui->horizontalSlider->setValue(writableSettings->value("testinterfacescale",100).toInt());
    ui->horizontalSlider->setToolTip(QString::number(ui->horizontalSlider->value())+"%");
    //ui->horizontalSlider->blockSignals(false);
    myFtp->createConfigItem("LOGFILE",dataLocation+"/log/examinator.log",writableSettings);
    myFtp->createConfigItem("LOGVERBOSITY",INFO,writableSettings);
    myFtp->createConfigItem("warnmissinglights", true, writableSettings);
    logger.setLogFile(writableSettings->value("LOGFILE").toString());
    logger.setLogVerbosity(static_cast<LOGVERBOSITY>( writableSettings->value("LOGVERBOSITY",INFO).toInt()));
    connect(this, SIGNAL(logMsg(QString,LOGVERBOSITY)), &logger, SLOT(logMessage(QString,LOGVERBOSITY)));
    connect(myFtp, SIGNAL(sendToLog(QString,LOGVERBOSITY)), &logger, SLOT(logMessage(QString,LOGVERBOSITY)));
    myFtp->createConfigItem("maxdnnarea",4e6, writableSettings);
    CompDef::maxDNNArea=writableSettings->value("maxdnnarea",CompDef::maxDNNArea).toUInt();
    myFtp->createConfigItem("iocontrolport","",writableSettings);
    ioControlPort=writableSettings->value("iocontrolport","").toString();
    ioControl.setPort(ioControlPort);
    connect(&ioControl, SIGNAL(start()), this, SLOT(ioControlRun()), Qt::QueuedConnection);
    connect(this, SIGNAL(statusChanged(int)), &ioControl, SLOT(sendStatus(int)));
    connect(&ioControl, SIGNAL(initiateImageCapture()), this, SLOT(startCapture()));
    myFtp->createConfigItem("isproductionsystem",true,writableSettings);
    isProductionSystem=writableSettings->value("isproductionsystem", true).toBool();
    ioControl.setWarnMissingLights(isProductionSystem);
    myFtp->createConfigItem("pathconverter","//hugo/genrad/boards=G:/boards",writableSettings);
    //rotation of image, in clockwise direction, displayed in by the image graphics scene, in degrees.
    myFtp->createConfigItem("imagerotation",0,writableSettings);
    myFtp->createConfigItem("autocheckforupdates",1,writableSettings);
    myFtp->createConfigItem("clearoldsn",clearOldSN,writableSettings);
    clearOldSN=writableSettings->value("clearoldsn", true).toBool();
    ui->splitter->setSizes(QList<int>({1,0}));
    connect(ui->testDebugWidget, SIGNAL(imageChanged(QString, QList<DetectedComponent> &, bool)), this, SLOT(showImageResult(QString, QList<DetectedComponent> &, bool)));
    connect(ui->testDebugWidget, SIGNAL(loadBoard(QString)), this, SLOT(selectBoard(QString)));
    ui->testDebugWidget->setBoardDefPtr(&boardDef);
    ui->testDebugWidget->setRotateComponents(rotateComponents);
    connect(ui->testDebugWidget, SIGNAL(imageForClassifier(long long, const QString &)), this, SLOT(addToClassifier(long long, const QString &)));
    connect(ui->testDebugWidget, SIGNAL(showBoard(long long)), this, SLOT(showBoard(long long)));
    connect(ui->testDebugWidget, SIGNAL(centerAt(double, double)), this, SLOT(centerAt(double, double)));
    connect(ui->testDebugWidget, SIGNAL(clearRects()), &testScene, SLOT(clearRects()));
    connect(&checkForUpdatesTimer, SIGNAL(timeout()), this, SLOT(checkForUpdates()));
    if(writableSettings->value("autocheckforupdates",1).toBool()){
        checkForUpdatesTimer.start(900000);
    }
    qDebug()<<"constructor done";
    logger.logMessage("Testinterface constructor finished, app version: "+qApp->applicationVersion(), INFO);

}

TestInterface::~TestInterface()
{
    ImagesAndLights lights;
    lights.lightData="000";
    lights.lightsEnabled=true;
    ioControl.setLights(lights);
    QThread::msleep(500);
    if(boardDef!=nullptr)
        delete boardDef;
    delete ui;
    delete writableSettings;
    delete myFtp;
    delete snCheck;
    delete myCamera;
    delete ocvCamera;
}

void TestInterface::on_editorBtn_clicked()
{
    if(editor==nullptr){
        editor=new Widget(nullptr, this);
        if(editor==nullptr){
            qDebug()<<"can't open editor";
            return;
        }
        QString pathConverter=writableSettings->value("pathconverter").toString();
        editor->fileDeleter.setPathConverter(pathConverter);
    }
    if(editor->isVisible())
        editor->close();
    else
        editor->show();
}

void TestInterface::on_loadBtn_clicked()
{
    static QString fileName;
    //qDebug()<<"fileName"<<fileName;
    QString name = QFileDialog::getOpenFileName(this,
                                                tr("Imagine"), fileName, tr("Fișiere Imagine (*.png *.jpg *.jpeg *.bmp);;Toate fișierele (*.*)"));
    if(!name.isEmpty())
        qDebug()<<"load"<<name<<"runTask"<<runTask;
    if(name.isEmpty())
        return;
    fileName=name;
    if((runTask==ANALYZE)||(PROCESSING==runTask)){
        QImage img;
        img.load(fileName);
        emit imageLoaded(img);
    } else {
        if(RunTask::IDLE == runTask)
            imageList.clear();
        showImage(fileName);
    }
}

void TestInterface::on_horizontalSlider_valueChanged(int position)
{
    ui->testView->setTransform(QTransform::fromScale(position/100.0,position/100.0).rotate(writableSettings->value("imagerotation",0).toReal()));
    ui->horizontalSlider->setToolTip(QString::number(position)+"%");
    writableSettings->setValue("testinterfacescale",position);
}

void TestInterface::on_saveBtn_clicked()
{
    qDebug()<<"save"<<testScene.bg.size()<<testScene.bg.isNull();
    if(testScene.bg.isNull())
        return;
    qDebug()<<"getSaveFileName";
    static QString fileName;
    QString filters("Imagini (*.png);;Imagini (*.jpg)");
    static QString defaultFilter("Imagini (*.png)");

    fileName = QFileDialog::getSaveFileName(this, "Salveaza fisierul ca", fileName, filters, &defaultFilter);
    if(fileName.isEmpty())
        return;
    testScene.bg.save(fileName);
}

void TestInterface::on_boardBtn_clicked()
{
    selectBoard();
    if(boardDef){
        qDebug()<<boardDef->testStartType;
        if(boardDef->lights.count()>=1){
            if(boardDef->lights[0].lightsEnabled)
                ioControl.setLights(boardDef->lights[0]);
        }
        QString cameraType=writableSettings->value("cameratype").toString();
        if((cameraType=="ocvcamera") && isProductionSystem){
            //just take a picture, to give the automatic brightness control a chance to adapt
            QTimer::singleShot(250, this, SLOT(on_cameraBtn_clicked()));
        }
    }
}

void TestInterface::createDatabase()
{
    QString path, defaultPath;
    bool defaultDb=true;
#if QT_VERSION >= 0x050400
    defaultPath=QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
#elif QT_VERSION >= 0x050000
    defaultPath=QStandardPaths::writableLocation(QStandardPaths::DataLocation);
#else
    defaultPath=QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif

    if(parser.isSet("db")){
        path=parser.value("db");
        if(QFileInfo::exists(path)){
            defaultDb=false;
        } else {
            QMessageBox::warning(this,
                                 tr("fișier inexistent"),
                                 tr("fișierul ")+path+
                                 tr(" nu există. Se va folosi locația implicită pentru baza de date:")+defaultPath);
        }
    }
    if(defaultDb){
        path=defaultPath;
        QDir dir(path);
        dir.mkpath(path);
        path.append("/aoidata.sqlite");
    }
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(path);
    if(db.open())
        qDebug()<<"database"<<path<<"opened";
    else {
        qDebug()<<"unable to open database"<<path;
    }

    QSqlQuery query;
    bool result=query.exec("CREATE TABLE IF NOT EXISTS compdefs "
                           "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                           "name VARCHAR(30), "
                           "minarea INTEGER, "
                           "aspectratio DOUBLE,"
                           "orientationmode INTEGER,"
                           "upsample INTEGER,"
                           "horizontalflip BOOLEAN NOT NULL CHECK (horizontalflip IN (0,1)),"
                           "verticalflip  BOOLEAN NOT NULL CHECK (verticalflip IN (0,1)),"
                           "c DOUBLE,"
                           "eps DOUBLE,"
                           "numthreads INTEGER,"
                           "trainedfhog BOOLEAN NOT NULL DEFAULT 0,"
                           "oldfhog BOOLEAN NOT NULL DEFAULT 1,"
                           "traineddnn BOOLEAN NOT NULL DEFAULT 0,"
                           "olddnn BOOLEAN NOT NULL DEFAULT 1,"
                           "fhogthreshold DOUBLE DEFAULT 0.0,"
                           "fhognorient INTEGER DEFAULT 1,"
                           "includedataset INTEGER NOT NULL DEFAULT -1,"
                           "savetime INTEGER DEFAULT (cast(strftime('%s','now') as int)),"
                           "fhogclassifier BLOB,"
                           "dnnclassifier BLOB,"
                           "colorvalidator BLOB,"
                           "shapepredictor BLOB,"
                           "spparams BLOB,"
                           "dnnparams BLOB"
                           ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS compdefimages "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "imagepath VARCHAR(256), "
                      "implicitnegative  BOOLEAN NOT NULL CHECK (implicitnegative IN (0,1)),"
                      "compdef INTEGER, "
                      "FOREIGN KEY(compdef) REFERENCES compdefs(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS compdefimages_compdef ON compdefimages ( compdef )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS compdefrects "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "x DOUBLE, "
                      "y DOUBLE, "
                      "w DOUBLE, "
                      "h DOUBLE, "
                      "orientation INTEGER, "
                      "rectangletype INTEGER, "
                      "compdefimage INTEGER, "
                      "angle FLOAT DEFAULT 0, "
                      "FOREIGN KEY(compdefimage) REFERENCES compdefimages(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS compdefrects_compdefimage ON compdefrects ( compdefimage )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boards "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "refimagepath VARCHAR(256), "
                      "name VARCHAR(128), "
                      // reference object image location
                      "x DOUBLE, "
                      "y DOUBLE, "
                      "w DOUBLE, "
                      "h DOUBLE, "
                      "angle FLOAT DEFAULT 0, "
                      "orientation INTEGER, "
                      "minarea INTEGER, "
                      "maxarea INTEGER, "
                      "alignmethod INTEGER, "
                      "refclassifier INTEGER, "
                      "numberofboards INTEGER NOT NULL DEFAULT 1, "
                      "snvalidator VARCHAR(32),"
                      "testcodeoverride VARCHAR(10),"
                      "savetime INTEGER DEFAULT (cast(strftime('%s','now') as int))"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boardcomptypes "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "name VARCHAR(64), "
                      "compType INTEGER, "
                      "board INTEGER, "
                      "testAbsence BOOLEAN NOT NULL DEFAULT 0, "
                      "FOREIGN KEY(board) REFERENCES boards(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS boardcomptypes_board ON boardcomptypes ( board  )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boardcompclassifiers "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "classifier INTEGER, "
                      "comptype INTEGER, "
                      "name VARCHAR(64), "
                      "FOREIGN KEY(classifier) REFERENCES compdefs(id), "
                      "FOREIGN KEY(comptype) REFERENCES boardcomptypes(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS boardcompclassifiers_comptype ON boardcompclassifiers ( comptype  )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }

    result=query.exec("CREATE TABLE IF NOT EXISTS boardcomponents "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "name VARCHAR(64), "
                      "x DOUBLE, "
                      "y DOUBLE, "
                      "w DOUBLE, "
                      "h DOUBLE, "
                      "orientation INTEGER, "
                      "xtol DOUBLE, "
                      "ytol DOUBLE, "
                      "sizetol DOUBLE, "
                      "rectangletype INTEGER, "
                      "comptype INTEGER, "
                      "symbology INTEGER DEFAULT 0, "
                      "parentcomponent INTEGER DEFAULT -1, "
                      "images INTEGER DEFAULT 1, "
                      "FOREIGN KEY(comptype) REFERENCES boardcomptypes(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS boardcomponents_comptype ON boardcompclassifiers ( comptype )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boarddetails "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "name VARCHAR(64) DEFAULT '', "
                      "type INTEGER DEFAULT -1, "
                      "status INTEGER DEFAULT 0, "
                      "data BLOB,"
                      "board INTEGER, "
                      "FOREIGN KEY(board) REFERENCES boards(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }

    result=query.exec("CREATE TABLE IF NOT EXISTS generalinfo "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "name VARCHAR(64) UNIQUE DEFAULT '', "
                      "value INTEGER DEFAULT -1"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }

    result=query.exec("PRAGMA foreign_keys = ON");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    if(!sqliteTableHasField("compdefs","fhogthreshold")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN fhogthreshold double default -0.1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boardcomponents","symbology")){
        result=query.exec("ALTER TABLE boardcomponents ADD COLUMN symbology INTEGER DEFAULT 0");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boardcomponents","parentcomponent")){
        result=query.exec("ALTER TABLE boardcomponents ADD COLUMN parentcomponent INTEGER DEFAULT -1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","spparams")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN spparams BLOB");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","shapepredictor")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN shapepredictor BLOB");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","dnnparams")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN dnnparams BLOB");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","fhognorient")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN fhognorient INT");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","trainingiouthresh")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN trainingiouthresh INTEGER default 50");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefrects","angle")){
        result=query.exec("ALTER TABLE compdefrects ADD COLUMN angle FLOAT default 0");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boards","testcodeoverride")){
        result=query.exec("ALTER TABLE boards ADD COLUMN testcodeoverride VARCHAR(10) default ''");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boards","angle")){
        result=query.exec("ALTER TABLE boards ADD COLUMN angle FLOAT default 0");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boardcomponents","images")){
        result=query.exec("ALTER TABLE boardcomponents ADD COLUMN images INTEGER DEFAULT 1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boardcompclassifiers","name")){
        result=query.exec("ALTER TABLE boardcompclassifiers ADD COLUMN name VARCHAR(64) DEFAULT ''");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boardcomptypes","testAbsence")){
        result=query.exec("ALTER TABLE boardcomptypes ADD COLUMN testAbsence BOOLEAN NOT NULL DEFAULT 0");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","includedataset")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN includedataset INTEGER NOT NULL DEFAULT -1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }

    if(!sqliteTableHasField("boards","numberofboards")){
        result=query.exec("ALTER TABLE boards ADD COLUMN numberofboards INTEGER NOT NULL DEFAULT 1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boards","savetime")) {
        //default value is not important, it will be used as unix timestamp, but any value from the past is OK
        result=query.exec("ALTER TABLE boards ADD COLUMN savetime INTEGER DEFAULT 1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","savetime")) {
        //default value is not important, it will be used as unix timestamp, but any value from the past is OK
        result=query.exec("ALTER TABLE compdefs ADD COLUMN savetime INTEGER DEFAULT 1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
}


QString TestInterface::parseArguments()
{
    const QStringList args = parser.positionalArguments();
    const QString inspectionProgramm = args.isEmpty() ? QString() : args.first();

    return inspectionProgramm;
}

void TestInterface::selectBoard(QString boardName, bool reload)
{
    long long id=-1;
    if(boardDef!=nullptr)
        id=boardDef->id;
    BoardSelect dlg(displaySearchFrames, showFiducials, autoSaveOption, autoSaveDir, writableSettings);
    if(!reload) {
        //dlg.setDisplaySearchFrames(displaySearchFrames);
        Widget::loadBoardList(dlg.getBoardCombo(),id);
        if(boardName.isEmpty()){
            int result=dlg.exec();
            if(result!=QDialog::Accepted)
                return;
            id=dlg.getSelectedBoard();
        } else {
            id=dlg.getId(boardName);
            if(id<0)
                return;
        }
    }
    if(id<0)
        return;
    if(boardDef!=nullptr)
        delete boardDef;
    boardDef = new BoardDef(this);
    boardDef->setImageAcquisitionDone(&imageAquisitionDone);
    boardDef->loadFromDB(id, true);
    if(!boardDef->isValid()){
        delete boardDef;
        boardDef=nullptr;
        return;
    }
    connect(boardDef, SIGNAL(logMsg(QString,LOGVERBOSITY)), &logger, SLOT(logMessage(QString,LOGVERBOSITY)));
    outdated=false;
    barCodeMode=boardDef->hasBarcodeSN();
    emit logMsg( "board "+ boardDef->name + " loaded, having serial number validator set to '" +
                 boardDef->snValidator+"', barcodeMode "+QString::number(barCodeMode), IMPORTANT);
    this->setWindowTitle(boardDef->name);
    displaySearchFrames=dlg.getDisplaySearchFrames();
    writableSettings->setValue("displaySearchFrames",displaySearchFrames);
    showFiducials=dlg.getShowFiducials();
    writableSettings->setValue("showFiducials",showFiducials);
    autoSaveOption=dlg.getSaveOption();
    writableSettings->setValue("autoSaveOption", autoSaveOption);
    autoSaveDir=dlg.getSaveDir();
    writableSettings->setValue("autoSaveDir", autoSaveDir);
    QDir dir(autoSaveDir);
    if(!dir.exists())
        dir.mkpath(autoSaveDir);
    autoSaveDir=dir.absolutePath();
    if(barCodeMode){
        QPalette palette=ui->snEdit->palette();
        palette.setColor(QPalette::Base,Qt::red);
        ui->snEdit->setPalette(palette);
        ui->snEdit->setToolTip(tr("Program cu citire număr serie din imagine. Orice introduceți aici va fi ignorat!"));
    } else {
        //QPalette palette=ui->snEdit->palette();
        //palette.setColor(QPalette::Base,Qt::white);
        ui->snEdit->setPalette(QApplication::palette(ui->snEdit));
        ui->snEdit->setToolTip(QString());
    }
    ui->controlWidget->setPalette(this->palette());
    ui->resultLbl->setText("");
    ui->snEdit->clear();
    ui->snEdit->selectAll();
    ui->snEdit->setFocus();
    ui->testDebugWidget->setBoardDefPtr(&boardDef);
}

bool TestInterface::makeProtocol(QString serialNumber, bool boardOk,
                                 const QList<DetectedComponent> &foundComponents)
{
    writableSettings->sync();
    QString t=QDateTime::currentDateTime().toString("ddMMyyHHmmss");
    QString testPlaceType=writableSettings->value("testPlaceType","AOI").toString();
    QString testPlaceNumber=writableSettings->value("testPlaceNumber").toString();
    QString fileName=t+"_"+boardDef->name+"_"+serialNumber+"."+
            testPlaceType+
            testPlaceNumber;
    fileName.replace( QRegExp( "[" + QRegExp::escape( "\\/:*?\"<>|@$%&'~`#^+={}[];" ) + "]" ),  QString( "_" ) );
    QString destination;
    if(!boardDef->snValidator.isEmpty()){
        destination=writableSettings->value("localPath").toString();
    }else{
        destination=writableSettings->value("localStoragePath").toString();
    }
    QDir dir;
    dir.mkpath(destination);
    QFile f(destination+fileName);
    f.open(QIODevice::WriteOnly);
    if(!f.isOpen()){
        qDebug()<<"Unable to open file "+f.fileName();
        emit logMsg("Unable to open file "+f.fileName(),CRITICAL);
        return false;
    }
    QString mat=boardDef->name.section("_",0,0);
    bool statusSent=snCheck->fastReportSNStatusSocket(serialNumber, mat, "", testPlaceType, testPlaceNumber, boardOk?0:1);
    QString testCode=writableSettings->value("testCode").toString();
    if(!boardDef->testCodeOverride.isEmpty())
        testCode=boardDef->testCodeOverride;
    QTextStream ts( &f );
    ts <<  "sender            :  " << testPlaceType;
    ts <<  "\ndatum             :  " << t;
    ts <<  "\nlaenge            :";
    ts <<  "\nprotokoll         :  LEIKA\n\n";

    ts <<  "-----------------------------------------------------------\n\n";
    ts <<   "#KOPF {\n" <<
            "    #BARCODE = \"" << serialNumber << "\";\n" <<
            "    #ZEICHNUNGSNR = \"" << mat << "\";\n"<<
            "    #PRUEFCODE = \"" << testCode << "\";\n" <<
            "    #PRUEFMETHODE = \"" << writableSettings->value("testMethod").toString() << "\";\n"<<
            "    #PLATZNUMMER = \"" << testPlaceNumber << "\";\n" <<
            "    #PLATZART = \""<< testPlaceType << "\";\n" <<
            "    #ZEITPUNKT = \"" << t <<"\";\n" <<
            "    #PRUEFKENN = \"" << (boardOk?"P":"F") << "\";\n";
    if(statusSent)
        ts<<"    #NOSTATUS = \"1\";\n";
    if(ts.status()!=QTextStream::Ok){
        //qDebug()<<"error writing to file"+f.fileName();
        emit logMsg("error writing to file "+f.fileName(),CRITICAL);
        return false;
    }
    ts <<   "}\n";
    if(!boardOk){
        ts << "#PRUEFUNG {\n";
        if(foundComponents.count()>0){
            QStringList lst;
            for(int i=0; i<foundComponents.count(); i++){
                const DetectedComponent & comp=foundComponents[i];
                qDebug()<<comp.x<<comp.y<<comp.w<<comp.h<<comp.orientation<<
                          comp.boardComp.name<<comp.status;
                //only save not detected components and barcodes to protocol
                if((comp.status!=DetectedComponent::Expected)&&(comp.status!=DetectedComponent::ExpectedBarcode))
                    continue;
                QString compName=comp.boardComp.name;
                if(compName.isEmpty())
                    compName="unnamed component";
                if(lst.contains(compName)){
                    for(int i=2; i<1000; i++){
                        QString tmpName=compName+"_"+QString::number(i);
                        if(lst.contains(tmpName))
                            continue;
                        compName=tmpName;
                        break;
                    }
                }
                QString comment;
                if(comp.status==DetectedComponent::ExpectedBarcode){
                    comment="        #KOMMENTAR = \"Barcode not found\";\n";
                } else if(!comp.classifierName.isEmpty()){
                    comment="        #KOMMENTAR = \"" + comp.classifierName + "\";\n";
                }
                ts << "    #SATZ {\n"
                      "        #EINBAUBEZ = \"" << compName <<"\";\n"<<
                      "        #TESTART = \"PRESENCE\";\n"<<
                      //"        #KOMMENTAR = \"Component not found\";\n"<<
                      comment <<
                      "        #STATUS = \"F\";\n"<<
                      //"        #ZERTIFIKATE = \"N\";\n"<<
                      "    }\n";
            }

        } else {
            //board not found, make dummy component for the board
            ts << "    #SATZ {\n"
                  "        #EINBAUBEZ = \"" << mat <<"\";\n"<<
                  "        #TESTART = \"Board presence\";\n"<<
                  //"        #KOMMENTAR = \"Board not found\";\n"<<
                  "        #STATUS = \"F\";\n"<<
                  //"        #ZERTIFIKATE = \"N\";\n"<<
                  "    }\n";

        }
        ts << "}\n";
    }
    ts.flush();
    if(ts.status()!=QTextStream::Ok){
        qDebug()<<"error writing to file"+f.fileName();
        emit logMsg("Error writing to file "+f.fileName(),CRITICAL);
        return false;
    }
    f.close();
    myFtp->ftpUpload();
    return true;
}

bool TestInterface::sqliteTableHasField(QString tableName, QString fieldName)
{
    QSqlQuery query;
    QString sql="PRAGMA table_info("+ tableName+")";
    if(!query.exec(sql)){
        qDebug()<<"error while checking for field"<<query.lastError()<<query.lastQuery();
    }
    while (query.next()) {
        if(query.value("name").toString()==fieldName)
            return true;
    }
    return false;
}

void TestInterface::saveImage(const QList<QImage> imageList, const QList<DetectedComponent> boardRects, const QString realSN)
{
    //make a copy of the input data, because the original might get changed before saving the data is finished
    auto imageListCopy=imageList;
    imageListCopy.detach();
    auto boardRectsCopy=boardRects;
    boardRectsCopy.detach();
    //save images
    QString dirName=autoSaveDir+"/"+boardDef->name;
    QDir dir;
    dir.mkpath(dirName);
    QString sn=realSN;
    sn.replace( QRegExp( "[" + QRegExp::escape( "\\/:*?\"<>|@$%&'~`#^+={}[];" ) + "]" ),  QString( "_" ) );
    QString fileName=dirName+"/"+QDateTime::currentDateTime().toString("ddMMyyHHmmss")+"_"+sn;
    for(int i=0; i<imageList.count(); i++){
        QString index;
        if(i>0){
            //index.asprintf(".%02d", i);
            index=QString(".%1").arg(i,2,10,QChar('0'));
        }
        qDebug()<<"saving"<<fileName+index+".png, size"<<imageListCopy[i].size();
        imageListCopy[i].save(fileName+index+".png");
    }
    std::ofstream out((fileName+".result").toStdString(), std::ofstream::out|std::ofstream::binary);
    if(out.is_open()){
        serialize(boardRectsCopy, out);
        out.close();
    }

}

void TestInterface::clearForNewImage()
{
    ui->controlWidget->setPalette(this->palette());
    if(continuousAcquire)
        testScene.clearDetectedRects();
    else
        testScene.clear();
    boardRects.clear();
    ui->resultLbl->setText("");
    imageList.clear();
    imageAquisitionDone=false;

}

void TestInterface::startCapture()
{
    QString cameraType=writableSettings->value("cameratype").toString();
    qDebug()<<"cameratype"<<cameraType<<QDateTime::currentDateTime();
    if(cameraType=="qcamera") {
        myCamera->capture();
    } else if(cameraType=="ipcamera"){
        ipCamera.capture();
    } else if(cameraType=="simcamera"){
        on_loadBtn_clicked();
    }
#ifdef OCVCAM
    else  if(cameraType=="ocvcamera"){
        ocvCamera->capture();
    }
#endif
#ifdef RASPICAM
    else  if(cameraType=="raspicvcamera"){
        raspiCvCamera.capture();
    }
#endif

    return;
#if 0
    qDebug()<<"cameraBtn"<<QDateTime::currentDateTime();
    if(!imageCapture)
        configureCamera();
    if(imageCapture){
        qDebug()<<"isreadyforcapture"<<imageCapture->isReadyForCapture();
        if(camera->status()!=QCamera::ActiveStatus){
            pictureRequested=true;
            qDebug()<<"retry capture";
        }else {
            qDebug()<<"capture";
            //capture writes to file. There is no known to prevent it
            QDir dir("/tmp/images/");
            dir.setNameFilters(QStringList() << "*");
            dir.setFilter(QDir::Files);
            foreach (QString entry, dir.entryList()) {
                dir.remove(entry);
            }
            imageCapture->capture("/tmp/images/");
        }
    }
#endif

}

void TestInterface::checkForUpdates()
{
    if(boardDef==nullptr)
        return;
    if(!boardDef->isValid())
        return;
    QSqlDatabase db=QSqlDatabase::database();
    unsigned lastupdate=boardDef->getLastUpdated(db);
    if(lastupdate>boardDef->lastupdate){
        outdated=true;
    }
    qDebug()<<"checkForUpdates db:"<<QDateTime::fromSecsSinceEpoch(lastupdate)<<" mem:"<<QDateTime::fromSecsSinceEpoch(boardDef->lastupdate);
}

long long TestInterface::dbCheckAndSetAppBuildTime(bool warn, bool force)
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    sql="SELECT * FROM generalinfo WHERE name='appbuildtime'";
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        return -1;
    }
    if(!query.next()){
        //the app build time has not yet been saved.
       sql="INSERT INTO generalinfo (name, value) VALUES('appbuildtime', "+
               QString::number(appBuildTime())+")";
       if(!query.exec(sql)){
           qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
           return -1;
       }
       return appBuildTime();
    }
    long long t=query.value("value").toLongLong();
    if(appBuildTime()==t)
        return t;
    if((t<appBuildTime())||force){
        sql="UPDATE generalinfo SET value="+QString::number(appBuildTime())+
                " WHERE name='appbuildtime'";
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
            return -1;
        }
        return appBuildTime();
    }
    //we have t>appBuildTime()
    if(warn)
        QMessageBox::warning(this, tr("Aplicație veche"), tr("Atenție, pentru aceste program există o versiune mai nouă. Rezultatele obținute ar putea fi eronate."));
    return t;
}


void TestInterface::updateIfOutdated()
{
    if(!outdated)
        return;
    auto btn=QMessageBox::question(this, tr("Actualizare program"),
                                   tr("Există o versiune nouă a programului de inspectie. Doriți reîncărcarea programului?"));
    if(QMessageBox::Yes==btn)
        selectBoard("", true);
}

void TestInterface::getImage(const QImage &img)
{
    emit logMsg("capture done with camera or manually", VERBOSE);
    qDebug()<<"captured"<<img.width()<<img.height()<<img.depth()<<img.byteCount()<<img.bytesPerLine()<<
              QDateTime::currentDateTime()<<"image"<<imageList.count()<<"done"<<imageAquisitionDone<<"task"<<runTask;
    //QImage i2=img.convertToFormat(QImage::Format_RGB888);
    //qDebug()<<i2.width()<<i2.height()<<i2.depth()<<i2.byteCount()<<i2.bytesPerLine();
    //i2.save("i2.png");
    if(imageAquisitionDone){
        return;
        //imageList.clear();
        //imageAquisitionDone=false;
    }
    if(imageList.isEmpty()){
        testScene.setBackgroundImage(img);
        testScene.update();
    }
    imageList.append(img);
    emit gotNewImage();
    if(boardDef==nullptr){
        imageAquisitionDone=true;
    } else {
        if(boardDef->lights.count()<=imageList.count())
            //all needed images have been acquired
            imageAquisitionDone=true;
        else {
            // set lights and thus initiate capture
            int idx=imageList.count();
            if(boardDef->lights[idx].imageUsed){
                ///@todo don't wait, do this with signals and slots
                ioControl.setLights(boardDef->lights[idx],true);
                //startCapture();
            } else {
                //just append empty image
                getImage(QImage());
            }
        }

    }
    qDebug()<<"shown"<<QDateTime::currentDateTime()<<imageList.count()<<((boardDef!=nullptr)?boardDef->lights.count():0);
    //if this slot has been called while running a test, continue with the next step
    if(runTask==ANALYZE){
        runTask=PROCESSING;
        runTestSlot();
        runTask=IDLE;
        imageAquisitionDone=true;
    } else if(runTask != PROCESSING) {
        //normal acquisition
        imageAquisitionDone=true;
        auto lst=ui->cameraBtn->menu()->actions();
        if(lst.count()>1){
            if(lst[1]->isChecked()){
                //continuous acquisition is active, start acquisition
                qDebug()<<"continuous acquisition";
                qApp->processEvents();
                on_cameraBtn_clicked();
            }
        }
    }

}

/*
void TestInterface::cameraIsReady(bool ready)
{
    qDebug()<<"statusChanged"<<ready<<pictureRequested;
    if(!pictureRequested)
        return;
    if(ready){
        on_cameraBtn_clicked();
    }
}
*/

void TestInterface::on_cameraBtn_clicked()
{
    emit logMsg("starting capture operation",VERBOSE);
    clearForNewImage();
    startCapture();
}

/* not used any more
void TestInterface::getPixmap(QPixmap pixmap)
{
    emit logMsg("capture done", VERBOSE);
    testScene.setBackgroundImage(pixmap);
    testScene.update();
    //if this slot has been called while running a test, continue with the next step
    if(runTask==ANALYZE){
        runTestSlot();
        runTask=IDLE;
    } else {
        //normal acquisition
        auto lst=ui->cameraBtn->menu()->actions();
        if(lst.count()>1){
            if(lst[1]->isChecked()){
                //continuous acquisition is active, start acquisition
                qDebug()<<"getPixmap continuous";
                QTimer::singleShot(50,this, SLOT(on_cameraBtn_clicked()));
            }
        }
    }
}
*/


void TestInterface::on_playBtn_clicked()
{
    //this slot is either activated by pressing the button, or during a test, after acquiring the image
    bool testOk=false;
    try {

        if(nullptr==boardDef){
            return;
        }
        if(testScene.bg.isNull()){
            return;
        }
        testScene.clearRects();
        qDebug()<<"on_playBtn_clicked";
        qDebug()<<boardDef->refClassifier.name<<boardDef->refClassifier.minArea<<boardDef->minArea;
        //comp.classifiers.insert(-1,QSharedPointer<MemCompDef>(&(boardDef->refClassifier)));
        //scale=0.55;
        if(imageList.isEmpty()){
            imageList.append(testScene.bg.toImage().convertToFormat(QImage::Format_RGB888));
        }
        boardRects=boardDef->runOnImage(imageList, displaySearchFrames, testOk, rotateComponents);
    }  catch (std::exception& e) {
        emit logMsg( QString("Standard exception in on_playBtn_clicked: ") + e.what(), CRITICAL);
        testOk=false;
    } catch (...) {
        emit logMsg("default exception caught while running test in on_playBtn_clicked", CRITICAL);
        testOk=false;
    }
    showTestResult(boardRects, testOk);
    emit statusChanged(testOk?0:1);
}

void TestInterface::on_runButton_clicked()
{

    if(ANALYZE==runTask){
        // be careful to not create an infinite loop!
        ioControl.emitStart();
        return;
    }
    if(IDLE==runTask){
        emit statusChanged(-1 );
    }
    try {
        if(boardDef){
            if(boardDef->lights.count()>=1){
                ioControl.setLights(boardDef->lights[0]);
            }
        }
        //disable continuous acquire
        auto lst=ui->cameraBtn->menu()->actions();
        if(lst.count()>1){
            lst[1]->setChecked(false);
        }
        continuousAcquire=false;
        //already done in clearForNewImage. Just to be sure
        imageList.clear();
        testScene.clear();
        boardRects.clear();
        //clear background
        testScene.setBackgroundImage(QPixmap());
        testScene.update();
        emit logMsg("starting test with input SN="+ui->snEdit->text()+" runTask="+QString::number(runTask), INFO);
        if(runTask==ANALYZE){
            emit logMsg("Received test start while previous test not finished. Resettng status to IDLE", IMPORTANT);
            runTask=IDLE;
            return;
        }

        if(nullptr==boardDef){
            QMessageBox::information(this, tr("nu e incărcat nici un program"),
                                     tr("dacă doriți să verificați plăci, incărcați întâi un program de inspecție"));
            emit logMsg("nu e incarcat nici un program. Abandon test SN="+ui->snEdit->text(), IMPORTANT);
            ui->snEdit->setFocus();
            ui->snEdit->selectAll();
            runTask=IDLE;
            return;
        }
        if(!boardDef->isValid()){
            QMessageBox::information(this, tr("incărcare program nereușită"),
                                     tr("dacă doriți să verificați plăci, incărcați întâi un program de inspecție"));
            emit logMsg("program de inspecție invalid. Abandon test SN="+ui->snEdit->text(), IMPORTANT);
            ui->snEdit->setFocus();
            ui->snEdit->selectAll();
            runTask=IDLE;
            return;
        }
        qDebug()<<"runButton"<<"task"<<runTask;
        if(runTask==IDLE){
            //this is used only for aborted tests. After serial number check it will be set to false
            imageAquisitionDone=true;
            serialNumber=ui->snEdit->text()/*.toUpper()*/;
            QStringList dummySerials;
            dummySerials<<"TESTA"<<"TESTAA"<<"111111111"<<"11111111111";
            if(!boardDef->snValidator.isEmpty()){
                //check if the board has a valid serial number when the serial number is provided at the beginning of the test
                if(!barCodeMode){
                    if(dummySerials.contains(serialNumber), Qt::CaseInsensitive){
                        //just accept the serial number
                    } else {
                        bool ok=false;
                        QString realSN=serialNumber;
                        ok= RegExTester::validateSn(serialNumber,boardDef->snValidator, realSN);
                        if(!ok){
                            QString msg="Numărul de serie "+serialNumber+" nu e valid";
                            QMessageBox::information(this, "număr serie invalid", msg);
                            emit logMsg(msg,INFO);
                            ui->snEdit->setFocus();
                            ui->snEdit->selectAll();
                            return;
                        }
                        QString testPlaceType, testPlaceNumber, testCode;
                        testPlaceType=writableSettings->value("testPlaceType").toString();
                        testPlaceNumber=writableSettings->value("testPlaceNumber").toString();
                        testCode=writableSettings->value("testCode").toString();
                        if(!boardDef->testCodeOverride.isEmpty())
                            testCode=boardDef->testCodeOverride;
                        QString mat=boardDef->name.section('_',0,0);
                        if(!snCheck->checkSNSocket(realSN, mat, testCode, testPlaceType, testPlaceNumber)){
                            QString msg="număr serie invalid, eroare "+ snCheck->getLastError();
                            QMessageBox::information(this, "număr serie invalid", snCheck->getLastError());
                            emit logMsg(msg,INFO);
                            ui->snEdit->setFocus();
                            ui->snEdit->selectAll();
                            return;
                        }
                    }
                }
            }
            switch(boardDef->testStartType){
            case 0://start immediate
            default:
                //prepare for the next task and fall through to it
                runTask=CAPTURE;
                break;
            case 1://start delayed
                //prepare for the next task and start the timer to initiate the task.
                //this will also allow to start the task by pressing RUN a second time, but there is the risk
                //to receive the timer signal after the analyze is finished, starting a new test
                runTask=CAPTURE;
                QTimer::singleShot(boardDef->testStartDelay,this, SLOT(on_runButton_clicked()));
                //exit from the function
                return;
            case 2://start when RUN is pressed a second time
                //prepare for the next task and exit from the function
                runTask=CAPTURE;
                return;
            }
        }
        if(runTask==CAPTURE){
            emit logMsg("capture image",VERBOSE);
            runTask=ANALYZE;
            on_cameraBtn_clicked();
        }
    }  catch (std::exception& e) {
        emit logMsg( QString("Standard exception in on_runButton_clicked: ") + e.what(), CRITICAL);
    } catch (...) {
        emit logMsg("default exception caught while running test in on_runButton_clicked", CRITICAL);
    }
}

void TestInterface::runTestSlot()
{
    //this slot is activated during a test as the last phase, after acquiring the first image
    //do the test
    on_playBtn_clicked();
    //check if image needs to be saved
    bool imageOk=(boardRects.count()>=boardDef->numberOfBoards);
    foreach (const DetectedComponent & board, boardRects) {
        if(board.status!=DetectedComponent::Detected)
            imageOk=false;
    }

    lastBoardsFound=boardRects.count();
    // save results
    QString realSN=serialNumber;
    if(boardDef->snValidator.isEmpty()){
        //no serial number requested, so nothing needs to be saved
        emit logMsg("test run without result saving, SN="+ui->snEdit->text()+(imageOk?" PASS":" FAIL"), INFO);
    } else {
        if(boardRects.count()==0){
            //board not found
            QList<DetectedComponent> list;
            emit logMsg("board not found, SN="+ui->snEdit->text(), IMPORTANT);
            if(!barCodeMode){
                //if no board has been detected, for sure we do not have scanned the serial number from the picture, so if barCodeMode is active we have nothing to save
                //get the real serail number using the validator/extractor and save the result
                RegExTester::validateSn(serialNumber, boardDef->snValidator, realSN);
                makeProtocol(realSN,false,list);
            }
        } else {
            emit logMsg("found "+QString::number(boardRects.count()) +" boards, making protocol, provided SN="+ui->snEdit->text()+" image result "+(imageOk?"PASS":"FAIL"),INFO);
            if(boardRects.count()!=boardDef->numberOfBoards){
                qDebug()<<"found"<<boardRects.count()<<"boards instead of"<<boardDef->numberOfBoards;
            }
            for(int i=0; i<boardRects.count(); i++){
                bool snOk=true;
                realSN=serialNumber;
                if(barCodeMode){
                    // serial number check for serial numbers decoded from the image
                    // find serial number
                    serialNumber.clear();
                    //index of the DetectedComponent
                    int bcIndex=-1;
                    for(int j=0; j<boardRects[i].parts.count(); j++){
                        DetectedComponent &comp =boardRects[i].parts[j];
                        if(comp.status==DetectedComponent::DetectedBarcode){
                            bcIndex=j;
                            if(comp.parts.count()!=1){
                                qDebug()<<"barcode detection went wrong, parts="<<comp.parts.count();
                            } else {
                                qDebug()<<"barcode"<<comp.boardComp.name<<comp.parts[0].boardComp.name;
                                if(comp.boardComp.name.compare("barcode",Qt::CaseInsensitive)==0){
                                    serialNumber=comp.parts[0].boardComp.name;
                                    break;
                                }
                            }
                        } else if(comp.status==DetectedComponent::ExpectedBarcode){
                            if(comp.boardComp.name.compare("barcode",Qt::CaseInsensitive)==0){
                                bcIndex=j;
                                break;
                            }
                        }
                    }
                    realSN=serialNumber;
                    QString testPlaceType, testPlaceNumber, testCode;
                    testPlaceType=writableSettings->value("testPlaceType").toString();
                    testPlaceNumber=writableSettings->value("testPlaceNumber").toString();
                    testCode=writableSettings->value("testCode").toString();
                    if(!boardDef->testCodeOverride.isEmpty())
                        testCode=boardDef->testCodeOverride;
                    QString snErrorMsg;
                    if(serialNumber.isEmpty()){
                        snErrorMsg=tr("Codul de bare nu s-a putut citi");
                        emit logMsg(snErrorMsg,INFO);
                    } else {
                        //check if the serial number read from the image is valid, This is never a dummy serial number
                        bool ok=RegExTester::validateSn(serialNumber, boardDef->snValidator, realSN);
                        if(!ok){
                            snErrorMsg=tr("Numărul de serie ")+serialNumber+tr(" e incompatibil cu validatorul ")+boardDef->snValidator;
                            emit logMsg(snErrorMsg,INFO);
                        }
                    }
                    if(snErrorMsg.isEmpty()){
                        QString mat=boardDef->name.section('_',0,0);
                        if(!snCheck->checkSNSocket(realSN, mat, testCode, testPlaceType, testPlaceNumber)){
                            snErrorMsg=tr("număr serie invalid, eroare ")+ snCheck->getLastError();
                            emit logMsg(snErrorMsg,INFO);
                        }
                    }
                    if(!snErrorMsg.isEmpty()){
                        //invalid serial number read - ask for another one
                        BarcodeCorrection dlg;
                        dlg.serErrorText(snErrorMsg);
                        auto result=dlg.exec();
                        snErrorMsg.clear();
                        realSN=serialNumber=dlg.getSerialNumber();
                        if(QDialog::Rejected==result){
                            //just abort the test silently
                            snOk=false;
                        } else {
                            //check the serial number entered by the user
                            if(serialNumber.isEmpty()){
                                snErrorMsg=tr("Codul de bare introdus este gol");
                                emit logMsg(snErrorMsg,INFO);
                            } else {
                                realSN=serialNumber;
                                bool ok=RegExTester::validateSn(serialNumber, boardDef->snValidator, realSN);
                                if(!ok){
                                    snErrorMsg=tr("Numărul de serie ")+serialNumber+tr(" e incompatibil cu validatorul ")+boardDef->snValidator;
                                    emit logMsg(snErrorMsg,INFO);
                                } else {
                                    QString mat=boardDef->name.section('_',0,0);
                                    if(!snCheck->checkSNSocket(realSN, mat, testCode, testPlaceType, testPlaceNumber)){
                                        snErrorMsg=tr("număr serie invalid, eroare ")+ snCheck->getLastError();
                                        emit logMsg(snErrorMsg,INFO);
                                    }
                                }
                            }
                            if(!snErrorMsg.isEmpty()){
                                QMessageBox::information(this, tr("număr serie invalid, rezultatul pentru placa nu se va salva"),
                                                         snErrorMsg);
                                snOk=false;
                                boardRects[i].status=DetectedComponent::Expected;
                            }
                        }
                    }
                    qDebug()<<"snOK"<<snOk<<serialNumber<<bcIndex;
                    if(bcIndex>=0){
                        if(snOk&&(!serialNumber.isEmpty())){
                            boardRects[i].parts[bcIndex].status=DetectedComponent::DetectedBarcode;
                        } else {
                            boardRects[i].parts[bcIndex].status=DetectedComponent::ExpectedBarcode;
                        }
                    }
                    boardDef->setBoardStatus(boardRects[i]);
                    qDebug()<<boardRects[i].parts[bcIndex].status<<boardRects[i].status;
                } else {
                    //we need to get the real serial number in order to make the protocol and save the results
                    //this has already been done one, so we don't expect it to fail
                    RegExTester::validateSn(serialNumber, boardDef->snValidator, realSN);
                }
                if(snOk&&(!serialNumber.isEmpty())){
                    makeProtocol(realSN/*serialNumber*/,boardRects[i].status==DetectedComponent::Detected, boardRects[i].parts);
                }
            }
        }
    }
    //redo check if image needs to be saved, maybe something was changed
    imageOk=(boardRects.count()>=boardDef->numberOfBoards);
    foreach (const DetectedComponent & board, boardRects) {
        if(board.status!=DetectedComponent::Detected)
            imageOk=false;
    }
    if(barCodeMode){
        testScene.clearRects();
        showTestResult(boardRects, imageOk);
        emit statusChanged(imageOk?0:1);
    }
    //save the image if needed
    if((autoSaveOption==SAVEALL)||
            ((autoSaveOption==SAVEFAIL)&&(!imageOk))){
        qDebug()<<"saving"<<imageList.count()<<"images";
        QtConcurrent::run(this, &TestInterface::saveImage, imageList, boardRects, realSN);
    }
    //clear the serial number input field when it's not used anyway
    if(barCodeMode){
        ui->snEdit->clear();
    }
    ui->snEdit->setFocus();
    if(clearOldSN){
        ui->snEdit->clear();
    } else {
        ui->snEdit->selectAll();
    }
    updateIfOutdated();
}

void TestInterface::on_cameraBtn_triggered(QAction *arg1)
{
    qDebug()<<"triggered"<<arg1->objectName()<<arg1->text();
    if(arg1->objectName()=="continuousAcquire"){
        qDebug()<<"continuousAcquire";
        continuousAcquire=arg1->isChecked();
        on_cameraBtn_clicked();
        return;
    }
    if(arg1->objectName()=="stressTestArduino"){
        qDebug()<<"stressTestArduino";
        ImagesAndLights lights(3);
        lights.lightsEnabled=true;
        lights.lightData="001";
        ioControl.stressTest(lights,arg1->isChecked());
        return;
    }
    CameraConfig dlg((writableSettings), myCamera, &ipCamera,
                 #ifdef OCVCAM
                     ocvCamera,
                 #endif
                 #ifdef RASPICAM
                     &raspiCvCamera,
                 #endif
                     this);
    connect(&dlg, SIGNAL(loadImageRequest()), this, SLOT(on_loadBtn_clicked()));
    dlg.exec();
}


void TestInterface::on_snEdit_returnPressed()
{
    on_runButton_clicked();
}

void TestInterface::addToClassifier(long long classifierId, const QString &imageName)
{
    if(editor==nullptr)
        on_editorBtn_clicked();
    if(editor==nullptr)
        return;
    editor->addToClassifier(classifierId, imageName);
}

void TestInterface::showBoard(long long boardId)
{
    qDebug()<<"showBoard"<<boardId;
    if(editor==nullptr)
        on_editorBtn_clicked();
    if(editor==nullptr)
        return;
    editor->showBoard(boardId);
}

void TestInterface::ioControlRun()
{
    if((boardDef==nullptr) || (boardDef->testStartExtDisabled)){
        if(boardDef!=nullptr)
            qDebug()<<"Not starting test because external test start is disabled for this board";
        return;
    }
    //don't call on_runButton_clicked() with ANALYZE, as this would be an infinite loop
    if((runTask==IDLE)||(runTask==CAPTURE)){
        static bool busy=false;
        if(!busy){
            QDateTime t=QDateTime::currentDateTime();
            QMessageBox::StandardButton btn=QMessageBox::Yes;
            if((runTask==IDLE)&&(0==lastBoardsFound)){
                ///@todo update lastBoardsFound, in order to allow fast triggering when there are boards found
                if(lastTrigger.secsTo(t)<10){
                    busy=true;
                    QApplication::beep();
                    btn=QMessageBox::question(this, tr("Activare rapidă senzor"),tr("sigur doriți pornirea unui test?"));
                    busy=false;
                }
            }
            lastTrigger=t;
            if(QMessageBox::Yes==btn)
                on_runButton_clicked();
        }
    }
    // else do nothing - the iocontrol should be waiting for the trigger and should autotrigger if requested by the test program
}

void TestInterface::showTestResult(QList<DetectedComponent> &boardRects, const bool testOk)
{
    //show result
    testScene.blockSignals(true);
    for(int i=0; i<boardRects.count(); i++){
        QList<DetectedComponent> & foundComponents = boardRects[i].parts;
        for(int j=0; j<foundComponents.count(); j++){
            DetectedComponent & comp=foundComponents[j];
            if(!showFiducials){
                if((comp.status==DetectedComponent::DetectedFiducial)||
                        (comp.status==DetectedComponent::ExpectedFiducial))
                    continue;
            }
            ComponentRectItem * item=new ComponentRectItem(comp);
            testScene.addItem(item);
        }
        ComponentRectItem *item=new ComponentRectItem(boardRects[i]);
        //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
        testScene.addItem(item);
    }
    testScene.blockSignals(false);
    //display the global status of the test
    QPalette pal=ui->controlWidget->palette();
    ui->controlWidget->setAutoFillBackground(true);
    if(testOk){
        //ui->controlWidget->setStyleSheet("background-color:green;");
        pal.setColor(QPalette::Window,Qt::green);
        ui->controlWidget->setPalette(pal);
        if(writableSettings->value("showResultLabel").toBool())
            ui->resultLbl->setText("PASS");
    } else {
        //ui->controlWidget->setStyleSheet("background-color:red;");
        pal.setColor(QPalette::Window,Qt::red);
        ui->controlWidget->setPalette(pal);
        if(writableSettings->value("showResultLabel").toBool())
            ui->resultLbl->setText("FAIL");
    }
    testScene.update();
}

void TestInterface::showImage(QString fileName)
{
    QImage img;
    img.load(fileName);
    testScene.clear();
    boardRects.clear();
    //testScene.setBackgroundImage(img);
    ui->controlWidget->setPalette(this->palette());
    ui->resultLbl->setText("");
    //emit imageLoaded(img);
    //testScene.setBackgroundImage(fileName);
    testScene.setBackgroundImage(img);
    testScene.update();
}

void TestInterface::showImageResult(QString fileName, QList<DetectedComponent> &boardRects, const bool testOk)
{
    showImage(fileName);
    showTestResult(boardRects, testOk);
    qApp->processEvents();
}

void TestInterface::centerAt(double x, double y)
{
    qDebug()<<"centerAt"<<x<<y;
    ui->testView->centerOn(x, y);
}

void TestInterface::captureError(QString msg)
{
    QMessageBox::warning(this, tr("eroare achiziție imagine"),msg );
    runTask=IDLE;
    emit logMsg(tr("eroare achiziție imagine")+": "+msg, IMPORTANT);

}
