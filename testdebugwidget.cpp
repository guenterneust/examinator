/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "testdebugwidget.h"
#include "ui_testdebugwidget.h"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QClipboard>
#include <QElapsedTimer>
#include <QLabel>
#include <QMenu>
#include <QAction>
#include "classifierpicker.h"
#include "ocvhelper.h"

QString TestDebugWidget::destination;

TestDebugWidget::TestDebugWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::testDebugWidget)
{
    ui->setupUi(this);
    boardStatModel.setImages(&images);
    imageStatModel.setImages(&images);
    ui->masterListView->setModel(&boardStatModel);
    ui->detailTreeView->setModel(&imageStatModel);
    //ui->detailTreeView->header()->hide()

   ui->detailTreeView->setSortingEnabled(true);
   proxyModel=new QSortFilterProxyModel();
   proxyModel->setSourceModel(&imageStatModel);
   ui->detailTreeView->setModel(proxyModel);
   //ui->detailTreeView->setModel(&imageStatModel);
   //ui->detailTreeView->expandAll();


    ui->detailTreeView->setRootIsDecorated(false);
    //ui->masterListView->selectionModel()->currentChanged();
    connect(ui->masterListView->selectionModel(),SIGNAL(currentChanged(const QModelIndex&,const QModelIndex&)),
            this, SLOT(masterViewCurrentChanged(const QModelIndex&,const QModelIndex&)));
    connect(&boardStatModel, SIGNAL(dataModified(int)), this, SLOT(updateDetail(int)));
    connect(&imageStatModel, SIGNAL(dataModified(uint,int)), this, SLOT(updateMaster(uint,int)));
    connect(&imageStatModel, SIGNAL(modelReset()), this, SLOT(expandDetailView()));
    timer.setInterval(2000);
    timer.setSingleShot(true);
    connect(&timer, SIGNAL(timeout()), this, SLOT(on_saveBtn_clicked()));
    QMenu *menu = new QMenu();
    QAction * action = new QAction(tr("adaugă la placă"), this);
    action->setObjectName("addToBoard");
    menu->addAction(action);
    ui->copyAndOpenBtn->setMenu(menu);
    menu = new QMenu();
    action = new QAction(tr("încarcă program placă"), this);
    action->setObjectName("loadBoard");
    menu->addAction(action);
    ui->loadDirBtn->setMenu(menu);
}

TestDebugWidget::~TestDebugWidget()
{
    delete ui;
}

void TestDebugWidget::on_loadDirBtn_clicked()
{
    QString fname=QFileDialog::getExistingDirectory(this, tr("director cu imagini de analizat"),dir.absolutePath());
    qDebug()<<"fname"<<fname;
    if(fname.isEmpty())
        return;
    boardStatModel.prepareForUpdate();
    images.clear();
    dir.setPath(fname);
    dir.setFilter(QDir::Files);
    ui->dirSelEdit->setText(fname);
    QStringList files=dir.entryList();
    QStringList imageExtensions;
    imageExtensions<<"png"<<"jpg"<<"jpeg"<<"bmp"<<"webp";
    qDebug()<<files;
    for(unsigned i=0; i<dir.count(); i++){
        QString fname=dir[i];
        QFileInfo info(fname);
        //qDebug()<<dir.absolutePath()<<fname<<info.fileName()<<info.baseName()<<info.suffix();
        if(!imageExtensions.contains(info.suffix(),Qt::CaseInsensitive))
            continue;
        QString fileName=dir.absolutePath()+"/"+fname;
        QString rName=info.completeBaseName()+".result";
        //bool hasResults=false;
        foreach(QString name, files){
            if(name.compare(rName,Qt::CaseInsensitive)==0){
                rName=name;
                //hasResults=true;
                break;
            }
        }
        ImageResults res;
        res.resultsName=dir.absolutePath()+"/"+rName;
        res.imageName=fileName;
        res.load();
        /*
        if(hasResults){
            std::ifstream in((res.resultsName).toStdString(), std::ifstream::in|std::ifstream::binary);
            res.unknown=false;
            try {
                deserialize(res.boards,in);
                try {
                    dlib::deserialize(res.decision, in);
                } catch(...){
                    //no decision in file for board
                    res.calculateDecision();
                }
            } catch(...){
                qDebug()<<"deserialize of saved results failed";
                res.boards.clear();
                res.unknown=true;
            }

        }
        */
        images.append(res);
    }
    boardStatModel.updateFinished();
    if(!ui->masterListView->currentIndex().isValid()){
        if(images.count()>0)
            ui->masterListView->setCurrentIndex(boardStatModel.index(0));
    }
}

BoardStatModel::BoardStatModel(QObject *parent)
{
    Q_UNUSED(parent);
}

QVariant BoardStatModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // not needed
    Q_UNUSED(section);
    Q_UNUSED(orientation);
    Q_UNUSED(role);
    return QVariant();
}

int BoardStatModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    if(images==nullptr)
        return 0;
    return images->count();

}

int BoardStatModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 2;

}

QVariant BoardStatModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    if(index.row()<images->count()){
        if(role == Qt::DisplayRole){
            return (*images)[index.row()].imageName;
        } else if(role==Qt::BackgroundRole){
            if((*images)[index.row()].unknown)
                return QVariant();
            //qDebug()<<"required boards"<<(*images)[index.row()].boards.count()<<requiredBoards;
            if((*images)[index.row()].boards.count()==0){
                //board not found
                switch((*images)[index.row()].decision){
                case 0:
                    return QColor(Qt::red);
                case 1:
                    return QColor(Qt::yellow);
                default:
                     return QColor(Qt::red).lighter(175);
                }
            }
            bool imageOk=((*images)[index.row()].boards.count()>=requiredBoards);
            for(int i=0; i<(*images)[index.row()].boards.count(); i++){
                if((*images)[index.row()].boards[i].status!=DetectedComponent::Detected)
                    imageOk=false;
            }
            if(imageOk){
                switch((*images)[index.row()].decision){
                case 0:
                    return QColor(Qt::cyan);
                case 1:
                    return QColor(Qt::green);
                default:
                     return QColor(Qt::green).lighter(175);
                }
            }
            switch((*images)[index.row()].decision){
            case 0:
                return QColor(Qt::red);
            case 1:
                return QColor(Qt::yellow);
            default:
                 return QColor(Qt::red).lighter(175);
            }

        } else if(role == Qt::CheckStateRole){
            if(index.column()==0){
                switch ( (*images)[index.row()].decision){
                case 1: return(Qt::Checked);
                case 0: return(Qt::Unchecked);
                default: return(Qt::PartiallyChecked);
                }
            }
        }

    } else {
        qDebug()<<"invalid index row"<<index.row();
    }
    return QVariant();

}

Qt::ItemFlags BoardStatModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);
     if (index.isValid()){
         if(index.column()==0)
            return defaultFlags | Qt::ItemIsUserCheckable | Qt::ItemIsTristate;
     }
     return defaultFlags;
}

void BoardStatModel::setImages(QList<ImageResults> *images)
{
    this->images=images;
}

void TestDebugWidget::setBoardDefPtr(BoardDef **boardDefPtr)
{
    this->boardDefPtr=boardDefPtr;
    if((boardDefPtr!=nullptr)&&((*boardDefPtr)!=nullptr))
        boardStatModel.setRequiredBoards((*boardDefPtr)->numberOfBoards);
}

void BoardStatModel::prepareForUpdate()
{
    beginResetModel();
}

void BoardStatModel::updateFinished()
{
    endResetModel();
}

void TestDebugWidget::on_boardTestBtn_clicked()
{
    if(boardDefPtr==nullptr)
        return;
    if((*boardDefPtr)==nullptr)
        return;
    boardStatModel.setRequiredBoards((*boardDefPtr)->numberOfBoards);
    //qDebug()<<"boardStatModel.setRequiredBoards((*boardDefPtr)->numberOfBoards)"<<(*boardDefPtr)->numberOfBoards;
    auto lst=ui->masterListView->selectionModel()->selectedIndexes();
    if(lst.isEmpty())
        lst<<ui->masterListView->currentIndex();

    foreach(auto &idx, lst){
        if(!idx.isValid()){
            qDebug()<<"invalid index";
            continue;
        }
        if(idx.row()>=images.count()){
            qDebug()<<"invalid index";
            continue;
        }
        ImageResults& res=images[idx.row()];
        bool testOk=processOneImage(res);
        emit imageChanged(res.imageName, res.boards, testOk);
        boardStatModel.update(idx);
        imageStatModel.setImage(idx.row());
        //ui->detailTreeView->expandAll();
        qApp->processEvents();
    }
}

bool TestDebugWidget::processOneImage(ImageResults &res)
{
    if((boardDefPtr==nullptr)||((*boardDefPtr)==nullptr))
        return false;
    auto imageList=BoardDef::loadBoardImages(res.imageName);

    bool testOk=false;
    /// because res might be the active image, and might be repainted during runOnImage, a new results object will be used during component search to avoid the risk of memory race
    ImageResults res2;
    res2.imageName=res.imageName;
    res2.load(res.resultsName);
    res2.boards=(*boardDefPtr)->runOnImage(imageList,false, testOk, rotateComponents, res2.resultsName,
                                          res2.boards, &(res2.decision));
    res=res2;
    res.unknown=false;
    if(imageList.count()>0){
        QRect r=imageList[0].rect();
        if(res.boards.count()>0){
            if((res.boards[0].orientation==CompDefRect::N)||(res.boards[0].orientation==CompDefRect::S))
                r=QRect(res.boards[0].x,res.boards[0].y, res.boards[0].w, res.boards[0].h);
            else
                r=QRect(res.boards[0].x,res.boards[0].y, res.boards[0].h, res.boards[0].w);
        }
        OcvHelper::imageSharpness(imageList[0], r);
    }
    return testOk;
}

void TestDebugWidget::copyAndOpenImage(bool boardClassifier)
{
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid())
        return;
    QFile file(images[idx.row()].imageName);
    if(!file.exists())
        return;
    QFileInfo info(file);
    QString srcFile=info.completeBaseName();

    long long classifierId=-1;
    QString warningText;
    if(boardClassifier){
        if((boardDefPtr!=nullptr)&&((*boardDefPtr)!=nullptr)){
            classifierId=(*boardDefPtr)->refClassifier.id;
            QString fname=(*boardDefPtr)->refClassifier.getLastImagePath();
            QFileInfo dstInfo(fname);
            destination=dstInfo.absoluteDir().absolutePath();
        }
    } else {
        auto idx2=ui->detailTreeView->currentIndex();
        if(idx2.isValid()){
            auto compPtr=static_cast<DetectedComponent *>(idx2.data(Qt::UserRole).value<void *>());
            //qDebug()<<"doubleclick"<<index.internalPointer()<<index.data()<<index.data(Qt::UserRole)<<compPtr;
            if(compPtr==nullptr)
                return;
            long long compId=compPtr->boardComp.id;
            if(compId>=0){
                //regular component
                if((boardDefPtr!=nullptr)&&((*boardDefPtr)!=nullptr)){
                    QString compTypeName;
                    auto lst=(*boardDefPtr)->classifierFromComponent(compId, compTypeName);
                    if(lst.count()>0){
                        int classifier=0;
                        if(lst.count()>1){
                            QString compName=idx2.data().toString();
                            ClassifierPicker picker(this);
                            picker.setValues(compName, compTypeName, lst);
                            picker.exec();
                            classifier=picker.result();
                            if(classifier<0)
                                return;
                        }
                        classifierId=lst[classifier].compDefPtr->id;
                        //qDebug()<<"classifier"<<classifier<<classifierId<<lst[classifier]->name;
                        //QString fname=lst[classifier]->images[n-1].imagePath;
                        QString fname=lst[classifier].compDefPtr->getLastImagePath();
                        QFileInfo dstInfo(fname);
                        destination=dstInfo.absoluteDir().absolutePath();
                    } else {
                        warningText=tr("clasificator neidentificat")+"; ";
                    }
                }
            } else {
                //board
                if((boardDefPtr!=nullptr)&&((*boardDefPtr)!=nullptr)){
                    classifierId=(*boardDefPtr)->refClassifier.id;
                    QString fname= (*boardDefPtr)->refClassifier.getLastImagePath();
                    QFileInfo dstInfo(fname);
                    destination=dstInfo.absoluteDir().absolutePath();
                } else {
                    warningText=tr("clasificator neidentificat")+"; ";
                }
            }
        } else {
            warningText=tr("clasificator neidentificat")+"; ";
        }
    }

    QString destFile=QFileDialog::getSaveFileName(this, warningText +
                                                  tr("Salvare copie imagine. Apăsați CANCEL pentru a nu copia nimic"),
                                                  destination+"/"+srcFile, tr("Imagini (*.png *.jpg *.jpeg);; Toate fișierele (*.*)"));
    if(!destFile.isEmpty()){
        QFileInfo dstInfo(destFile);
        qDebug()<<destFile<<dstInfo.absoluteDir().absolutePath();
        destination=dstInfo.absoluteDir().absolutePath();
        if(dstInfo.exists()){
            QMessageBox::information(this, tr("copiere abandonată"), tr("Nu se poate copia fișierul, deoarece există deja un fișier cu același nume") );
        } else {
            bool ok=file.copy(destFile);
            if(!ok)
                QMessageBox::information(this, tr("copiere nereușită"), tr("Nu s-a putut copia fișierul") );
        }
        QClipboard *clipboard = QGuiApplication::clipboard();
        clipboard->setText(destFile);
    }
    emit imageForClassifier(classifierId, destFile);
}

void TestDebugWidget::setRotateComponents(bool newRotateComponents)
{
    rotateComponents = newRotateComponents;
}

void TestDebugWidget::updateDetail(int position)
{
    if(!ui->masterListView->currentIndex().isValid())
        return;
    timer.start();
    if(position==ui->masterListView->currentIndex().row()){
        imageStatModel.setImage(position);
        ui->detailTreeView->expandAll();
    }
}

void TestDebugWidget::updateMaster(unsigned image, int decision)
{
    if(images.count()<=image){
        //illegal value
        return;
    }
    DetectedComponent * root=imageStatModel.getRootItem();
    if(root!=nullptr){
        /* copy decision*/
        assert(images[image].boards.count() == root->parts.count());
        for(int b=0; b<images[image].boards.count(); b++){
            assert(images[image].boards[b].parts.count() == root->parts[b].parts.count());
            for(int c=0; c<images[image].boards[b].parts.count(); c++){
                images[image].boards[b].parts[c].decision=root->parts[b].parts[c].decision;
            }
        }
    }
    timer.start();
    if(decision==0){
        //wrong detection, so entire image is wrongly asessed
        images[image].decision=0;

    } else {
        images[image].calculateDecision();
    }
    boardStatModel.update(boardStatModel.index(image));
    return;
}

void TestDebugWidget::expandDetailView()
{
    ui->detailTreeView->expandAll();
}

void TestDebugWidget::on_analyzeBtn_clicked()
{
    QElapsedTimer timer;
    timer.start();
    if(boardDefPtr==nullptr)
        return;
    if((*boardDefPtr)==nullptr)
        return;
    boardStatModel.setRequiredBoards((*boardDefPtr)->numberOfBoards);
    for(int i=0; i<images.count(); i++){
        images[i].unknown=true;
    }
    for(int i=0; i<images.count(); i++){
        ImageResults& res=images[i];
        processOneImage(res);
        boardStatModel.update(boardStatModel.index(i));
        qApp->processEvents();
    }
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid()){
        if(images.count()>0){
            idx=boardStatModel.index(0);
        }else
            return;
        ui->masterListView->setCurrentIndex(idx);
    }
    qDebug()<<"analysis took: "<<timer.elapsed();
    if(idx.row()>=images.count()){
        qDebug()<<"invalid index";
        return;
    }
    masterViewCurrentChanged(idx,idx);
}

void TestDebugWidget::on_delBtn_clicked()
{
    auto lst=ui->masterListView->selectionModel()->selectedIndexes();
    if(lst.isEmpty())
        lst<<ui->masterListView->currentIndex();
    if(lst.isEmpty())
        return;
    QList<int> list;
    foreach(auto idx, lst){
        if(!idx.isValid())
            return;
        list.append(idx.row());
    }
    qDebug()<<lst;
    std::sort(list.begin(), list.end());
    qDebug()<<lst;
    int i=-1;
    for(int j=list.count()-1; j>=0; j--){
        qDebug()<<"j"<<j;
        i=list[j];
        qDebug()<<"i"<<i<<"j"<<j;
        qDebug()<<images[i].imageName;
        auto btn=QMessageBox::question(this, tr("Confirmare ștergere fișier"), tr("Sigur doriți ștergerea definitivă a fișierului ")+images[i].imageName+" ?");
        if(btn!=QMessageBox::Yes)
            continue;
        boardStatModel.prepareForUpdate();
        if(!QFile::remove(images[i].imageName))
            qDebug()<<"Can't delete file"<<images[i].imageName;
        QFile::remove(images[i].resultsName);
        images.removeAt(i);
        boardStatModel.updateFinished();
    }
    QModelIndex idx;
    if(i>=0){
        if(images.count()>i)
            idx=boardStatModel.index(i);
        else
            if(i>0)
                idx=boardStatModel.index(i-1);
        ui->masterListView->setCurrentIndex(idx);
        masterViewCurrentChanged(idx,idx);
    }
}

void TestDebugWidget::on_copyAndOpenBtn_clicked()
{
    copyAndOpenImage(false);
}

void TestDebugWidget::masterViewCurrentChanged(const QModelIndex &idxTo, const QModelIndex & idxFrom)
{
    Q_UNUSED(idxFrom);
    if(idxTo.row()>=images.count()){
        qDebug()<<"invalid index received";
        return;
    }
    qDebug()<<idxTo.row()<<images[idxTo.row()].imageName<<ui->masterListView->currentIndex().row();
    int nBoards=1;
    if((nullptr!=boardDefPtr)&&(nullptr!=(*boardDefPtr)))
        nBoards=(*boardDefPtr)->numberOfBoards;
    bool imageOk=images[idxTo.row()].boards.count()>=nBoards;
    for(int i=0; i<images[idxTo.row()].boards.count(); i++){
        if(images[idxTo.row()].boards[i].status!=DetectedComponent::Detected)
            imageOk=false;
    }
    imageStatModel.setImage(idxTo.row());
    ui->detailTreeView->expandAll();
    emit imageChanged(images[idxTo.row()].imageName, images[idxTo.row()].boards, imageOk);
}

int ImageStatModel::rowCount(const QModelIndex &parent) const
{
    //qDebug()<<"rowcount"<<parent.isValid()<<parent.row()<<parent.column()<<parent.internalPointer();
    if (parent.column() > 0)
        return 0;
    const DetectedComponent * dc=(!parent.isValid())?(rootItem):static_cast<DetectedComponent *>(parent.internalPointer());
    if(dc==nullptr)
        return 0;
    //qDebug()<<"rowCount"<<dc->boardComp.name<<dc->parts.count();
    return(dc->parts.count());
    if(images->count()<=imageNum)
        return 0;
    if((*images)[imageNum].boards.count()<=0)
        return 0;
    return (*images)[imageNum].boards[0].parts.count();
}

int ImageStatModel::columnCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 6;
    return 6;
}

QVariant ImageStatModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    //QList<DetectedComponent> &components=(*images)[imageNum].boards[0].parts;
    DetectedComponent *comp=static_cast<DetectedComponent *>(index.internalPointer());
    //qDebug()<<"data"<<index.row()<<index.column()<<comp->boardComp.name<<comp->parts.count();
    //if(index.row()<components.count()){
    if(comp){
        //qDebug()<<"data"<<index.row()<<index.column()<<components.count()<<components[index.row()].boardComp.name;
        if(role == Qt::DisplayRole){
            switch(index.column()){
            case 0:
                return comp->boardComp.name;
            default:
            case 1:
                return comp->score[0];
            case 2:
                return comp->xerr * 100;
            case 3:
                return comp->yerr*100;
            case 4:
                return comp->scaleerr*100;
            case 5:
                return comp->classifierName;
            }
        } else if(role == Qt::CheckStateRole){
            if(index.column()==0){
                switch ( comp->decision){
                case 1: return(Qt::Checked);
                case 0: return(Qt::Unchecked);
                default: return(Qt::PartiallyChecked);
                }
            }
        } else if(role==Qt::BackgroundRole){
            if(index.column()==0){
               if( (comp->status==DetectedComponent::Detected) ||
                       (comp->status==DetectedComponent::DetectedFiducial) ||
                       (comp->status==DetectedComponent::DetectedBarcode)){
                   switch(comp->decision){
                   case 0:
                       return QColor(Qt::cyan);
                   case 1:
                       return QColor(Qt::green);
                   default:
                        return QColor(Qt::green).lighter(175);
                   }
                }
               switch(comp->decision){
               case 0:
                   return QColor(Qt::red);
               case 1:
                   return QColor(Qt::yellow);
               default:
                    return QColor(Qt::red).lighter(175);
               }
            }
        } else if(role==Qt::UserRole){
            //hack to make internal ponter available trough QSortFilterProxy
            return qVariantFromValue(static_cast<void*>(comp));
        }
    }
    return QVariant();
}

Qt::ItemFlags ImageStatModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index);
     if (index.isValid()){
         if(index.column()==0)
            return defaultFlags | Qt::ItemIsUserCheckable | Qt::ItemIsTristate;
     }
     return defaultFlags;
}

void ImageStatModel::setImages(QList<ImageResults> *images)
{
    this->images=images;
}

QModelIndex ImageStatModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent)){
        //qDebug()<<"index hasindex"<<row<<column<<parent;
        return QModelIndex();
    }
    DetectedComponent *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<DetectedComponent*>(parent.internalPointer());
    if(row>=parentItem->parts.count()){
        qDebug()<<"no index"<<row<<column<<parentItem->boardComp.name<<parentItem->parts.count()<<parent.isValid();
        return QModelIndex();
    }
    DetectedComponent *childItem = &(parentItem->parts[row]);
    //qDebug()<<"createindex"<<row<<column<<childItem->parts.count()<<childItem->boardComp.name;
    return createIndex(row, column, childItem);
}

QModelIndex ImageStatModel::parent(const QModelIndex &index) const
{
    //qDebug()<<"parent request"<<index.row()<<index.column()<<index.internalPointer();
    if (!index.isValid())
        return QModelIndex();
    DetectedComponent * item = static_cast<DetectedComponent*>(index.internalPointer());
    if(item==nullptr)
        return QModelIndex();
    //qDebug()<<"parent request"<<index.row()<<index.column()<<index.internalPointer()<<item->boardComp.name;
    DetectedComponent * parentItem=item->parent;
    if(parentItem==nullptr)
        return QModelIndex();
    if(parentItem->boardComp.name=="rootItem")
        return QModelIndex();
    //qDebug()<<"parent is"<<parentItem->boardComp.name;
    return createIndex(parentItem->row,0,parentItem);
}

void ImageStatModel::setImage(int image)
{
    this->beginResetModel();
    imageNum=image;
    qDebug()<<"imageNum"<<image;
    rootItem->parts=(*images)[imageNum].boards;
    setParentAndRow(rootItem);
    this->endResetModel();
}

void TestDebugWidget::on_openBoardBtn_clicked()
{
    qDebug()<<"showBoard"<<boardDefPtr<<*boardDefPtr;
    if((boardDefPtr==nullptr)||((*boardDefPtr)==nullptr))
        return;
    long long boardId=(*boardDefPtr)->id;
    emit showBoard(boardId);

}

void TestDebugWidget::on_saveBtn_clicked()
{
    for(int i=0; i<images.count(); i++){
        if(!images[i].dirty)
            continue;
        std::ofstream out((images[i].resultsName).toStdString(), std::ofstream::out|std::ofstream::binary);
        if(out.is_open()){
            qDebug()<<"saving"<<images[i].resultsName;
            serialize(images[i].boards, out);
            dlib::serialize(images[i].decision, out);
            out.close();
            images[i].dirty=false;
        }
    }
}


void ImageResults::calculateDecision()
{
    decision=BoardDef::calculateDecision(this->boards);
}

void TestDebugWidget::on_detailTreeView_doubleClicked(const QModelIndex &index)
{
    if(!index.isValid())
        return;
    //auto idx2=index;
    //auto idx2=proxyModel->mapToSource(proxyModel->index(index.row(),0, index));
    auto idx2=ui->detailTreeView->currentIndex();
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid())
        return;
    if((images[idx.row()].boards.count()>0)&&(images[idx.row()].boards[0].parts.count()>idx2.row())){
        //auto comp=images[idx.row()].boards[0].parts[idx2.row()];
        //auto compPtr=static_cast<DetectedComponent *>(index.internalPointer());
        auto compPtr=static_cast<DetectedComponent *>(index.data(Qt::UserRole).value<void *>());
        //qDebug()<<"doubleclick"<<index.internalPointer()<<index.data()<<index.data(Qt::UserRole)<<compPtr;
        if(compPtr==nullptr)
            return;
        auto comp= *compPtr;
        double x, y;
        qDebug()<<"comp"<<comp.x<<comp.y<<comp.w<<comp.h<<comp.orientation;
        switch(comp.orientation){
        case CompDefRect::E:
        case CompDefRect::W:
        case CompDefRect::EW:
            x=comp.x+comp.h/2;
            y=comp.y+comp.w/2;
            break;
        default:
            x=comp.x+comp.w/2;
            y=comp.y+comp.h/2;
            break;
        }
        emit centerAt(x, y);
    }
}

void TestDebugWidget::on_cropAndOpenBtn_clicked()
{
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid())
        return;
    QFile file(images[idx.row()].imageName);
    if(!file.exists())
        return;
    QFileInfo info(file);
    QString srcFile=info.completeBaseName();

    auto idx2=ui->detailTreeView->currentIndex();
    long long classifierId=-1;
    DetectedComponent * compPtr=nullptr;
    if(idx2.isValid()){
        compPtr=static_cast<DetectedComponent *>(idx2.data(Qt::UserRole).value<void *>());
        //qDebug()<<"doubleclick"<<index.internalPointer()<<index.data()<<index.data(Qt::UserRole)<<compPtr;
        if(compPtr!=nullptr){
            long long compId=compPtr->boardComp.id;
            if(compId>=0){
                //regular component
                if((boardDefPtr!=nullptr)&&((*boardDefPtr)!=nullptr)){
                    QString compTypeName;
                    auto lst=(*boardDefPtr)->classifierFromComponent(compId, compTypeName);
                    if(lst.count()>0){
                        int classifier=0;
                        if(lst.count()>1){
                            QString compName=idx2.data().toString();
                            ClassifierPicker picker(this);
                            picker.setValues(compName,compTypeName, lst);
                            picker.exec();
                            classifier=picker.result();
                            if(classifier<0)
                                return;
                        }
                        classifierId=lst[classifier].compDefPtr->id;
                        //qDebug()<<"classifiers"<<lst.count();
                        //qDebug()<<"images"<<lst[0]->images.count();
                        //QString fname=lst[classifier]->images[n-1].imagePath;
                        QString fname=lst[classifier].compDefPtr->getLastImagePath();
                        QFileInfo dstInfo(fname);
                        destination=dstInfo.absoluteDir().absolutePath();
                    }
                }
            } else {
                //board
                if((boardDefPtr!=nullptr)&&((*boardDefPtr)!=nullptr)){
                    classifierId=(*boardDefPtr)->refClassifier.id;
                    QString fname= (*boardDefPtr)->refClassifier.getLastImagePath();
                    QFileInfo dstInfo(fname);
                    destination=dstInfo.absoluteDir().absolutePath();
                }
            }
        }
    }
    if(classifierId == -1){
        QMessageBox::information(this, tr("copiere abandonată"), tr("Nu se poate identifica clasificatorul") );
        return;
    }
    //temporar
    auto comp= *compPtr;
    double w, h;
    switch(comp.orientation){
    case CompDefRect::E:
    case CompDefRect::W:
    case CompDefRect::EW:
        w=comp.h;
        h=comp.w;
        break;
    default:
        w=comp.w;
        h=comp.h;
        break;
    }

    QImage img( info.absoluteFilePath() );
    double x1, y1, x2, y2;
    //margins around the object ar twice the component width or height. For narrow/flat objects, 0.75 of the height/width will be used if this value is larger.
    x1=std::max(comp.x-std::max(2*w,0.75*h), 0.);
    y1=std::max(comp.y-std::max(2*h,0.75*w),0.);
    x2=std::min(comp.x+w+std::max(2*w,0.75*h), img.width()-1.0);
    y2=std::min(comp.y+h+std::max(2*h,0.75*w), img.height()-1.0);
    qDebug()<<"component "<<comp.x<<comp.y<<w<<h<<x1<<y1<<x2<<y2<<info.absoluteFilePath()<<img.width()<<img.height();
    if((x1>=x2)||(y1>=y2)){
        QMessageBox::information(this, tr("salvare nereușită"), tr("Nu se poate salva fișierul, deoarece componenta e in afara imaginii") );
        return;
    }
    auto img2=img.copy(x1, y1, x2-x1, y2-y1);
    qDebug()<<img2.width()<<img2.height()<<info.completeBaseName()<<srcFile;
//    QLabel lbl;
//    lbl.setPixmap(QPixmap::fromImage(img2));
//    lbl.show();
//    QEventLoop loop;
//    connect(&lbl, SIGNAL(destroyed()), & loop, SLOT(quit()));
//    loop.exec();

    //return;

    QString destFile=QFileDialog::getSaveFileName(this, tr("Salvare copie imagine. Apăsați CANCEL pentru a nu copia nimic"),
                                                  destination+"/"+srcFile+"_crop.png", tr("Imagini (*.png *.jpg *.jpeg);; Toate fișierele (*.*)"));
    if(!destFile.isEmpty()){
        QFileInfo dstInfo(destFile);
        qDebug()<<destFile<<dstInfo.absoluteDir().absolutePath();
        destination=dstInfo.absoluteDir().absolutePath();
        if(dstInfo.exists()){
            QMessageBox::information(this, tr("salvare abandonată"), tr("Nu se poate salva fișierul, deoarece există deja un fișier cu același nume. Va fi folosit fișierul existent!") );
        } else {
            bool ok=img2.save(destFile);
            if(!ok)
                QMessageBox::information(this, tr("copiere nereușită"), tr("Nu s-a putut salva fișierul") );
        }
        QClipboard *clipboard = QGuiApplication::clipboard();
        clipboard->setText(destFile);
    }
    emit imageForClassifier(classifierId, destFile);

}

void TestDebugWidget::on_clearBtn_clicked()
{
    emit clearRects();
}

void TestDebugWidget::on_copyAndOpenBtn_triggered(QAction *arg1)
{
    Q_UNUSED(arg1);
    copyAndOpenImage(true);
}


void TestDebugWidget::on_loadDirBtn_triggered(QAction *arg1)
{
    Q_UNUSED(arg1);
    //qDebug()<<"on_loadDirBtn_triggered"<<ui->dirSelEdit->text();
    QString dir=ui->dirSelEdit->text();
    if(dir.isEmpty())
        return;
    QFileInfo info(dir);
    //qDebug()<<dir<<info.baseName()<<info.fileName()<<info.isDir();
    if(!info.exists())
        return;
    if(!info.isDir())
        return;
    /// @todo clear test status when done
    emit loadBoard(info.fileName());
}

