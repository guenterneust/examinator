/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BARCODE_H
#define BARCODE_H
#include <QImage>
#include <QString>
#include <QDebug>
#ifdef ZBAR
#include <zbar.h>
#endif
#ifdef DMTX
#include <dmtx.h>
#endif
#ifdef ZXING
#include <ZXing/ReadBarcode.h>
#include <ZXing/ZXVersion.h>
#endif
#include <QStringList>


class Barcode
{
public:
    Barcode();
    enum BCType {ALLCODES=0, DATAMATRIX=1, QRCODE=2, CODE39=3, CODE128=4} ;
    static const QStringList bcTypes;
    static QString decodeBC(const QImage img, const QString validator, int &errorCode, BCType symbology=ALLCODES);
private:
    static QString decodeBC1(const QImage img, const QString validator, int &errorCode, BCType symbology=ALLCODES);
#ifdef ZBAR
    static QString decodeBCZbar(const uchar * dataPtr, const int w, const int h, const int l,
                                const BCType symbology, int & errorCode);
#endif
#ifdef DMTX
    /// maximum time in milliseconds for dmtxRegionFindNext. When this is not used, the program spends over 10 seconds here when there is nothing to be found
    static const int DMTXTIMEOUT = 500;
    static QString decodeBCDmtx(uchar *dataPtr, const int w, const int h, int & errorCode);
#endif
#ifdef ZXING
    static QString decodeBCZXing(uchar *dataPtr, const int w, const int h,
                                 const Barcode::BCType symbology, int & errorCode);
#endif
};

#endif // BARCODE_H
