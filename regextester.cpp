#include "regextester.h"
#include "ui_regextester.h"
#include <QDebug>

RegExTester::RegExTester(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegExTester)
{
    ui->setupUi(this);
}

RegExTester::~RegExTester()
{
    delete ui;
}

void RegExTester::setValidator(const QString &validator)
{
    ui->validatorEdit->setText(validator);
}

QString RegExTester::getValidator()
{
    return ui->validatorEdit->text();
}

void RegExTester::on_validatorEdit_textChanged(const QString &arg1)
{
    analyze(arg1, ui->exampleEdit->text());
}

void RegExTester::analyze(const QString &validator, const QString &example)
{
    QString val, extractor;
    val=validator.section("____",0,0);
    extractor=validator.section("____",1,-1);
    qDebug()<<val<<extractor;
    bool extractorOK=true;
    if(!extractor.isEmpty()){
        QRegExp r("(\\$\\d{1,2}){1,99}");
        extractorOK=r.exactMatch(extractor);
    }
    regExp.setPattern(val);
    if(!regExp.isValid()){
        QPalette palette=ui->validatorEdit->palette();
        palette.setColor(QPalette::Base,Qt::red);
        ui->validatorEdit->setPalette(palette);
        ui->plainTextEdit->clear();
        ui->plainTextEdit->appendHtml(regExp.errorString());
    } else {
        ui->validatorEdit->setPalette(QApplication::palette(ui->validatorEdit));
        ui->plainTextEdit->clear();
        bool ok=regExp.exactMatch(example);
        qDebug()<<regExp.matchedLength()<<regExp.capturedTexts();
        ui->plainTextEdit->appendHtml(tr("validator: ")+val);
        ui->plainTextEdit->appendHtml(tr("extractor: ")+extractor);
        if(!extractorOK)
            ui->plainTextEdit->appendHtml(tr("extractor invalid!!!"));
        ui->plainTextEdit->appendHtml(tr("potrivire: ")+(ok?tr("da"):tr("nu")));
        QStringList lst=regExp.capturedTexts();
        for(int i=0; i<lst.count(); i++){
            ui->plainTextEdit->appendHtml(tr("grup")+QString::number(i)+": "+lst[i]);
        }

    }
}


void RegExTester::on_exampleEdit_textChanged(const QString &arg1)
{
    analyze(ui->validatorEdit->text(), arg1);
}

