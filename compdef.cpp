/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "compdef.h"
#include <QFileInfo>
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QDebug>
#include <QStringList>
#include <QRgb>
#include <QMessageBox>
#include <cmath>
#include <QDir>
#include <QStandardPaths>

#include <QElapsedTimer>

#include <dlib/svm_threaded.h>
#include <dlib/string.h>
#include <dlib/gui_widgets.h>
#include "componentpathcorrection.h"

//#include <sstream>
unsigned CompDef::maxDNNArea=4e6;

CompDef::CompDef()
{
    dirty=false;
    aspectRatio=1;
    minArea=6400;
    id=-1;
    name=QDateTime::currentDateTime().toString();
    orientationMode=CompDefRect::N_E_S_V;
    upsample=0;
    horizontalFlip=false;
    verticalFlip=false;
    C=4;
    eps=0.001;
    numThreads=2;
    colorValidator[0][1]=0.8;
}

CompDefImage *CompDef::addImage(QString filename)
{
    QFileInfo info(filename);
    if(info.isFile()){
        images.append(CompDefImage (filename));
        CompDefImage & ref=images.last();
        return &ref;
    } else
        return nullptr;
}

/// @todo return status, and warn user if saving failed.
void CompDef::saveToDB()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    oldDNN=true;
    oldFHOG=true;
    makeNameUnique();
    unsigned savetime=QDateTime::currentSecsSinceEpoch();
    if(id>=0){
        //update already existing component definition
        sql="UPDATE compdefs SET name=:name, minarea="+
                QString::number(minArea)+", aspectratio="+
                QString::number(aspectRatio)+", orientationmode="+
                QString::number(orientationMode)+", upsample="+
                QString::number(upsample)+", horizontalflip="+
                QString::number(horizontalFlip)+", verticalflip="+
                QString::number(verticalFlip)+", c="+
                QString::number(C)+", eps="+
                QString::number(eps)+", numthreads="+
                QString::number(numThreads)+", trainedfhog="+
                QString::number(trainedFHOG)+", oldfhog="+
                QString::number(oldFHOG)+", traineddnn="+
                QString::number(trainedDNN)+", olddnn="+
                QString::number(oldDNN)+", fhogthreshold="+
                QString::number(fhogThreshold)+", fhognorient="+
                QString::number(fhogNOrient)+", trainingiouthresh="+
                QString::number(trainingIOUThresh)+", includedataset="+
                QString::number(includeDataset)+", savetime="+
                QString::number(savetime)+
                " WHERE id="+
                QString::number(id);

    } else {
        //save new component
        sql="INSERT INTO compdefs (name, minarea, aspectratio, orientationmode, "
            "upsample, horizontalflip, verticalflip, c, eps, numthreads, "
            "trainedfhog, oldfhog, traineddnn, olddnn, fhogthreshold, fhognorient, trainingiouthresh, includedataset)"
            "VALUES (:name, "+QString::number(minArea)+", "+
                QString::number(aspectRatio)+", "+
                QString::number(orientationMode)+", "+
                QString::number(upsample)+", "+
                QString::number(horizontalFlip)+", "+
                QString::number(verticalFlip)+", "+
                QString::number(C)+", "+
                QString::number(eps)+", "+
                QString::number(numThreads)+", "+
                QString::number(trainedFHOG)+", "+
                QString::number(oldFHOG)+", "+
                QString::number(trainedDNN)+", "+
                QString::number(oldDNN)+", "+
                QString::number(fhogThreshold)+", "+
                QString::number(fhogNOrient)+", "+
                QString::number(trainingIOUThresh)+", "+
                QString::number(includeDataset)+
                ")";
    }
    query.prepare(sql);
    query.bindValue(":name", name);
    if(!query.exec()){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        defaultdb.close();
        return;
    }
    lastsave=savetime;
    qDebug()<<query.lastQuery();
    {
        std::ostringstream buf, buf2, buf3;
        for(unsigned int i=0; i<colorValidator.size(); i++)
            dlib::serialize(colorValidator[i], buf);
        serialize(compSpParams, buf2);
        serialize(compDNNParams, buf3);
        QByteArray spParams=QByteArray(buf2.str().c_str(),buf2.str().length());
        qDebug()<<"buf2"<<spParams.toHex();
        QByteArray dnnParams=QByteArray(buf3.str().c_str(),buf3.str().length());
        qDebug()<<"buf3"<<dnnParams.toHex();
        QString sql="UPDATE compdefs SET colorvalidator= :validator, spparams= :spparams, dnnparams= :dnnparams "
                    "WHERE id=:id";
        query.prepare(sql);
        query.bindValue(":validator", QByteArray(buf.str().c_str(),buf.str().length()));
        query.bindValue(":spparams", spParams);
        query.bindValue(":dnnparams", dnnParams);
        query.bindValue(":id", id);
        if(!query.exec()){
            qDebug()<<"error while saving color validator and training params:"<<query.lastError()<<query.lastQuery();
            defaultdb.close();
            defaultdb.open();
        }
    }

    if(id<0){
        id=query.lastInsertId().toLongLong();
    } else {
        //delete deleted images from database
        QString idList;
        for(int i=0; i<images.count(); i++) {
            if(images[i].id<0)
                continue;
            if(!idList.isEmpty())
                idList.append(",");
            idList.append(QString::number(images[i].id));
        }
        sql="DELETE FROM compdefrects WHERE compdefimage IN (SELECT id FROM compdefimages WHERE "
            "compdef="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        sql+=")";
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
        sql="DELETE FROM compdefimages WHERE compdef="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
            defaultdb.close();
            defaultdb.open();
        }

    }
    for(int i=0; i<images.count(); i++) {
        images[i].saveToDB(id);
    }
    dirty=false;
}

QString CompDef::makeNameUnique()
{

    QSqlQuery query;
    QString sql;
    QString newname;
    for(int suffix=1; suffix<1000; suffix++){
        if(suffix==1)
            newname=name;
        else
            newname=name+"_"+QString::number(suffix);
        sql="SELECT count(*) as items FROM compdefs WHERE name='"+newname+"' AND id<>"+QString::number(id);
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
            //error, just return the old name, it's unknown if it's good or not
            return name;
        }
        if(query.next()){
            if(query.value(0/*"items"*/).toInt()<=0){
                //this name has not been used before
                break;
            }
        }
    }
    if(name!=newname){
        this->dirty=true;
        name=newname;
    }
    return newname;
}

bool CompDef::removeFromDB()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    sql="DELETE FROM compdefrects WHERE compdefimage IN (SELECT id FROM compdefimages WHERE "
        "compdef="+QString::number(id)+")";
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
        return false;
    }
    sql="DELETE FROM compdefimages WHERE compdef="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
        return false;
    }
    sql="DELETE FROM compdefs WHERE id="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
        return false;
    }
    return true;
}

void CompDef::loadFromDB(const long long id, bool checkImageFiles, bool loadImageMetadata, bool loadClassifiers)
{
    ///@todo use loadClassifiers
    qDebug()<<"loadFromDB"<<id<<checkImageFiles<<loadImageMetadata<<loadClassifiers;
    try {
        QSqlDatabase defaultdb=QSqlDatabase::database();
        if(!defaultdb.isOpen()){
            defaultdb.open();
        }
        if(!defaultdb.isOpen()){
            qDebug()<<"unable to open database"<<defaultdb.databaseName();
            return;
        }
        QSqlQuery query;
        QString sql;
        images.clear();
        QString classifiers;
        if(loadClassifiers)
            classifiers="dnnclassifier, fhogclassifier, shapepredictor, ";
        sql="SELECT name, aspectratio, orientationmode, minarea, upsample, horizontalflip, " + classifiers +
            "verticalflip, c, eps, numthreads, trainedfhog, oldfhog, traineddnn, olddnn, fhogthreshold, "+
            "colorvalidator, spparams, dnnparams, fhognorient, includedataset, savetime "
            "FROM compdefs WHERE id="+QString::number(id);
        if(!query.exec(sql)){
            qDebug()<<query.lastError();
            defaultdb.close();
            return;
        }
        if(query.next()){
            this->id=id;
            name=query.value(0/*"name"*/).toString();
            aspectRatio=query.value(1/*"aspectratio"*/).toDouble();
            orientationMode=static_cast<CompDefRect::OrientationMode>( query.value(2/*"orientationmode"*/).toInt());
            if(orientationMode==CompDefRect::ANY){
                //might be illegal - correct if necessary
                if((aspectRatio>1.01)||(aspectRatio<0.99))
                    orientationMode=CompDefRect::H_V;
            }
            minArea=query.value(3/*"minarea"*/).toInt();
            upsample=query.value(4/*"upsample"*/).toInt();
            horizontalFlip=query.value(5/*"horizontalflip"*/).toInt();
            verticalFlip=query.value(6/*"verticalflip"*/).toInt();
            C=query.value(7/*"c"*/).toDouble();
            eps=query.value(8/*"eps"*/).toDouble();
            numThreads=query.value(9/*"numthreads"*/).toInt();
            trainedFHOG=query.value("trainedfhog").toInt();
            oldFHOG=query.value("oldfhog").toInt();
            trainedDNN=query.value("traineddnn").toInt();
            oldDNN=query.value("olddnn").toInt();
            fhogThreshold=query.value("fhogthreshold").toDouble();
            fhogNOrient=query.value("fhognorient").toInt();
            includeDataset=query.value("includedataset").toLongLong();
            lastsave=query.value("savetime").toUInt();
            //qDebug()<<query.record();
            QByteArray tmp=query.value("colorvalidator").toByteArray();
            if(tmp.count()>0){
                try {
                    std::stringstream buf(std::string(tmp.constData(), tmp.length()),std::ios_base::in | std::ios_base::binary);
                    for(unsigned int i=0; i<colorValidator.size(); i++){
                        dlib::deserialize(colorValidator[i],buf);
                    }
                } catch(...){
                    qDebug()<<"deserialize for color validator failed";
                }
            }
            tmp=query.value("spparams").toByteArray();
            if(tmp.count()>0){
                //qDebug()<<"buf"<<tmp.toHex();
                try {
                    std::stringstream buf(std::string(tmp.constData(), tmp.length()),std::ios_base::in | std::ios_base::binary);
                    deserialize(compSpParams, buf);
                } catch(...){
                    qDebug()<<"deserialize for shape predictor params failed";
                    compSpParams=CompSpParams();
                }
            }
            tmp=query.value("dnnparams").toByteArray();
            if(tmp.count()>0){
                qDebug()<<"dnn params buf"<<tmp.toHex();
                try {
                    std::stringstream buf(std::string(tmp.constData(), tmp.length()),std::ios_base::in | std::ios_base::binary);
                    deserialize(compDNNParams, buf);
                } catch(...){
                    qDebug()<<"deserialize for dnn predictor params failed";
                    compDNNParams=CompDNNParams();
                }
            }
            if(loadClassifiers){
                prepareToRun(query);
            }
        } else {
            qDebug()<<"unable to find the provided compdef id"<<id<<"in the database";
            return;
        }
        if(loadImageMetadata){
            sql="SELECT * FROM compdefimages WHERE compdef="+QString::number(id);
            if(!query.exec(sql)){
                qDebug()<<query.lastQuery();
                qDebug()<<query.lastError();
                return;
            }
            while(query.next()){
                QString imagePath=query.value("imagepath").toString();
                QString newPath;
                QSqlRecord record=query.record();
                if(checkImageFiles&&(!QFile::exists(imagePath))){
                    ComponentPathCorrection dlg(query.value("id").toLongLong());
                    if(!dlg.exec()){
                        /// @todo invalid image kept -> block editing
                    } else {
                        record.setValue("imagepath",dlg.getNewPath());
                        newPath=dlg.getNewPath();
                        qDebug()<<"newPath"<<newPath;
                    }

                }
                images.append(CompDefImage(record,newPath));
            }
            loadIncludedImageMetadata(checkImageFiles);
        }
    } catch(...){
        qDebug()<<"exception thrown while loading component definition";
        QSqlDatabase defaultdb=QSqlDatabase::database();
        if(!defaultdb.isOpen()){
            defaultdb.close();
        }
        defaultdb.open();
    }
}

bool CompDef::setIncludeDataset(long long id, bool loadImageMetadata)
{
    if(id==includeDataset)
        return true;
    includeDataset=id;
    includedImages.clear();
    if(loadImageMetadata)
        return loadIncludedImageMetadata(false);
    return true;
}

void CompDef::trainFHOG(QString databaseName)
{
    QElapsedTimer timer;
    timer.start();
    fhogDetector.clear();
    /// for each image in imageArray, one vector of ignores and objects is stored
    std::vector<std::vector<dlib::rectangle> > ignores, objects;
    std::vector<std::vector<float> > angles;
    //dlib::array<dlib::array2d<unsigned char> > imageArray;
    dlib::array<dlib::array2d<dlib::rgb_pixel> > imageArray;
    ignores=createDlibDataset(imageArray, objects, angles, true);
    qDebug()<<"created dataset after"<<timer.elapsed();
    /*
    typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<scannerPyramid> > image_scanner_type;
    for (int i = 0; i < upsample; ++i){
        //dlib::upsample_image_dataset<dlib::pyramid_down<2> >(imageArray, objects, ignores);
        conditionalUpsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea);
    }
    conditionalDownsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea*4);
    */
    for(int orient=0; orient<(1<<fhogNOrient);orient++){
        image_scanner_type scanner;
        unsigned long width, height;
        if(orient>0){
            if(fhogNOrient==2)
                dlib::rotate_image_dataset(-M_PI_2,imageArray, objects, ignores);
            else
                dlib::rotate_image_dataset(M_PI,imageArray, objects, ignores);
        }
        if((fhogNOrient<2)||(orient==0)||(orient==2)){
            width=minRect().width();
            height=minRect().height();
        } else {
            height=minRect().width();
            width=minRect().height();
        }
        scanner.set_detection_window_size(width, height);
        //qDebug()<<"nuclear"<<scanner.get_nuclear_norm_regularization_strength();
        //scanner.set_nuclear_norm_regularization_strength(0.1);
        dlib::structural_object_detection_trainer<image_scanner_type> trainer(scanner);
        trainer.set_num_threads(numThreads);
        trainer.be_verbose();
        trainer.set_c(C);
        trainer.set_epsilon(eps);
        if(trainingIOUThresh>=0)
            trainer.set_overlap_tester(dlib::test_box_overlap(0.01*trainingIOUThresh,1.0));
        // Now make sure all the boxes are obtainable by the scanner.
        qDebug()<<"dataset"<<objects.size();
        for(uint i=0; i<objects.size(); i++){
            auto tmp=objects[i];
            for(uint j=0; j<tmp.size(); j++){
                qDebug()<<i<<tmp[j].left()<<tmp[j].right()<<tmp[j].top()<<tmp[j].bottom();
            }
        }
        //        std::cout<<"press any key\n";
        //        std::getchar();
        std::vector<std::vector<dlib::rectangle> > removed;
        removed = dlib::remove_unobtainable_rectangles(trainer, imageArray, objects);
        uint removedrects=0, remaining=0;
        for(uint i=0; i<removed.size(); i++){
            auto tmp=removed[i];
            removedrects+=tmp.size();
        }
        for(uint i=0; i<objects.size(); i++){
            auto tmp=objects[i];
            remaining+=tmp.size();
        }
        qDebug()<<"removed"<<removedrects<<"rectangles as unobtainable";
        qDebug()<<"remaining dataset"<<remaining;


        {
            // Do the actual training and save the results into the detector object.
            qDebug()<<"starting training after"<<timer.elapsed();
            //dlib::object_detector<image_scanner_type> detector
            fhogDetector.push_back(trainer.train(imageArray, objects, ignores));
            qDebug()<<"finished training after"<<timer.elapsed()
                   << "num filters:"<< dlib::num_separable_filters(fhogDetector[orient])
                    << "iou_thresh" << fhogDetector[orient].get_overlap_tester().get_iou_thresh()
                    << "covered_thresh" << fhogDetector[orient].get_overlap_tester().get_percent_covered_thresh();

            //std::cout << "Saving trained detector to object_detector.svm" << std::endl;
            //dlib::serialize("object_detector.svm") << fhogDetector;

            std::cout << "Testing detector on training data..." << std::endl;
            std::cout << "Test detector (precision,recall,AP): " <<
                         dlib::test_object_detection_function(fhogDetector[orient], imageArray, objects, ignores) << std::endl;
            qDebug()<<"tested classifier after"<<timer.elapsed();
            std::cout << "Parameters used: " << std::endl;
            std::cout << "  threads:                 "<< numThreads << std::endl;
            std::cout << "  C:                       "<< C << std::endl;
            std::cout << "  eps:                     "<< eps << std::endl;
            std::cout << "  target-size:             "<< minArea << std::endl;
            std::cout << "  detection window width:  "<< scanner.get_detection_window_width() << std::endl;
            std::cout << "  detection window height: "<< scanner.get_detection_window_height() << std::endl;
            std::cout << "  upsample this many times : "<< upsample << std::endl;
            std::cout << std::endl;
        }
    }
    //clear unused object detectors
    //for(int i=1<<fhogNOrient; i<4; i++){
    //    fhogDetector[i]=dlib::object_detector<image_scanner_type>();
    //}
    qDebug()<<"numDetectors"<<numFHOGDetectors();
    bool ok=false;
    if(!(ok=saveFHOGDetector(databaseName))){
        //try again
        if(!(ok=saveFHOGDetector(databaseName))){
            //try again
            if(!(ok=saveFHOGDetector(databaseName))){
                //try again
                ok=saveFHOGDetector(databaseName);
            }
        }
    }
    qDebug()<<(ok?"saved":"failed to save")<<"classifier after"<<timer.elapsed()<<"ok"<<ok;
}

void CompDef::trainShapePredictor(QString databaseName)
{
    shapePredictor.clear();
    QElapsedTimer timer;
    timer.start();
    /// for each image in imageArray, one vector of ignores and objects is stored
    std::vector<std::vector<dlib::rectangle> > ignores, objects;
    std::vector<std::vector<float> > angles;
    //dlib::array<dlib::array2d<unsigned char> > imageArray;
    dlib::array<dlib::array2d<dlib::rgb_pixel> > imageArray;
    ignores=createDlibDataset(imageArray, objects, angles, true);
    qDebug()<<"shapepredictor: created dataset after"<<timer.elapsed();
    //typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<scannerPyramid> > image_scanner_type;
    /*
    for (int i = 0; i < upsample; ++i){
        //dlib::upsample_image_dataset<dlib::pyramid_down<2> >(imageArray, objects, ignores);
        conditionalUpsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea);
    }
    conditionalDownsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea*4);
    */
    /// @todo remove images without positive samples

    std::vector<std::vector<dlib::full_object_detection> > fullObjects;
    for(uint i=0; i<objects.size(); i++){
        //add empty vector for current image
        fullObjects.push_back(std::vector<dlib::full_object_detection>());
        QList<DetectedComponent> dets;
        {
            //run detector without shape predictor, backup temporary old value
            bool spEnabled=compSpParams.enabled;
            compSpParams.enabled=false;
            dets=detectObjects(imageArray[i],false);
            compSpParams.enabled=spEnabled;
        }
        qDebug()<<"shapepredictor: dets"<<oldFHOG<<dets.count();
        for(uint j=0; j<objects[i].size(); j++){
            std::vector<dlib::point> points;
            /// @todo rotate the rectangle by angle
            float t=0.5*tan(angles[i][j]);
            float dx=objects[i][j].width()*t;
            float dy=objects[i][j].height()*t;
            points.push_back(dlib::point(objects[i][j].left(), 0.5+(objects[i][j].top()+objects[i][j].bottom())/2+dx));//middle of left edge, 0.5 added for rounding
            points.push_back(dlib::point(objects[i][j].right(),0.5+(objects[i][j].top()+objects[i][j].bottom())/2-dx));//middle of right edge, 0.5 added for rounding
            points.push_back(dlib::point(0.5+(objects[i][j].left()+objects[i][j].right())/2 - dy,objects[i][j].top()));//middle of top edge, 0.5 added for rounding
            points.push_back(dlib::point(0.5+(objects[i][j].left()+objects[i][j].right())/2 + dy,objects[i][j].bottom()));//middle of botom edge, 0.5 added for rounding
            points.push_back(dlib::point((0.5+objects[i][j].left()+objects[i][j].right())/2,0.5+(objects[i][j].top()+objects[i][j].bottom())/2));// 0.5 added for rounding
            CompDefRect cRect(objects[i][j].left(),objects[i][j].top(),objects[i][j].width(), objects[i][j].height(), CompDefRect::N, CompDefRect::POSITIVE);
            bool detected=false;
            //check which of the detected rectangles corresponds to the current ground truth
            //if a match is found, use it to train the shape predictor
            foreach(auto det , dets){
                if(det.checkOverlap(cRect,true)>0.5){
                    //the training is done by using the ground truth as points to be learned, and the detected rectangle as rectangle to which these points belong
                    fullObjects[i].push_back(dlib::full_object_detection(dlib::rectangle(det.x, det.y, det.x2(), det.y2()),points));
                    qDebug()<<"object"<<objects[i][j].left()<<objects[i][j].top()<<objects[i][j].right()<<objects[i][j].bottom()<<objects[i][j].width()<<objects[i][j].height();
                    qDebug()<<"detected"<<det.x<< det.y<< det.x2()<< det.y2()<<"t"<<angles[i][j]<<t<<"points"<<points[0](0)<<points[0](1)<<points[2](0)<<points[2](1)<<points[1](0)<<points[1](1)<<points[3](0)<<points[3](1);
                    detected=true;
                    break;
                }
            }
            //if no match is found, use he ground truth to train the shape predictor
            if(!detected)
                fullObjects[i].push_back(dlib::full_object_detection(objects[i][j],points));
        }
    }
    //we are training up to 4 shape predictors, one for each possible orientation of the component
    //all are trained from the dataset already created, by rotating he dataset into the right orientation
    int nOrient=(1<<fhogNOrient);
    if(compDNNParams.enabled)
        nOrient=1;
    for(int orient=0; orient<nOrient;orient++){
        if(orient>0){
            //rotate the dataset if needed.
            //unfortunately, the rotate_dataset function leaves the order of the parts unchanged, while we need
            //the parts in the order left, right, top, bottom, center
            if(fhogNOrient==2){
                rotate_image_dataset(-M_PI_2,imageArray, fullObjects);
                for(unsigned i=0; i<fullObjects.size(); i++){
                    for(unsigned j=0; j<fullObjects[i].size(); j++){
                        std::vector<dlib::point> points;
                        points.push_back(fullObjects[i][j].part(3));
                        points.push_back(fullObjects[i][j].part(2));
                        points.push_back(fullObjects[i][j].part(0));
                        points.push_back(fullObjects[i][j].part(1));
                        points.push_back(fullObjects[i][j].part(4));
                        dlib::full_object_detection tmp(fullObjects[i][j].get_rect(),points);
                        std::swap(fullObjects[i][j],tmp);
                    }
                }
            } else {
                rotate_image_dataset(M_PI,imageArray, fullObjects);
                for(unsigned i=0; i<fullObjects.size(); i++){
                    for(unsigned j=0; j<fullObjects[i].size(); j++){
                        std::vector<dlib::point> points;
                        points.push_back(fullObjects[i][j].part(1));
                        points.push_back(fullObjects[i][j].part(0));
                        points.push_back(fullObjects[i][j].part(3));
                        points.push_back(fullObjects[i][j].part(2));
                        points.push_back(fullObjects[i][j].part(4));
                        dlib::full_object_detection tmp(fullObjects[i][j].get_rect(),points);
                        std::swap(fullObjects[i][j],tmp);
                    }
                }
            }
        }
        if(0){
            dlib::image_window win;
            qDebug()<<"press any key to show next image";
            std::cin.get();
            for (unsigned i=0; i<std::min(imageArray.size(), (size_t)3); i++)
            {
                auto& img=imageArray[i];
                //auto dets = net(img);
                win.clear_overlay();
                win.set_image(img);
                for (auto&& d : fullObjects[i]){
                    qDebug()<<"rect"<<d.get_rect().left()<<d.get_rect().right()<<d.get_rect().top()<<d.get_rect().bottom();
                    win.add_overlay(d);
                }
                std::cout<<"press any key to show next image\r";
                std::cin.get();
            }
        }

        dlib::shape_predictor_trainer trainer;
        trainer.set_cascade_depth(compSpParams.cascadeDepth);
        trainer.set_feature_pool_size(compSpParams.featurePoolSize);
        trainer.set_nu(compSpParams.nu);
        trainer.set_num_test_splits(compSpParams.numTestSplits);
        trainer.set_oversampling_amount(compSpParams.oversamplingAmount);
        trainer.set_oversampling_translation_jitter(compSpParams.oversamplingTranslationJitter);
        trainer.set_tree_depth(compSpParams.treeDepth);

        trainer.set_num_threads(numThreads);
        trainer.be_verbose();

        // Now finally generate the shape model
        shapePredictor.push_back(trainer.train(imageArray, fullObjects));
        qDebug()<<orient<<"finished training after"<<timer.elapsed();
        testShapePredictor(imageArray, fullObjects, orient);
    }
    bool ok=true;
    if(!saveShapePredictor(databaseName)){
        //try again
        if(!saveShapePredictor(databaseName)){
            //try again
            if(!saveShapePredictor(databaseName)){
                //try again
                ok=saveShapePredictor(databaseName);
            }
        }
    }
    qDebug()<<(ok?"saved":"failed to save")<<"shape predictor after"<<timer.elapsed();


}

void CompDef::trainDNN(QString databaseName)
{
    try{
        QElapsedTimer timer;
        timer.start();
        /// for each image in imageArray, one vector of ignores and objects is stored
        std::vector<std::vector<dlib::rectangle> > ignores, objects;
        std::vector<std::vector<float> > angles;
        //dlib::array<dlib::array2d<unsigned char> > imageArray;
        //the dnn trainer uses vector of matrix as image type, while the fhog trainer uses array of array2d
        std::vector<dlib::matrix<dlib::rgb_pixel> > imageArray;
        {
            dlib::array<dlib::array2d<dlib::rgb_pixel> > imageArray1;
            qDebug()<<"loading dataset";
            ignores=createDlibDataset(imageArray1, objects, angles, true);
            qDebug()<<"converting images to mat";
            for(unsigned i=0; i<imageArray1.size(); i++){
                //dlib::matrix<dlib::rgb_pixel> m(imageArray1[i]);
                imageArray.push_back(dlib::mat(imageArray1[i]));
                imageArray1[i].clear();
            }
        }
        qDebug()<<"dnn: created dataset after"<<timer.elapsed();
        /*
        for (int i = 0; i < upsample; ++i){
            //dlib::upsample_image_dataset<dlib::pyramid_down<2> >(imageArray, objects, ignores);
            conditionalUpsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea);
        }
        conditionalDownsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea*4);
        */
        //the dnn trainer requires mmod_rect boxes, while the fhog trainer uses two separate vectors of rectangles
        //create mmod_rect boxes from these rectangles
        std::vector<std::vector<dlib::mmod_rect>> boxes;
        for(unsigned i=0; i<imageArray.size(); i++){
            boxes.push_back(std::vector<dlib::mmod_rect>());
            for(unsigned j=0; j<objects[i].size(); j++){
                dlib::mmod_rect r(objects[i][j]);
                r.ignore=false;
                r.label=name.toStdString();
                boxes[i].push_back(r);
            }
            for(unsigned j=0; j<ignores[i].size(); j++){
                dlib::mmod_rect r(ignores[i][j]);
                r.ignore=true;
                r.label=name.toStdString();
                boxes[i].push_back(r);
            }
        }
        unsigned numIgnored=0;
        const dlib::test_box_overlap& overlapTester=dlib::test_box_overlap(0.5,1);
        //remove boxes that overlap or are small
        int remaining=0;
        for (auto& v : boxes)
        {
            //numOverlappedIgnored += dlib::ignore_overlapped_boxes(v, test_box_overlap(0.5, 1));
            for(unsigned i=0; i<v.size(); i++){
                auto& bb = v[i];
                if(bb.ignore)
                    continue;
                if (bb.rect.area() < static_cast<unsigned>(minArea))
                {
                    bb.ignore = true;
                    qDebug()<<"ignoring small rectangle, area"<<bb.rect.area();
                    ++numIgnored;
                    continue;
                }
                for(unsigned j=i+1; j<v.size(); j++){
                    auto& bb2 = v[j];
                    if(bb2.ignore)
                        continue;
                    if(bb==bb2){
                        ++numIgnored;
                        bb2.ignore=true;
                        continue;
                    }
                    if(overlapTester(bb, bb2)){
                        ++numIgnored;
                        if(bb.rect.area()<bb2.rect.area()){
                            bb.ignore=true;
                        } else {
                            bb2.ignore=true;
                        }
                    }
                }
                if(!bb.ignore){
                    remaining++;
                }
            }
        }
        if(0){
            dlib::image_window win;
            qDebug()<<"press any key to show next image";
            std::cin.get();
            for (unsigned i=0; i<imageArray.size(); i++)
            {
                auto& img=imageArray[i];
                //auto dets = net(img);
                win.clear_overlay();
                win.set_image(img);
                for (auto&& d : boxes[i]){
                    qDebug()<<"confidence"<<d.detection_confidence;
                    win.add_overlay(d);
                }
                std::cout<<"press any key to show next image\r";
                std::cin.get();
            }
        }
        qDebug()<<"boxes ignored:"<<numIgnored<<" remaining:"<<remaining;
        if(remaining<=0)
            return;
        dlib::set_dnn_prefer_smallest_algorithms();
        dlib::mmod_options options(boxes,std::max(minRect().width(),minRect().height()),std::min(minRect().width(),minRect().height()));
        int miniBatchSize=compDNNParams.batchSize1;
        options.overlaps_ignore=overlapTester;
        options.use_bounding_box_regression=compDNNParams.bbRegression;
        options.loss_per_false_alarm=1.0/(1+compDNNParams.bias);
        options.overlaps_nms=overlapTester;
        net_type1 net(options);
        // The MMOD loss requires that the number of filters in the final network layer equal
        // options.detector_windows.size().  So we set that here as well.
        int multiplier=1;
        if(options.use_bounding_box_regression)
            multiplier=5;
        net.subnet().layer_details().set_num_filters(options.detector_windows.size()*multiplier);
        QString syncFile=QStandardPaths::writableLocation(QStandardPaths::TempLocation)+"/dnnSyncFile.dat";
        if(compDNNParams.trainingMode!=3){
            //continue last training => don't delete sync file
            if(QFile::exists(syncFile))
                QFile::remove(syncFile);
            if(QFile::exists(syncFile+"_"))
                QFile::remove(syncFile+"_");
        }
        if(compDNNParams.trainingMode==0){
            qDebug()<<"num_filters"<<dlib::layer<1>(net).layer_details().num_filters();

            if(compDNNParams.nFilter<=24){
                dlib::layer<23>(net).layer_details().set_num_filters(16);
                dlib::layer<26>(net).layer_details().set_num_filters(8);
            } else {
                if(compDNNParams.nFilter<40){
                    dlib::layer<23>(net).layer_details().set_num_filters(24);
                    dlib::layer<26>(net).layer_details().set_num_filters(12);
                } else {
                    dlib::layer<23>(net).layer_details().set_num_filters(32);
                    dlib::layer<26>(net).layer_details().set_num_filters(16);
                }
            }
            dlib::layer<5>(net).layer_details().set_num_filters(compDNNParams.nFilter);
            dlib::layer<8>(net).layer_details().set_num_filters(compDNNParams.nFilter);
            dlib::layer<13>(net).layer_details().set_num_filters(compDNNParams.nFilter);
            dlib::layer<16>(net).layer_details().set_num_filters(compDNNParams.nFilter);
            dlib::layer<20>(net).layer_details().set_num_filters(compDNNParams.nFilter);

        }
        if(compDNNParams.trainingMode==1){
            //retrain
            qDebug()<<"load net"<<id;
            if(!loadDNN(id,net, databaseName)){
                //a failed deserialization damages the network
                //loadDNN does not touch the network if unsuccessful
            }
        }
        if(compDNNParams.trainingMode==2){
            //initialize with weights from other neural net
            qDebug()<<"load net for"<<compDNNParams.transferClassifier;
            net_type1 othernet;
            if(compDNNParams.transferClassifier>0){
                if(loadDNN(compDNNParams.transferClassifier,othernet, databaseName))
                    net.subnet().subnet()=othernet.subnet().subnet();
                else {
                    //do nothing, net is already initialized properly
                }
            }
        }
        dlib::dnn_trainer<net_type1> trainer(net);
        trainer.set_learning_rate(compDNNParams.maxLearningRate);
        trainer.set_min_learning_rate(compDNNParams.minLearningRate);
        trainer.be_verbose();
        trainer.set_synchronization_file(syncFile.toStdString(), std::chrono::minutes(5));
        trainer.set_iterations_without_progress_threshold(compDNNParams.stepsWihoutProgress);
        trainer.set_learning_rate_shrink_factor(compDNNParams.lrShrinkFactor);
        trainer.set_mini_batch_size(200);
        std::vector<dlib::matrix<dlib::rgb_pixel>> miniBatchSamples;
        std::vector<std::vector<dlib::mmod_rect>> miniBatchLabels;
        dlib::random_cropper cropper;
        cropper.set_max_rotation_degrees(compDNNParams.maxRotation);
        // Usually you want to give the cropper whatever min sizes you passed to the
        // mmod_options constructor, which is what we do here.
        unsigned minSize=std::max(minRect().width(),minRect().height());
        cropper.set_min_object_size(minSize-1,std::min(minRect().width(),minRect().height())-1);
        minSize+=70+minSize/10; //add 70 plus 10% for minumum chip size
        cropper.set_chip_dims(std::max(compDNNParams.chipWidth, minSize), std::max(compDNNParams.chipHeight, minSize));
        cropper.set_background_crops_fraction(0.5/(1+compDNNParams.bias));
        cropper.set_randomly_flip(false);
        std::cout<<net;
        std::cout<<trainer;
        std::cout<<cropper;
        qDebug()<<QString::fromStdString(trainer.get_synchronization_file());
        dlib::rand rnd;
        // Run the trainer until the learning rate gets small.  This will probably take several
        // hours.
        while(trainer.get_learning_rate() >= compDNNParams.minLearningRate)
        {
            unsigned step=trainer.get_train_one_step_calls();
            if(step==2){
                qDebug()<<QString::fromStdString(trainer.get_synchronization_file());

            }
            if(step<compDNNParams.bsSamples1){
                miniBatchSize=compDNNParams.batchSize1;
            } else if(step<compDNNParams.bsSamples2){
                miniBatchSize=compDNNParams.batchSize2;
            } else {
                miniBatchSize=compDNNParams.batchSize3;
            }
            cropper(miniBatchSize, imageArray, boxes, miniBatchSamples, miniBatchLabels);

            // We can also randomly jitter the colors and that often helps a detector
            // generalize better to new images.
            if(compDNNParams.disturbColors){
                for (auto&& img : miniBatchSamples)
                    dlib::disturb_colors(img, rnd);
            }
            if(0)
            {
                dlib::image_window win;
                qDebug()<<"press any key to show next image";
                std::cin.get();
                for (unsigned i=0; i<miniBatchSamples.size(); i++)
                {
                    auto& img=miniBatchSamples[i];
                    auto dets = miniBatchLabels[i];
                    win.clear_overlay();
                    win.set_image(img);
                    for (auto&& d : dets){
                        qDebug()<<"confidence"<<d.detection_confidence;
                        win.add_overlay(d);
                    }
                    std::cout<<"press any key to show next image\r";
                    std::cin.get();
                }
            }
            trainer.train_one_step(miniBatchSamples, miniBatchLabels);


        }
        trainer.get_net();
        qDebug()<<"dnn: finished training after"<<timer.elapsed()<<"using"<<imageArray.size()<<"images";
        trainedDNN=true;
        net.clean();
        bool ok=true;
        if(!saveDNN(databaseName, net)){
            //try again
            if(!saveDNN(databaseName, net)){
                //try again
                ok=saveDNN(databaseName, net);
            }
        }
        dnnDetector=net;//activate the trained network in the component definition
        dnnLoaded=true;
        auto tmp=net;
        //using net here causes the size of the detector to increse ~1000 fold, so we ues dnnDetector instead
        std::cout<<dlib::test_object_detection_function(tmp,imageArray, boxes);
        if(!ok)
            ok=saveDNN(databaseName, net);
        qDebug()<<(ok?"saved":"failed saving")<<"dnn after"<<timer.elapsed()<<"using"<<imageArray.size()<<"images";
        if(0){
            dlib::image_window win;
            qDebug()<<"press any key to show next image";
            std::cin.get();
            for (unsigned i=0; i<imageArray.size(); i++)
            {
                auto& img=imageArray[i];
                //std::vector<std::pair<double, dlib::rectangle> > dets;
                //net(img,dets,0.0);
                auto & dets=net.process(img,-0.5);
                win.clear_overlay();
                win.set_image(img);
                for (auto&& d : dets){
                    qDebug()<<"confidence"<<d.detection_confidence;
                    win.add_overlay(d);
                }
                std::cout<<"press any key to show next image\r";
                std::cin.get();
            }
        }

    } catch(std::exception& e){
        qDebug()<<"training failed"<<e.what();
        trainedDNN=false;
    } catch(...){
        qDebug()<<"training failed, default exception";
        trainedDNN=false;
    }

}

bool CompDef::importDNN(QString fileName)
{
    net_type1 net;
    try {
        dlib::deserialize(fileName.toStdString())>>net;
        dnnDetector=net;
        qDebug()<<"dnn imported, num_filters:"<<net.subnet().layer_details().num_filters();
    } catch(...){
        qDebug()<<"importDNN: deserialize for dnn failed";
        trainedDNN=false;
        return false;
    }
    dnnLoaded=true;
    trainedDNN=true;
    QSqlDatabase defaultdb=QSqlDatabase::database();
    QString databaseName=defaultdb.databaseName();
    if(!saveDNN(databaseName, net)){
        //try again
        if(!saveDNN(databaseName, net)){
            //try again
            saveDNN(databaseName, net);
        }
    }
    return true;
}

bool CompDef::importFHOG(QString fileName)
{
    try {
        dlib::object_detector<image_scanner_type> detector;
        dlib::deserialize(fileName.toStdString())>>detector;
        fhogDetector.clear();
        fhogDetector.push_back(detector);
        qDebug()<<"fhog imported, num_detectors:"<<detector.num_detectors();
    } catch(...){
        qDebug()<<"importFHOG: deserialize for fhog failed";
        trainedFHOG=false;
        return false;
    }
    fhogLoaded=true;
    trainedFHOG=true;
    QSqlDatabase defaultdb=QSqlDatabase::database();
    QString databaseName=defaultdb.databaseName();
    if(!saveFHOGDetector(databaseName))
        //try again
        saveFHOGDetector(databaseName);
    return true;
}

bool CompDef::importShapePredictor(QString fileName)
{
    try {
        dlib::shape_predictor sp;
        dlib::deserialize(fileName.toStdString())>>sp;
        shapePredictor.clear();
        shapePredictor.push_back(sp);
        qDebug()<<"shape predictor imported, num_parts:"<<sp.num_parts();
    } catch(...){
        qDebug()<<"importShapePredictor: deserialize for shape predictor failed";
        return false;
    }
    spLoaded=true;
    QSqlDatabase defaultdb=QSqlDatabase::database();
    QString databaseName=defaultdb.databaseName();
    if(!saveShapePredictor(databaseName))
        //try again
        saveShapePredictor(databaseName);
    return true;
}

bool CompDef::exportDNN(QString fileName)
{
    if(!trainedDNN)
        return false;
    QSqlDatabase defaultdb=QSqlDatabase::database();
    QString databaseName=defaultdb.databaseName();
    net_type1 net;
    if(!loadDNN(id,net, databaseName)){
        return false;
    }
    try {
        dlib::serialize(fileName.toStdString())<<net;
    } catch(...){
        qDebug()<<"serialize for dnn failed";
        return false;
    }
    return true;

}

bool CompDef::exportFHOG(QString fileName)
{
    if(!trainedFHOG)
        return false;
    if(!fhogLoaded)
        loadFHOGDetector();
    if(!fhogLoaded)
        return false;
    if(fhogDetector.size()<=0)
        return false;
    try {
        dlib::serialize(fileName.toStdString())<<fhogDetector[0];
    } catch(...){
        qDebug()<<"serialize for fhog failed";
        return false;
    }
    return true;
}

bool CompDef::exportShapePredictor(QString fileName)
{
    if(!spLoaded){
        loadShapePredictor();
    }
    if(!spLoaded)
        return false;
    if(shapePredictor.size()<=0)
        return false;
    try {
        dlib::serialize(fileName.toStdString())<<shapePredictor[0];
    } catch(...){
        qDebug()<<"serialize for shape predictor failed";
        return false;
    }
    return true;
}

double CompDef::testShapePredictor(const dlib::array<dlib::array2d<dlib::rgb_pixel> > &imageArray, const std::vector<std::vector<dlib::full_object_detection> > &fullObjects, int spIdx)
{
    assert(imageArray.size()==fullObjects.size());
    uint rectangles=0;
    double error0=0, error1=0, error2=0, error3=0, error4=0, error0x=0, error1x=0, error2y=0, error3y=0;
    double derror0=0, derror1=0, derror2=0, derror3=0;
    for(uint i=0; i<imageArray.size(); i++){
        for(uint j=0; j<fullObjects[i].size(); j++){
            rectangles++;
            dlib::rectangle r=fullObjects[i][j].get_rect();
            int x=-4;
            r.set_top(r.top()+x);
            r.set_left(r.left()+x);
            r.set_bottom(r.bottom()+x);
            r.set_right(r.right()+x);
            dlib::full_object_detection object=shapePredictor[spIdx](imageArray[i],r);
            error0+=std::abs(object.part(0).x()-fullObjects[i][j].part(0).x())+std::abs(object.part(0).y()-fullObjects[i][j].part(0).y());
            error1+=std::abs(object.part(1).x()-fullObjects[i][j].part(1).x())+std::abs(object.part(1).y()-fullObjects[i][j].part(1).y());
            error2+=std::abs(object.part(2).x()-fullObjects[i][j].part(2).x())+std::abs(object.part(2).y()-fullObjects[i][j].part(2).y());
            error3+=std::abs(object.part(3).x()-fullObjects[i][j].part(3).x())+std::abs(object.part(3).y()-fullObjects[i][j].part(3).y());
            error4+=std::abs(object.part(4).x()-fullObjects[i][j].part(4).x())+std::abs(object.part(4).y()-fullObjects[i][j].part(4).y());
            error0x+=std::abs(object.part(0).x()-fullObjects[i][j].part(0).x());
            error1x+=std::abs(object.part(1).x()-fullObjects[i][j].part(1).x());
            error2y+=std::abs(object.part(2).y()-fullObjects[i][j].part(2).y());
            error3y+=std::abs(object.part(3).y()-fullObjects[i][j].part(3).y());
            derror0+=std::abs(fullObjects[i][j].part(0).x()-fullObjects[i][j].get_rect().left());
            derror1+=std::abs(fullObjects[i][j].part(1).x()-fullObjects[i][j].get_rect().right());
            derror2+=std::abs(fullObjects[i][j].part(2).y()-fullObjects[i][j].get_rect().top());
            derror3+=std::abs(fullObjects[i][j].part(3).y()-fullObjects[i][j].get_rect().bottom());
            qDebug()<<"shape predictor errors"<<error0<<error1<<error2<<error3<<error4<<error0x<<error1x<<error2y<<error3y<<derror0<<derror1<<derror2<<derror3;
        }
    }
    if(rectangles>0){
        error0 /=rectangles;
        error1 /=rectangles;
        error2 /=rectangles;
        error3 /=rectangles;
        error4 /=rectangles;
        error0x /=rectangles;
        error1x /=rectangles;
        error2y /=rectangles;
        error3y /=rectangles;
        derror0 /=rectangles;
        derror1 /=rectangles;
        derror2 /=rectangles;
        derror3 /=rectangles;
        qDebug()<<"shape predictor average errors"<<error0<<error1<<error2<<error3<<error4<<error0x<<error1x<<error2y<<error3y<<derror0<<derror1<<derror2<<derror3;
    } else {
        qDebug()<<"zero objects provided to shape predictor test";
    }
    return (error0+error1+error2+error3+error4)/5;
}

bool CompDef::importAutoinspect(QString dirName)
{
    QDir dir;
    dir.setPath(dirName);
    if(!dir.exists()){
        qDebug()<<"directory"<<dirName<<"not found";
        return false;
    }
    if((!dir.exists(dir.absolutePath()+"/description.txt"))||
            (!dir.exists(dir.absolutePath()+"/pos.txt"))||
            (!dir.exists(dir.absolutePath()+"/neg.txt"))){
        qDebug()<<dirName<<"is not an autoinspect classifier directory";
        return false;
    }
    dirty=true;
    images.clear();
    double width=24;
    double height=24;
    QFile file;
    file.setFileName(dir.absolutePath()+"/description.txt");
    file.open( QIODevice::ReadOnly|QIODevice::Text);
    qDebug()<<file.fileName()<<file.isOpen()<<file.canReadLine();
    while(!file.atEnd()){
        QString line=file.readLine();
        if(line.endsWith('\n'))
            line.truncate(line.count()-1);
#if ((QT_VERSION_MAJOR == 5) && (QT_VERSION_MINOR >= 14))||(QT_VERSION_MAJOR > 5)
        QStringList lst=line.split(' ', Qt::SkipEmptyParts );
#else
        QStringList lst=line.split(' ', QString::SkipEmptyParts );
#endif
        if(lst.count()<2)
            continue;
        QString key=lst[0];
        QString val=lst[1];
        if(key.compare("width",Qt::CaseInsensitive)==0){
            width=val.toDouble();
        } else if(key.compare("height",Qt::CaseInsensitive)==0){
            height=val.toDouble();
        } else if(key.compare("hsympos",Qt::CaseInsensitive)==0){
            if(val.toInt()==0)
                horizontalFlip=false;
            else
                horizontalFlip=true;
        } else if(key.compare("vsympos",Qt::CaseInsensitive)==0){
            if(val.toInt()==0)
                verticalFlip=false;
            else
                verticalFlip=true;
        } else if(key.compare("orientation",Qt::CaseInsensitive)==0){
            switch(val.toInt()){
            case 0:
            default:
                orientationMode=CompDefRect::N_E_S_V;
                break;
            case 1:
                orientationMode=CompDefRect::H_V;
                break;
            case 2:
                orientationMode=CompDefRect::ANY;
                break;
            }

            qDebug()<<"line is:"<<line<<lst;
        }
    }
    aspectRatio=width/height;
    qDebug()<<dir.absolutePath();
    file.close();
    QString lastPath="";
    file.setFileName(dir.absolutePath()+"/pos.txt");
    file.open(QIODevice::ReadOnly|QIODevice::Text);
    while(!file.atEnd()){
        QString line=file.readLine();
        if(line.endsWith('\n'))
            line.truncate(line.count()-1);
        QStringList lst=quotedSplit(line);
        qDebug()<<line;
        if(line.startsWith('"')){
            QString imgPath=unquote(lst[0]);
            if(!QFile::exists(imgPath)){
                QString newPath=QFileDialog::getOpenFileName(nullptr, imgPath, lastPath);
                imgPath=newPath;
            }
            if(imgPath.length()>0){
                QFileInfo fi(imgPath);
                lastPath=fi.absolutePath();
            }
            CompDefImage * img = getImage(imgPath);
            if(img==nullptr){
                if(imgPath.length()>0){
                    images.append(CompDefImage(imgPath));
                    img=&(images.last());
                }
            }

            int objects=lst[1].toInt();
            for(;objects>0; objects--){
                line=file.readLine();
                if(imgPath.length()<=0){
                    qDebug()<<"skipping";
                    continue;
                }
                QStringList list=line.simplified().split(' ');
                qDebug()<<"rectangle"<<list;
                if(list.count()<5){
                    qDebug()<<"skipped line";
                    continue;
                }
                CompDefRect r;
                r.orientation=CompDefRect::NONE;
                if(orientationMode==CompDefRect::H_V){
                    if((list[0].startsWith("E"))||(list[0].startsWith("W")))
                        r.orientation=CompDefRect::EW;
                    else
                        r.orientation=CompDefRect::NS;
                } else if(orientationMode==CompDefRect::N_E_S_V){
                    if(list[0].startsWith("E"))
                        r.orientation=CompDefRect::E;
                    else if(list[0].startsWith("W"))
                        r.orientation=CompDefRect::W;
                    else if(list[0].startsWith("S"))
                        r.orientation=CompDefRect::S;
                    else
                        r.orientation=CompDefRect::N;
                }
                r.x=list[1].toLong();
                r.y=list[2].toLong();
                r.w=list[3].toLong();
                r.h=list[4].toLong();
                r.rectangleType=CompDefRect::POSITIVE;
                img->rect.append(r);
            }
        } else {
            qDebug()<<"unknown";
        }
    }
    file.close();
    file.setFileName(dir.absolutePath()+"/neg.txt");
    file.open(QIODevice::ReadOnly|QIODevice::Text);
    while(!file.atEnd()){
        QString line=file.readLine();
        if(line.endsWith('\n'))
            line.truncate(line.count()-1);
        QStringList lst=quotedSplit(line);
        qDebug()<<line;
        if(line.startsWith('"')){
            QString imgPath=unquote(lst[0]);
            if(!QFile::exists(imgPath)){
                QString newPath=QFileDialog::getOpenFileName(nullptr, imgPath, lastPath);
                imgPath=newPath;
            }
            if(imgPath.length()>0){
                QFileInfo fi(imgPath);
                lastPath=fi.absolutePath();
            }
            CompDefImage * img = getImage(imgPath);
            if(img==nullptr){
                if(imgPath.length()>0){
                    images.append(CompDefImage(imgPath));
                    img=&(images.last());
                }
            }

            int objects=lst[1].toInt();
            for(;objects>0; objects--){
                line=file.readLine();
                if(imgPath.length()<=0){
                    qDebug()<<"skipping";
                    continue;
                }
                QStringList list=line.simplified().split(' ');
                qDebug()<<"rectangle"<<list;
                if(list.count()<5){
                    qDebug()<<"skipped line";
                    continue;
                }
                CompDefRect r;
                r.orientation=CompDefRect::NONE;
                if(orientationMode==CompDefRect::H_V){
                    r.orientation=CompDefRect::NS;
                } else if(orientationMode==CompDefRect::N_E_S_V){
                    r.orientation=CompDefRect::N;
                }
                r.x=list[1].toLong();
                r.y=list[2].toLong();
                r.w=list[3].toLong();
                r.h=list[4].toLong();
                r.rectangleType=CompDefRect::NEGATIVE;
                img->rect.append(r);
            }
        } else {
            qDebug()<<"unknown";
        }
    }
    file.close();
    //emit updated();
    return true;
}

template <
        typename array_type
        >
std::vector<std::vector<dlib::rectangle> >
CompDef::createDlibDataset(dlib::array<array_type> &imageArray,
                           std::vector<std::vector<dlib::rectangle> > &objectLocations,
                           std::vector<std::vector<float> > &angles, bool growAndShrink)
{
    std::vector<std::vector<dlib::rectangle> > ignoredRects;
    imageArray.clear();
    objectLocations.clear();
    angles.clear();
    for(int i=0; i<images.count()+includedImages.count(); i++){
        std::vector<std::vector<dlib::rectangle> > ignoredRects1;
        std::vector<std::vector<dlib::rectangle> > objectLocations1;
        std::vector<std::vector<float> > angles1;
        dlib::array<array_type> imageArray1;
        const CompDefImage & compDefImg=(i<images.count())?images[i]:includedImages[i-images.count()];
        //dlib::array2d<unsigned char> fullImage, imageChip, rotatedImageChip;
        array_type fullImage, imageChip, rotatedImageChip;
        //qDebug()<<"image loaded";
        try {
            dlib::load_image(fullImage,compDefImg.imagePath.toStdString());
        } catch (dlib::image_load_error& e){
            std::cout<<"load_image: "<<e.what()<<" "<<e.info<<"\n";
            continue;
        }
        catch(...){
            qDebug()<<"load_image: failed";
            continue;
        }

        //qDebug()<<"image loaded";
        for(int j=0; j<compDefImg.rect.count(); j++){
            std::vector<dlib::rectangle> objects, ignored;
            std::vector<float> rotations;
            const CompDefRect & compDefRect=compDefImg.rect[j];
            //qDebug()<<"processing rectangle"<<j<<compDefRect.x<<compDefRect.y<<compDefRect.w<<compDefRect.h<<compDefRect.rectangleType;
            if(compDefRect.rectangleType==CompDefRect::POSITIVE){
                //for each positive sample, add an image chip containing the desired object image, surrounded by some margin, to the images, and objectlocations vectors
                //also add a big ignore region over the entire image chip, in order to ignore other object images which might appear in the margin area
                dlib::rectangle background;
                //the offset is calculated for the central rectangle with respect to the background chip, not to the initial image!
                double xoffset=std::max(compDefRect.w, 0.6*compDefRect.h) * chipMargin;
                double yoffset=std::max(compDefRect.h, 0.6*compDefRect.w) * chipMargin;
                switch(compDefRect.orientation){
                case CompDefRect::N:
                case CompDefRect::NS:
                default: {//block used to allow local variables
                    background=dlib::rectangle(compDefRect.x-xoffset, compDefRect.y-yoffset,compDefRect.x+compDefRect.w+xoffset, compDefRect.y+compDefRect.h+yoffset);
                    auto r=dlib::rectangle(xoffset,yoffset,xoffset+compDefRect.w, yoffset+compDefRect.h);
                    //only add rectangle if not duplicate
                    if(std::find(objects.begin(), objects.end(), r) == objects.end()){
                        objects.push_back(r);
                        rotations.push_back(compDefRect.angle);
                    }
                    //objects.back().ignore=false;
                    ignored.push_back(dlib::rectangle(0,0,2*xoffset+compDefRect.w, 2*yoffset+compDefRect.h));
                    //ignored.back().ignore=true;
                    dlib::chip_details chip(background);
                    dlib::extract_image_chip(fullImage, chip, imageChip);
                    imageArray1.push_back(imageChip);
                    break;
                }
                case CompDefRect::EW:
                case CompDefRect::E:{//block used to allow local variables
                    background=dlib::rectangle(compDefRect.x-yoffset, compDefRect.y-xoffset,compDefRect.x+compDefRect.h+yoffset, compDefRect.y+compDefRect.w+xoffset);
                    auto r=dlib::rectangle(xoffset,yoffset,xoffset+compDefRect.w, yoffset+compDefRect.h);
                    if(std::find(objects.begin(), objects.end(), r) == objects.end()){
                        objects.push_back(r);
                        rotations.push_back(compDefRect.angle);
                    }
                    //objects.back().ignore=false;
                    ignored.push_back(dlib::rectangle(0,0,2*xoffset+compDefRect.w, 2*yoffset+compDefRect.h));
                    //ignored.back().ignore=true;
                    dlib::chip_details chip(background);
                    dlib::extract_image_chip(fullImage, chip, imageChip);
                    dlib::rotate_image(imageChip, rotatedImageChip, M_PI/2);
                    imageArray1.push_back(rotatedImageChip);
                    break;
                }
                case CompDefRect::S:{//block used to allow local variables
                    background=dlib::rectangle(compDefRect.x-xoffset, compDefRect.y-yoffset,compDefRect.x+compDefRect.w+xoffset, compDefRect.y+compDefRect.h+yoffset);
                    auto r=dlib::rectangle(xoffset,yoffset,xoffset+compDefRect.w, yoffset+compDefRect.h);
                    if(std::find(objects.begin(), objects.end(), r) == objects.end()){
                        objects.push_back(r);
                        rotations.push_back(compDefRect.angle);
                    }
                    //objects.back().ignore=false;
                    ignored.push_back(dlib::rectangle(0,0,2*xoffset+compDefRect.w, 2*yoffset+compDefRect.h));
                    //ignored.back().ignore=true;
                    dlib::chip_details chip(background);
                    dlib::extract_image_chip(fullImage, chip, imageChip);
                    dlib::rotate_image(imageChip, rotatedImageChip, M_PI);
                    imageArray1.push_back(rotatedImageChip);
                    break;
                }
                case CompDefRect::W:{//block used to allow local variables
                    background=dlib::rectangle(compDefRect.x-yoffset, compDefRect.y-xoffset,compDefRect.x+compDefRect.h+yoffset, compDefRect.y+compDefRect.w+xoffset);
                    auto r=dlib::rectangle(xoffset,yoffset,xoffset+compDefRect.w, yoffset+compDefRect.h);
                    if(std::find(objects.begin(), objects.end(), r) == objects.end()){
                        objects.push_back(r);
                        rotations.push_back(compDefRect.angle);
                    }
                    //objects.back().ignore=false;
                    ignored.push_back(dlib::rectangle(0,0,2*xoffset+compDefRect.w, 2*yoffset+compDefRect.h));
                    //ignored.back().ignore=true;
                    dlib::chip_details chip(background);
                    dlib::extract_image_chip(fullImage, chip, imageChip);
                    dlib::rotate_image(imageChip, rotatedImageChip, -M_PI/2);
                    imageArray1.push_back(rotatedImageChip);
                    break;
                }
                }
#if 0
                //this is not working, as the rectngle positions need to be adjusted according to the orientation
                for(int k=0; k<compDefImg.rect.count();k++){
                    const CompDefRect &r=compDefImg.rect[k];
                    if(compDefRect.rectangleType!=r.rectangleType)
                        continue;
                    if(compDefRect.orientation!=r.orientation)
                        continue;
                    if(r.checkOverlap(compDefRect,true)>0.4)
                        continue;
                    dlib::rectangle object(r.x, r.y, r.x+r.w, r.y+r.h);
                    if(background.contains(object)){
                        dlib::rectangle r2(r.x+xoffset-compDefRect.x, r.y+yoffset-compDefRect.y,
                                           r.x+r.w+xoffset-compDefRect.x, r.y+r.h+yoffset-compDefRect.y);
                        /// @todo fix!!!
                        objects.push_back(r2);
                        qDebug()<<objects[0].left()<<objects[0].top()<<objects[0].width()<<objects[0].height()
                                <<objects[1].left()<<objects[1].top()<<objects[1].width()<<objects[1].height();
                    }
                }
#endif
                objectLocations1.push_back(objects);
                ignoredRects1.push_back(ignored);
                angles1.push_back(rotations);
                continue;
                //ignoredRectsRef
            } else if(compDefRect.rectangleType==CompDefRect::NEGATIVE){
                //for now, this is always true
                //was ignored=...
                ignored=createNegativeMarking(compDefImg, fullImage, imageChip,
                                              compDefRect.x, compDefRect.y, compDefRect.w, compDefRect.h,
                                              compDefRect.orientation, objects, rotations);
                objectLocations1.push_back(objects);
                ignoredRects1.push_back(ignored);
                imageArray1.push_back(imageChip);
                angles1.push_back(rotations);
            }
        }
        //
        if(compDefImg.implicitNegative){
            std::vector<dlib::rectangle> objects, ignored;
            std::vector<float> rotations;
            //was ignored=...
            ignored=createNegativeMarking(compDefImg, fullImage, imageChip,
                                          0, 0, fullImage.nc(), fullImage.nr(),
                                          CompDefRect::N, objects, rotations);
            objectLocations1.push_back(objects);
            ignoredRects1.push_back(ignored);
            imageArray1.push_back(imageChip);
            angles1.push_back(rotations);
        }
        assert(imageArray1.size()==objectLocations1.size());
        assert(imageArray1.size()==ignoredRects1.size());
        assert(imageArray1.size()==angles1.size());
        //qDebug()<<"images"<<imageArray1.size()<<"angles"<<angles1.size();
        if(horizontalFlip){
            //qDebug()<<"flip left right";
            dlib::add_image_left_right_flips(imageArray1, objectLocations1, ignoredRects1);
            int s=angles1.size();
            for(int i=0; i<s; i++){
                std::vector<float> tmp;
                for(unsigned j=0; j<angles1[i].size(); j++){
                    tmp.push_back(-angles1[i][j]);
                }
                angles1.push_back(tmp);
            }
        }
        //qDebug()<<"images"<<imageArray1.size()<<"angles"<<angles1.size();
        if(verticalFlip){
            //qDebug()<<"flip up down";
            addImageUpDownFlips(imageArray1, objectLocations1, ignoredRects1);
            int s=angles1.size();
            for(int i=0; i<s; i++){
                std::vector<float> tmp;
                for(unsigned j=0; j<angles1[i].size(); j++){
                    tmp.push_back(-angles1[i][j]);
                }
                angles1.push_back(tmp);
            }
        }
        //qDebug()<<"Images"<<imageArray1.size()<<"angles"<<angles1.size();
        if(growAndShrink){
            for (int i = 0; i < upsample; ++i){
                //dlib::upsample_image_dataset<dlib::pyramid_down<2> >(imageArray, objects, ignores);
                conditionalUpsampleImageDataset<dlib::pyramid_down<2> >(imageArray1, objectLocations1, ignoredRects1, minArea);
            }
            conditionalDownsampleImageDataset<dlib::pyramid_down<2> >(imageArray1, objectLocations1, ignoredRects1, minArea*4);
        }

        for(unsigned i=0; i<imageArray1.size(); i++){
            assert(objectLocations1[i].size()==angles1[i].size());
            imageArray.push_back(imageArray1[i]);
            objectLocations.push_back(objectLocations1[i]);
            ignoredRects.push_back(ignoredRects1[i]);
            angles.push_back(angles1[i]);
        }
    }
#if 0
    //display dataset
    for(int i=0; i<imageArray.size(); i++){
        dlib::image_window win;
        win.clear_overlay();
        win.set_image(imageArray[i]);
        win.add_overlay(objectLocations[i], dlib::rgb_pixel(255,0,0));
        win.add_overlay(ignoredRects[i], dlib::rgb_pixel(0,255,0));
        /*
        for(int j=0; j<objectLocations[i].size(); j++){
            win.add_overlay(objectLocations[i][j], dlib::rgb_pixel(255,0,0));
            qDebug()<<"obj"<<objectLocations[i][j].rect.left()<<objectLocations[i][j].rect.top()<<objectLocations[i][j].rect.width()<<objectLocations[i][j].rect.height();
        }
        for(int j=0; j<ignoredRects[i].size(); j++){
            win.add_overlay(ignoredRects[i][j], dlib::rgb_pixel(0,255,0));
            qDebug()<<"ign"<<ignoredRects[i][j].rect.left()<<ignoredRects[i][j].rect.top()<<ignoredRects[i][j].rect.width()<<ignoredRects[i][j].rect.height();
        }
        */
        win.wait_until_closed();
    }
#endif

    return ignoredRects;
}

dlib::rectangle CompDef::minRect()
{
    unsigned long w, h;
    w=ceil(sqrt(minArea*aspectRatio));
    h=ceil(w/aspectRatio);
    return dlib::rectangle(w, h);
}


template<typename array_type>
std::vector<dlib::rectangle> CompDef::createNegativeMarking(const CompDefImage &image, const array_type &fullImage, array_type &imageChip,
                                                            double x, double y, double w, double h,
                                                            CompDefRect::Orientation orientation,
                                                            std::vector<dlib::rectangle> &objects,
                                                            std::vector<float> &angles)
{
    //qDebug()<<"neg. orientation"<<orientation;
    std::vector<dlib::rectangle> ignores;
    dlib::rectangle background(x, y, x+w, y+h);
    if((orientation==CompDefRect::E)||(orientation==CompDefRect::EW)||(orientation==CompDefRect::W)){
        background=dlib::rectangle(x, y, x+h, y+w);
    }
    for(int i=0; i<image.rect.count(); i++){
        if((image.rect[i].rectangleType==CompDefRect::POSITIVE)||(image.rect[i].rectangleType==CompDefRect::IGNORERECT)){
            int orientationDiff=image.rect[i].orientationDifference(image.rect[i].orientation,orientation, orientationMode);
            if(orientationDiff!=0)
                continue;
            double xoffset, yoffset;
            xoffset=x;
            yoffset=y;
            double width, height;
            if((image.rect[i].orientation==CompDefRect::E)||(image.rect[i].orientation==CompDefRect::EW)||(image.rect[i].orientation==CompDefRect::W)){
                width=image.rect[i].h;
                height=image.rect[i].w;
            } else {
                height=image.rect[i].h;
                width=image.rect[i].w;
            }
            dlib::rectangle r(image.rect[i].x-xoffset, image.rect[i].y-yoffset, image.rect[i].x+width-xoffset, image.rect[i].y+height-yoffset);
            dlib::rectangle rsource(image.rect[i].x, image.rect[i].y, image.rect[i].x+width, image.rect[i].y+height);
            if((image.rect[i].rectangleType==CompDefRect::POSITIVE)&&(background.contains(rsource))){
                //check if this is not a duplicate
                if(std::find(objects.begin(), objects.end(), r) == objects.end()){
                    objects.push_back(r);
                    angles.push_back(image.rect[i].angle);
                }
            }
            else {
                if(percentCoveredBy(rsource, background)>0){
                    ignores.push_back(r);
                    qDebug()<<"percentCoveredBy"<<percentCoveredBy(rsource, background)<<"contains"<<background.contains(rsource);
                }
            }
        }
    }
    double width, height;
    if((orientation==CompDefRect::E)||(orientation==CompDefRect::EW)||(orientation==CompDefRect::W)){
        width=h;
        height=w;
    } else {
        width=w;
        height=h;
    }
    dlib::chip_details chip(dlib::rectangle(x, y, x+width, y+height));
    dlib::extract_image_chip(fullImage, chip, imageChip);
    if((orientation==CompDefRect::N)||(orientation==CompDefRect::NS)||(orientation==CompDefRect::NONE)){
        //qDebug()<<"createNegativeMarking return";
        return ignores;
    }
    //negative marking is not oriented northwards. Rotate all the data to be rotated towards north
    std::vector<std::vector<dlib::rectangle> > ignoreVector, objectVector;
    dlib::array<array_type> imageArray;
    ignoreVector.push_back(ignores);
    objectVector.push_back(objects);
    imageArray.push_back(imageChip);
    double rotation=0;
    if((orientation==CompDefRect::E)||(orientation==CompDefRect::EW)){
        rotation=M_PI_2;
    } else if(orientation==CompDefRect::S){
        rotation=M_PI;
    } else {
        assert(orientation==CompDefRect::W);
        rotation=-M_PI_2;
    }
    //qDebug()<<"rotation"<<rotation;
    dlib::rotate_image_dataset(rotation, imageArray, objectVector, ignoreVector);
    ignores=ignoreVector[0];
    objects=objectVector[0];
    //imageChip=imageArray[0];
    dlib::assign_image(imageChip,imageArray[0]);
    return ignores;
}


template <typename image_array_type>
void CompDef::flipImageDatasetUpDown (
        image_array_type& images,
        std::vector<std::vector<dlib::rectangle> >& objects,
        std::vector<std::vector<dlib::rectangle> >& objects2
        )
{
    // make sure requires clause is not broken
    DLIB_ASSERT( images.size() == objects.size() &&
                 images.size() == objects2.size(),
                 "\t void flip_image_dataset_left_right()"
                 << "\n\t Invalid inputs were given to this function."
                 << "\n\t images.size():   " << images.size()
                 << "\n\t objects.size():  " << objects.size()
                 << "\n\t objects2.size(): " << objects2.size()
                 );

    typename image_array_type::value_type temp;
    for (unsigned long i = 0; i < images.size(); ++i)
    {
        //flipImageUpDown(images[i], temp);
        //qDebug()<<"flip ud"<<i << images[i].nc() << images[i].nr();
        dlib::flip_image_up_down(images[i], temp);
        swap(temp, images[i]);
        qDebug()<<"objects size"<<objects[i].size()<<objects2[i].size();
        for (unsigned long j = 0; j < objects[i].size(); ++j)
        {
            //qDebug()<<"flip rect ud"<<j<<objects[i][j].left()<<objects[i][j].top()<<objects[i][j].right()<<objects[i][j].bottom();
            objects[i][j] = flipRectUpDown(objects[i][j], get_rect(images[i]));
        }
        for (unsigned long j = 0; j < objects2[i].size(); ++j)
        {
            //qDebug()<<"flip rect ud"<<j<<objects2[i][j].left()<<objects2[i][j].top()<<objects2[i][j].right()<<objects2[i][j].bottom();
            objects2[i][j] = flipRectUpDown(objects2[i][j], get_rect(images[i]));
        }
    }
}

template <
        typename image_type1,
        typename image_type2
        >
dlib::point_transform_affine CompDef::flipImageUpDown (
        const image_type1& in_img,
        image_type2& out_img
        )
{
    //qDebug()<<"flip image ud"<<in_img.nc()<<in_img.nr();
    // make sure requires clause is not broken
    DLIB_ASSERT( dlib::is_same_object(in_img, out_img) == false ,
                 "\t void flip_image_up_down()"
                 << "\n\t Invalid inputs were given to this function."
                 << "\n\t is_same_object(in_img, out_img):  " << is_same_object(in_img, out_img)
                 );

    dlib::assign_image(out_img, dlib::flipud(mat(in_img)));
    //qDebug()<<"out_img"<<out_img.nc()<<out_img.nr();
    std::vector<dlib::vector<double,2> > from, to;
    dlib::rectangle r = dlib::get_rect(in_img);
    from.push_back(r.br_corner()); to.push_back(r.tr_corner());
    from.push_back(r.tl_corner()); to.push_back(r.bl_corner());
    from.push_back(r.bl_corner()); to.push_back(r.tl_corner());
    from.push_back(r.tr_corner()); to.push_back(r.br_corner());
    //qDebug()<<r.left()<<r.top()<<r.right()<<r.bottom();
    return dlib::find_affine_transform(from,to);
}

dlib::rectangle CompDef::flipRectUpDown (
        const dlib::rectangle& rect,
        const dlib::rectangle& window
        )
{
    dlib::rectangle temp;
    temp.left() = rect.left();
    temp.right() = rect.right();

    const long top_dist = rect.top()-window.top();

    temp.bottom() = window.bottom()-top_dist;
    temp.top()  = temp.bottom()-rect.height()+1;
    return temp;
}

template <
        typename pyramid_type,
        typename image_array_type
        >
void CompDef::conditionalUpsampleImageDataset (
        image_array_type& images,
        std::vector<std::vector<dlib::rectangle> >& objects,
        std::vector<std::vector<dlib::rectangle> >& objects2,
        unsigned long minRectArea,
        unsigned long max_image_size
        )
{
    // make sure requires clause is not broken
    DLIB_ASSERT( images.size() == objects.size() &&
                 images.size() == objects2.size(),
                 "\t void upsample_image_dataset()"
                 << "\n\t Invalid inputs were given to this function."
                 << "\n\t images.size():   " << images.size()
                 << "\n\t objects.size():  " << objects.size()
                 << "\n\t objects2.size(): " << objects2.size()
                 );

    typename image_array_type::value_type temp;
    pyramid_type pyr;
    for (unsigned long i = 0; i < images.size(); ++i)
    {
        const unsigned long img_size = num_rows(images[i])*num_columns(images[i]);
        long unsigned minObjArea=std::numeric_limits<long unsigned>::max();
        for (unsigned long j = 0; j < objects[i].size(); ++j){
            dlib::rectangle & r=objects[i][j];
            long unsigned area=r.width()*r.height();
            if(area<minObjArea)
                minObjArea=area;
        }
        if(img_size<minArea*backgroundMultiplier){
            //don't consider upsampling for very big images
            if(minObjArea>=minRectArea){
                //all objects are bigger than minObjArea -> no need to upsample
                //if the images are not very small, don't upsample
                if(img_size>minArea*backgroundMultiplier){
                    continue;
                } else {
                    //very small image, probably without positive markings. Upsample, in order to learn to reject small objects
                }
            }
        }
        if (img_size <= max_image_size)
        {
            pyramid_up(images[i], temp, pyr);
            swap(temp, images[i]);
            for (unsigned long j = 0; j < objects[i].size(); ++j)
            {
                objects[i][j] = pyr.rect_up(objects[i][j]);
            }
            for (unsigned long j = 0; j < objects2[i].size(); ++j)
            {
                objects2[i][j] = pyr.rect_up(objects2[i][j]);
            }
        }
    }
}
template <
        typename pyramid_type,
        typename image_array_type
        >
void CompDef::conditionalDownsampleImageDataset (
        image_array_type& images,
        std::vector<std::vector<dlib::rectangle> >& objects,
        std::vector<std::vector<dlib::rectangle> >& objects2,
        unsigned long maxRectArea
        )
{
    // make sure requires clause is not broken
    DLIB_ASSERT( images.size() == objects.size() &&
                 images.size() == objects2.size(),
                 "\t void upsample_image_dataset()"
                 << "\n\t Invalid inputs were given to this function."
                 << "\n\t images.size():   " << images.size()
                 << "\n\t objects.size():  " << objects.size()
                 << "\n\t objects2.size(): " << objects2.size()
                 );

    typename image_array_type::value_type temp;
    pyramid_type pyr;
    for (long i = 0; i < static_cast<long>(images.size()); ++i)
    {
        //const unsigned long img_size = num_rows(images[i])*num_columns(images[i]);
        long unsigned minObjArea=std::numeric_limits<long unsigned>::max();
        for (unsigned long j = 0; j < objects[i].size(); ++j){
            dlib::rectangle & r=objects[i][j];
            long unsigned area=r.width()*r.height();
            if(area<minObjArea)
                minObjArea=area;
        }
        //if(objects[i].size()==0){
        if(1){
            //don't downsample images smaller than 1MB
            if(images[i].nr()*images[i].nc()<1000000)
                continue;
        }
        if(minObjArea<=maxRectArea){
            //smallest object is small enough -> no need to downsample if image is not very big
            if(images[i].nr()*images[i].nc()<2000000)
                continue;
        }
        pyr(images[i], temp);
        swap(temp, images[i]);
        for (unsigned long j = 0; j < objects[i].size(); ++j)
        {
            objects[i][j] = pyr.rect_down(objects[i][j]);
        }
        for (unsigned long j = 0; j < objects2[i].size(); ++j)
        {
            objects2[i][j] = pyr.rect_down(objects2[i][j]);
        }
        qDebug()<<"shrunken"<<i<<images[i].nr()<<images[i].nc();
        i--;
    }
}

bool CompDef::saveFHOGDetector(QString databaseName)
{
    if(id<=0){
        qDebug()<<"save component definition first";
        return true;
    }

    trainedFHOG=true;
    oldFHOG=false;
    bool result=true;
    {
        qDebug()<<"name"<<databaseName;
        QSqlDatabase threadDb=QSqlDatabase::addDatabase("QSQLITE","THREADDB");
        threadDb.setDatabaseName(databaseName);
        if(!threadDb.open()){
            QMessageBox::critical(nullptr,"eroare","nu se poate deschide baza de date "+databaseName);
            return false;
        }
        unsigned savetime=QDateTime::currentSecsSinceEpoch();
        QSqlQuery query(threadDb);
        std::ostringstream buf;
        dlib::serialize(fhogDetector, buf);
        QString sql="UPDATE compdefs SET fhogclassifier= :classifier, "
                    "trainedfhog= :trainedfhog, "
                    "oldfhog= :oldfhog, "
                    "savetime="+QString::number(savetime)+
                    " WHERE id=:id";
        query.prepare(sql);
        query.bindValue(":classifier", QByteArray(buf.str().c_str(),buf.str().length()));
        query.bindValue(":id", id);
        query.bindValue(":trainedfhog", trainedFHOG);
        query.bindValue(":oldfhog", oldFHOG);
        if(!query.exec()){
            qDebug()<<"error while saving fhog classifier:"<<query.lastError()<<query.lastQuery();
            result=false;
        } else {
            lastsave=savetime;
        }
        threadDb.close();
    }
#if 0
    //test serialization
    QByteArray ba(buf.str().c_str(),buf.str().length());
    //qDebug()<<"saved"<<ba.count();
    dlib::serialize("object_detector.svm") << fhogDetector;
    QFile f("bytearray.bin");
    f.open(QIODevice::WriteOnly);
    f.write(ba);
    f.close();
    loadFHOGDetector();
#endif
    QSqlDatabase::removeDatabase("THREADDB");
    return result;
}

bool CompDef::saveShapePredictor(QString databaseName)
{
    if(id<=0){
        qDebug()<<"save component definition first";
        return true;
    }
    bool result=true;
    {
        qDebug()<<"name"<<databaseName;
        QSqlDatabase threadDb=QSqlDatabase::addDatabase("QSQLITE","THREADDB");
        threadDb.setDatabaseName(databaseName);
        if(!threadDb.open()){
            QMessageBox::critical(nullptr,tr("eroare"),tr("nu se poate deschide baza de date ")+databaseName);
            return false;
        }
        unsigned savetime=QDateTime::currentSecsSinceEpoch();
        QSqlQuery query(threadDb);
        std::ostringstream buf;
        dlib::serialize(shapePredictor, buf);
        QString sql="UPDATE compdefs SET shapepredictor= :sp, trainedfhog = :trainedfhog, savetime="+QString::number(savetime)+
                    " WHERE id=:id";
        query.prepare(sql);
        query.bindValue(":sp", QByteArray(buf.str().c_str(),buf.str().length()));
        query.bindValue(":id", id);
        query.bindValue(":trainedfhog", trainedFHOG);
        if(!query.exec()){
            threadDb.close();
            threadDb.open();
            query.prepare(sql);
            query.bindValue(":sp", QByteArray(buf.str().c_str(),buf.str().length()));
            query.bindValue(":id", id);
            query.bindValue(":trainedfhog", trainedFHOG);
            qDebug()<<"problem while saving shape predictor:"<<query.lastError()<<query.lastQuery();
            if(query.exec()){
                qDebug()<<"solved";
            } else {
                qDebug()<<"error";
                result=false;
            }
        }
        if(result)
            lastsave=savetime;
        std::ostringstream buf2;
        serialize(compSpParams, buf2);
        QByteArray spParams=QByteArray(buf2.str().c_str(),buf2.str().length());
        sql="UPDATE compdefs SET spparams= :spparams "
                    "WHERE id=:id";
        query.prepare(sql);
        query.bindValue(":spparams", spParams);
        query.bindValue(":id", id);
        if(!query.exec()){
            qDebug()<<"error while saving shape predictor params:"<<query.lastError()<<query.lastQuery();
            result=false;
        }
        threadDb.close();
    }
    QSqlDatabase::removeDatabase("THREADDB");
    return result;
}

bool CompDef::saveDNN(QString databaseName, net_type1 net)
{
    if(id<=0){
        qDebug()<<"save component definition first";
        return true;
    }
    bool result=true;
    {
        qDebug()<<"name"<<databaseName;
        QSqlDatabase threadDb=QSqlDatabase::addDatabase("QSQLITE","THREADDB");
        threadDb.setDatabaseName(databaseName);
        if(!threadDb.open()){
            QMessageBox::critical(nullptr,tr("eroare"),tr("nu se poate deschide baza de date ")+databaseName);
            return false;
        }
        QSqlQuery query(threadDb);
        std::stringstream buf(std::ios_base::out |std::ios_base::in | std::ios_base::binary);
        dlib::serialize(net, buf);
        unsigned savetime=QDateTime::currentSecsSinceEpoch();
        QString sql="UPDATE compdefs SET dnnclassifier= :net, "
                    "traineddnn= :traineddnn, "
                    "savetime="+QString::number(savetime)+
                    " WHERE id=:id";
        query.prepare(sql);
        query.bindValue(":net", QByteArray(buf.str().c_str(),buf.str().length()));
        query.bindValue(":traineddnn", trainedDNN);
        query.bindValue(":id", id);
        if(!query.exec()){
            //try again
            if(query.exec())
                lastsave=savetime;
            else {
                qDebug()<<"error while saving dnn detector:"<<query.lastError()<<query.lastQuery();
                result=false;
            }
        } else {
            lastsave=savetime;
        }
        threadDb.close();
        qDebug()<<"saved to database, buf size:"<<buf.str().length();
        /*
        dlib::serialize("proba.dat")<<net;
        try {
            net_type1 net2;
            dlib::deserialize(net2,buf);
            qDebug()<<"converted buf to net, num_filters:"<<net.subnet().layer_details().num_filters();
        } catch (dlib::serialization_error& e){
            std::cout<<"saveDNN: deserialize of buf failed "<<e.what()<<" "<<e.info<<"\n";
        }
        catch(...){
            qDebug()<<"saveDNN: deserialize of buf failed";
        }
        try {
            net_type1 net2;
            dlib::deserialize("proba.dat")>>net2;
            qDebug()<<"loaded proba.dat, num_filters:"<<net.subnet().layer_details().num_filters();
        } catch (dlib::serialization_error& e){
            std::cout<<"saveDNN: deserialize of proba.dat failed "<<e.what()<<" "<<e.info<<"\n";
        }
        catch(...){
            qDebug()<<"saveDNN: deserialize of proba.dat failed";
        }
        */
    }
    QSqlDatabase::removeDatabase("THREADDB");
    return result;
}

void CompDef::loadDNN()
{
    if(id<=0){
        qDebug()<<"save component definition first";
        return;
    }
    if(!trainedDNN){
        qDebug()<<"train dnn first";
        return;
    }
    if(dnnLoaded){
        qDebug()<<"dnn already loaded";
        return;
    }
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    //std::ifstream fin("object_detector.svm", std::ios::binary);

    //dlib::deserialize(fhogDetector,fin);
    //qDebug()<<"deserialized";
    QSqlQuery query;
    QString sql="SELECT dnnclassifier FROM compdefs WHERE id="+QString::number(id);
    query.exec(sql);
    if(query.next()){
        QByteArray tmp=query.value("dnnclassifier").toByteArray();
        setLoadedDNN(tmp);
    } else {
        trainedDNN=false;
    }

}
void CompDef::setLoadedDNN(const QByteArray data){
    qDebug()<<"dnn size"<<data.count();
    if(data.count()>0){
        //buf.str()=std::string(tmp.constData(), tmp.length());
        net_type1 net;
        try {
            std::stringstream buf(std::string(data.constData(), data.length()),std::ios_base::in | std::ios_base::binary);
            dlib::deserialize(net,buf);
            dnnDetector=net;
            //dlib::fuse_layers(dnnDetector);
            qDebug()<<"dnn loaded for inference, num_filters:"<<net.subnet().layer_details().num_filters();
        } catch(...){
            qDebug()<<"loadDNN: deserialize for dnn failed";
            trainedDNN=false;
        }

    }
    dnnLoaded=true;
}

void CompDef::clearDNN()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    if(!defaultdb.isOpen()){
        qDebug()<<"can't open default database"<<defaultdb.databaseName();
        return;
    }
    trainedDNN=false;
    compDNNParams.enabled=false;
    unsigned savetime=QDateTime::currentSecsSinceEpoch();
    QString sql="UPDATE compdefs SET dnnclassifier= NULL, "
                "dnnparams= :dnnparams, "
                "traineddnn= :traineddnn, "
                "savetime="+QString::number(savetime)+
                " WHERE id=:id";
    QSqlQuery query;
    query.prepare(sql);
    std::ostringstream buf3;
    serialize(compDNNParams, buf3);
    QByteArray dnnParams=QByteArray(buf3.str().c_str(),buf3.str().length());
    qDebug()<<"buf3"<<dnnParams.toHex();
    query.bindValue(":dnnparams", dnnParams);
    query.bindValue(":traineddnn", trainedDNN);
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug()<<"error while clearing dnn detector:"<<query.lastError()<<query.lastQuery();
        //try to revive the database
        defaultdb.close();
        defaultdb.open();
        if(query.exec())
            lastsave=savetime;
    } else {
        lastsave=savetime;
    }

}

void CompDef::clearShapePredictor()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    if(!defaultdb.isOpen()){
        qDebug()<<"can't open default database"<<defaultdb.databaseName();
        return;
    }
    unsigned savetime=QDateTime::currentSecsSinceEpoch();
    compSpParams.enabled=false;
    QString sql="UPDATE compdefs SET shapepredictor= NULL, "
                "spparams= :spparams, savetime="+QString::number(savetime)+
                " WHERE id=:id";
    QSqlQuery query;
    query.prepare(sql);
    std::ostringstream buf;
    serialize(compSpParams, buf);
    QByteArray spParams=QByteArray(buf.str().c_str(),buf.str().length());
    qDebug()<<"buf"<<spParams.toHex();
    query.bindValue(":spparams", spParams);
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug()<<"error while clearing shape predictor:"<<query.lastError()<<query.lastQuery();
        //try to revive the database
        defaultdb.close();
        defaultdb.open();
    } else {
        lastsave=savetime;
    }
}

void CompDef::clearFHOG()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    if(!defaultdb.isOpen()){
        qDebug()<<"can't open default database"<<defaultdb.databaseName();
        return;
    }
    trainedFHOG=false;
    unsigned savetime=QDateTime::currentSecsSinceEpoch();
    QString sql="UPDATE compdefs SET fhogclassifier= NULL, "
                "trainedfhog= :trainedfhog, savetime="+QString::number(savetime)+
                " WHERE id=:id";
    QSqlQuery query;
    query.prepare(sql);
    query.bindValue(":trainedfhog", trainedFHOG=false);
    query.bindValue(":id", id);
    if(!query.exec()){
        //try to revive the database
        defaultdb.close();
        defaultdb.open();
        if(!query.exec()){
            qDebug()<<"error while clearing FHOG classifier:"<<query.lastError()<<query.lastQuery();
        } else {
            lastsave=savetime;
        }
    } else {
        lastsave=savetime;
    }

}

bool CompDef::loadDNN(long long compId, net_type1& net, QString databaseName)
{
    if(compId<=0){
        qDebug()<<"save component definition first";
        return false;
    }
    bool ok=false;
    {
        QSqlDatabase threadDb=QSqlDatabase::addDatabase("QSQLITE","THREADDB");
        threadDb.setDatabaseName(databaseName);
        if(!threadDb.open()){
            QMessageBox::critical(nullptr,tr("eroare"),tr("nu se poate deschide baza de date ")+databaseName);
            return false;
        }

        QSqlQuery query(threadDb);
        QString sql="SELECT traineddnn, dnnclassifier FROM compdefs WHERE id="+QString::number(compId);
        query.exec(sql);
        if(query.next()){
            net_type1 tmpnet;
            bool traineddnn=query.value("traineddnn").toBool();
            if(!traineddnn){
                ok=false;
            } else {
                QByteArray tmp=query.value("dnnclassifier").toByteArray();
                qDebug()<<"dnn size"<<tmp.count();
                if(tmp.count()>0){
                    //buf.str()=std::string(tmp.constData(), tmp.length());
                    try {
                        std::stringstream buf(std::string(tmp.constData(), tmp.length()),std::ios_base::in | std::ios_base::binary);
                        dlib::deserialize(tmpnet,buf);
                        ok=true;
                    } catch(...){
                        qDebug()<<"deserialize for trainable dnn failed";
                        ok=false;
                    }
                }
            }
            if(ok){
                net=tmpnet;
                //dlib::fuse_layers(net);
            }
        } else {
            ok=false;
        }
    }
    QSqlDatabase::removeDatabase("THREADDB");
    return ok;
}

bool CompDef::loadIncludedImageMetadata(bool checkImageFiles)
{
    if(includeDataset>=0){
        QSqlDatabase defaultdb=QSqlDatabase::database();
        if(!defaultdb.isOpen()){
            defaultdb.open();
        }
        if(!defaultdb.isOpen()){
            qDebug()<<"unable to open database"<<defaultdb.databaseName();
            return false;
        }
        QSqlQuery query;
        QString sql;

        bool ok=true;
        // check if include is compatible
        sql="SELECT aspectratio FROM compdefs WHERE id="+QString::number(includeDataset);
        query.exec(sql);
        if(query.next()){
            double ratio=query.value(0).toDouble();
            if(std::abs(aspectRatio-ratio)>0.01){
                //wrong aspect ratio
                qDebug()<<"wrong aspect ratio"<<aspectRatio<<ratio;
                ok=false;
            }
        } else {
            //not found
            qDebug()<<"not finding include compdef"<<includeDataset;
            ok=false;
        }
        if(ok){
            sql="SELECT * FROM compdefimages WHERE compdef="+QString::number(includeDataset);
            if(!query.exec(sql)){
                qDebug()<<query.lastQuery();
                qDebug()<<query.lastError();
                return false;
            }
            while(query.next()){
                QString imagePath=query.value("imagepath").toString();
                QString newPath;
                QSqlRecord record=query.record();
                if(checkImageFiles&&(!QFile::exists(imagePath))){
                    ComponentPathCorrection dlg(query.value("id").toLongLong());
                    if(!dlg.exec()){
                        /// @todo invalid image kept -> block editing
                    } else {
                        record.setValue("imagepath",dlg.getNewPath());
                        newPath=dlg.getNewPath();
                        qDebug()<<"newPath"<<newPath;
                    }

                }
                includedImages.append(CompDefImage(record,newPath));
            }
        }
        qDebug()<<"included"<<includedImages.count()<<"images";
    }
    return true;
}

unsigned CompDef::getLastSaved(QSqlDatabase &db)
{
    if(!db.isOpen())
        db.open();
    if(!db.isOpen())
        return 0;
    QString sql="SELECT savetime FROM compdefs WHERE id="+QString::number(id);
    QSqlQuery query(db);
    query.exec(sql);
    if(query.next())
        return query.value(0).toUInt();
    return 0;
}

void CompDef::loadFHOGDetector()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    if(id<=0){
        qDebug()<<"save component definition first";
        return;
    }
    //std::ifstream fin("object_detector.svm", std::ios::binary);

    //dlib::deserialize(fhogDetector,fin);
    //qDebug()<<"deserialized";
    QSqlQuery query;
    QString sql="SELECT fhogclassifier FROM compdefs WHERE id="+QString::number(id);
    query.exec(sql);
    if(query.next()){
        QByteArray tmp=query.value("fhogclassifier").toByteArray();
        setLoadedFHOGDetector(tmp);
    }
}

void CompDef::setLoadedFHOGDetector(const QByteArray data){
    qDebug()<<"classifier size"<<data.count();
    if(data.count()>0){
        //buf.str()=std::string(tmp.constData(), tmp.length());
        try {
            //should be vector of
            std::stringstream buf(std::string(data.constData(), data.length()),std::ios_base::in | std::ios_base::binary);
            dlib::deserialize(fhogDetector,buf);
            if(fhogDetector.size()>0) qDebug()<<"iou"<<fhogDetector[0].get_overlap_tester().get_iou_thresh()<<fhogDetector[0].get_overlap_tester().get_percent_covered_thresh()<<fhogDetector[0].num_detectors()<<fhogDetector[0].get_w().size();
            if(fhogDetector.size()>1) qDebug()<<"iou"<<fhogDetector[1].get_overlap_tester().get_iou_thresh()<<fhogDetector[1].get_overlap_tester().get_percent_covered_thresh()<<fhogDetector[1].num_detectors();
            if(fhogDetector.size()>2) qDebug()<<fhogDetector[2].num_detectors();
            if(fhogDetector.size()>3) qDebug()<<fhogDetector[3].num_detectors();
        } catch(...){
            try {
                std::array<dlib::object_detector<image_scanner_type>,4> fhogDetector2;
                std::stringstream buf(std::string(data.constData(), data.length()),std::ios_base::in | std::ios_base::binary);
                dlib::deserialize(fhogDetector2,buf);
                qDebug()<<"iou"<<fhogDetector2[0].get_overlap_tester().get_iou_thresh()<<fhogDetector2[0].get_overlap_tester().get_percent_covered_thresh()<<fhogDetector2[0].num_detectors()<<fhogDetector2[0].get_w().size();
                qDebug()<<"iou"<<fhogDetector2[1].get_overlap_tester().get_iou_thresh()<<fhogDetector2[1].get_overlap_tester().get_percent_covered_thresh()<<fhogDetector2[1].num_detectors();
                qDebug()<<fhogDetector2[2].num_detectors();
                qDebug()<<fhogDetector2[3].num_detectors();
                fhogDetector.clear();
                for(unsigned i=0; i<4; i++)
                    fhogDetector.push_back(fhogDetector2[i]);
            } catch(...){
                qDebug()<<"deserialize for array failed, trying one item";
                std::stringstream buf(std::string(data.constData(), data.length()),std::ios_base::in | std::ios_base::binary);
                dlib::object_detector<image_scanner_type> detector;
                dlib::deserialize(detector,buf);
                fhogDetector.clear();
                fhogDetector.push_back(detector);
            }
        }

        fhogLoaded=true;
        if(trainedFHOG==false){
            qDebug()<<"inconsistent trainedFHOG";
        }
    } else {
        if(trainedFHOG){
            qDebug()<<"inconsistent trainedFHOG";
        }
    }
}


void CompDef::loadShapePredictor()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    if(id<=0){
        qDebug()<<"save component definition first";
        return;
    }
    //std::ifstream fin("object_detector.svm", std::ios::binary);

    //dlib::deserialize(fhogDetector,fin);
    //qDebug()<<"deserialized";
    QSqlQuery query;
    QString sql="SELECT shapepredictor FROM compdefs WHERE id="+QString::number(id);
    query.exec(sql);
    if(query.next()){
        QByteArray tmp=query.value("shapepredictor").toByteArray();
        setLoadedShapePredictor(tmp);
    }

}

void CompDef::setLoadedShapePredictor(const QByteArray data){
    qDebug()<<"shape predictor size"<<data.count();
    if(data.count()>0){
        //buf.str()=std::string(tmp.constData(), tmp.length());
        try {
            //load vector
            std::stringstream buf(std::string(data.constData(), data.length()),std::ios_base::in | std::ios_base::binary);
            dlib::deserialize(shapePredictor,buf);
            if(shapePredictor.size()>0)
                qDebug()<<"shape predictor parts"<<shapePredictor[0].num_parts();
        } catch(...){
            try {
                //vector failed, try to load array
                std::array<dlib::shape_predictor,4> shapePredictor2;
                std::stringstream buf(std::string(data.constData(), data.length()),std::ios_base::in | std::ios_base::binary);
                dlib::deserialize(shapePredictor2,buf);
                qDebug()<<"shape predictor parts"<<shapePredictor2[0].num_parts();
                shapePredictor.clear();
                for(unsigned i=0; i<4; i++)
                    shapePredictor.push_back(shapePredictor2[i]);
            } catch(...){
                qDebug()<<"deserialize for array failed, trying one item";
                std::stringstream buf(std::string(data.constData(), data.length()),std::ios_base::in | std::ios_base::binary);
                dlib::shape_predictor sp;
                dlib::deserialize(sp,buf);
                shapePredictor.clear();
                shapePredictor.push_back(sp);
            }
        }

    }
    spLoaded=true;
}


void CompDef::averageColor(const QImage img, double & r, double & g, double & b,
                           double & h, double & s, double & v)
{
    int width=img.width();
    r=g=b=0;
    qDebug()<<"avg"<<img.width()<<img.height();
    for(int row=0; row<img.height(); row++){
        for(int c=0; c<width; c++){
            QRgb pixel=img.pixel(c, row);
            r+=qRed(pixel);
            g+=qGreen(pixel);
            b+=qBlue(pixel);
            //qDebug()<<c<<row<<qRed(pixel)<<qGreen(pixel)<<qBlue(pixel);
        }
    }
    int pixels=img.height()*width;
    r /= pixels;
    g /= pixels;
    b /= pixels;
    qDebug()<<r<<g<<b<<pixels;
    QColor color;
    color.setRgbF(r/255, g/255, b/255);
    color.getHsvF(&h, &s, &v);
    qDebug()<<h<<s<<v;
}

void CompDef::calcAverageColor(AverageColorResult &res)
{
    //QVector<double> rv, gv, bv, hv, sv, vv;
    AverageColorData data;
    res.clear();
    double rSum=0, gSum=0, bSum=0, hSum=0, sSum=0, vSum=0;
    double rSumSquare=0, gSumSquare=0, bSumSquare=0, hSumSquare=0, sSumSquare=0, vSumSquare=0;
    foreach (const auto & defImg, images) {
        QImage fullImg;
        if(fullImg.load(defImg.imagePath)){
            QImage img;
            foreach(const auto & defRect, defImg.rect){
                if(defRect.rectangleType!=CompDefRect::POSITIVE)
                    continue;
                if((CompDefRect::E==defRect.orientation)||
                        (CompDefRect::W==defRect.orientation)||
                        (CompDefRect::EW==defRect.orientation)){
                    img=fullImg.copy(defRect.x, defRect.y, defRect.h, defRect.w);
                } else {
                    img=fullImg.copy(defRect.x, defRect.y, defRect.w, defRect.h);
                }
                qDebug()<<"full"<<fullImg.width()<<fullImg.height()<<"chip"<<img.width()<<img.height();
                //temporary
                double rt, gt, bt, ht, st, vt;
                averageColorCenter(img, rt, gt, bt, ht, st, vt);
                data.r.append(rt);
                data.g.append(gt);
                data.b.append(bt);
                data.h.append(ht);
                data.s.append(st);
                data.v.append(vt);
            }
        } else {
            qDebug()<<"unable to load image"<<defImg.imagePath;
        }
    }
    int samples=data.h.count();
    //calculate average and standard deviation of hue considering circular values and normally for the other values
    if(samples>0){
        double sinSum=0;
        double cosSum=0;
        for(int i=0; i<samples; i++){
            sinSum+=sin(data.h[i]*M_PI*2);
            cosSum+=cos(data.h[i]*M_PI*2);
        }
        res.h=atan2(sinSum,cosSum)/(2*M_PI);
        if(res.h<0)
            res.h+=1;
        double sumSquare=0;
        for(int i=0; i<samples; i++){
            double diff=std::abs(data.h[i]-res.h);
            diff=std::min(diff, 1-diff);
            sumSquare += diff*diff;
            rSum+=data.r[i];
            gSum+=data.g[i];
            bSum+=data.b[i];
            //hSum+=data.h[i];
            sSum+=data.s[i];
            vSum+=data.v[i];
            rSumSquare+=data.r[i]*data.r[i];
            gSumSquare+=data.g[i]*data.g[i];
            bSumSquare+=data.b[i]*data.b[i];
            //hSumSquare+=data.h[i]*data.h[i];
            sSumSquare+=data.s[i]*data.s[i];
            vSumSquare+=data.v[i]*data.v[i];
        }
        res.hSigma=sqrt(sumSquare/samples);
        qDebug()<<"average hue"<<res.h<<res.hSigma<<hSum/samples<<sqrt(samples*hSumSquare-hSum*hSum)/samples;
    }
    qDebug()<<data.h;
    if(samples>0){
        res.r=rSum/samples;
        res.g=gSum/samples;
        res.b=bSum/samples;
        //h=hSum/samples;
        res.s=sSum/samples;
        res.v=vSum/samples;
        res.rSigma=sqrt(samples*rSumSquare-rSum*rSum)/samples;
        res.gSigma=sqrt(samples*gSumSquare-gSum*gSum)/samples;
        res.bSigma=sqrt(samples*bSumSquare-bSum*bSum)/samples;
        //hSigma=sqrt(samples*hSumSquare-hSum*hSum)/samples;
        res.sSigma=sqrt(samples*sSumSquare-sSum*sSum)/samples;
        res.vSigma=sqrt(samples*vSumSquare-vSum*vSum)/samples;
    }
    qDebug()<<colorValidator[0][1]<<res.r<<res.g<<res.b<<res.h<<res.s<<res.v<<res.rSigma<<res.gSigma<<res.bSigma<<res.hSigma<<res.sSigma<<res.vSigma;
}

void CompDef::averageColorCenter(const QImage img, double &r, double &g, double &b, double &h, double &s, double &v)
{
    double wperc, hperc;
    wperc=colorValidator[0][1];
    hperc=colorValidator[0][1];
    wperc=std::max(std::min(wperc,1.0),0.0);
    hperc=std::max(std::min(hperc,1.0),0.0);
    //wperc=hperc=1;
    QRect rect(img.width()*((1-wperc)/2),img.height()*((1-hperc)/2),img.width()*wperc,
               img.height()*hperc);
    QImage centralRect=img.copy(rect);
    averageColor(centralRect, r, g, b, h, s, v);
}

bool CompDef::validateColor(QImage img, DetectedComponent& dc)
{
    //double r, g, b, h, s, v;
    averageColorCenter(img, dc.red, dc.green, dc.blue, dc.hue, dc.sat, dc.val);
    qDebug()<<"w"<<img.width()<<"h"<<img.height()<<"rgbhsv"<<dc.red<<dc.green<<dc.blue<<dc.hue<<dc.sat<<dc.val;
    double diff;
    if(colorValidator[1][1]>0){
        diff=abs(dc.red-colorValidator[1][0]);
        if(colorValidator[1][1]<diff){
            qDebug()<<"r"<<dc.red<<"diff"<<diff<<"val"<<colorValidator[1][0]<<"tol"<<colorValidator[1][1];
            return false;
        }
    }
    if(colorValidator[2][1]>0){
        diff=abs(dc.green-colorValidator[2][0]);
        if(colorValidator[2][1]<diff){
            qDebug()<<"g"<<dc.green<<"diff"<<diff<<"val"<<colorValidator[2][0]<<"tol"<<colorValidator[2][1];
            return false;
        }
    }
    if(colorValidator[3][1]>0){
        diff=abs(dc.blue-colorValidator[3][0]);
        if(colorValidator[3][1]<diff){
            qDebug()<<"b"<<dc.blue<<"diff"<<diff<<"val"<<colorValidator[3][0]<<"tol"<<colorValidator[3][1];
            return false;
        }
    }
    if(colorValidator[4][1]>0){
        diff=abs(dc.hue-colorValidator[4][0]);
        if(diff>0.5){
            diff=1-diff;
        }
        if(colorValidator[4][1]<diff){
            qDebug()<<"h"<<dc.hue<<"diff"<<diff<<"val"<<colorValidator[4][0]<<"tol"<<colorValidator[4][1];
            return false;
        }
    }
    if(colorValidator[5][1]>0){
        diff=abs(dc.sat-colorValidator[5][0]);
        if(colorValidator[5][1]<diff){
            qDebug()<<"s"<<dc.sat<<"diff"<<diff<<"val"<<colorValidator[5][0]<<"tol"<<colorValidator[5][1];
            return false;
        }
    }
    if(colorValidator[6][1]>0){
        diff=abs(dc.val-colorValidator[6][0]);
        if(colorValidator[6][1]<diff){
            qDebug()<<"v"<<dc.val<<"diff"<<diff<<"val"<<colorValidator[6][0]<<"tol"<<colorValidator[6][1];
            return false;
        }
    }
    return true;
}

bool CompDef::usesImage(QString fileName)
{
    QFileInfo info(fileName);
    for(int i=0; i<images.count(); i++){
        if(QFileInfo(images[i].imagePath)==info)
            return true;
    }
    return false;
}

bool CompDef::saveDlibDataset(QString fileName)
{
    std::vector<std::vector<dlib::rectangle> > ignores, objects;
    std::vector<std::vector<float> > angles;
    //dlib::array<dlib::array2d<unsigned char> > imageArray;
    dlib::array<dlib::array2d<dlib::rgb_pixel> > imageArray;
    ignores=createDlibDataset(imageArray, objects, angles, true);
    /*
    for (int i = 0; i < upsample; ++i){
        //dlib::upsample_image_dataset<dlib::pyramid_down<2> >(imageArray, objects, ignores);
        conditionalUpsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea);
    }
    conditionalDownsampleImageDataset<dlib::pyramid_down<2> >(imageArray, objects, ignores, minArea*4);
    */
    QFileInfo info(fileName);
    qDebug()<<info.absoluteFilePath()<<info.baseName()<<info.exists()<<info.suffix();
    bool ok=true;
    QDir d;
    ok=d.mkpath(info.absolutePath());
    if(ok)
        d.mkpath(info.absolutePath()+"/"+info.baseName());
    if(ok){
        QFile f(info.absoluteFilePath());
        f.open(QIODevice::WriteOnly|QIODevice::Text);
        if(!f.isOpen()){
            qDebug()<<"Unable to open file "+f.fileName();
            //emit logMsg("Unable to open file "+f.fileName(),CRITICAL);
            return false;
        }
        QTextStream ts( &f );
        ts<<
             "<?xml version='1.0' encoding='ISO-8859-1'?>\n"
             "<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>\n"
             "<dataset>\n"
             "<name>imglab dataset</name>\n"
             "<comment>Created by examinator.</comment>\n"
             "<images>\n";
        for(unsigned int i=0; i<imageArray.size(); i++){
            QString imageName=info.baseName()+"/"+QString::number(i)+".png";
            std::string fn=(info.absolutePath()+"/"+imageName).toStdString();
            //dlib::matrix<dlib::rgb_pixel> img=dlib::mat( imageArray[i] );
            dlib::save_png(imageArray[i],fn);
            ts<<"<image file='"<<imageName<<"'>\n";
            for(unsigned int j=0; j<objects[i].size();j++){
                auto r=objects[i][j];
                ts<<"<box top='"<<r.top()<<
                    "' left='"<<r.left()<<
                    "' width='"<<r.width()<<
                    "' height='"<<r.height()<<"'/>\n";
            }
            for(unsigned int j=0; j<ignores[i].size();j++){
                auto r=ignores[i][j];
                ts<<"<box top='"<<r.top()<<
                    "' left='"<<r.left()<<
                    "' width='"<<r.width()<<
                    "' height='"<<r.height()<<"' ignore='1'/>\n";
            }
            ts<<"</image>\n";

        }
        ts<<"</images>\n"
            "</dataset>\n";
        f.close();

    }
    return true;
}

QString CompDef::getLastImagePath()
{
    if(images.count()>0){
        return images.last().imagePath;
    }
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    if(id<=0){
        //qDebug()<<"save component definition first";
        return QString();
    }
    QSqlQuery query;
    QString sql="SELECT * FROM compdefimages WHERE compdef="+QString::number(id) + " ORDER BY id DESC LIMIT 1";
    query.exec(sql);
    if(query.next()){
        return query.value("imagepath").toString();
    }
    return QString();
}

QPixmap CompDef::getCompChip(const QPixmap &bg, const CompDefRect *rect)
{
    double w,h;
    if((rect->orientation==CompDefRect::E)||
            (rect->orientation==CompDefRect::W)||
            (rect->orientation==CompDefRect::EW)){
        w=rect->h;
        h=rect->w;
    } else {
        w=rect->w;
        h=rect->h;
    }
    //qDebug()<<"copy from"<<compRect->compDefRect->x<<compRect->compDefRect->y<<w<<h;
    QPixmap compPixmap=bg.copy(rect->x,rect->y, w, h);
    int rotation=0;
    switch(rect->orientation){
    case CompDefRect::E:
    case CompDefRect::EW:
        rotation=270;
        break;
    case CompDefRect::S:
        rotation=180;
        break;
    case CompDefRect::W:
        rotation=90;
        break;
    default:
        break;
    }
    if(rotation){
        QTransform t2;
        QTransform t=t2.rotate(rotation);

        //qDebug()<<"orientation"<<compRect->compDefRect->orientation<<"rotation"<<rotation<<t;
        compPixmap=compPixmap.transformed(t);
    } else {
        //qDebug()<<"orientation"<<compRect->compDefRect->orientation<<"rotation"<<rotation;
    }
    return compPixmap;
}

QPixmap CompDef::getPrimaryRect(bool reload)
{
    if(primaryRect.isNull() || reload){
        primaryRect=QPixmap();
        if(id<=0){
            qDebug()<<"save component definition first";
            return primaryRect;
        }
        QSqlDatabase defaultdb=QSqlDatabase::database();
        if(!defaultdb.isOpen()){
            defaultdb.open();
        }
        QSqlQuery query;
        QString sql="SELECT compdefimages.id as imageid, compdefimages.imagepath, compdefrects.* FROM compdefimages, compdefrects WHERE compdef="+QString::number(id)+" AND compdefrects.compdefimage=compdefimages.id AND rectangletype=1 ORDER BY imageid, id LIMIT 1";
        query.exec(sql);
        if(query.next()){
            QString imagePath=query.value("imagepath").toString();
            QPixmap bg;
            if(!bg.load(imagePath)){
                qDebug()<<"can't load"<<imagePath;
                return primaryRect;
            }
            CompDefRect rect(query.record());
            primaryRect=getCompChip(bg, &rect);
        }
    }
    return primaryRect;
}

template <
        typename image_array_type,
        typename T,
        typename U
        >
void CompDef::addImageUpDownFlips (
        image_array_type& images,
        std::vector<std::vector<T> >& objects,
        std::vector<std::vector<U> >& objects2
        )
{
    // make sure requires clause is not broken
    DLIB_ASSERT( images.size() == objects.size() &&
                 images.size() == objects2.size(),
                 "\t void add_image_left_right_flips()"
                 << "\n\t Invalid inputs were given to this function."
                 << "\n\t images.size():   " << images.size()
                 << "\n\t objects.size():  " << objects.size()
                 << "\n\t objects2.size(): " << objects2.size()
                 );

    typename image_array_type::value_type temp;
    std::vector<T> rects;
    std::vector<U> rects2;

    const unsigned long num = images.size();
    for (unsigned long j = 0; j < num; ++j)
    {
        //qDebug()<<"image"<<j<<images[j].nc()<<images[j].nr();
        const dlib::point_transform_affine tran = flipImageUpDown(images[j], temp);
        //qDebug()<<"image temp"<<j<<temp.nc()<<temp.nr();
        images.push_back(temp);

        rects.clear();
        for (unsigned long i = 0; i < objects[j].size(); ++i){
            //qDebug()<<"rect"<<i<<objects[j][i].left()<<objects[j][i].top()<<objects[j][i].right()<<objects[j][i].bottom();
            rects.push_back(dlib::impl::tform_object(tran, objects[j][i]));
        }
        objects.push_back(rects);

        rects2.clear();
        for (unsigned long i = 0; i < objects2[j].size(); ++i){
            //qDebug()<<"rect"<<i<<objects2[j][i].left()<<objects2[j][i].top()<<objects2[j][i].right()<<objects2[j][i].bottom();
            rects2.push_back(dlib::impl::tform_object(tran, objects2[j][i]));
        }
        objects2.push_back(rects2);
    }
}

QStringList CompDef::quotedSplit(QString text)
{
    QStringList tmpList = text.split(QRegExp("\"")); // Split by "
    bool inside = false;
    QStringList list;
    foreach (QString s, tmpList) {
        if (inside) { // If 's' is inside quotes ...
            list.append(s); // ... get the whole string
        } else { // If 's' is outside quotes ...
#if ((QT_VERSION_MAJOR == 5) && (QT_VERSION_MINOR >= 14))||(QT_VERSION_MAJOR > 5)
            list.append(s.split(QRegExp("\\s+"), Qt::SkipEmptyParts)); // ... get the splitted string
#else
            list.append(s.split(QRegExp("\\s+"), QString::SkipEmptyParts)); // ... get the splitted string
#endif
        }
        inside = !inside;
    }
    return list;
}

QString CompDef::unquote(QString str)
{
    if(str[0]=='\"')
        str.remove(0,1);
    if(str.endsWith('\"'))
        return str.left(str.count()-1);
    return str;
}

CompDefImage *CompDef::getImage(const QString &imagePath)
{
    for(int i=0; i<images.count(); i++){
        if(imagePath.compare(images[i].imagePath)==0)
            return &images[i];
    }
    return nullptr;
}

double CompDef::percentCoveredBy(dlib::rectangle &r1, dlib::rectangle &r2)
{
    if(r1.area()<=0){
        //this might be a point or a line
        return 0;//this is not necessarily true, but we don't need accuracy for this degenerate case
    }
    double intersection = std::max(0L, -std::max(r1.left(), r2.left()) + std::min(r1.right(), r2.right())) *
            std::max(0L, -std::max(r1.top(), r2.top()) + std::min(r1.bottom(), r2.bottom()));
    return intersection/r1.area();
}


//template<typename image_type1>
QList<DetectedComponent> CompDef::detectObjects(const dlib::cv_image<dlib::rgb_pixel> &img, bool displaySearchFrames)
{
    dlib::array2d<dlib::rgb_pixel> arr;
    dlib::assign_image(arr,img);
    return detectObjects(arr, displaySearchFrames);
}

QList<DetectedComponent> CompDef::detectObjects(const dlib::array2d<dlib::rgb_pixel> &img, bool displaySearchFrames, const QImage qImg, double adjustThreshold)
{
    //qDebug()<<"detectObjects"<<name;
    QList<DetectedComponent> detectedObjects=
            (compDNNParams.enabled)?
                detectObjectsDNN(img,qImg,adjustThreshold):
                detectObjectsFHOG(img,qImg,adjustThreshold);
    if(displaySearchFrames){
        dlib::image_window win;
        win.clear_overlay();
        win.set_image(img);
        /*
        win.add_overlay(objectLocations[i], dlib::rgb_pixel(255,0,0));
        win.add_overlay(ignoredRects[i], dlib::rgb_pixel(0,255,0));
        */
        foreach(auto& obj, detectedObjects){
            win.add_overlay(dlib::rectangle(obj.x, obj.y, obj.x2(), obj.y2()), dlib::rgb_pixel(255,0,0));
        }
        /*
        for(int j=0; j<ignoredRects[i].size(); j++){
            win.add_overlay(ignoredRects[i][j], dlib::rgb_pixel(0,255,0));
            qDebug()<<"ign"<<ignoredRects[i][j].rect.left()<<ignoredRects[i][j].rect.top()<<ignoredRects[i][j].rect.width()<<ignoredRects[i][j].rect.height();
        }
        */
        win.wait_until_closed();
    }
    return(detectedObjects);
}

QList<DetectedComponent> CompDef::detectObjectsFHOG(const dlib::array2d<dlib::rgb_pixel> &img, const QImage &qImg, double adjustThreshold)
{
    // this does not work from a different thread
    if(!fhogLoaded){
        qDebug()<<"loading detector";
        loadFHOGDetector();
    }
    //std::vector<std::pair<double, dlib::rectangle> > dets;
    std::vector<dlib::rect_detection > dets;
    /*
    std::vector<dlib::object_detector<image_scanner_type>> detectors;
    for(unsigned i=0; i<4; i++){
        if(fhogDetector[i].num_detectors()>0){
            detectors.push_back(fhogDetector[i]);
        } else {
            //detector has not been trained or loaded
            break;
        }
    }
    */
    //detector(img, dets,fhogThreshold);
    dlib::evaluate_detectors(fhogDetector/*detectors*/, img, dets, fhogThreshold+adjustThreshold);
    //qDebug()<<"shape parts"<<shapePredictor[0].num_parts()<<"features"<<shapePredictor[0].num_features();
    unsigned numDetectors=numFHOGDetectors();
    unsigned numSP=numShapePredictors();
    QList<DetectedComponent> detectedObjects;
    for(unsigned i=0; i<dets.size(); i++){
        dlib::rectangle r=dets[i].rect;
        DetectedComponent dc;
        dlib::full_object_detection object;
        if(compSpParams.enabled){
            if(!spLoaded)
                loadShapePredictor();
        }
        if((compSpParams.enabled)&&((numSP==numDetectors)||((dets[i].weight_index==0)&&(numSP>0)))){
            assert(dets[i].weight_index<numDetectors);
            object=shapePredictor[dets[i].weight_index](img, r);
            //dc.x=object.part(0).x();
            //dc.y=object.part(2).y();
            double x=(object.part(0).x()+object.part(1).x()+object.part(2).x()+object.part(3).x()+object.part(4).x())/5.0;
            double y=(object.part(0).y()+object.part(1).y()+object.part(2).y()+object.part(3).y()+object.part(4).y())/5.0;
            dc.w=object.part(1).x()-object.part(0).x()+1;
            dc.h=object.part(3).y()-object.part(2).y()+1;
            dc.x=x-dc.w/2;
            dc.y=y-dc.h/2;
            float a1= -atan2(object.part(0).y()-object.part(1).y(),object.part(0).x()-object.part(1).x());
            if(a1>M_PI_2)
                a1 -= M_PI;
            if(a1<= -M_PI_2)
                a1 += M_PI;
            float a2= -atan2(object.part(2).y()-object.part(3).y(),object.part(2).x()-object.part(3).x());
            a2 += M_PI_2;
            if(a2>M_PI_2)
                a2 -= M_PI;
            if(a2<= -M_PI_2)
                a2 += M_PI;
            qDebug()<<"parts"<<object.part(0).x()<<object.part(0).y()<<
                object.part(1).x()<<object.part(1).y()<<
                object.part(2).x()<<object.part(2).y()<<
                object.part(3).x()<<object.part(3).y()<<
                object.part(4).x()<<object.part(4).y();
            dc.angle= (a1*dc.w+a2*dc.h)/(dc.w+dc.h);
            //qDebug()<<"detected:"<<r.left()<<r.top()<<r.width()<<r.height()<<"sp"<<dc.x<<dc.y<<dc.w<<dc.h<<"angles"<<a1<<a2;
        } else {
            dc.x=r.left();
            dc.y=r.top();
            dc.w=r.width();
            dc.h=r.height();
        }
        //qDebug()<<"orientation"<<dets[i].weight_index;
        switch(numDetectors){
        case 0:
        case 1:
            dc.orientation=CompDefRect::N;
            break;
        case 2:
            if(dets[i].weight_index==0)
                dc.orientation=CompDefRect::N;
            else
                dc.orientation=CompDefRect::S;
            break;
        case 4:
            switch(dets[i].weight_index){
            case 0:
            default:
                dc.orientation=CompDefRect::N;
                break;
            case 1:
                dc.orientation=CompDefRect::E;
                break;
            case 2:
                dc.orientation=CompDefRect::S;
                break;
            case 3:
                dc.orientation=CompDefRect::W;
                break;
            }
            break;

        }
        dc.score[0]=dets[i].detection_confidence;
        if(colorValidator[0][0]!=0){
            if(!qImg.isNull()){
                QImage chip=qImg.copy(dc.x, dc.y, dc.w, dc.h);
                if(!validateColor(chip,dc)){
                    qDebug()<<"validator rejected chip";
                    dc.score[0] -= 100;
                }
            } else {
                qDebug()<<"validator needs image, but no image has been provided";
            }
        } else {
            //qDebug()<<"validator not enabled";
        }
        detectedObjects.append(dc);;
        qDebug()<<"detected object"<<r.left()<<r.top()<<r.right()<<r.bottom()<<dc.score[0];
    }
    return detectedObjects;
}

QList<DetectedComponent> CompDef::detectObjectsDNN(const dlib::array2d<dlib::rgb_pixel> &img, const QImage &qImg, double adjustThreshold)
{
    if(!dnnLoaded){
        qDebug()<<"attempting to load network";
        loadDNN();
    }
    QList<DetectedComponent> detectedObjects;
    if(!trainedDNN){
        qDebug()<<"train dnn before using it";
        return detectedObjects;
    }
    std::vector<dlib::mmod_rect > dets;
#ifdef PARALLELCOMPDET
    auto net=dnnDetector;
    //dets=net.process(dlib::mat(img),fhogThreshold+adjustThreshold);
#else
    auto &net=dnnDetector;
    //dets=dnnDetector.process(dlib::mat(img),fhogThreshold+adjustThreshold);
#endif

#ifdef SINGLESTEPPROCESS
    dets=net.process(dlib::mat(img),fhogThreshold+adjustThreshold);
#else
    dets = detectObjectsDNNBig(img, net, fhogThreshold+adjustThreshold, true);
    /*
    for(unsigned i=0; i<dets.size(); i++){
        auto det=dets[i];
        qDebug()<<"detsb"<<det.rect.left()<<det.rect.right()<<det.rect.top()<<det.rect.bottom()<<det.detection_confidence;
    }
    */
#endif
#if 0
    {
    //experiment to use the network trained as pyramid on a single image
    anet_type1 netb;
    //netb=dnnDetector;
    //dlib::layer<0>(netb).layer_details()=dlib::layer<0>(dnnDetector).layer_details();
    dlib::layer<1>(netb).layer_details()=dlib::layer<1>(dnnDetector).layer_details();
    dlib::layer<2>(netb).layer_details()=dlib::layer<2>(dnnDetector).layer_details();
    dlib::layer<3>(netb).layer_details()=dlib::layer<3>(dnnDetector).layer_details();
    dlib::layer<4>(netb).layer_details()=dlib::layer<4>(dnnDetector).layer_details();
    dlib::layer<5>(netb).layer_details()=dlib::layer<5>(dnnDetector).layer_details();
    dlib::layer<6>(netb).layer_details()=dlib::layer<6>(dnnDetector).layer_details();
    dlib::layer<7>(netb).layer_details()=dlib::layer<7>(dnnDetector).layer_details();
    dlib::layer<8>(netb).layer_details()=dlib::layer<8>(dnnDetector).layer_details();
    //dlib::layer<9>(netb).layer_details()=dlib::layer<9>(dnnDetector).layer_details();
    dlib::layer<10>(netb).layer_details()=dlib::layer<10>(dnnDetector).layer_details();
    dlib::layer<11>(netb).layer_details()=dlib::layer<11>(dnnDetector).layer_details();
    dlib::layer<12>(netb).layer_details()=dlib::layer<12>(dnnDetector).layer_details();
    dlib::layer<13>(netb).layer_details()=dlib::layer<13>(dnnDetector).layer_details();
    dlib::layer<14>(netb).layer_details()=dlib::layer<14>(dnnDetector).layer_details();
    dlib::layer<15>(netb).layer_details()=dlib::layer<15>(dnnDetector).layer_details();
    dlib::layer<16>(netb).layer_details()=dlib::layer<16>(dnnDetector).layer_details();
    //dlib::layer<17>(netb).layer_details()=dlib::layer<17>(dnnDetector).layer_details();
    dlib::layer<18>(netb).layer_details()=dlib::layer<18>(dnnDetector).layer_details();
    dlib::layer<19>(netb).layer_details()=dlib::layer<19>(dnnDetector).layer_details();
    dlib::layer<20>(netb).layer_details()=dlib::layer<20>(dnnDetector).layer_details();
    dlib::layer<21>(netb).layer_details()=dlib::layer<21>(dnnDetector).layer_details();
    dlib::layer<22>(netb).layer_details()=dlib::layer<22>(dnnDetector).layer_details();
    dlib::layer<23>(netb).layer_details()=dlib::layer<23>(dnnDetector).layer_details();
    dlib::layer<24>(netb).layer_details()=dlib::layer<24>(dnnDetector).layer_details();
    dlib::layer<25>(netb).layer_details()=dlib::layer<25>(dnnDetector).layer_details();
    dlib::layer<26>(netb).layer_details()=dlib::layer<26>(dnnDetector).layer_details();
    //dlib::layer<27>(netb).layer_details()=dlib::layer<27>(dnnDetector).layer_details();
    //dlib::layer<28>(netb).layer_details()=dlib::layer<28>(dnnDetector).layer_details();
    //dlib::layer<29>(netb).layer_details()=dlib::layer<29>(dnnDetector).layer_details();
    netb.loss_details()=dnnDetector.loss_details();
    std::cout<<netb;
    std::vector<dlib::mmod_rect > detsb, detsb1;
    auto imgb=dlib::mat(img);
    detsb1=netb.process(imgb,fhogThreshold+adjustThreshold);
    detsb.insert(detsb.end(), detsb1.begin(), detsb1.end());
    qDebug()<<"detected"<<detsb.size();
    if(detsb.size()>0){
        auto det=detsb[0];
        qDebug()<<"detsb"<<det.rect.left()<<det.rect.right()<<det.rect.top()<<det.rect.bottom()<<det.detection_confidence;
    }
    }
#endif
    if(shapePredictor.size()>0)
        qDebug()<<"shape"<<shapePredictor[0].num_parts()<<shapePredictor[0].num_features();
    for(unsigned i=0; i<dets.size(); i++){
        dlib::rectangle r=dets[i].rect;
        DetectedComponent dc;
        dlib::full_object_detection object;
        if(compSpParams.enabled){
            if(!spLoaded)
                loadShapePredictor();
        }
        if((compSpParams.enabled)&&(shapePredictor.size()>0)&&(shapePredictor[0].num_parts()==5)){
            object=shapePredictor[0](img, r);
            dc.x=object.part(0).x();
            dc.y=object.part(2).y();
            dc.w=object.part(1).x()-dc.x+1;
            dc.h=object.part(3).y()-dc.y+1;
            qDebug()<<"detected:"<<r.left()<<r.top()<<r.width()<<r.height()<<"sp"<<dc.x<<dc.y<<dc.w<<dc.h;
        } else {
            dc.x=r.left();
            dc.y=r.top();
            dc.w=r.width();
            dc.h=r.height();
        }
        dc.orientation=CompDefRect::N;
        dc.score[0]=dets[i].detection_confidence;
        /// @todo apply color validator
        if((colorValidator[0][0]!=0)&&(!qImg.isNull())){
            QImage chip=qImg.copy(dc.x, dc.y, dc.w, dc.h);
            if(!validateColor(chip, dc)){
                qDebug()<<"validator rejected chip";
                dc.score[0] -= 100;
            }
        } else {
            //qDebug()<<"validator not enabled";
        }
        detectedObjects.append(dc);;
        qDebug()<<"dnn detected object"<<r.left()<<r.top()<<r.right()<<r.bottom()<<"score"<<dets[i].detection_confidence;
    }
    return detectedObjects;
}

template < typename net_type >
std::vector<dlib::mmod_rect> CompDef::detectObjectsDNNBig(const dlib::array2d<dlib::rgb_pixel> &img, net_type &net, const double threshold, const bool toplevel)
{
    qDebug()<<"detectObjectsDNNBig"<<maxDNNArea<<img.nc()<<"x"<<img.nr()<<img.nc()*img.nr()<<minRect().width()<<minRect().height();
    std::vector<dlib::mmod_rect> dets;
    if(img.nc()*img.nr()<=static_cast<long>(CompDef::maxDNNArea)){
        dets=net.process(dlib::mat(img),threshold);
        return dets;
    }
    unsigned overlap=std::max(minRect().width(),minRect().height())*1.1;
    std::vector<dlib::rectangle> rects;
    std::vector<dlib::point> offsets;
    if(img.nc()>img.nr()){
        if(img.nr()*((1+img.nc())/2 + overlap)<static_cast<long>(CompDef::maxDNNArea)){
            //only cut the image in two
            rects.push_back(dlib::rectangle(0,0,img.nc()/2+overlap, img.nr()-1));
            offsets.push_back(dlib::point(0,0));
            rects.push_back(dlib::rectangle(img.nc()/2-overlap, 0, img.nc()-1, img.nr()-1));
            offsets.push_back(dlib::point(img.nc()/2-overlap,0));
        }
    } else {
        if(img.nc()*((1+img.nr())/2 + overlap)<static_cast<long>(CompDef::maxDNNArea)){
            //only cut the image in two
            rects.push_back(dlib::rectangle(0,0,img.nc()-1, img.nr()/2+overlap));
            offsets.push_back(dlib::point(0,0));
            rects.push_back(dlib::rectangle(0,img.nr()/2-overlap, img.nc()-1, img.nr()-1));
            offsets.push_back(dlib::point(0,img.nr()/2-overlap));
        }
    }
    if(rects.size()==0){
        //the image is too big to only be split into two parts -> split in 4
        rects.push_back(dlib::rectangle(0,0,img.nc()/2+overlap, img.nr()/2+overlap));
        offsets.push_back(dlib::point(0,0));
        rects.push_back(dlib::rectangle(0,img.nr()/2-overlap, img.nc()/2+overlap, img.nr()-1));
        offsets.push_back(dlib::point(0,img.nr()/2-overlap));
        rects.push_back(dlib::rectangle(img.nc()/2-overlap,0, img.nc()-1, img.nr()/2+overlap));
        offsets.push_back(dlib::point(img.nc()/2-overlap,0));
        rects.push_back(dlib::rectangle(img.nc()/2-overlap,img.nr()/2-overlap, img.nc()-1, img.nr()-1));
        offsets.push_back(dlib::point(img.nc()/2-overlap,img.nr()/2-overlap));
    }

    dlib::array2d<dlib::rgb_pixel> temp;
    dlib::pyramid_down<2> pyr;
    pyr(img, temp);
    //dets=net.process(dlib::mat(temp),threshold);
    //run the detection on a shrunken version of the entire image, to find objects crossing the splitting lines
    dets=detectObjectsDNNBig(temp, net, threshold, false);
    for(unsigned i=0; i<dets.size(); i++){
        //scale the rectangles
        dets[i].rect=dlib::scale_rect(dets[i].rect, 2);
    }
    //run the detection on the parts of the image
    for(unsigned i=0; i<rects.size(); i++){
        //auto img2=dlib::sub_image(img,dlib::rectangle(img.nc(),img.nr()));
        dlib::array2d<dlib::rgb_pixel> img2;
        dlib::assign_image(img2, dlib::sub_image(img,rects[i]));
        std::vector<dlib::mmod_rect> dets2;
        dets2=detectObjectsDNNBig(img2, net, threshold, false);
        for(unsigned j=0; j<dets2.size(); j++){
            //add offset to the rectangles
            dets2[j].rect=dlib::translate_rect(dets2[j].rect, offsets[i]);
        }
        dets.insert(dets.end(), dets2.begin(), dets2.end());
    }
    if(toplevel){
        //do nonmaxima suppression
        std::sort(dets.begin(), dets.end(), [](const dlib::mmod_rect & a, const dlib::mmod_rect & b) -> bool
        {
            return a.detection_confidence > b.detection_confidence;
        });
        std::vector<dlib::mmod_rect> dets2;
        dlib::test_box_overlap tester(0.5, 1);
        for(unsigned i=0; i<dets.size(); i++){
            bool overlap=false;
            for(unsigned j=0; j<dets2.size(); j++){
                if(tester(dets2[j], dets[i])){
                    overlap=true;
                    break;
                }
            }
            if(!overlap){
                dets2.push_back(dets[i]);
            }
        }
        return dets2;
    }
    return dets;
}

void CompDef::prepareToRun(const QSqlQuery &query)
{
    if(!fhogLoaded){
        qDebug()<<"loading detector";
        //loadFHOGDetector();
        setLoadedFHOGDetector(query.value("fhogclassifier").toByteArray());
    }
    if(compSpParams.enabled){
        if(!spLoaded){
            qDebug()<<"loading shape predictor";
            //loadShapePredictor();
            setLoadedShapePredictor(query.value("shapepredictor").toByteArray());
        }
    }
    if(compDNNParams.enabled){
        if(trainedDNN && (!dnnLoaded)){
            qDebug()<<"loading dnn";
            //loadDNN();
            setLoadedDNN(query.value("dnnclassifier").toByteArray());
        }
    }
}

void CompDef::prepareToRun()
{
    if(fhogLoaded && (!compSpParams.enabled) && (!compDNNParams.enabled))
        return;

    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen()){
        defaultdb.open();
    }
    if(id<=0){
        qDebug()<<"save component definition first";
        return;
    }
    QSqlQuery query;
    QString sql="SELECT fhogclassifier, shapepredictor, dnnclassifier FROM compdefs WHERE id="+QString::number(id);
    query.exec(sql);
    if(query.next()){

        prepareToRun(query);
    }
}

long long  CompDef::saveToDBAs(QString newName)
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    oldDNN=true;
    oldFHOG=true;
    name=newName;
    unsigned savetime=QDateTime::currentSecsSinceEpoch();

    ///@todo - save dnn
    //save new component
    sql="INSERT INTO compdefs (name, minarea, aspectratio, orientationmode, "
        "upsample, horizontalflip, verticalflip, c, eps, numthreads, "
        "trainedfhog, oldfhog, traineddnn, olddnn, fhogthreshold, fhognorient, trainingiouthresh, includedataset, "
        "fhogclassifier, colorvalidator, shapepredictor, spparams, dnnparams, dnnclassifier, savetime)"
        "VALUES (:name ,"+QString::number(minArea)+", "+
            QString::number(aspectRatio)+", "+
            QString::number(orientationMode)+", "+
            QString::number(upsample)+", "+
            QString::number(horizontalFlip)+", "+
            QString::number(verticalFlip)+", "+
            QString::number(C)+", "+
            QString::number(eps)+", "+
            QString::number(numThreads)+", "+
            QString::number(trainedFHOG)+", "+
            QString::number(oldFHOG)+", "+
            QString::number(trainedDNN)+", "+
            QString::number(oldDNN)+", "+
            QString::number(fhogThreshold)+", "+
            QString::number(fhogNOrient)+", "+
            QString::number(trainingIOUThresh)+", "+
            QString::number(includeDataset)+", "+
            "(SELECT fhogclassifier FROM compdefs WHERE id="+QString::number(id) +")"+", "+
            "(SELECT colorvalidator FROM compdefs WHERE id="+QString::number(id) +")"+", "+
            "(SELECT shapepredictor FROM compdefs WHERE id="+QString::number(id) +")"+", "+
            "(SELECT spparams FROM compdefs WHERE id="+QString::number(id) +")"+", "+
            "(SELECT dnnparams FROM compdefs WHERE id="+QString::number(id) +"), "+
            "(SELECT dnnclassifier FROM compdefs WHERE id="+QString::number(id) +"), "+
            QString::number(savetime)+
            ")";
    query.prepare(sql);
    query.bindValue(":name", name);
    if(!query.exec()){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        defaultdb.close();
        defaultdb.open();
        return -1;
    }
    lastsave=savetime;
    id=query.lastInsertId().toLongLong();
    for(int i=0; i<images.count(); i++) {
        images[i].id=-1;//force creation of new image
        images[i].saveToDB(id);
    }
    return id;
}
