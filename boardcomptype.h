/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARDCOMPTYPE_H
#define BOARDCOMPTYPE_H
#include <QList>
#include <QMap>
#include <QSqlRecord>
#include <QSharedPointer>

#include "boardcomp.h"

class BoardCompTypeClassifier;

class BoardCompType
{
public:
    enum ComponentType {COMPONENT=0, FIDUCIAL=1, BARCODE=2, SUBCOMPONENT=3, GROUP=4, MAXCOMPONENTTYPE} ;
    BoardCompType();
    ~BoardCompType();
    BoardCompType(QSqlRecord record, bool loadForTest=false);
    QString name;
    ComponentType componentType;
    long long id;
    bool testAbsence=false;
    /**
     * @brief method used to save the component type to the database, including all items belonging to it, used both for regular saves and for making a copy of the entire board definition
     * @param boardDef id of the board definition to which this component type belongs
     * @param compIdMap QMap containing the correspondence of the component ID's when making a copy of the board
     */
    void saveToDB(long long boardDef, QMap<long long, long long> &compIdMap);
    /**
     * @brief classifiers stores the component type database ID as key, and a BoardCompTypeClassifier as value
     * This ensures that the order of the classifiers applied when testing a component is oldest to newest
     * This creates an issue when more than one classifier is added before saving, as before saving all the added classifiers will have the same key
     */
    QMultiMap<long long, BoardCompTypeClassifier > classifiers;
    QList<BoardComp> components;
    BoardComp * addNewComp(const double x, const double y, const BoardComp &recentComp);
    double aspectRatio(void);
    //returns the minimum area of a sample, considering that it will be upsampled as specified
    double minArea(void) const;
    unsigned upsample(void) const;
    static QStringList componentTypeStrings();
    bool isValid(){return valid;};
private:
    bool valid=true;
};

#include "compdef.h"
class BoardCompTypeClassifier{
public:
    BoardCompTypeClassifier(QSharedPointer<CompDef> ptr){
        compDefPtr=ptr;
    }
    QString name;
    QSharedPointer<CompDef> compDefPtr;
};


#endif // BOARDCOMPTYPE_H
