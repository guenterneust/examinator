/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef OPENCVCAM_H
#define OPENCVCAM_H

#include <QObject>
#include <QImage>
#include <QSettings>
#include <opencv2/videoio.hpp>
#include <opencv2/videoio/registry.hpp>

class OpenCVCam : public QObject
{
    Q_OBJECT
public:
    explicit OpenCVCam(QSettings * settings, QObject *parent = nullptr);
    QList<int> getAvailableCameras(int cameraBackend=0);
    QList<QSize> getResolutions();

    QSize getResolution() const;
    void setResolution(const QSize &value);

    int getNImages() const;
    void setNImages(int value);

    int getNIgnore() const;
    void setNIgnore(int value);

    int getDeviceId() const;

    const cv::VideoCaptureAPIs& getUsedAPI();
    void setCameraAPI(cv::VideoCaptureAPIs cvAPI);
    bool detectResolutions=true;

signals:
    void imageCaptured(QImage image);
    void captureError(QString errorMessage);

public slots:
    void capture(void);
    void setCamera(QString deviceName, bool enabled=true);
    void setCamera(int deviceId, bool enabled=true);
private:
    cv::VideoCapture cap;
    cv::VideoCaptureAPIs cvAPI;
    int deviceId=0;
    QString deviceName;
    QSize resolution=QSize(10000,10000);
    ///number of images to average in order to improve the image quality
    int nImages=2;
    ///number of images to ignore in order tu flush the buffers
    int nIgnore=1;
    bool reopen();
    QByteArray myfourcc="YUY2";
    bool imageIsReasonable(cv::Mat m);
};

#endif // OPENCVCAM_H
