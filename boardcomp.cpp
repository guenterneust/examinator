/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "boardcomp.h"
#include <QVariant>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

BoardComp::BoardComp()
{
    id=-1;
    xTol=10;
    yTol=10;
    sizeTol=10;
    rectangleType=CompDefRect::BOARDCOMP;
    symbology=0;
}

BoardComp::BoardComp(QSqlRecord record) : CompDefRect(record)
{
    //id=record.value("id").toLongLong();
    name=record.value("name").toString();
    xTol=record.value("xtol").toDouble();
    yTol=record.value("ytol").toDouble();
    sizeTol=record.value("sizetol").toDouble();
    symbology=record.value("symbology").toInt();
    parentComponent=record.value("parentcomponent").toLongLong();
    images=record.value("images").toUInt();
    rectangleType=CompDefRect::BOARDCOMP;
}

void BoardComp::saveToDB(long long compType)
{
    QSqlQuery query;
    QString sql;
    //save component type
    if(id>=0){
        //update already existing board component
        sql="UPDATE boardcomponents SET name='"+name+
                "', x="+QString::number(x)+
                ", y="+QString::number(y)+
                ", w="+QString::number(w)+
                ", h="+QString::number(h)+
                ", orientation="+QString::number(orientation)+
                ", xtol="+QString::number(xTol)+
                ", ytol="+QString::number(yTol)+
                ", sizetol="+QString::number(sizeTol)+
                ", rectangletype="+QString::number(rectangleType)+
                ", comptype="+QString::number(compType)+
                ", symbology="+QString::number(symbology)+
                ", parentcomponent="+QString::number(parentComponent)+
                ", images="+QString::number(images.to_ulong())+
                " WHERE id="+ QString::number(id);
    } else {
        sql="INSERT INTO boardcomponents (name, x, y, w, h, orientation, xtol, ytol, sizetol, rectangletype, comptype, symbology, parentcomponent, images)"
                "VALUES ('"+name+
                "', "+QString::number(x)+
                ", "+QString::number(y)+
                ", "+QString::number(w)+
                ", "+QString::number(h)+
                ", "+QString::number(orientation)+
                ", "+QString::number(xTol)+
                ", "+QString::number(yTol)+
                ", "+QString::number(sizeTol)+
                ", "+QString::number(rectangleType)+
                ", "+QString::number(compType)+
                ", "+QString::number(symbology)+
                ", "+QString::number(parentComponent)+
                ", "+QString::number(images.to_ulong())+")";
    }
    qDebug()<<"saving boardcomponent"<<sql;
    if(!query.exec(sql)){
        qDebug()<<query.lastError();
        return;
    }
    if(id<0){
        id=query.lastInsertId().toLongLong();
    }

}

bool BoardComp::isLastImage(int currentImage) const
{
    unsigned long imageSet=images.to_ulong();
    unsigned long mask=0xFFFFFFFF;
    mask <<= currentImage+1;
    return (imageSet & mask)==0;

}
