/*
Copyright 2015, 2016, 2017, 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARDCOMP_H
#define BOARDCOMP_H
#include "compdefrect.h"
#include <QSqlRecord>
#include <bitset>


class BoardComp : public CompDefRect
{
public:
    //long long id;
    BoardComp();
    BoardComp(QSqlRecord record);
    void saveToDB(long long compType);
    QString name;
    //double x, y, w, h;
    //MemCompDefRect::Orientation orientation;
    double xTol, yTol;
    double sizeTol;
    int symbology;
    long long parentComponent=-1;
    std::bitset<32> images;///< images in which to search for this component. The search stops when the component is found, or when there are no more images left.
    bool isLastImage(int currentImage) const;
    inline bool isActiveImage(int imageIndex) const{
        return images[imageIndex];
    }
    friend void serialize (const BoardComp& item, std::ostream& out);
    friend void deserialize (BoardComp& item, std::istream& in);
};

inline void serialize(const BoardComp &item, std::ostream &out)
{
    int version = 5;
    dlib::serialize(version, out);
    const CompDefRect &itemBase=static_cast<const CompDefRect&>(item);
    serialize(itemBase, out);
    dlib::serialize(item.name.toStdString(), out);
    dlib::serialize(item.xTol, out);
    dlib::serialize(item.yTol, out);
    dlib::serialize(item.sizeTol, out);
    dlib::serialize(item.symbology, out);
    dlib::serialize(item.parentComponent, out);
    dlib::serialize(item.images.to_ulong(), out);
}

inline void deserialize (BoardComp & item, std::istream& in)
{
    //qDebug()<<"deserialize DetectedComponent";
    int version = 0;
    dlib::deserialize(version, in);
    if ((version != 3)&&(version != 4)&&(version != 5))
        throw dlib::serialization_error("Unexpected version found while deserializing DetectedComponent.");
    CompDefRect &itemBase=static_cast<CompDefRect&>(item);
    deserialize(itemBase, in);
    std::string tmp;
    dlib::deserialize(tmp, in);
    item.name=QString::fromStdString(tmp);
    dlib::deserialize(item.xTol, in);
    dlib::deserialize(item.yTol, in);
    dlib::deserialize(item.sizeTol, in);
    dlib::deserialize(item.symbology, in);
    if(version >= 4){
        dlib::deserialize(item.parentComponent, in);
        if(version >= 5){
            unsigned long tmp;
            dlib::deserialize(tmp, in);
            item.images=tmp;
        }
    }
}

#endif // BOARDCOMP_H
