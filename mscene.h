/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MSCENE_H
#define MSCENE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QPainter>
#include "cornergrabber.h"
#include "compdef.h"
#include <QDebug>

class MScene;
/**
 * @brief helper class for editing component locations in an image using a QGrapicsScene
 *
 * ComponentRectItem represents a component location, and draws a rectangle, with a polarity mark where appropiate.
 * Additionally, when selected, it displays handles at the corners, implemented using the CornerGrabber class, which can be used to resize the component.
 */
class ComponentRectItem : public QGraphicsRectItem {
    /**
      * @brief reimplementation of paint
      *
      * Draw the base rectangle and, if appropiate, the polarity mark. All parameters are from the base implementation.
      * @param p
      * @param opt
      * @param wdg
      */
    void paint(QPainter *p, const QStyleOptionGraphicsItem *opt, QWidget *wdg = nullptr);
public:
    /**
    * @brief Constructor for ComponentRectItem using values directly
    * @param x
    * @param y
    * @param w
    * @param h
    * @param parent
    */
    ComponentRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem * parent = nullptr) :
        QGraphicsRectItem(x, y, w, h, parent) {
        //this->setPen();
        this->setPen(QPen(Qt::yellow));
        aspectRatio=w/h;
        compDefRect=nullptr;
    }
    /**
     * @brief store the changed ComponentRectItem to the corresponding compDefRect
     */
    void updateCompDefRect(void);
    /**
    * @brief constructor for ComponentRectItem based on a MemCompDefRect. This is the constructor normally used
    * @param comp
    */
    ComponentRectItem(CompDefRect & comp) :
        QGraphicsRectItem(comp.x,comp.y, comp.w, comp.h, nullptr) {
        /// @todo - combine with updateFromCompDef
        if((comp.orientation==CompDefRect::E)||(comp.orientation==CompDefRect::W)||
                (comp.orientation==CompDefRect::EW)){
            this->setRect(comp.x,comp.y,comp.h,comp.w);
        }
        if((comp.rectangleType==CompDefRect::POSITIVE)||
                (comp.rectangleType==CompDefRect::BOARDRECT)||
                (comp.rectangleType==CompDefRect::BOARDCOMP)||
                (comp.rectangleType==CompDefRect::FOUNDCOMP)){
            if(comp.rectangleType==CompDefRect::BOARDRECT){
                QPen pen(Qt::cyan);
                pen.setStyle(Qt::DashDotLine);
                pen.setWidth(4);
                this->setPen(pen);
            } else if(comp.rectangleType==CompDefRect::BOARDCOMP){
                this->setPen(QPen(Qt::blue));
            } else if(comp.rectangleType==CompDefRect::FOUNDCOMP){
                DetectedComponent *dc=static_cast<DetectedComponent*>(&comp);
                if((dc->status==DetectedComponent::Detected)||(dc->status==DetectedComponent::DetectedFiducial)||(dc->status==DetectedComponent::DetectedBarcode))
                    this->setPen(QPen(Qt::blue));
                else
                    this->setPen(QPen(Qt::red));
            } else
                this->setPen(QPen(Qt::yellow));
            if((comp.orientation==CompDefRect::E)||(comp.orientation==CompDefRect::W)||
                    (comp.orientation==CompDefRect::EW)){
                aspectRatio=comp.h/comp.w;
            } else {
                aspectRatio=comp.w/comp.h;
            }
        } else if(comp.rectangleType==CompDefRect::IGNORERECT){
            this->setPen(QPen(Qt::magenta));
            aspectRatio=-1;
        } else {
            this->setPen(QPen(Qt::cyan));
            aspectRatio=-1;
        }
        QPen pen=this->pen();
        pen.setWidth(2);
        if(comp.rectangleType==CompDefRect::FOUNDCOMP){
            DetectedComponent *dc=static_cast<DetectedComponent*>(&comp);
            if((dc->status!=DetectedComponent::Detected)&&
                    (dc->status!=DetectedComponent::DetectedFiducial))
                pen.setWidth(4);
        } else if(comp.rectangleType==CompDefRect::BOARDRECT){
            pen.setWidth(4);
        }
        this->setPen(pen);
        compDefRect=&comp;
    }
    //QRectF boundingRect() const;
    /**
    * @brief Pointer to the component definition for the current component. This contains all the data necessary for this component instance, and is kept up-to-date at changes of the component position.
    */
    CompDefRect * compDefRect=nullptr;
    /**
    * @brief update the GraphicsItem based on the companion MemCompDefRect
    */
    void updateFromCompDef();
    qreal setScale(qreal scale);
private:
    /**
    * @brief reimplementation of itemChange provided by QT
    *
    * This function creates and deletes the CornerGrabber handles upon selection/deselection, doing the necessary housekeepeing
    * @param change
    * @param value
    * @return
    */
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    CornerGrabber*  _corners[4];// 0,1,2,3  - starting at x=0,y=0 and moving clockwise around the box
    /**
    * @brief helper function setCornerPositions used by itemChange
    This function updates the positions of the 4 corner grabbers, and should be used every time the rectangle has been resized, moved or rotated.
    */
    void setCornerPositions();
    /**
    * @brief reimplementation of sceneEventFilter provided by QT
    *
    * Here, the actual resizing of the component is done.
    * @param watched - the QGrapicsItem which generated the event
    * @param event
    * @return returns false if the event was not handled by this function
    */
    bool sceneEventFilter(QGraphicsItem *watched, QEvent *event);
    static const int HANDLESIZE=10;
    static const int polarityMarkSize=11;
    qreal aspectRatio;///< height/width, or a negative value if the aspect ratio is not to be enforced;
};

/**
 *
*/
class MScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit MScene(QObject *parent = 0);
    ~MScene(){
        qDebug()<<"~mscene";
        delete(compDef);
        compDef=nullptr;
    }
    /** function used to update the data in the user interface when the object in the graphics scene gets modified
      As the QGraphicsItems are not QOBJECTS, they can not emit signals, so this function is used to emit the needed signal
    */
    void emitRectChanged(ComponentRectItem * item, bool selected);
    CompDef * compDef; ///< current component definition. Only one component definition should be loaded
    CompDefImage * compDefImage;///< current image
    ComponentRectItem * compDefRect;///< current rectangle
    QPixmap bg; ///< background image - this is the actual image from which the positive and negative samples are extracted
    //QPixmap getCompChip(CompDefRect * rect);
    qreal scale=1;
private:
    //QGraphicsPixmapItem bg;
    /** @brief event handler for mouse click events, used to add new rectangles
    */
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void keyReleaseEvent(QKeyEvent *event);
public slots:
    bool setBackgroundImage(QString imgPath);
    bool setBackgroundImage(const QImage & img);
    bool setBackgroundImage(const QPixmap & pixmap);
    /**
     * @brief update current item (rectangle) for the QGraphicscene
     * @param item updated Graphicsitem
     * @param selected boolean indicating if the current item is selected or not (so there is no item selected in the Graphicscene)
     */
    void compRectChanged(ComponentRectItem * item, bool selected);
    /**
     * @brief clear any old detections from the scene, and add the detections received as argument which have the score bigger than threshold to the scene
     * @param detections
     * @param threshold
     */
    void showDetections(QList<DetectedComponent> &detections, double threshold=0);
    /**
     * @brief change Image
     * @param idx index of the new image in the image list of the component, including the included dataset
     */
    void changeImage(long long idx);
    void changeCompDef(long long id);
    /**
     * @brief delete all rectangles from the scene
     */
    void clearRects(void);
    /**
     * @brief delete only detected rectangles from the scene
     */
    void clearDetectedRects(void);
signals:
    void rectChanged(ComponentRectItem *item, bool selected);
    void middleMousePressed(double x, double y);
    void sceneKeyEvent(QKeyEvent * event);
};

#endif // MSCENE_H
