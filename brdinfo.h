#ifndef BRDINFO_H
#define BRDINFO_H

#include <QDialog>
#include "boarddef.h"
#include "mscene.h"
#include <QAbstractTableModel>
#include <QTextDocument>
#include <QPrintPreviewDialog>
#include <QPrinterInfo>
#include <QPrinter>
#include <QPainter>


namespace Ui {
    class BrdInfo;
}

class CompInfo {
public:
    QString comp;
    QString compType;
    QString classifier;
    QString className;
    float threshold;
    int area = 0;
    bool dnn = false;
    bool sp = false;
    bool colorValidator = false;
    QPixmap sample;
};

class CompInfoModel : public QAbstractTableModel {
public:
    void setContent(BoardDef* boardDef);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override {
        Q_UNUSED(parent)
            return compInfo.count();
    };
    int columnCount(const QModelIndex& parent = QModelIndex()) const override {
        Q_UNUSED(parent)
            return 10;
    };
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override {
        if (!index.isValid()) {
            return QVariant();
        }
        if (Qt::DecorationRole == role)
            if (index.column() == 9)
                return compInfo[index.row()].sample;
        /*
        if(Qt::SizeHintRole==role)
            if(index.column()==9)
                return compInfo[index.row()].sample.size()+QSize(4,4);
        */
        if (Qt::DisplayRole == role) {
            switch (index.column()) {
            case 0:
                return compInfo[index.row()].comp;
            case 1:
                return compInfo[index.row()].compType;
            case 2:
                return compInfo[index.row()].classifier;
            case 3:
                return compInfo[index.row()].className;
            case 4:
                return compInfo[index.row()].threshold;
            case 5:
                return compInfo[index.row()].area;
            case 6:
                return compInfo[index.row()].dnn;
            case 7:
                return compInfo[index.row()].sp;
            case 8:
                return compInfo[index.row()].colorValidator;
            case 9:
                return QVariant();
                //return compInfo[index.row()].sample;
            default:
                return QVariant();
            }

        }
        return QVariant();
    }
    QVariant headerData(int section, Qt::Orientation orientation,
        int role = Qt::DisplayRole) const override {
        if (Qt::DisplayRole != role) {
            return QVariant();
        }
        if (Qt::Horizontal != orientation)
            return QVariant();
        switch (section) {
        case 0:
            return tr("componenta");
        case 1:
            return tr("tip componenta");
        case 2:
            return tr("clasificator");
        case 3:
            return tr("clasă");
        case 4:
            return tr("prag");
        case 5:
            return tr("suprafața");
        case 6:
            return tr("dnn");
        case 7:
            return tr("poziționare");
        case 8:
            return tr("validator culoare");
        case 9:
            return tr("mostră");
        default:
            return QVariant();
        }
    }

public:
    MScene mainScene;
private:
    QVector<CompInfo> compInfo;
};

class BrdInfo : public QDialog
{
    Q_OBJECT

public:
    explicit BrdInfo(BoardDef* brdDef, QWidget* parent = nullptr);
    ~BrdInfo();
private slots:
    void printPreview();
    void writeDocument(QPrinter* printer);
    void on_horizontalSlider_valueChanged(int value);

private:
    Ui::BrdInfo* ui;
    CompInfoModel compInfoModel;

    QPrinter* printer = nullptr;
    QPrintPreviewDialog* printPreviewDialog = nullptr;
};

#endif // BRDINFO_H
