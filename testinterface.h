/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TESTINTERFACE_H
#define TESTINTERFACE_H

#include "widget.h"
#include "mscene.h"
#include "myftp.h"
#include "checksn.h"
#include "ipcamera.h"
#include "mycamera.h"
#ifdef OCVCAM
#include "opencvcam.h"
#endif
#ifdef RASPICAM
#include "raspicvcam.h"
#endif

#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include <QWidget>
#include <QCommandLineParser>
#include <QTimer>
#include "logger.h"
#include "iocontrol.h"

extern QCommandLineParser parser;

namespace Ui {
class TestInterface;
}
enum SaveImages {SAVENEVER,SAVEFAIL, SAVEALL};

class TestInterface : public QWidget
{
    Q_OBJECT

public:
    explicit TestInterface(QWidget *parent = 0);
    ~TestInterface();

private slots:
    void on_editorBtn_clicked();

    void on_loadBtn_clicked();

    void on_horizontalSlider_valueChanged(int position);

    void on_saveBtn_clicked();
    /// slot used to display the board select dialog and load the selected board if appropiate
    void on_boardBtn_clicked();
    /// camera trigger slot
    void on_cameraBtn_clicked();
    /* not used any more
    void getPixmap(QPixmap pixmap);
    */
    void getImage(const QImage & img);
    /*void cameraIsReady(bool ready);*/
    void on_playBtn_clicked();
    void on_runButton_clicked();
    void runTestSlot();
    void on_cameraBtn_triggered(QAction *arg1);

    void on_snEdit_returnPressed();
    void addToClassifier(long long classifierId, const QString &imageName);
    void showBoard(long long boardId);
    void ioControlRun(void);
    void startCapture(void);
    void checkForUpdates(void);
    void selectBoard(QString boardName="", bool reload=false);
public slots:
    void showTestResult(QList<DetectedComponent> &boardRects, const bool testOk);
    void showImage(QString fileName);
    void showImageResult(QString fileName,QList<DetectedComponent> &boardRects, const bool testOk);
    void centerAt(double x, double y);
    void captureError(QString msg);

private:
    Ui::TestInterface *ui;
    Widget * editor=nullptr;
    MScene testScene;
    BoardDef * boardDef=nullptr;
    void createDatabase();
    MyCamera * myCamera;
    IpCamera ipCamera;
#ifdef OCVCAM
    OpenCVCam * ocvCamera;
#endif
#ifdef RASPICAM
    RaspiCvCam raspiCvCamera;
#endif
    /*
    void configureCamera(const QByteArray &deviceName);
    void configureCamera();
    bool pictureRequested=false;
    */
    QString parseArguments(void);
    MyFtp * myFtp=nullptr;
    QdmSNCheck * snCheck=nullptr;
    QString serialNumber;
    QString ioControlPort;
    bool makeProtocol(QString serialNumber, bool boardOk,
                      const QList<DetectedComponent> & foundComponents);
    /**
     * @brief check if the specified sqlite table has the specified field
     * @param tableName
     * @param fieldName
     * @return true if the table has the field
     */
    bool sqliteTableHasField(QString tableName, QString fieldName);
    QSettings * writableSettings=nullptr;
    //settings
    bool displaySearchFrames=false;
    int autoSaveOption=1;
    QString autoSaveDir;
    bool showFiducials=false;
    //storage for inspection results
    QList<DetectedComponent> boardRects;
    enum RunTask {IDLE, CAPTURE, ANALYZE, PROCESSING};
    RunTask runTask=IDLE;
    Logger logger;
    /// flag indicating that serial numbers are supposed to be read from the processed images, instead of being provided by the keyboard
    bool barCodeMode=false;
    bool clearOldSN=true;
    IOControl ioControl;
    bool isProductionSystem=true;
    QList<QImage> imageList; ///< list of images acquired by camera or loaded from file, used to run the board test on. The processing starts when the first image is acquired.
    bool imageAquisitionDone=true;
    /// save image to autosave location. This is usually done in a background thread, to save running time
    void saveImage(const QList<QImage> imageList, const QList<DetectedComponent> boardRects, const QString realSN);
    /// when this is true, the testinterface will continuously acquire and display images from the camera for debug purpose (like adjusting the camera focus). No procesing is done.
    bool continuousAcquire = false;
    void clearForNewImage(void);
    ///return the build number of the application (the unix timestamp of the build time of the application in not a good option, as not all systems are using the same binary program)
    ///This is set as last token of the applicationVersion property
    long long appBuildTime(void){
        QString buildTime=qApp->applicationVersion().section('.',-1);
        return buildTime.toLongLong();
    }
    /**
     * @brief checks if the app build number stored in the database is the same as the build number saved in the application.
     * If it's equal, just return it.
     * If it's greater/newer, warn the user that he's using an old application.
     * If it's smaller/older, update tha value stored in the database.
     * @param force - if true, the database is updated irrespective of the old value
     * @param warn - if true, the user is warned with a message box that he's using an old application when appropiate. Irrespective of this parameter, the warning goes to the log file when appropiate.
     * @return the value stored in the database after running this method
     */
    long long dbCheckAndSetAppBuildTime(bool warn=true, bool force=false);
    //bool validateSn(QString sn, QString validator, QString &extractedSN);
    int lastBoardsFound=0;
    QDateTime lastTrigger=QDateTime::currentDateTime();
    QTimer checkForUpdatesTimer;
    bool outdated=false;
    void updateIfOutdated(void);
    bool rotateComponents=false;
signals:
    void logMsg(QString message, LOGVERBOSITY verbosity);
    /**
     * @brief signal used when manually loading an image from disk
     * @param img
     */
    void imageLoaded(QImage img);
    /**
     * @brief signal emmited by getImage after inserting the new image into the imageList
     */
    void gotNewImage();
    /**
     * @brief statusChanged
     * @param status -1 undefined, 0 PASS, 1 FAIL
     */
    void statusChanged(int status);
};

#endif // TESTINTERFACE_H
