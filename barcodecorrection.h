/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BARCODECORRECTION_H
#define BARCODECORRECTION_H

#include <QDialog>

namespace Ui {
class BarcodeCorrection;
}

class BarcodeCorrection : public QDialog
{
    Q_OBJECT

public:
    explicit BarcodeCorrection(QWidget *parent = 0);
    ~BarcodeCorrection();
    QString getSerialNumber(void);
    void serErrorText(QString errorText);

private slots:
    void on_acceptBtn_clicked();

private:
    Ui::BarcodeCorrection *ui;
    QString serialNumber;
};

#endif // BARCODECORRECTION_H
