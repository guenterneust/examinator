/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "boardcomptype.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

BoardCompType::BoardCompType()
{
    id=-1;
    componentType=COMPONENT;

}

BoardCompType::~BoardCompType()
{
    /* not needed for shared pointer
    foreach(QSharedPointer<MemCompDef>c, classifiers){
        delete c;
    }
    */
}

BoardCompType::BoardCompType(QSqlRecord record, bool loadForTest)
{
    id=record.value("id").toLongLong();
    name=record.value("name").toString();
    testAbsence=record.value("testAbsence").toBool();
    componentType=static_cast<BoardCompType::ComponentType>(record.value("comptype").toInt());
    if((componentType>=MAXCOMPONENTTYPE)||(componentType<0))
        componentType=COMPONENT;
    QSqlQuery query;
    QString sql;
    sql="SELECT * FROM boardcomponents WHERE comptype="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        valid=false;
        return;
    }
    while(query.next()){
        components.append(BoardComp(query.record()));
    }
    sql="SELECT * FROM boardcompclassifiers WHERE comptype="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        valid=false;
        return;
    }
    double classifierAspectRatio=-1;
    while(query.next()){
        QSharedPointer<CompDef> compDefPtr(new CompDef);
        BoardCompTypeClassifier c(compDefPtr);
        long long cid=query.value("id").toLongLong();
        c.name=query.value("name").toString();
        long long classifier=query.value("classifier").toLongLong();
        c.compDefPtr->loadFromDB(classifier,false, false, loadForTest);
        //if(loadForTest)
        //    c.compDefPtr->prepareToRun();
        if(classifierAspectRatio<0){
            classifierAspectRatio=c.compDefPtr->aspectRatio;
        } else {
            if(classifierAspectRatio!=c.compDefPtr->aspectRatio){
                //just don't keep the classifier. It will be deleted when the board definition is saved.
                qDebug()<<"ignoring classifier"<<c.compDefPtr->name<<"having aspect ratio"<<c.compDefPtr->aspectRatio<<"instead of"<<classifierAspectRatio;
                continue;
            }
        }
        //c.compDefPtr->loadFHOGDetector();
        classifiers.insert(cid,c);
    }
    if(classifierAspectRatio>0){
        for(int i=0; i<components.count(); i++){
            components[i].applySettings(components[i].orientation, components[i].N_E_S_V, classifierAspectRatio, components[i].rectangleType);
        }
    }
}

void BoardCompType::saveToDB(long long boardDef, QMap<long long, long long> &compIdMap)
{
    QSqlQuery query;
    QString sql;
    bool newId=false;
    //save component type
    query.exec("BEGIN TRANSACTION");
    if(id>=0){
        //update already existing board component type
        sql="UPDATE boardcomptypes SET name='"+name+
                "', comptype="+QString::number(componentType)+
                ", testAbsence="+QString::number(testAbsence)+
                " WHERE id="+ QString::number(id);
    } else {
        sql="INSERT INTO boardcomptypes (name, comptype, board, testAbsence)"
                "VALUES ('"+name+"', "+QString::number(componentType)+
                ", "+QString::number(boardDef)+
                ", "+QString::number(testAbsence)+")";
    }
    qDebug()<<"saving boardcomptype"<<sql;
    if(!query.exec(sql)){
        qDebug()<<query.lastError();
        return;
    }
    //keep id if it's new
    if(id<0){
        newId=true;
        id=query.lastInsertId().toLongLong();
    } else {
        //delete deleted rects and alternative classifiers from database
        QString idList;
        for(int i=0; i<components.count(); i++) {
            if(components[i].id<0)
                continue;
            if(!idList.isEmpty())
                idList.append(",");
            idList.append(QString::number(components[i].id));
        }
        sql="DELETE FROM boardcomponents WHERE comptype="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
        idList.clear();
        foreach(long long cid, classifiers.keys()) {
            if(cid<0)
                continue;
            if(!idList.isEmpty())
                idList.append(",");
            idList.append(QString::number(cid));
        }
        sql="DELETE FROM boardcompclassifiers WHERE comptype="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
    }
    // only new classifiers have to be saved, as there is no way to modify existing classifier alternatives
    qDebug()<<"saving classifiers";
    if(newId){
        for(;;){
            auto it=classifiers.begin();
            long long k;
            while((k=it.key())<=-1){
                it++;
                if(it==classifiers.end())
                    break;
            }
            if(it==classifiers.end())
                break;
            BoardCompTypeClassifier comp(it.value());
            classifiers.insert(-k,comp);
            classifiers.erase(it);
        }
    }
    for(auto it=classifiers.begin(); it!=classifiers.end(); ++it){
        //update the names of the classifiers
        if(it.key()<0)
            continue;
        sql="UPDATE boardcompclassifiers SET name=:name WHERE id="+QString::number(it.key());
        query.prepare(sql);
        qDebug()<<sql;
        query.bindValue(":name", it.value().name);
        if(!query.exec()){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
    }
    for(;;){
        //save the classifiers which are new
        //find a classifier to save starting from the end
        auto it=classifiers.end();
        bool found=false;
        while(1){
            if(it==classifiers.end()){
                if(it!=classifiers.begin()){
                    it--;
                    continue;
                }
                break;
            }
            if(it.key()<0){
                found=true;
                break;
            }
            if(it==classifiers.begin())
                break;
            it--;
        }
        //exit loop if no classifier to be saved has been found
        if(!found)
            break;
        //save component classifier, remove from map and reinsert with new id; search again
        BoardCompTypeClassifier comp=it.value();
        long long classifier=comp.compDefPtr->id;
        sql="INSERT INTO boardcompclassifiers (classifier, comptype, name)"
                "VALUES("+QString::number(classifier)+","+
                QString::number(id)+", :name )";
        query.prepare(sql);
        qDebug()<<sql;
        query.bindValue(":name", comp.name);
        if(!query.exec()){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
        long long altid=query.lastInsertId().toLongLong();
        classifiers.insert(altid,comp);
        classifiers.erase(it);
    }
    for(int i=0; i<components.count(); i++){
        long long oldId=-1;
        if(newId){
            oldId=components[i].id;
            components[i].id=-1;
            //if the component will get a new ID, and the parent component got a new ID, change the parentComponent to the new ID of the parent component
            if(compIdMap.contains(components[i].parentComponent)){
                components[i].parentComponent=compIdMap[components[i].parentComponent];
            }
        }
        components[i].saveToDB(id);
        if(newId && (oldId>0))
            compIdMap.insert(oldId, components[i].id);
    }
    query.exec("END TRANSACTION");

}

BoardComp *BoardCompType::addNewComp(const double x, const double y, const BoardComp &recentComp)
{
    components.append(recentComp);
    BoardComp * comp = &(components.last());
    comp->x=x;
    comp->y=y;
    comp->id=-1;
    return comp;
}

double BoardCompType::aspectRatio()
{
    if(classifiers.count()>0)
        return classifiers.first().compDefPtr.data()->aspectRatio;
    return -1;
}

double BoardCompType::minArea() const
{

    if(classifiers.count()>0){
        auto classifierslist=classifiers.values();
        double minArea=0;
        for(int i=0; i<classifierslist.count(); i++){
            auto compDefPtr=classifierslist[i].compDefPtr;
            double minArea1=compDefPtr.data()->minArea;
            for(int upsample=compDefPtr.data()->upsample; upsample>0; upsample--){
                minArea1 /= 4;
            }
            minArea=std::max(minArea, minArea1);
        }
        return minArea;
    }
    return 6400;
}

unsigned BoardCompType::upsample() const
{
    if(classifiers.count()>0){
        return classifiers.first().compDefPtr.data()->upsample;
    }
    return 0;
}

QStringList BoardCompType::componentTypeStrings()
{
    QStringList componentTypeStrings;
    componentTypeStrings.append(QObject::tr("Componenta"));
    componentTypeStrings.append(QObject::tr("Fiducial"));
    componentTypeStrings.append(QObject::tr("Cod Bare"));
    componentTypeStrings.append(QObject::tr("Subcomponenta"));
    componentTypeStrings.append(QObject::tr("Grup"));
    return componentTypeStrings;
}
