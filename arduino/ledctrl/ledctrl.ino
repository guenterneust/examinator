//PinChangeInerrupt by NicoHood
//#include "PinChangeInterrupt.h"
//HCSR04 by Martin Sosic
#include "HCSR04.h"
/*
led control
D5 -> D2
D6 -> D3
D7 -> D4
*/
const uint8_t LED1CTRL = 5;
const uint8_t LED2CTRL = 6;
const uint8_t LED3CTRL = 7;
uint8_t ledctrl=0;
const uint8_t LED1 =2;
const uint8_t LED2 =3;
const uint8_t LED3 =4;
const uint8_t triggerPin=12;//pin connected to the HC-SR04 trig input
const uint8_t echoPin=11;//pin connected to the HC-SR04 Echo output
bool lastDistanceTrigger=true;
const double TRIGGERDISTANCE=10;//15cm
#define BUFFERSIZE 8
char inputBuffer[BUFFERSIZE];
uint8_t inputPos=0;
bool commOK=false;
unsigned long commOkMillis=0;
const long int baudrate1=9600;
const long int baudrate2=115200;
long int baudrate=baudrate1;
const int RXPin = 0;//D0 for nano, might need to be changed if something different is used

const long unsigned autostoptimeout=10*60*1000L;
long unsigned autostopstart=0;


UltraSonicDistanceSensor distanceSensor(triggerPin, echoPin);

void setup() {
  // put your setup code here, to run once:
  pinMode(LED1CTRL, INPUT_PULLUP);
  pinMode(LED2CTRL, INPUT_PULLUP);
  pinMode(LED3CTRL, INPUT_PULLUP);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);

  uint8_t tmp;
  tmp=digitalRead(LED1CTRL);
  ledctrl=tmp;
  digitalWrite(LED1, tmp);
  tmp=digitalRead(LED2CTRL);
  ledctrl |= tmp<<1;
  digitalWrite(LED2, tmp);
  tmp=digitalRead(LED3CTRL);
  ledctrl |= tmp<<2;
  digitalWrite(LED3, tmp);
  /* 
  led1Change();
  led2Change();
  led3Change();
  attachPCINT(digitalPinToPCINT(LED1CTRL), led1Change, CHANGE);
  attachPCINT(digitalPinToPCINT(LED2CTRL), led2Change, CHANGE);
  attachPCINT(digitalPinToPCINT(LED3CTRL), led3Change, CHANGE);
  */
  Serial.begin(baudrate);
  Serial.print("T\n");
  pinMode(13, OUTPUT);
  digitalWrite(13,LOW);
}

void loop() {
  uint8_t ctrltmp=0;
  uint8_t tmp=0;
  tmp=digitalRead(LED1CTRL);
  ctrltmp=tmp;
  tmp=digitalRead(LED2CTRL);
  ctrltmp |= tmp<<1;
  tmp=digitalRead(LED3CTRL);
  ctrltmp |= tmp<<2;
  if(ledctrl!=ctrltmp){
    ledctrl=ctrltmp;
    autostopstart=millis();
    digitalWrite(LED1, (ledctrl&1)?HIGH:LOW);
    digitalWrite(LED2, (ledctrl&2)?HIGH:LOW);
    digitalWrite(LED3, (ledctrl&4)?HIGH:LOW);
  }
  if(millis()-autostopstart>autostoptimeout){
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, LOW);
  }
  if(Serial.available()>0){
    char c=Serial.read();
    if(c=='\n'){
      commOK=true;
      processInput();
    } else {
      if(inputPos<BUFFERSIZE){
        inputBuffer[inputPos]=c;
        inputPos++;
      } else {
        //something went wrong
        digitalWrite(13, HIGH);
        if(!commOK){
          //switch baudrate
          Serial.flush();
          baudrate=(baudrate==baudrate1)?baudrate2:baudrate1;
          digitalWrite(13, (baudrate==baudrate1)?0:1);
          Serial.begin(baudrate);
        }
      }
    }
    return;
  }
  
  double distance = distanceSensor.measureDistanceCm();
  if((distance>0)&&(distance<TRIGGERDISTANCE)){
    //trigger active
    if(!lastDistanceTrigger){
      //trigger activated
      Serial.print("T\n");
      lastDistanceTrigger=true;
      delay(100);
    } else {
      //trigger was already active -> do nothing
    }
  } else {
    //trigger inactive
    lastDistanceTrigger=false;
  }
  delay(10);
}

void processInput(){
  digitalWrite(13,LOW); //turn off LED
  if(inputPos==0)//no data
    return;
  if(inputBuffer[0]=='L'){
    if(inputPos==4){
      autostopstart=millis();
      if(inputBuffer[1]=='0'){
        digitalWrite(LED3, LOW);
      } else {
        digitalWrite(LED3, HIGH);
      }
      if(inputBuffer[2]=='0'){
        digitalWrite(LED2, LOW);
      } else {
        digitalWrite(LED2, HIGH);
      }
      if(inputBuffer[3]=='0'){
        digitalWrite(LED1, LOW);
      } else {
        digitalWrite(LED1, HIGH);
      }
      Serial.print("L\n");
    } else {
      //something went wrong
      digitalWrite(13,HIGH); //turn off LED
      Serial.print("?\n");
    }
  } else {
    if(inputPos==2){
      if((inputBuffer[0]=='S')&&((inputBuffer[0]=='T')||(inputBuffer[0]=='P')||(inputBuffer[0]=='F'))){
        inputPos=0;
        return;
      }
    }
    //unknown command
    digitalWrite(13,HIGH); //turn off LED
    Serial.print("?\n");
  }
  inputPos=0;
}
