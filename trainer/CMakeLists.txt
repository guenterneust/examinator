cmake_minimum_required(VERSION 3.5)

project(trainer LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
add_subdirectory(../../dlib dlib_build)

add_executable(trainer trainer.cpp)
target_link_libraries(trainer dlib::dlib)
target_include_directories(trainer PRIVATE ${../../dlib})
