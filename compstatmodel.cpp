/*
Copyright 2018, 2019, 2020 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "compstatmodel.h"


CompStatModel::CompStatModel(QObject *parent)
    : QAbstractListModel(parent)
{
    w=dynamic_cast<Widget *>(parent);
}

QVariant CompStatModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // not needed
    Q_UNUSED(section);
    Q_UNUSED(orientation);
    Q_UNUSED(role);
    return QVariant();
}

int CompStatModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    if((w==nullptr)||(w->compDefScene.compDef==nullptr))
        return 0;
    return w->compDefScene.compDef->images.count() + w->compDefScene.compDef->includedImages.count();
}

QVariant CompStatModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    int nImages=w->compDefScene.compDef->images.count();
    if(index.row()<nImages){
        if(role == Qt::DisplayRole){
            return w->compDefScene.compDef->images[index.row()].imagePath;
        } else if(role==Qt::BackgroundRole){
            if(stats.count()<index.row()){
                qDebug()<<"invalid stats count"<<stats.count();
                return QVariant();
            }
            if(!stats[index.row()].valid)
                return QVariant();
            if(stats[index.row()].values.missing>0){
                return QColor(Qt::red);
            }
            if(stats[index.row()].values.orientation>0){
                return QColor(Qt::cyan);
            }
            if(stats[index.row()].values.extra>0){
                return QColor(Qt::magenta);
            }
            return QColor(Qt::green);
        }
    } else {
        if(index.row()<nImages+w->compDefScene.compDef->includedImages.count()){
            if(role == Qt::DisplayRole){
                return w->compDefScene.compDef->includedImages[index.row()-nImages].imagePath;
            } else if(role==Qt::BackgroundRole){
                if(stats.count()<index.row()){
                    qDebug()<<"invalid stats count"<<stats.count();
                    return QVariant();
                }
                if(!stats[index.row()].valid)
                    return QVariant();
                if(stats[index.row()].values.missing>0){
                    return QColor(Qt::red);
                }
                if(stats[index.row()].values.orientation>0){
                    return QColor(Qt::cyan);
                }
                if(stats[index.row()].values.extra>0){
                    return QColor(Qt::magenta);
                }
                return QColor(Qt::green);
            }
        } else
            qDebug()<<"invalid index row"<<index.row();
    }
    return QVariant();
}

void CompStatModel::prepareForUpdate()
{
    //beginResetModel();
}

void CompStatModel::updateFinished()
{
    stats.clear();
    stats.resize(rowCount());
#if 0
    //some tests
    if(rowCount()>2){
        stats[0].valid=true;
        stats[1].valid=true;
    }
#endif
    //endResetModel();
    emit dataChanged(this->index(0), this->index(rowCount()-1));
}

void CompStatModel::processImageDetections(const CompDef *compDef, const QModelIndex &index)
{
    //if threshold has changed, reprocess all detections for all images
    if(std::abs(oldThreshold-compDef->fhogThreshold)>0.001){
        oldThreshold=compDef->fhogThreshold;

        for(int i=0; i<this->rowCount(); i++){
            bool valid=stats[i].valid;
            //qDebug()<<"stats"<<i<<stats[i].valid;
            processImageDetections(compDef, this->index(i));
            //qDebug()<<"stats"<<i<<stats[i].valid;
            stats[i].valid=valid;

        }
        calculateGlobalStat();
        emit dataChanged(this->index(0), this->index(rowCount()-1));
        return;
    }
    auto &stat=stats[index.row()];
    stat.clearStat();
    if(compDef==nullptr){
        qDebug()<<"compdef==NULL";
        return;
    }
    auto nImages=compDef->images.count();
    auto nIncludedImages=compDef->includedImages.count();
    if(nImages+nIncludedImages<=index.row()){
        qDebug()<<"insufficient images";
        return;
    }
    ///@todo check which rectangle has to cover which in checkOverlap when not calculating iou
    auto &img=(index.row()<nImages)?compDef->images[index.row()]:compDef->includedImages[index.row()-nImages];
    //check for missing detections
    for(int i=0; i<img.rect.count(); i++){
        if(img.rect[i].rectangleType!=CompDefRect::POSITIVE)
            continue;
        auto &rect=img.rect[i];
        bool found=false;
        for(int j=0; j<stats[index.row()].detections.count(); j++){
            auto &det=stats[index.row()].detections[j];
            if(rect.orientationDifference(rect.orientation, det.orientation)!=0){
                //orientation is different, so this detection does not count
                continue;
            }
            if(det.score[0]<compDef->fhogThreshold){
                //ignore detections with bad score
                continue;
            }
            if(rect.checkOverlap(det, true)>=0.5){
                //this rectangle has a corresponding detection
                found=true;
                break;
            }
        }
        if(!found){
            //qDebug()<<"missing"<<i;
            stat.values.missing++;
        } else {
            //qDebug()<<"not missing"<<i;
        }
    }
    for(int j=0; j<stats[index.row()].detections.count(); j++){
        auto &det=stats[index.row()].detections[j];
        bool ignore=false;
        bool found=false;
        for(int i=0; i<img.rect.count(); i++){
            auto &rect=img.rect[i];
            if(rect.rectangleType==CompDefRect::IGNORERECT){
                if(det.orientationDifference(det.orientation, rect.orientation)==0){
                    if(det.checkOverlap(rect,false)>=0.5){
                        //if the detection is covered by an ignore marking, remember this and quit the loop
                        ignore=true;
                        break;
                    }
                }
            }
        }
        bool correct=false;
        for(int i=0; i<img.rect.count(); i++){
            auto &rect=img.rect[i];
            if(rect.rectangleType==CompDefRect::POSITIVE){
                if(det.checkOverlap(rect,true)>=0.5){
                    //we found a match, check if the orientation is right
                    if(rect.orientationDifference(rect.orientation, det.orientation)==0){
                        stat.addCorrect(det.score[0], compDef->fhogThreshold);
                        found=true;
                        correct=true;
                        break;
                    } else {
                        //even if on an ignore marking, if here is a positive marking, the detection has to match orientation
                        found=true;
                    }
                }
            }
        }
        if(found && (!correct))
            stat.addOrientation(det.score[0], compDef->fhogThreshold);
        if(!found){
            //count detections without positive marking as extraneous only if not on ignore marking
            if(!ignore)
                stat.addExtra(det.score[0], compDef->fhogThreshold);
        }
    }
    qDebug()<<"correct"<<stat.values.correct<<stat.values.orientation<<stat.values.extra<<stat.values.missing<<stat.detections.count();
}

void CompStatModel::calculateGlobalStat()
{
    globalStat.clearStat();
    int rowCount=stats.count();
    for(int i=0; i<rowCount; i++){
        if(stats[i].valid)
            globalStat.accumultateStat(stats[i]);
    }
}

void CompStatModel::statToChart(const CompStat &stat, QtCharts::QChart &chart, double threshold)
{
    QtCharts::QLineSeries *sc, *so, *se, *thresh; //correct, orientation, extra
    sc=new QtCharts::QLineSeries();
    so=new QtCharts::QLineSeries();
    se=new QtCharts::QLineSeries();
    thresh=new QtCharts::QLineSeries();
    thresh->append(threshold,0);
    thresh->append(threshold,1);
    double maxc=1, maxo=1, maxe=1;
    for(int i=0; i<CompStat::histSteps; i++){
        maxc=std::max<double>(maxc, stat.hist[i].correct);
        maxo=std::max<double>(maxo, stat.hist[i].orientation);
        maxe=std::max<double>(maxe, stat.hist[i].extra);
    }
    qDebug()<<"max"<<maxc<<maxo<<maxe;
    for(int i=0; i<CompStat::histSteps; i++){
        double pos=stat.histPosition(i);
        sc->append(pos,stat.hist[i].correct/maxc);
        so->append(pos,std::min(stat.hist[i].orientation/maxc,1.0));
        se->append(pos,std::min(stat.hist[i].extra/maxc,1.0));
    }
    sc->setName(tr("corect"));
    so->setName(tr("orientare"));
    se->setName(tr("suplimentar"));
    thresh->setName(tr("prag"));
    sc->setPen(QColor(Qt::green));
    so->setPen(QColor(Qt::cyan));
    se->setPen(QColor(Qt::magenta));
    thresh->setPen(QColor(Qt::red));

    chart.removeAllSeries();
    chart.addSeries(sc);
    chart.addSeries(so);
    chart.addSeries(se);
    chart.addSeries(thresh);
    chart.createDefaultAxes();
}
