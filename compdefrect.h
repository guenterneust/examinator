/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPDEFRECT_H
#define COMPDEFRECT_H
//#include "memcompdef.h"
#include <QSqlRecord>
#include <QtGlobal>
#include <dlib/image_processing.h>
#include <QDebug>

class CompDef;
/**
 * @brief The CompDefRect class contains the data defining a single rectangle
 *
 *
 */
class CompDefRect
{
private:
public:
    enum OrientationMode {ANY=0, H_V=1, N_E_S_V=2} ;
    enum Orientation {NONE=-1, N=0, E=1, S=2, W=3, NS=4, EW=5};
    enum RectangleType {NEGATIVE=0, POSITIVE=1, IGNORERECT=2, BOARDRECT=3, BOARDCOMP=4, FOUNDCOMP=5};
    /// default constructor, not used
    CompDefRect();
    /// constructor used when a new rectangle is added
    CompDefRect(double x, double y, double w, double h, Orientation orientation, RectangleType rType);
    /// constructor used when the dataset is loaded from the database
    CompDefRect(QSqlRecord record);
    Orientation orientation;
    RectangleType rectangleType;
    float angle=0;
    long long id; ///< id from the database. Default value == -1 before saving to the database
    double x,y;///< upper left corner coordinates in pixels
    double w; ///< width of the rectangle
    double h; ///< height of the rectangle
    double x2() const; ///< right edge
    double y2() const; ///< lower edge
    /**
     * @brief update the rectangle to the valuese provided as parameters
     * @param x new x
     * @param y new y
     * @param w new width
     * @param h new height
     * @return true if the rectangle was actually changed
     */
    bool update(double x, double y, double w, double h);
    /// apply setting from the GUI which might affect the rectangle, either global or local settings
    void applySettings(Orientation _orientation, OrientationMode orientationMode, qreal aspectRatio,
                       RectangleType rectangleType);
    double xCenter() const;
    double yCenter() const;
    /// get number of clockwise rotations to get from the src orientation to the dst orientation
    static int orientationDifference(Orientation src, Orientation dst, OrientationMode orientationMode=N_E_S_V);
    /// add a number of clockwise orientation to the current orientation
    void addOrientationDifference(int diff);
    //calculate overlap as intersection over union when useUnion==true, or as percent covered of the smaller rectangle otherwise.
    double checkOverlap(const CompDefRect & other, bool useUnion) const;

    friend void serialize (const CompDefRect& item, std::ostream& out);
    friend void deserialize (CompDefRect& item, std::istream& in);

};

inline void serialize(const CompDefRect &item, std::ostream &out)
{
    int version = 2;
    dlib::serialize(version, out);
    dlib::serialize(item.orientation, out);
    dlib::serialize(item.id, out);
    dlib::serialize(item.rectangleType, out);
    dlib::serialize(item.x, out);
    dlib::serialize(item.y, out);
    dlib::serialize(item.w, out);
    dlib::serialize(item.h, out);
    dlib::serialize(item.angle, out);
}

inline void deserialize (CompDefRect& item, std::istream& in)
{
    //qDebug()<<"deserialize CompDefRect";
    int version = 0;
    dlib::deserialize(version, in);
    if ((version != 1)&&(version!=2))
        throw dlib::serialization_error("Unexpected version found while deserializing CompDefRect.");
    int tmp;
    dlib::deserialize(tmp, in);
    item.orientation=static_cast<CompDefRect::Orientation>(tmp);
    dlib::deserialize(item.id, in);
    dlib::deserialize(tmp, in);
    item.rectangleType=static_cast<CompDefRect::RectangleType>(tmp);
    dlib::deserialize(item.x, in);
    dlib::deserialize(item.y, in);
    dlib::deserialize(item.w, in);
    dlib::deserialize(item.h, in);
    if(version==2)
        dlib::deserialize(item.angle, in);
}

#endif // COMPDEFRECT_H
